<?php
define('SMARTY_DIR', $_SERVER['DOCUMENT_ROOT'] . "/flexymvc_core/libsext/Smarty/libs/");  // Location of Smarty libraries
define('VAR_DIR', AFIXI_ROOT . "../var/" . SITE_USED . "/");  // Location of temmporay files created by the application 
define("REMOVE_SCRIPT_NAME", 'SCRIPT_NAME');
define("TEMPLATE_EXTENSION", '.tpl.html');
define("SITE_CONSTANTS", "configs/" . SITE_USED . ".site_constants.php");
define("MOGRIFY_CMD", 'mogrify');

if (isset($_SERVER['QUERY_STRING'])) {
    define("PARSE_FOR_COLLECT_INPUT", 'QUERY_STRING');
} elseif (isset($_SERVER['ORIG_PATH_INFO'])) {
    define("PARSE_FOR_COLLECT_INPUT", 'ORIG_PATH_INFO');
} elseif (isset($_SERVER['PATH_INFO'])) {
    define("PARSE_FOR_COLLECT_INPUT", 'PATH_INFO');
} elseif (isset($_SERVER['REQUEST_URI'])) {
    define("PARSE_FOR_COLLECT_INPUT", 'REQUEST_URI');
} else {
    define("PARSE_FOR_COLLECT_INPUT", 'REDIRECT_URL');
}
[ADDR_GMAP]
gmap_status = "true"

[ALL_CAPTCHA]
login = "0"
contact = "1"
register = "1"
select = "1"
select_file = "1"

[ALL_PAYMENT_APIS]
services = "paypal,paypalpro,googlecheckout,authorizenet,stripe"

[ATTRIBUTE_TYPE]
radio = "2"
checkbox = "3"
calender_widget = "4"
dropdown = "5"
text = "1"

[AUDIT]
status = "1"

[AUTHORIZENET]
MODE = "test"
API_LOGIN_ID = "7G7X5wBpv"
TRANSACTION_KEY = "5ZE49u69X24FYhbG"

[CAPTCHA]
contact = "0"
register = "0"
login = "0"

[CATEGORY]
level = "3"
category_status = "1"

[CHECK_LIST]
ch = "Changes"
cr = "Creation"
pr = "Permission"

[COLOR_CODE]
css = "#058143"

[CSV_USER_DATA]
First Name = "first_name,first name,fname,f_name"
Last Name = "last name,last_name,l_name,lname"
Phone Number = "phone no,phone_no,phone number,phone,mobile,contact_no"
Username = "username,uname,u_name"
Password = "password,pwd,pass"
Email ID = "email,mail,email_id,email id,mail_id,mail id"

[DEFAULT]
API_KEY = "a4ffd445afd32ea116c24b607c68fa01-us2"
ID_LIST = "705cfe6c6e"

[FACEBOOK]
fconnect = "1"
app_id = "1747265778855574"
app_secret = "29f636f16323c1500699e19ca4bade35"

[FLASH_MESSAGE_TIMING]
flash_time = "50"

[FOOTER]
copyright = "Copyright 2016"

[GENDER]
M = "Male"
F = "Female"

[GOOGLE]
is_google = "1"
client_id = "102367029832-r9ipvj66vnjqnikkgkrr474a7ie39luo.apps.googleusercontent.com"
client_secret = "MZnL9f_86LqWP2DmWDUjzZGd"
developer_key = "0"

[GOOGLECHECKOUT]
merchant_id = "333699160616384"
merchant_key = "dRRWk--oE8KkY7xT4_Rmog"
mode = "sandbox"

[HOBBIES]
status = "true"

[IMAGE]
image_orig = "image/orig/"
image_thumb = "image/thumb/"
preview_orig = "image/preview/orig/"
preview_thumb = "image/preview/thumb/"
thumb_width = "100"
thumb_height = "100"
number = "5"
image_carousel_path = "product/carousel/"
carousel_width = "400"
carousel_height = "400"
new_crop_thumb_path = "image/newCrop/"

[IMAGE_HANDLER]
library = "gd"

[IPINFO]
applyblock = "0"
blocktimeinterval = "00:15:00"
loginlogs = "1"
failure_attempt = "3"

[IS_PUBLISH]
news = "0"
events = "1"
testimonial = "1"

[KPCONFIG]
kpconfig = "kp-config"
kparea = "Describe"

[LANGUAGE]
English = "en"
French = "fr"
Chinese = "ch"

[LINKEDIN]
is_linkedin = "0"
api_key = "tk0kuzm1nde6"
secret_key = "Sb5LjwkQSrxRlI9o"

[LOGIN]
name = "1"
type = "0"
register = "0"
autologin = "0"

[MAIL]
MAIL_SEND = "1"

[MENU]
level = "2"
Menu = "1"

[MESSAGE_DISPLAY]
type = "2"

[MULTI_LANG]
islang = "1"

[MYSQL_INFO]
version = "mysql5"

[NEWS_TYPE]
1 = "entertainment"
2 = "cricket"
3 = "finance"

[PAGINATE_ADMIN]
rec_per_page = "50"
show_page = "5"

[PAGINATE_USER]
show_page = "2"
rec_per_page = "10"

[PAGINATION_VIEW]
pagination_view = "1"

[PASSWORD]
type = "1"

[PAYPAL]
business = "afixi.gourab@gmail.com"
paypal_mode = "sandbox"

[PAYPALPRO]
API_USERNAME = "afixi._1256540322_biz_api1.gmail.com"
API_PASSWORD = "1256540338"
API_SIGNATURE = "ATPAEmQLmRPGbcn0t.2OBraTOO9TA0XwyMsL05WZBeZBr2hGdA0Pi9Zs"
PROXY_HOST = "127.0.0.1"
PROXY_PORT = "808"
USE_PROXY = "FALSE"
VERSION = "65.1"
MODE = "sandbox"

[PING]
google = "http://www.google.com/webmasters/tools/ping?sitemap="
bing = "http://www.bing.com/webmaster/ping.aspx?siteMap="
ask = "http://submissions.ask.com/ping?sitemap="
yahoo = "http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=YahooDemo&url="

[PRODUCT]
image_upload_type = "1"
cropping_no = "0"
image_type = "2"

[RATE]
access = "user,viewer"
comment_type = "simple"
services = "rating,review"
max_rate = "10"
comment_level = "5"

[RATING]
access = "v"
type = "s"
services = "r,c,l"
review = "f"

[RECAPTCHA_KEY]
publickey = "6LeftNgSAAAAAA4nlp-no7HkYwu1eVXXSOstUcQD "
privatekey = "6LeftNgSAAAAAJUcstBLpoFC5wIN5u5TiudEYDNy-tE "

[REGISTRATION_MODE]
registration = "2"

[SENDGRID]
username = "afixi"
password = "p455w0rd"

[SHIPPING]
method = "UPS"
shipping_mode = "testing"

[SITE_ADMIN]
email = "aa@afixi.com"
admin_email = "Afixi Technologies Pvt. Ltd."

[SOCIAL_REGD_TYPE]
regd_type = "4"

[STRIPE]
secret_key = "sk_test_Geec7nlx5DOgbPYGdROroGKh"
publishable_key = "pk_test_RqbyhfhBofSEVdoEyHvNN464"

[TWITTER]
is_twitter = "0"
consumerKey = "EfQ90vcTsQmO1Ji8yRyxDNYEE"
consumerSecret = "Le6YIqmKjjSj97kCzg8VtEmFWwU0QnuBEuG3oMJRSymxpRdwi5"

[UPS_SHIPPING]
SERVICE_CODE = "03"
ORIGIN_POSTAL_CODE = "48010"
USERNAME = "ezekielh"
PASSWORD = "vibing12"
height = "15"
width = "15"
length = "15"

[USER_TYPE]
1 = "Admin"
2 = "Director"
3 = "Account Specifier"
4 = "Account"
99 = "Developer"

[USPS_SHIPPING]
SHIPPING_METHOD = "Priority"
ORIGIN_POSTAL_CODE = "10022"
USERNAME = "975SURYA7479"
PASSWORD = "865KP57VW894"
container_type = "Flat Rate Box"
size = "Regular"
height = "15"
width = "15"
length = "30"
girth = "55"

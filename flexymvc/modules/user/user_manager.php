<?php

if (!empty($GLOBALS['conf']['CAPTCHA']['register']) || !empty($GLOBALS['conf']['CAPTCHA']['login']) || !empty($GLOBALS['conf']['CAPTCHA']['contact'])) {
    if (file_exists(APP_ROOT . 'recaptcha/recaptchalib.php'))
        require_once(APP_ROOT . 'recaptcha/recaptchalib.php');
}

if (file_exists(AFIXI_CORE . "classes/common/validation.php"))
    include_once(AFIXI_CORE . "classes/common/validation.php");

class user_manager extends mod_manager {

    /**
     *
     * This is a constructor to initialized user module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'user', $modTable = 'user') {
        $this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }

    /**
     *
     * @method get_module_name
     * @return String Returns module name of class
     */
    function get_module_name() {
        return 'user';
    }

    /**
     *
     * @method get_manager_name
     * @return String Returns manager name of class
     */
    function get_manager_name() {
        return 'user';
    }

    /**
     * * @method manager_error_handler() Function shows the error message when the requested choice is not found.
     */
    function manager_error_handler() {
        $call = "_" . $this->_input['choice'];
        if (function_exists($call)) {
            $call($this); //$call(&$this);
        } else {
            print "<h1>" . getmessage('USER_ERROR_MSG') . "</h1>";
        }
    }

    /**
     * * @method _default() This is a default function for user manager when there will be no choice in url
     */
    function _default() {
        return $this->_loginCheck();
    }

    function _loginCheck($uname = "", $pwd = "") {
        setcookie('refer', $this->_input['refer'], time() + 60 * 60 * 24 * 365, "/" . SUB_DIR);
        $login = 1;
        if (isset($_COOKIE['username']) && isset($_COOKIE['password'])) {
            $this->getCheckCookie();
        } elseif ($_SESSION['id_user'] && !$_SESSION['id_admin']) {
            redirect(LBL_SITE_URL . "user/userHome");
        } else {
            $this->_output['tpl'] = "static/about";
        }
    }

    /**
     *
     * Check for user session and cookies.If not found then display login page.
     * @method _loginForm
     * @return Template User log in form
     */
    function _loginForm($uname = "", $pwd = "") {
        if (!$_SESSION['id_user']) {
            if (!empty($uname)) {
                $this->_output['captcha_msg'] = getmessage('USER_WRONG_CAPTCHA_CODE');
                $this->_output['uname'] = $uname;
                $this->_output['pwd'] = $pwd;
            }
            $goauthurl = LBL_SITE_URL."socialNetwork/google_loginurl";//$this->_google_loginurl();
            $this->_output['gauth'] = $goauthurl;
            $this->_output['tpl'] = 'user/login_form';
        } else {
//redirect(LBL_SITE_URL . "caller/listing");
            redirect(LBL_SITE_URL . "user/userHome");
        }
    }

    /**
     * Function is used to check the cookie.
     * * @method getCheckCookie()
     */
    function getCheckCookie() {
        $result = recordExists("user", "username = '" . $_COOKIE['username'] . "'");
        if (!empty($result)) {
            $checkpass = md5($result['id_user'] . $result['username'] . $result['password']);
            if ($checkpass == $_COOKIE['password']) {
                $_SESSION['id_user'] = $result['id_user'];
                $_SESSION['email'] = $result['email'];
                $_SESSION['username'] = $result['username'];
                if ($result['id_admin'] == $result['id_user']) {
                    $_SESSION['admin'] = isset($this->_input['admin']) ? $this->_input['admin'] : 1;
                    if ($_SESSION['admin'] == 1) {
                        $_SESSION['id_admin'] = $result['id_user'];
                    }
                }
                if ($_SESSION['id_admin']) {
                    redirect(LBL_ADMIN_SITE_URL);
                } else {
                    redirect(LBL_SITE_URL . "user/userHome");
                }
            }
        } else {
            redirect(LBL_SITE_URL);
        }
    }

    /**
     *
     * Show register template form
     * * @method _register()
     * @return template User registration form
     */
    function _register($data = '') {
        if (!empty($data)) {
            $this->_output['captcha_msg'] = getmessage('USER_WRONG_CAPTCHA_CODE');
            $this->_output['res'] = $data;
        }
        $data = get_rows_as_assoc(get_search_sql('hobbies'), 'bflag', 'hobbies_name', $err);
        $this->_output['hobbies'] = $data;
        $this->_output['gender'] = $GLOBALS['conf']['GENDER'];
        $this->_output['tpl'] = 'user/register';
    }

    /**
     * Function to check duplicacy of username.
     * That means the username is always unique.
     * * @method _checkUsername()
     * @return int Return 1.
     */
    function _checkUsername() {
        $id = $this->_input['id_user'];
        if ($id) {
            $res = recordExists("user", "username = '" . $this->_input['username'] . "'AND id_user != {$this->_input['id_user']}", "username", 0, "1");
            $res = count($res) != 0 ? 1 : 0;
        } else {
            $res = recordExists("user", "username = '" . $this->_input['username'] . "'", "username", 0, "1");
            $res = count($res) != 0 ? 1 : 0;
        }
        echo $res;
    }

    function _checkEmail() {
//        $cond = "";
//        if (!empty($this->_input['id_user'])) {
//            $cond = " AND id_user <> " . $this->_input['id_user'];
//        }
//        recordExists("user", "email = '" . $this->_input['email'] . "' " . $cond, "email", 0, "1");
        $id = $this->_input['id_user'];
        if ($id) {
            $res = recordExists("user", "email = '" . $this->_input['email'] . "'AND id_user != {$this->_input['id_user']}", "email", 0, "1");
            $res = count($res) != 0 ? 1 : 0;
        } else {
            $res = recordExists("user", "email = '" . $this->_input['email'] . "'", "username", 0, "1");
            $res = count($res) != 0 ? 1 : 0;
        }
        echo $res;
    }

    /**
     * Function to insert the record of the user during registration.
     * Here the server side vallidation is done by the unction validate().
     * After the successful insertion one mail is sent to the user for confirmation.
     * * @method _insert()
     * @return template/redirect If both error or username exist the return template otherwise redirect to user home.
     */
    function _insert($arr = '') {
        ob_clean();
        $type = $_SESSION['login_type'];
        $arr = $_SESSION[$type] ? $_SESSION[$type] : $arr;
        unset($_SESSION[$type]);
        $this->_input = $arr ? $arr : $this->_input;
        $user = $this->_input['user'] ? $this->_input['user'] : $arr;
        $name = $user['username'];
        $user['username'] = strtolower($user['username']);
        $user['gender'] = $this->_input['gender'];
        $user['email'] = ($user['email'] != 'undefined') ?$user['email'] :'';
        if ($GLOBALS['conf']['HOBBIES']['status'] == 'true') {
            if ($this->_input['hobbies']) {
                $user['hobbies'] = array_sum($this->_input['hobbies']);
            } else {
                $user['hobbies'] = 0;
            }
        }

        $conf_pass = $this->_input['cpwd'];
        if (!$arr) {
            $user['dob'] = $this->_input['dob_Year'] . "-" . $this->_input['dob_Month'] . "-" . $this->_input['dob_Day'];
            if (!empty($GLOBALS['conf']['CAPTCHA']['register'])) { //Check Capcha For register set or not
                if (isset($this->_input['recaptcha_challenge_field'])) {
                    $privatekey = $GLOBALS['conf']['RECAPTCHA_KEY']['privatekey'];
                    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                    if (!$resp->is_valid) {
                        $this->_output['conf_pwd'] = $conf_pass;
                        $this->_register($user);
                        return;
                    }
                }
            }
        } else {
            $user['fconnect'] = $arr['fconnect'] ? $arr['fconnect'] : '';
            $user['id_linkedin'] = $arr['id_linkedin'] ? $arr['id_linkedin'] : '';
            $user['twitt_connect'] = $arr['twitt_connect'] ? $arr['twitt_connect'] : '';
            $user['id_google'] = $arr['id_google'] ? $arr['id_google'] : '';
        }
        $user['gender'] = $this->_input['gender'] ? $this->_input['gender'] : '';
        $result = recordExists("user", "username='" . $name . "'");
        if (!empty($result)) {
            if ($GLOBALS['conf']['LOGIN']['register'] == 1) {
                print "err::" . getmessage('USER_CHECK_USERNAME');
                exit;
            } else {
                $this->_output['hobbies'] = $GLOBALS['conf']['HOBBIES'];
                $this->_output['gender'] = $GLOBALS['conf']['GENDER'];
                $user['hobbies'] = $this->_input['hobbies'];
                $this->_output['res'] = $user;
                $this->_output['d_name'] = getmessage('USER_CHECK_USERNAME');
                $this->_output['conf_pwd'] = $conf_pass;
                $this->_output['tpl'] = "user/register";
            }
        } else {
            if (!$arr) {
                $err = $this->dataObj->validate($user, $conf_pass);
            }
            if ($err) {
                if ($GLOBALS['conf']['LOGIN']['register'] == 1) {
                    print "err::" . "Please enter a valid email address.";
                    exit;
                } else {
                    $this->_output['err'] = $err;
                    $this->_output['hobbies'] = $GLOBALS['conf']['HOBBIES'];
                    $this->_output['gender'] = $GLOBALS['conf']['GENDER'];
                    $user['hobbies'] = $this->_input['hobbies'];
                    $this->_output['res'] = $user;
                    $this->_output['conf_pwd'] = $conf_pass;
                    $this->_output['tpl'] = "user/register";
                }
            } else {
                if ($GLOBALS['conf']['PASSWORD']['type'] != 1) {
                    $user['password'] = $GLOBALS['conf']['PASSWORD']['type']($user['password']);
                }
                $confirm_code = (isset($GLOBALS['conf']['LOGIN']['autologin']) && $GLOBALS['conf']['LOGIN']['autologin'] || $arr) ? '0' : md5(uniqid(rand(), true));

                $val = $this->dataObj->insertArrayFormat($user, "random_num,add_date,ip", "'" . $confirm_code . "',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "'", "user"); //insert($user);

                if ($val) {
                    if ($GLOBALS['conf']['LOGIN']['name'] == 2) {
                        $this->dataObj->updateStringFormat("username", $this->_input['user']['email'], "id_user=" . $val);
                    }
                    if ($user['email']) {
                        $activate_link = LBL_SITE_URL . "user/checkUser/confirm/" . $confirm_code;
                        if ($user['fconnect']) {
                            $info['social_flag'] = "Facebook";
                        } elseif ($user['id_linkedin']) {
                            $info['social_flag'] = "Linkedin";
                        } elseif ($user['twitt_connect']) {
                            $info['social_flag'] = "Twitter";
                        } elseif ($user['id_google']) {
                            $info['social_flag'] = "Google";
                        } else {
                            $info['social_flag'] = "0";
                        }

                        $info['activate_link'] = $activate_link;
                        $info['first_name'] = $user['first_name'];
                        $info['username'] = $user['username'];
                        $info['password'] = $user['password'];
                        $this->smarty->assign('sm', $info);
                        $mail_message = $this->smarty->fetch($this->smarty->add_theme_to_template("user/account_activate"));
                        $subject = $arr ? "Get Started with " . $GLOBALS['conf']['SITE_ADMIN']['admin_email'] : "Account Activation";
                        $r = sendmail($user['email'], $subject, $mail_message, $GLOBALS['conf']['SITE_ADMIN']['email']);
                    }

                    if ($arr) {
                        if ($user['fconnect']) {
                            $_SESSION['fconnect_flag'] = "1";
                            user_manager::_setLogin($user['username'], $user['password'], 1);
                        } elseif ($user['id_linkedin']) {
                            $_SESSION['linkedin_flag'] = 1;
                            user_manager::_setLogin($user['username'], $user['password'], 0, 1);
                        } elseif ($user['twitt_connect']) {
                            user_manager::_setLogin($user['username'], $user['password'], 0, 0, 1);
//                            redirect(LBL_SITE_URL . "socialNetwork/twitterEmail/uid/" . md5($val));
                        } elseif ($user['id_google']) {
                            user_manager::_setLogin($user['username'], $user['password'], 0, 0, 0, 1);
                        }
                    } else {
                        if ($GLOBALS['conf']['LOGIN']['register'] == 1) {
                            if ($GLOBALS['conf']['FACEBOOK']['fconnect'] == 1)
                                print "fcon::" . md5($val);
                            else
                                print "suc::" . LBL_SITE_URL;
                            exit;
                        } else {
                            if ($GLOBALS['conf']['FACEBOOK']['fconnect'] == 1) {
                                redirect(LBL_SITE_URL . "socialNetwork/fconnect/fid/" . md5($val));
                            } else if ($GLOBALS['conf']['LOGIN']['autologin'] == 1) {
                                $_SESSION['raise_message']['global'] = getmessage('USER_REG_SUCC');
                                $this->_setLogin($user['username'], $user['password'], 0);
                            } else {
                                $_SESSION['raise_message']['global'] = getmessage('USER_REG_SUCC_NO_FB');
                                redirect(LBL_SITE_URL);
                            }
                        }
                    }
                } else {
                    if ($GLOBALS['conf']['LOGIN']['register'] == 1) {
                        print "err::" . getmessage('USER_REG_FAIL');
                        exit;
                    } else {
                        $_SESSION['raise_message']['global'] = getmessage('USER_REG_FAIL');
                        redirect(LBL_SITE_URL);
                    }
                }
            }
        }
    }

    /**
     * Function is called during the mail comfirmation done by the user and make the user logged in.
     * * @method _checkUser()
     * @return redirect Redirect to set_login() if random number is 0 else redirect to login page.
     */
    function _checkUser() {
        $res = recordExists("user", "random_num='" . $this->_input['confirm'] . "'");
        if (!empty($res)) {
            $this->dataObj->updateStringFormat("random_num", "0", "id_user=" . $res['id_user']);
            $this->_setLogin($res['username'], $res['password']);
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_TO_LOGIN');
            redirect(LBL_SITE_URL);
        }
    }

    /**
     * Function is meant for the user login.
     * * @method _setLogin()
     * @param type $name This is the username but bydefault is ""(null).
     * @param type $pass This is the password but bydefault is ""(null).
     */
    function _setLogin($name = "", $pass = "", $fconnect = 0, $lconnect = 0, $tconnect = 0, $gconnect = 0) {
        ob_clean();
        $rem = $this->_input['rem'];
        $admin_url = $this->_input['admin_st'];

        $uname = isset($this->_input['username']) ? strtolower($this->_input['username']) : strtolower($name);
        if (isset($this->_input['password'])) {
            if (!empty($admin_url)) {
                $pwd = $this->_input['password'];
            } else {
                $pwd = ($GLOBALS['conf']['PASSWORD']['type'] != 1) ? $GLOBALS['conf']['PASSWORD']['type']($this->_input['password']) : $this->_input['password'];
            }
        } else {
            $pwd = $pass;
        }

        if (empty($uname) || empty($pwd)) {
            $_SESSION['raise_message']['global'] = (!empty($this->_input['admin_st'])) ? getmessage('USER_VALID_UNAME_PASS') : getmessage('USER_UNAME_PASS');
//						$_SESSION['raise_message']['global'] = getmessage('USER_UNAME_PASS');
            if ($admin_url) {
                redirect(LBL_ADMIN_SITE_URL);
            } else {
                redirect(LBL_SITE_URL . "loginForm");
            }
        }

        $capFailure = 0;
        if (!empty($GLOBALS['conf']['CAPTCHA']['login'])) { //Check Capcha For Login set or not
            if (isset($this->_input['recaptcha_challenge_field'])) {
                $privatekey = $GLOBALS['conf']['RECAPTCHA_KEY']['privatekey'];
                $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                if (!$resp->is_valid) {
                    $msgLogin = getmessage('USER_WRONG_CAPTCHA_CODE');
                    $capFailure = 1;
                }
            }
        }

        if (empty($capFailure)) {
            $checkip = 0;
            if (!empty($GLOBALS['conf']['IPINFO']['applyblock']) && empty($admin_url)) {//Checking for ip blocking interval if set
                $checkip = $this->checkInterval($uname);
            }
            if (!$checkip) {//Checking ip blocking
                $logincount = $_SESSION['login_count'] ? $_SESSION['login_count'] : 0;
                $result = recordExists("user", "username = '" . $uname . "'");
                if (!empty($result)) {//checking result is empty or not
                    if ($uname == $result['username'] && $pwd == $result['password']) {//Matching username and password
                        if ($result['random_num'] == '0') {//Checking randum number is 0 or not
                            if ($result['flag'] == 1) {//Checking admin block this user or not
                                $info = array('autologin' => 1, 'id_user' => $result['id_user'],
                                    'username' => $result['username'],
                                    'email' => $result['email'],
                                    'password' => $result['password']);
                                if ($rem) {
                                    $this->setAutoLogin($info);
                                }
                                $dconf = array_flip($GLOBALS['conf']['USER_TYPE']);
                                if ($result['id_admin'] == $dconf['Admin']) {
                                    if($result['username']!='admin' && $result['id_admin']==1){
                                     date_default_timezone_set('Asia/Calcutta');
                                     $dt=date("Y-m-d H:i:s");
                                     if($dt>$result['admin_time']){
                                         $upsql="UPDATE ".TABLE_PREFIX."user SET id_admin=0 WHERE id_user={$result['id_user']}";
                                          execute($upsql);
                                        $_SESSION['raise_message']['global'] = "You are no longer be an Admin";
			                redirect(LBL_ADMIN_SITE_URL);
                                     }
                                    }
                                    $_SESSION['admin'] = isset($this->_input['admin']) ? $this->_input['admin'] : 1;
                                    if ($_SESSION['admin'] == 1) {
                                        $_SESSION['id_admin'] = $result['id_user'];
                                    }
                                }
                                if ($result['id_admin'] == $dconf['Developer']) {
                                    $_SESSION['id_developer'] = $result['id_user'];
                                    $_SESSION['id_admin'] = $result['id_user'];
                                }
                                if (!empty($this->_input['userui'])) {
                                    $_SESSION['userui'] = $this->_input['userui'];
                                }
                                if (empty($_SESSION['id_admin']) && ($this->_input['admin_st'])) {
                                    $this->_input['username'] = '';
                                    $this->_setLogin($this->_input['username']);
                                }
                                $_SESSION['id_user'] = $result['id_user'];
                                $_SESSION['email'] = $result['email'];
                                $_SESSION['username'] = $result['username'];
                                $_SESSION['fconnect'] = $result['fconnect'];
                                $_SESSION['id_linkedin'] = $result['id_linkedin'];
                                $_SESSION['twitt_connect'] = $result['twitt_connect'];
                                $_SESSION['id_google'] = $result['id_google'];
                                $_SESSION['raise_message']['global'] = getmessage('USER_LOGIN_SUCC');
                                $_SESSION['login_count'] = 0;
                                $_SESSION['ip_count'] = 0;
//	print "<pre>";print_r($_SESSION);print "</pre>";exit;
//Delete all entries of this IP if successfully login
                                if ($GLOBALS['conf']['IPINFO']['applyblock'] && empty($admin_url)) {
                                    $del_blockkip = $this->dataObj->delete("ip = '" . IP . "'", "blockedip");
                                }
//Insert to login table
                                if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                                    $this->dataObj->loginLogs($result, 1);
                                }

                                if ($_SESSION['id_admin']) {//pr($_SESSION);exit;
//                                    $this->_emptyPreview();
                                    if ($GLOBALS['conf']['LOGIN']['type'] == 1 && empty($this->_input['a'])) {
                                        redirect(LBL_ADMIN_SITE_URL);
                                    } else {
                                        redirect(LBL_ADMIN_SITE_URL);
                                    }
                                } else {
                                    if ($GLOBALS['conf']['LOGIN']['type'] == 1 && ($fconnect == 0 && $lconnect == 0 && $tconnect == 0 && $gconnect == 0)) {
                                        print "suc::" . LBL_SITE_URL . "user/userHome";
                                        exit;
                                    } else {
                                        if ($fconnect == 0 && $lconnect == 0 && $tconnect == 0 && $gconnect == 0) {
                                            redirect(LBL_SITE_URL . "user/userHome");
                                        } elseif ($fconnect) {
                                            $_SESSION['raise_message']['global'] = getmessage('USER_FB_LOGIN_SUCC');
                                            if ($_SESSION['reg']) {
                                                redirect(LBL_SITE_URL . "user/userHome");
                                            } else {
                                                redirect(LBL_SITE_URL . "user/userHome");
//                                                echo '1';
//                                                exit;
                                            }
                                        } elseif ($lconnect) {
                                            $_SESSION['raise_message']['global'] = getmessage('USER_LINKEDIN_LOGIN_SUCC');
                                            echo '1';
                                            exit;
                                        } elseif ($tconnect) {
                                            $_SESSION['raise_message']['global'] = getmessage('USER_TWITTER_LOGIN_SUCC');
                                            if ($tconnect == '11') {
                                                echo '1';
                                                exit;
                                            } else {
                                                redirect(LBL_SITE_URL . "user/userHome");
                                            }
                                        } elseif ($gconnect) {
                                            $_SESSION['raise_message']['global'] = "Successfully connect with google";
                                            redirect(LBL_SITE_URL . "user/userHome");
                                        }
                                    }
                                }
                            } else {
                                $msgLogin = getmessage('USER_BLOCK_MSG');
                                if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                                    $this->dataObj->loginLogs($result, 2); //Insert to login table
                                }
                            }
                        } else {
                            $msgLogin = getmessage('USER_CONF_MAIL');
                            if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                                $this->dataObj->loginLogs($result, 3);
                            }
                        }
                    } else {
                        $logincount++;
                        $_SESSION['login_count'] = $logincount;

                        $msgLogin = getmessage('USER_VALID_UNAME_PASS');
                        if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                            $this->dataObj->loginLogs($result, 4);
                        }

                        if (!empty($GLOBALS['conf']['IPINFO']['applyblock'])) {//Checking for ip blocking interval if set
                            if ($logincount >= $GLOBALS['conf']['IPINFO']['failure_attempt']) {
                                $msgLogin = getmessage('USER_BLOCK_IP_MSG');
                                if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                                    $this->dataObj->loginLogs($result, 6);
                                }
                                if (empty($admin_url))
                                    $this->dataObj->blockedIP($uname, "Password is Wrong");
                            }
                        }
                    }
                } else {
                    $logincount++;
                    $_SESSION['login_count'] = $logincount;

                    $msgLogin = getmessage('USER_VALID_UNAME_PASS');
                    if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                        $this->dataObj->loginLogs($result, 5);
                    }

                    if (!empty($GLOBALS['conf']['IPINFO']['applyblock'])) {//Checking for ip blocking interval if set
                        if ($logincount >= $GLOBALS['conf']['IPINFO']['failure_attempt']) {
                            $msgLogin = getmessage('USER_BLOCK_IP_MSG');
                            if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                                $this->dataObj->loginLogs($result, 6);
                            }
                            if (empty($admin_url))
                                $this->dataObj->blockedIP($uname, "Both Credentials are wrong");
                        }
                    }
                }
            } else {
                $msgLogin = getmessage('USER_BLOCK_IP_MSG') . "Try after sometime";
                if (!empty($GLOBALS['conf']['IPINFO']['loginlogs'])) {
                    $result = recordExists("user", "username = '" . $uname . "'");
                    $this->dataObj->loginLogs($result, 6);
                }
            }
        }

        if ($GLOBALS['conf']['LOGIN']['type'] == 1 && empty($admin_url) && ($fconnect == 0 && $lconnect == 0 && $tconnect == 0 && $gconnect == 0)) {
            print "err::" . $msgLogin;
            exit;
        } else {
            $_SESSION['raise_message']['global'] = $msgLogin;
            if ($admin_url) {
                redirect(LBL_ADMIN_SITE_URL);
            } else {
                redirect(LBL_SITE_URL . "loginForm#members");
            }
        }
    }

    /**
     * Function is checking the time interval for ip blocking.
     * * @method checkInterval()
     * @return bol i.e 0 or 1
     */
    function checkInterval($uname) {
        $cond = "ip='" . IP . "' ORDER BY id_block DESC";
        $diff = recordExists("blockedip", $cond, "time_fail,time_upto");
        if (empty($diff)) {
            return 0;
        } else {
            $sql = "SELECT TIMEDIFF(NOW(),'" . $diff['time_upto'] . "') as inter_val";
            $res = getsingleindexrow($sql);
            if (strpos($res['inter_val'], '-') === 0) {
                return 1;
            } else {
                $del_blockkip = $this->dataObj->delete("username = '" . $uname . "' AND ip = '" . IP . "'", "blockedip");
                return 0;
            }
        }
    }

    /**
     * Function is called in case of rememberme.
     * * @method setAutoLogin()
     * @param array $info This is a array containing username,password and id_user etc.
     */
    function setAutoLogin($info) {
        $v_user_password = md5($info['id_user'] . $info['username'] . $info['password']);
        setcookie('email', $info['email'], time() + 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('username', $info['username'], time() + 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('password', $v_user_password, time() + 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('id_user', $info['id_user'], time() + 60 * 60 * 24 * 365, "/" . SUB_DIR);
    }

    /**
     * Function is for the editing of user profile.
     * </p> Here we are first of all checking the session and then only go for the edit.
     * * @method _edit()
     * @return template Return the register template.
     */
    function _edit() {
        $type= $_SESSION['login_type'];
        if ($_SESSION['regd_type'] == 1) {
            $cond = "{$type} = '{$_SESSION[$type]}'";
            $tpl = "user/register_basic";
        } elseif ($_SESSION['regd_type'] == 2) {
            $cond = "{$type} = '{$_SESSION[$type]}'";
            $tpl = "user/register";
        } else {
            check_session();
            $cond = "id_user = {$_SESSION['id_user']}";
            $tpl = "user/register";
            $this->_output['flag'] = 1;
        }
        $data = get_rows_as_assoc(get_search_sql('hobbies'), 'bflag', 'hobbies_name', $err);
        $sql = get_search_sql("user", "{$cond} LIMIT 1");
        $result = getrows($sql, $err);

        $this->_output['hobbies'] = $data;
        $this->_output['gender'] = $GLOBALS['conf']['GENDER'];
        $this->_output['res'] = $result[0];
        $this->_output['tpl'] = $tpl;
    }

    /**
     * Function is used to update the user profile.
     * Here the session is checked first then only the updation is done.</p>
     * For updation we are using the updateArrayFormat() function .</p>
     * updateArrayFormat() function is a function present in db.class.php.
     * It takes information tablename as first parameter which you can pass it as "".
     * second parameter as a array of user data and third as string for condition.
     * * @method _updateProfile()
     * @return template/redirect Return template if there is a error else redirect home page.
     */
    function _updateProfile() {
        $_SESSION['id_user'] = $this->_input['id_user'] ? $this->_input['id_user'] : $_SESSION['id_user'];
        check_session();
        $user = $this->_input['user'];
        unset($user['username']);
        if ($GLOBALS['conf']['HOBBIES']['status'] == 'true') {
            if ($this->_input['hobbies']) {
                $user['hobbies'] = array_sum($this->_input['hobbies']);
            } else {
                $user['hobbies'] = 0;
            }
        }
        $user['gender'] = $this->_input['gender'];
        $user['dob'] = ($this->_input['dob_Year'] || $this->_input['dob_Year'] || $this->_input['dob_Month'] ) ? $this->_input['dob_Year'] . "-" . $this->_input['dob_Month'] . "-" . $this->_input['dob_Day'] : '';
        if (!($this->_input['id_google']||$this->_input['fconnect'] || $this->_input['twitt_connect']))
            $err = $this->dataObj->validate($user, '');
        if ($err) {
            $this->_output['err'] = $err;
            $user['hobbies'] = $this->_input['hobbies'];
            $this->_output['res'] = $user;
            $this->_output['flag'] = 1;
            $this->_output['hobbies'] = $GLOBALS['conf']['HOBBIES'];
            $this->_output['tpl'] = "user/register";
        } else {
            $user['random_num'] = 0;
            $user['username'] = $this->_input['user']['username'];
            $_SESSION['username'] = $user['username'];
            $this->dataObj->updateArrayFormat($user, "id_user=" . $_SESSION['id_user'], "");
            if ($this->_input['id_google'] || $this->_input['fconnect'] || $this->_input['twitt_connect']) {
                if ($this->_input['fconnect']) {
                    $msg = "facebook";
                } else if ($this->_input['id_google']) {
                    $msg = "google";
                } else if ($this->_input['twitt_connect']) {
                    $msg = "twitter";
                }
                $_SESSION['raise_message']['global'] = "Successfully connect with {$msg}";
                header("Location:" . LBL_SITE_URL . "user/userHome");
            } else {
                $_SESSION['raise_message']['global'] = getmessage('USER_UPDATE_SUCC');
                redirect(LBL_SITE_URL . "user/edit");
            }
        }
    }

    /**
     * Function takes the user to his home page.
     * Here first the session is checked.
     * Then only it go to the home page ,othewise it redirect to the login page for login again.
     * * @method _userHome()
     * @return template Return the user_home template.
     */
    function _userHome() {
        check_session();
        unset($_SESSION['token']);
        unset($_SESSION['regd_type']);
        $this->_output['tpl'] = 'user/user_home';
    }

    /**
     * Function is used to take the user to change_pwd page for changeing the password.
     * * @method _changePassword()
     * @return template Return the change_pwd template.
     */
    function _changePassword() {
        check_session();
        $res = getsingleindexrow(get_search_sql('user',"id_user={$_SESSION['id_user']}",'id_user,password'));
        $this->_output['res'] = $res;
        $this->_output['tpl'] = 'user/change_pwd';
    }

    /**
     * Function is used to update the password of the user.
     * Here session is also checked.i.e whether the user is a logged in one or not.
     * * @method _updatePassword()
     */
    function _updatePassword() {
        check_session();
        $password = $this->_input['pwd'];
        $old_pwd = ($GLOBALS['conf']['PASSWORD']['type'] != 1) ? $GLOBALS['conf']['PASSWORD']['type']($password['pwd']) : $password['pwd'];
        $new_pwd = $password['pass'];
        $sql = get_search_sql("user", "id_user = '" . $_SESSION['id_user'] . "' LIMIT 1");
        $rs = getsingleindexrow($sql);
        if ($rs['password'] == $old_pwd) {
            if ($GLOBALS['conf']['PASSWORD']['type'] != 1) {
                $new_pwd = ($GLOBALS['conf']['PASSWORD']['type'] != 1) ? $GLOBALS['conf']['PASSWORD']['type']($new_pwd) : $new_pwd;
            }
            $err = $this->dataObj->updateStringFormat("password", $new_pwd, "id_user='" . $_SESSION['id_user'] . "'");
            if ($err) {
                $_SESSION['raise_message']['global'] = getmessage('USER_PWD_CHANGE_SUCC');
                redirect(LBL_SITE_URL . "user/userHome");
            } else {
                $_SESSION['raise_message']['global'] = getmessage('USER_PWD_CHANGE_SUCC');
                redirect(LBL_SITE_URL . "user/userHome");
            }
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_WRONG_PASS');
            redirect(LBL_SITE_URL . "user/changePassword");
        }
    }

    /**
     * Function is used for the confirmation of user.
     * This function is called when the user click on the link provided in the mail for confirmation.
     * And also a updation of user record is done .
     * The random number filed is set to 0 from the random number assigned during registration.
     * * @method _confirmUser()
     * @return redirect Redirect to the checkLogin if username and password is there otherwise redirect to default function.
     */
    function _confirmUser() {
        $this->dataObj->i_random = $this->_input['r'];
        $res = $this->dataObj->get_user_records(array(0 => 'i_random'));
        $this->dataObj->id_user = $res['id_user'];
        $id_user = $this->dataObj->id_user;
        $row = getrows($this->user_bl->get_search_sql('id_user = ' . $id_user), $err);
        if ($this->dataObj->id_user) {
            $this->dataObj->i_random = 0;
            $this->_input['name'] = $row[0]['name'];
            $this->_input['u_password'] = $row[0]['u_password'];
            $this->dataObj->update_random($id_user, $this->dataObj->i_random);
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_MSG');
            redirect("user/checkLogin/name-" . $this->_input['name'] . "/u_password/" . $this->_input['u_password']);
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_CONF_AFTER');
        }
        redirect(LBL_SITE_URL);
    }

    /**
     * Function to show the forgeot_pwd page.
     * * @method _forgotPwd()
     */
    function _forgotPwd() {
        if (empty($this->_input['s'])) {
            $this->_output['tpl'] = 'user/forgot_pwd';
        } else {
            ob_clean();
            if ($this->_input['email']) {
                $cond = "username = '" . $this->_input['email'] . "'";
            } else {
                $cond = "username = '" . $this->_input['usr_name'] . "'";
            }
            $tpl = "user/forgot_password";
            $arr = recordExists("user", $cond);
            if (!empty($arr)) {
                $info['username'] = $arr['username'];
                $to = $arr['email'];
                $from = $GLOBALS['conf']['SITE_ADMIN']['email'];
                $subject = "Forgot password \n\n";

//changed for encripted password
                if ($GLOBALS['conf']['PASSWORD']['type']) {
                    $info['link'] = LBL_SITE_URL . 'user/setPwd/mid/' . md5($arr['id_user']);
                }//end
                $this->smarty->assign('sm', $info);
                $body = $this->smarty->fetch($this->smarty->add_theme_to_template($tpl));
                $msg = sendmail($to, $subject, $body, $from); // also u can pass  $cc,$bcc
                if (!$msg)
                    echo "Mail not send.<br>" . $body;
                exit;
                $_SESSION['raise_message']['global'] = getmessage('USER_PASS_TO_MAIL');
                ($this->_input['flag']) ? redirect(LBL_ADMIN_SITE_URL) : redirect(LBL_SITE_URL . "loginform#members");
            } else {
                $_SESSION['raise_message']['global'] = getmessage('USER_NO_ACC');
                ($this->_input['flag']) ? redirect(LBL_ADMIN_SITE_URL . "#lpwd") : redirect(LBL_SITE_URL . "user/forgotPwd");
            }
        }
    }

    /**
     * Function is used to load the set_pwd template.
     * * @method _setPwd()
     * @return template Return set_pwd template.
     */
    function _setPwd() {
        if (!isset($this->_input['mid'])) {
            $en_pass = ($GLOBALS['conf']['PASSWORD']['type'] != 1) ? $GLOBALS['conf']['PASSWORD']['type']($this->_input['pwd']['npass']) : $this->_input['pwd']['npass'];
            $res = $this->dataObj->updateStringFormat("password", $en_pass, "md5(id_user) ='" . $this->_input['id_usr'] . "'");
            if ($res) {
                $_SESSION['raise_message']['global'] = getmessage('USER_PASS_CHANGE_SUCC');
                redirect(LBL_SITE_URL);
            } else {
                $_SESSION['raise_message']['global'] = getmessage('USER_PASS_CHANGE_FAIL');
                redirect(LBL_SITE_URL . 'user/setPwd');
            }
        } else {
            $this->_output['mid'] = $this->_input['mid'];
            $this->_output['tpl'] = 'user/set_pwd';
        }
    }

    /**
     * Function is used to logged out the currently logged in user.
     * @method _looout()
     * @return void redirect to the admin login if it is admin side else to the user side login.
     */
    function _logout() {
        $site = $_SESSION['site_used'];
        setcookie('username', '', time() - 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('password', '', time() - 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('email', '', time() - 60 * 60 * 24 * 365, "/" . SUB_DIR);
        setcookie('id_user', '', time() - 60 * 60 * 24 * 365, "/" . SUB_DIR);
//setcookie('login_cnt','', time()-60*60*24*365,"/");
        $_COOKIE['username'] = "";
        $_COOKIE['password'] = "";
        $_COOKIE['email'] = "";
        $_COOKIE['id_user'] = "";
//$_COOKIE['login_cnt'] = "";
        $ui = (!empty($_SESSION['userui'])) ? '1' : 0;
        $_SESSION = array('');
        unset($_SESSION);
        session_unset();
        session_destroy();
        session_start();
        $_SESSION['username'] = "";
        $_SESSION['email'] = "";
        $_SESSION['id_user'] = "";
        $_SESSION['fconnect_flag'] = "";
        $_SESSION['linkedin_flag'] = "";
        $_SESSION['twitter_flag'] = "";
        $_SESSION['userui'] = "";
        $_SESSION['multi_language']="default";
        $_SESSION['raise_message']['global'] = getmessage('USER_LOGOUT_SUCC');
        if ($this->_input['a']) {
            $_SESSION['id_admin'] = "";
            $_SESSION['admin'] = "";
            if ($ui) {
                redirect(LBL_SITE_URL);
            } else {
                redirect(LBL_ADMIN_SITE_URL);
            }
        } else {
            redirect(LBL_SITE_URL);
        }
    }

    /**
     * Function is used for rejection of th user by admin.
     * That means the user record is deleted from the database.
     * Also a mail is sent to the user for rejection.
     * * @method _rejectUser()
     * @return void redirect to the LBL_SITE_URL.
     */
    function _rejectUser() {
        $id = $this->_input['id'];
        $str = $this->_input['str'];
        $cond = 'id_user = ' . $id . ' AND MD5(CONCAT(name,d_registration)) = "' . $str . '"';
        $row = getrows($this->user_bl->get_search_sql($cond), $err);
        if (count($row) == 0) {
            $_SESSION['raise_message']['global'] = getmessage('USER_NO_SUCH_USER');
            redirect(LBL_SITE_URL);
        }
        $this->dataObj->delete($cond); //CHANGED BY PRABHU
        $from = $GLOBALS['conf']['SITE_ADMIN']['email'];
        $to = $row[0]['v_email'];
        $subject = $GLOBALS['conf']['SITE_ADMIN']['registration_subject'];
        $info = $row[0]['name'];

        $info['u_password'] = $row[0]['u_password'];
        $info['confirm_link'] = LBL_SITE_URL;

        $to = $to;
        $subject = $subject;
        $tpl = "user/user_reg_reject";

        $this->smarty->assign('sm', $info);
        $body = $this->smarty->fetch($this->smarty->add_theme_to_template($tpl));

        $msg = sendmail($to, $subject, $body, $from); // also u can pass  $cc,$bcc
        $_SESSION['raise_message']['global'] = "<center>" . getmessage('USER_REJECT_CONF') . "</center>";
        redirect(LBL_SITE_URL);
    }

    /**
     * Function is used to block the user.
     * That means the record of the user is still in the database but he can not log in.
     * * @method _blockUer()
     * @return void redirect to login page if authentication fail otherwise redirect to search.
     */
    function _blockUer() {
        $type = $this->_input['t'];
        $id_user = $this->_input['uid'];
        $sql = get_search_sql('user', "id_user = " . $id_user . " LIMIT 1", "id_forum");
        $res = getrows($sql, $err);
        $id_forum = $res[0]['id_forum'];
        $mystring = 'xyz,' . $_SESSION['wfids'] . ',';
        if (strrpos($mystring, '23,')) {
            if ($type == 'b') {
                $setvalue = "-1";
            } elseif ($type == 'ub') {
                $setvalue = "0";
            }
            $this->dataObj->updateStringFormat("i_random", $setvalue, "id_user = " . $id_user); //CHANGED BY PRABHU
            if ($err) {
                if ($type == 'b') {
                    $_SESSION['raise_message']['global'] = getmessage('USER_BLOCK_USER_SUCC');
                } elseif ($type == 'ub') {
                    $_SESSION['raise_message']['global'] = getmessage('USER_UNBLOCK_USER_SUCC');
                }
                redirect(LBL_SITE_URL . 'forum/search');
            }
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_FAIL');
            redirect(LBL_SITE_URL);
        }
    }

    /*
     * Clear function clear the cache ,templates_admin_c and the templates_c folder.
     * @method _clear()
     */

    function _clear() {
        exec("rm var/cache/*.* -f");
        exec("rm var/templates_admin_c/*.* -f");
        exec("rm var/templates_c/*.* -f");
    }

    /**
     * Function is used for the listing of the user.
     * * @method _listUser()
     *
     */
    function _listUser() {
        if ($_SESSION['access'] & $GLOBALS['conf']['ACCESS_RIGHT']['add_user']) {
            $sql = "SELECT * FROM " . TABLE_PREFIX . "user WHERE 1";
            $uri = 'user/listuser';
            if ($this->_input['fname']) {
                $sql.=" AND fname LIKE '" . $this->_input['fname'] . "%'";
                $uri.='-fname-' . $this->_input['fname'];
            }
            if ($this->_input['u_status'] >= "0") {
                $sql.=" AND u_status=" . $this->_input['u_status'];
                $uri.='-u_status-' . $this->_input['u_status'];
            }
            $results_params = Array('URI' => $uri, 'SQL' => $sql, 'MODULE' => 'user', 'TPL' => 'user/list_user', 'CACHE_PREPEND' => 'sef|message|list|', 'RESULTS_UNDER' => 'res', 'DEBUG' => 1, 'DEF_FIELD' => 'fname');
            $this->user_bl->setupresults($results_params);
            $this->_output['cache_id'] = $this->user_bl->get_cache_id();
            $this->_output['tpl'] = "user/list_user";
            $this->_output['u_status'] = $GLOBALS['conf']['U_STATUS'];
            $this->_output['su_status'] = $this->_input['u_status'];
            $this->_output['fname'] = $this->_input['fname'];
            if (!$this->cache_alive()) {
                $this->_output = array_merge($this->_output, $this->user_bl->get_output());
                $this->_output['TITLE'] = "User List";
                $this->_output['use_session'] = "1";
                $this->_output['choice'] = 'list_user';
            }
            return true;
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_FAIL');
        }
    }

    /**
     * adduser() function is used to show the add_user page.
     * @param array $input
     */
    function _adduser($input = '') {
        if ($_SESSION['access'] & $GLOBALS['conf']['ACCESS_RIGHT']['add_user']) {
            $status = array_pop($GLOBALS['conf']['U_STATUS']);
            $this->_output = $input;
            $this->_output['shortname'] = '';
            $this->_output['status'] = $GLOBALS['conf']['U_STATUS'];
            $this->_output['add'] = 'ADD';
            $this->_output['smt'] = 'Submit';
            if ($this->_input['cid']) {
                $this->_output['cid'] = $this->_input['cid'];
                $this->_output['u_status'] = $GLOBALS['conf']['USER_STATUS']['client'];
            }
            $this->_output['choice'] = 'insertuser';
            $this->_output['tpl'] = 'user/add_user';
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_FAIL');
            redirect('user/listuser');
        }
    }

    /**
     * Function is used for user insertion.
     * First check whether the user is exist or not.
     * Then only it insert the record.
     * * @method _insertUser()
     */
    function _insertUser() {
        $dpl_cond = " shortname = '" . $this->_input['shortname'] . "'";
        $dplusr = get_search_sql('user', $dpl_cond);
        $dplres = getrows($dplusr, $err);
        if (getaffectedrows()) {
            print getmessage('USER_ALIAS_EXIST');
            $this->_adduser($this->_input);
        } else {
//$this->dataObj->loadfromarr($this->_input);
//$res = $this->dataObj->insert();
//redirect('user/listuser');
        }
    }

    /**
     * Function is used for user edit.
     * * @method _editUser()
     * @return template/errorMsg return add_user template with pre filled data if access is there otherwise error message.
     */
    function _editUser() {
        if ($_SESSION['access'] & $GLOBALS['conf']['ACCESS_RIGHT']['add_user']) {
            $cond = "id_user = " . $this->_input['id'];
            $sql = get_search_sql('user', $cond);
            $res = getsingleindexrow($sql);
            $this->_output = $res;
            $this->_output['acc'] = $GLOBALS['conf']['ACCESS_RIGHT'];
            $status = array_pop($GLOBALS['conf']['U_STATUS']);
            $this->_output['status'] = $GLOBALS['conf']['U_STATUS'];
            $this->_output['add'] = 'EDIT';
            $this->_output['smt'] = 'Update';
            if ($this->_input['cid']) {
                $this->_output['cid'] = $this->_input['cid'];
                $this->_output['u_status'] = $GLOBALS['conf']['USER_STATUS']['client'];
            }
            $this->_output['choice'] = 'updateuser';
            $this->_output["HTTP_REFERER"] = $_SERVER['HTTP_REFERER'];
            $this->_output['tpl'] = 'user/add_user';
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_FAIL');
        }
    }

    /**
     * * updateuser() function is used for
     */
    function _updateuser() {
        $cond = $this->_input['id_user'];
        $this->dataObj->loadfromarr($this->_input);
        $this->dataObj->update('', $cond);
        $_SESSION['raise_message']['global'] = getmessage('USER_UPDATE');


        $this->dataObj->updateStringFormat("assignee_name", "REPLACE(assignee_name,'" . $this->_input['oldname'] . "','" . $this->_input['shortname'] . "')", "", "project"); //CHANGED BY PRABHU
//////////////  Update the assignee name in project table    ////////////////
//$psql = "UPDATE " . TABLE_PREFIX . "project SET assignee_name = REPLACE(assignee_name,'" . $this->_input['oldname'] . "','" . $this->_input['shortname'] . "')";
//execute($psql);
////////   END   ////////
//////////////  Update the Assignee User,Reviewers,Assigner in Task table    ////////////////
        $tsql = "UPDATE " . TABLE_PREFIX . "task SET
			assign_users = REPLACE(assign_users,'" . $this->_input['oldname'] . "','" . $this->_input['shortname'] . "'),
			assigner = REPLACE(assigner,'" . $this->_input['oldname'] . "','" . $this->_input['shortname'] . "'),
			reviewers = REPLACE(reviewers,'" . $this->_input['oldname'] . "','" . $this->_input['shortname'] . "')";
        execute($tsql);
////////   END   ////////
//redirect('page-user-choice-listuser');
        header('Location: ' . $this->_input["HTTP_REFERER"]);
        exit;
    }

    /**
     * Function to delete the user.
     * * @method _deleteUser()
     */
    function _deleteUser() {
        print "This is i user manager fun name - deleteuser check it";
        exit;
        if ($_SESSION['access'] & $GLOBALS['conf']['ACCESS_RIGHT']['add_user']) {
            $this->dataObj->delete($this->_input['id']);
            $_SESSION['raise_message']['global'] = getmessage('USER_DELETE_SUCC');
            redirect('user/listuser');
        } else {
            $_SESSION['raise_message']['global'] = getmessage('USER_AUTH_FAIL');
        }
    }

    /**
     * Function is used for the captcha.
     * That means putting captcha in a file where it is required.
     * This function is called by {getmod ..}
     * Now it is not used.
     * * @method _getCaptcha()
     * @return template Return the getcaptcha template.
     */
    function _getCaptcha() {
        if ($_SESSION['login_count'] >= 2) {
            $captcha = new captcha_bl();
            $this->_output['captcha'] = $captcha->get_captcha();
            $this->_output['tpl'] = 'user/getcaptcha';
        }
    }

    /**
     * Function is used for setting ip_count for a user.
     * The number of time the user fails to login the count is increased by one each time.
     * * @method chkPrevUname()
     * @param string $uname
     */
    function chkPrevUname($uname) {
        $ipcount = $_SESSION['ip_count'];
        if (isset($_SESSION['ip_uname'])) {
            if ($_SESSION['ip_uname'] == $uname) {
                $ipcount++;
                $_SESSION['ip_count'] = $ipcount;
            } else {
                $_SESSION['ip_count'] = 0;
            }
        } else {
            $_SESSION['ip_uname'] = $uname;
        }
    }

    /**
     * Function is used for putting the recaptcha in files.
     * It is called by the {get_mod ..} method.
     * Using the public key the method recaptcha_get_html() put the recaptcha in the file where it is called.
     * * @method _getCaptchaDb()
     * @return template Return the getrecaptcha template.
     */
    function _getCaptchaDb() {
        $publickey = $GLOBALS['conf']['RECAPTCHA_KEY']['publickey'];
        $this->_output['captcha'] = recaptcha_get_html($publickey);
        $this->_output['tpl'] = 'user/getrecaptcha';
        /* $captcha = $GLOBALS['conf']['CAPTCHA']['login'];
          if ($captcha) {
          $cond = " ip='" . $_SERVER['REMOTE_ADDR'] . "' ORDER BY id_login DESC LIMIT 1";
          $sql = get_search_sql("login", $cond);
          $res = getrows($sql, $err);
          if ($_SERVER['SERVER_ADDR'] != '192.168.1.222' && $res[0]['failure_attempt'] >= 1) {
          $publickey = $GLOBALS['conf']['RECAPTCHA_KEY']['publickey'];
          $this->_output['captcha'] = recaptcha_get_html($publickey);
          $this->_output['tpl'] = 'user/getrecaptcha';
          }
          } */
    }

    /**
     * Functiopn to set the language.
     * * @method _setlang()
     */
    function _setlang() {
        $_SESSION['lang'] = $this->_input['lang'] ? $this->_input['lang'] : '';
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Function is used to remove the images of the preview folder.
     * That is from orig and thumb respectively.
     * * @method _emptyPreview()
     */
    function _emptyPreview() {
        $uploadDir_prev = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_orig'];
        $thumbnailDir_prev = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_thumb'];
        if (file_exists($uploadDir_prev)) {
            $image = scandir($uploadDir_prev);
            unset($image[0]);
            unset($image[1]);
            $image[] = array_shift($image);
        }
        for ($i = 0; $i < count($image); $i++) {
            if ((time() - filectime($uploadDir_prev . $image[$i]) > (5 * 24 * 60 * 60))) {
                unlink($uploadDir_prev . $image[$i]);
            }
        }
        if (file_exists($thumbnailDir_prev)) {
            $image_thum = scandir($thumbnailDir_prev);
            unset($image_thum[0]);
            unset($image_thum[1]);
            $image[] = array_shift($image_thum);
        }
        for ($i = 0; $i < count($image_thum); $i++) {
            if ((time() - filectime($uploadDir_prev . $image_thum[$i]) > (5 * 24 * 60 * 60))) {
                unlink($uploadDir_prev . $image_thum[$i]);
            }
        }
    }

    public function __destruct() {
        
    }

    function _showMap() {
        $this->_output['tpl'] = 'user/map_show';
    }
}

;

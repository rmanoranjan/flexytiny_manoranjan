<?php
    use Dompdf\Dompdf;
class admin_student extends student_manager {
    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
        if ($_SESSION['id_admin']) {
            parent::__construct($smarty, $_output, $_input);
        } else {
            redirect(LBL_ADMIN_SITE_URL);
        }
    }
    
function _studentlisting(){
    $uri=PAGINATE_ADMIN_PATH."student/studentlisting/psearch/1/searchval/{$this->_input['searchval']}";
    //parse_str($this->_input['searchval'],$data);
    //pr($data);exit;
    $cond="1";
    if($this->_input['psearch']==1){
        parse_str($this->_input['searchval'],$data);
        if($data[fname][0]){
            $nameval=$data[fname][0];
            $cond.= " AND firstname LIKE '$nameval%'";
        }
        if($data[mobile]){
            $cond.= " AND mobile LIKE '$data[mobile]%'";
            
        }
        if($data[gender]){
            $cond.= " AND gender LIKE '$data[gender]%'";
        }
    }
//    $cond.=" order by id desc";
            $sql=get_search_sql('practice_student',$cond);
            $qstart = isset($this->_input['qstart']) ? $this->_input['qstart'] : 0;
            $this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
            $this->_output['show'] =2;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
            $this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
            $this->_output['qstart'] = $qstart;
            $this->_output['sql'] = $sql;//pr($sql);exit;
            $this->_output['uri'] = urlencode($uri);
            $this->_output['sort_by'] = isset($this->_input['sort_by']) ? $this->_input['sort_by'] : "id_seq";
//            $this->_output['sort_order'] = "DESC";
            $this->_output['sort_order'] = "ASC";
            $this->_output['ajax'] = 1;
                
        if (($this->_input['insEditDel'] == 1) || ($this->_input['psearch'] == 1)) {
            $this->blObj->page_listing($this, 'admin/student/studentlisting');
        }
        else {
            $this->blObj->page_listing($this, 'admin/student/studentsearch');
        }
}

function _studentregister(){
    $id=$this->_input['id'];//pr($id);exit;
        if($id){
            $res=recordExists('practice_student','id='.$id);//pr($res);exit;
            $this->_output['searchval']=$this->_input['searchval'];
            $this->_output['qstart']=$this->_input['qstart'];
            $this->_output['insEditDel'] = 1;
            $this->_output['res']=$res;
        }
    $this->_output['tpl']="admin/student/studentregister";
}

function _insertstudent(){//pr($this->_input);pr($_FILES);exit;
        $student = $this->_input['student'];// pr($student[id]);exit;
        $hobbies = array_sum($this->_input['hobbies']);//pr($hobbies);exit;
        $student['hobbies']=$hobbies;
        $img=$_FILES['prof_image']['name'][0]?$_FILES['prof_image']['name'][0]:$this->_input[prev_image];//pr($img);exit;
        $student['profile_img']=$img;
        if ($student[id]){
            $val = $this->dataObj->updateArrayFormat($student, "id=" . $student[id], "practice_student");
        }else{
            $val = $this->dataObj->insertArrayFormat($student, "", "practice_student");//pr($val);exit;
        }
        $val=$student[id]?$student[id]:$val;
            if($_FILES['prof_image']['name'][0]){
                unlink(APP_ROOT.'image/practice/orig/'.$val."_".$this->_input['prev_image']);  
                unlink(APP_ROOT.'image/practice/thumb/'.$val."_".$this->_input['prev_image']);  
                $file=$_FILES['prof_image']['tmp_name'][0];
                $dest_file=$_FILES['prof_image']['name'][0]?$_FILES['prof_image']['name'][0]:$this->_input[prev_image];
                copy($file,APP_ROOT.'image/practice/orig/'.$val."_".$dest_file); 
                $new_thumb_width = $GLOBALS['conf']['IMAGE']['thumb_width'];//pr($new_thumb_width);exit;
                $new_thumb_height = $GLOBALS['conf']['IMAGE']['thumb_height'];
                $thumbnailDirNew = APP_ROOT . "image/practice/thumb/"; 
                $thumbObj = new thumbnail_manager($file, $thumbnailDirNew . $val . "_" . $dest_file); //pr($previewImg);pr($thumbnailDirNew . $val."_".$_FILES['profile_image']['name'][0]);//exit;
                $thumbObj->get_container_thumb($new_thumb_width, $new_thumb_height, 0, 0);
                @unlink($previewOrigDir . $file);
                @unlink($previewThumbDir . $file);
            }
            $this->_input['page']='student';
            $this->_input['choice']='studentlisting';
            $this->_input['insEditDel'] = 1;
            $this->_studentlisting();
}

function _deletestudent(){
        $id=$this->_input['id'];
            $this->dataObj->delete('id='.$id,"practice_student");
            unlink(APP_ROOT.'image/practice/orig/'.$id."_".$this->_input['img_nm']);  
            unlink(APP_ROOT.'image/practice/thumb/'.$id."_".$this->_input['img_nm']);
            $this->_input['page']='student';
            $this->_input['choice']='studentlisting';
            $this->_input['insEditDel'] = 1;
            $this->_studentlisting();
}

function _studentviewdata(){
        $id=$this->_input['id'];
        $sql=get_search_sql('practice_student','id='.$id);
        $data=getrows($sql,$err);
            $this->_output['data']=$data;
            $this->_output['tpl']="admin/student/studentviewdata";
}

function _checknumber(){
    ob_clean();
   $num=$this->_input['num'];
   $id= $this->_input['id'];//pr($id);exit;
   if($id){
    $res=recordExists('practice_student','mobile='.$num.' and id!='.$id);//pr($res);exit;
   }else{
       $res=recordExists('practice_student','mobile='.$num );
   }
    if($res){
        echo 1;exit;
    }else{
        echo 2;exit;
    } 
}

function _checkemail(){
    ob_clean();
   $email="'".$this->_input['email']."'";//pr($email);exit;
   $id= $this->_input['id'];//pr($id);exit;
    if($id){
     $res=recordExists('practice_student','email='.$email.' and id!='.$id);//pr($res);exit;
    }else{
        $res=recordExists('practice_student','email='.$email);
    }
    if($res){
        echo 1;exit;
    }else{
        echo 2;exit;
    } 
}

function _studentidsearch(){
    ob_clean();
    $cond = "firstname LIKE '%" . $this->_input['tag'] . "%'";
    $sql = "SELECT firstname,id FROM " . TABLE_PREFIX . "practice_student WHERE {$cond} ";
    $res = getindexrows($sql, "firstname", "id");
    $this->_output['res']=$res;
    if ($this->_input['json']) {
            foreach ($this->_output['res'] as $key => $val) {
                    $json .= "{\"name\":\"" . $key . "\",\"label\":\"" . ucwords($val) . "\",\"value\":\"$key\"},";
            }
            $json = trim($json, " ,");
            echo "[" . $json . "]";
            exit;
    }
}

function _uploadimage(){
    pr($this->_input);pr($_FILES);exit;

}
function _uploadcsvdata(){
    $this->_output['tpl']="admin/student/addcsvdata";
    
}
function _addcsvdata(){pr($_FILES ['csvid']['type']);
    if ($_FILES['csvid']['type'] == "text/csv") {
      $handel = fopen($_FILES['csvid']['tmp_name'], "r");
       while (($data = fgetcsv($handel, 500, ",")) !== FALSE) {
           $array1 = array("firstname", "lastname", "mobile", "email", "gender", "hobbies", "address");
           $array2 = array_combine($array1, $data);
         $val = $this->dataObj->insertArrayFormat($array2, '','', "practice_student");
       }
       fclose($handel);
    }else {
         $_SESSION['raise_message']['global'] ='invalid file type';
    }
            
            redirect(LBL_ADMIN_SITE_URL . "student/studentlisting");
}
function _downloadcsvdata(){
     ob_clean();
               global $link;
               $sql = "SELECT * FROM " .TABLE_PREFIX."practice_student WHERE 1";
               $res=getrows($sql);
               $f = fopen("php://memory", "w");
               $filename="studentlist.csv";
                foreach ($res as $key=>$value) {
                    foreach($value as $clk=>$clv){
                        $data .= $clv.",";
                    }
                    }
                 fputcsv($f, explode(',',$data));
                 fseek($f, 0);
                    // tell the browser it's going to be a csv file
                    header('Content-Type: application/csv');
                    // tell the browser we want to save it instead of displaying it
                    header('Content-Disposition: attachment; filename="'.$filename.'";');
                    // make php send the generated csv lines to the browser
                    fpassthru($f);
                    exit;
}

function _downloadpdf(){
////    ob_clean();
require_once APP_ROOT.'dompdf_0.7.1/autoload.inc.php';
//
$url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/student/studentlisting";
$lines_string=file_get_contents($url);
//
//
 

$file_content="jadgvsfkj";
$dompdf = new Dompdf();
$tpl="admin/student/studentlisting";
$content = $this->smarty->add_theme_to_template($tpl);
$html=$this->smarty->fetch($content);

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();
$pdfoutput = $dompdf->output();
header('Content-type: application/pdf');
print $pdfoutput;
exit(0);


// Output the generated PDF to Browser
$dompdf->stream();
}
         
}     
<?php

class admin_csvuser extends csvuser_manager {

	/**
	 * This is a constructor to initialized product admin module.This overrides user side constructor.
	 * 
	 * @param object $smarty Reference of smarty object
	 * 
	 * @param Array $_output Output query string 
	 * 
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input) {
		if ($_SESSION['id_admin']) {
			parent::__construct($smarty, $_output, $_input);
		} else {
			redirect(LBL_ADMIN_SITE_URL);
		}
	}
          
        function _voterlist(){
           $election=recordExists("election","id_election={$_SESSION['id_election']}");
           $post_map=  getindexrows(get_search_sql("postmapping","id_election={$_SESSION['id_election']}"),"id_post");
           $post_arr=  getindexrows(get_search_sql("post"),"id_post");
           if($election['type']==1){
             $arr[]=$election['name'];
             $this->_output['elname']=$arr;
             $this->_output['elname']=$arr;
             $this->_output['all']=1;
           }else{
               $this->_output['elname']=$post_map; 
           }
            $this->_output['post']=$post_arr;
           $allelection=getrows(get_search_sql("election"));
           foreach ($allelection as $k=>$v) {
               if($v['type']==2){
               $arr[$v['id_election']]=getrows(get_search_sql("postmapping","id_election={$v['id_election']} group by id_post"));
               }
           }
           $this->_output['elctions']=$allelection;
           $this->_output['postwise']=$arr;
          $this->_output['tpl'] = 'admin/csvuser/voterlist';  
        }
		function _listing(){
                $uri = PAGINATE_ADMIN_PATH . "csvuser/listing/psearch/1/searchval/".$this->_input['searchval'];
                parse_str($this->_input['searchval'],$data);
                $cond="1";
                if($data['name']){
                    if($data['name'] == 'any'){
                        $cond="1";
                    }else{
                        $cond.=" AND name LIKE '%{$data['name']}%'";  
                    }
                }
                $countsql="SELECT COUNT(*)as cnt,id_csv FROM ".TABLE_PREFIX."mapcsv where 1 GROUP BY id_csv";
		$countarr=  getindexrows($countsql, "id_csv","cnt");
                $this->_output['sql'] = get_search_sql("csvdata",$cond);//pr($sql);
                $this->_output['count'] = $countarr;
                $qstart = isset($this->_input['qstart']) ? $this->_input['qstart'] : 0;
                $this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
                $this->_output['uri'] = $uri;
		$this->_output['ajax'] = '1';
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = "id";
		$this->_output['sort_order'] = "DESC";
		$this->_output['ajax'] = 1;
                if($this->_input['psearch']==1 || $this->_input['inserteditdel']==1 || $this->_input['sort_by']){
		$this->blObj->page_listing($this, 'admin/csvuser/csvdata_list');
                }else{
		$this->blObj->page_listing($this, 'admin/csvuser/csvdata_search');
                }
		}
                
        function _showcsvocrnce(){
                $uri = PAGINATE_ADMIN_PATH . "csvuser/showdata/id/".$this->_input['id'];
                $res=  getindexrows(get_search_sql("mapcsv", "id_csv={$this->_input['id']}"),"id","id_election");
                $electionid=  implode(",", $res);//pr($postid);exit;
		$elections=get_search_sql("election","id_election IN ({$electionid})");
                $this->_output['sql'] = $elections;
                $qstart = isset($this->_input['qstart']) ? $this->_input['qstart'] : 0;
                $this->_output['limit'] =05;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
		$this->_output['qstart'] = $this->_input['qstart'];
                $this->_output['uri'] = urlencode($uri);
                $this->_output['today'] = date("Y-m-d H:i:s");
		$this->_output['ajax'] = '1';
                $this->_output['sort_by'] = "id_election";
		$this->_output['sort_order'] = "DESC";
                $this->blObj->page_listing($this, 'admin/csvuser/showcsv_menu');
            }
                
        function _userdata_list(){
                $uri = PAGINATE_ADMIN_PATH . "csvuser/userdata_list/csvtbllist/{$this->_input['voterslist']}/psearch/1/searchval/".$this->_input['searchval'];
                parse_str($this->_input['searchval'],$data);
                $cond="1";
                foreach($data as $key=>$val){
                     $k=str_replace('_', ' ', $key);
                 if($val){
                  $cond.=" AND `{$k}` LIKE '%{$val}%'";  
                }
               }
                $namedata = recordExists("csvdata", "name = '{$this->_input['voterslist']}'");
		$tblname="csvtbl_{$namedata['id']}";
                $rescontent= recordExists("csvcontent", "csvid={$namedata['id']}");
		$rescontentdata=  json_decode($rescontent['content'],true);
                $this->_output['sql'] = get_search_sql($tblname,$cond);
                $qstart = isset($this->_input['qstart']) ? $this->_input['qstart'] : 0;
				$clmnarr=recordExists($tblname, '1');
				unset($clmnarr['id_csvtbl']);
				unset($clmnarr['csv_flg']);
				unset($clmnarr['login_data']);
				unset($clmnarr['login_pass']);
				$coulmnarr=array();
				foreach($clmnarr as $clk=>$clv){
					array_push($coulmnarr, $clk);
				}
                $this->_output['loginval'] =  $rescontentdata['loginval'];
                $this->_output['namedata'] =  $namedata;
                $this->_output['passval'] =  $rescontentdata['passval'];                
                $this->_output['clmnm'] =  $coulmnarr;
                $this->_output['limit'] =10;//$GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] =3;// $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
                $this->_output['uri'] = $uri;
		$this->_output['ajax'] = '1';
                $this->_output['id_csvtbl'] = $namedata['id'];
		$this->_output['qstart'] = $this->_input['qstart'];
		$this->_output['uri'] = urlencode($uri);
		$this->_output['sort_by'] = "id_csvtbl";
		$this->_output['sort_order'] = "DESC";
                $this->_output['voterlist'] = $this->_input['voterslist'];
		$this->_output['ajax'] = 1;
                if($this->_input['psearch']==1 || $this->_input['inserteditdel']==1 || $this->_input['sort_by']){
		$this->blObj->page_listing($this, 'admin/csvuser/userdata_list');
                }else{
		$this->blObj->page_listing($this, 'admin/csvuser/userdata_search');
                }
          }
          
          function _adduser(){
         $rescontent= recordExists("csvcontent", "csvid={$this->_input['id']}");
            $rescontentdata=  json_decode($rescontent['content'],true);
            $coulmnarr=array();
            foreach($rescontentdata['columnnm'] as $clk=>$clv){
                    if($rescontentdata['save'][$clk]==1){
                    array_push($coulmnarr, $clv);
                    }
            }
            $this->_output['id'] = $this->_input['id']; 
            $this->_output['name'] = $this->_input['name']; 
            $this->_output['loginval'] =$rescontentdata['loginval']; 
            $this->_output['passval'] = $rescontentdata['passval']; 
            $this->_output['coulmnarr'] = $coulmnarr; 
            $this->_output['tpl'] = 'admin/csvuser/adduser';

	}
        
        function _insertuser(){
                 ob_clean();
		$user=$this->_input['user'];
		$user['login_data']=$user[$this->_input['loginval']];
		$user['login_pass']=$user[$this->_input['passval']];
		$tblid = $this->dataObj->insertArrayFormat($user,"","","csvtbl_{$this->_input['id']}");
		redirect(LBL_ADMIN_SITE_URL . "csvuser/userdata_list/voterslist/{$this->_input['csvname']}");
	}
        
	function _userdata() {
		if($this->_input['id']){
			$res=  recordExists("csvdata", "id={$this->_input['id']}");
		        $this->_output['data'] =$res;
		}
                $this->_output['code'] = $this->_input['code'];
		$this->_output['tpl'] = 'admin/csvuser/userdata';
	}
        
         function _deleteusers(){
              $namedata = recordExists("csvdata", "name = '{$this->_input['voterslist']}'");
              $delVal = $this->dataObj->delete("id_csvtbl IN ({$this->_input['id']})" ,"csvtbl_{$namedata['id']}"); 
              $this->_input['inserteditdel'] = 1;
              $this->_output['searchval']=$this->_input['searchval'];
              $this->_output['qstart']=$this->_input['qstart'];
              $this->_input['choice']='userdata_list';
              $this->_userdata_list();
        }
        
        function _deletecsv(){
              $delVal = $this->dataObj->delete("id = " .$this->_input['id'],"csvdata");
              $delVal1 = $this->dataObj->delete("csvid = " .$this->_input['id'],"csvcontent");
              $dropvotertbl="DROP TABLE ".TABLE_PREFIX."csvtbl_{$this->_input['id']}";
              execute($dropvotertbl);
              $this->_input['inserteditdel'] = 1;
              $this->_output['searchval']=$this->_input['searchval'];
              $this->_output['qstart']=$this->_input['qstart'];
              $this->_input['choice']='listing';
              $this->_listing();
        }
                	function _uploadcsvfile() {//pr($this->_input);exit;
		$matcharr = $GLOBALS['conf']['CSV_USER_DATA'];
		foreach ($matcharr as $mkey => $mval) {
			$matcharr[$mkey] = explode(",", $mval);
		}
		$postid = $this->_input['post'] ? '_' . $this->_input['post'] : '';
		if ($_FILES['csvfile']['type'] = "text/csv") {
			$filenm=$_FILES['csvfile']['name'];
			copy($_FILES['csvfile']['tmp_name'],APP_ROOT.'image/csv/'.$filenm);
			$handel = fopen($_FILES['csvfile']['tmp_name'], "r");
			$flg = 1;
			while (($data = fgetcsv($handel, 1000, ",")) !== FALSE) {
				if ($flg == 1) {
					$columnarr = array();
					$columnarrval = array();
					foreach ($data as $key => $val) {
						$setflg = 0;
						foreach ($matcharr as $k => $v) {
							if($setflg == 0){
							if (in_array(strtolower($val), $v)) {
								$setflg = 1;
								$clmval = $k;
							} else {
								$clmval = $val;
							}
							}
						}
						$clarra[0]=$clmval;
						$clarra[1]=$setflg;
						array_push($columnarr, $clarra);
					}
					$flg = 0;
				} else {
					foreach($data as $k=>$v){
						$arr[$columnarr[$k][0]][]=$v;
					}
				}
			}
			fclose($handel);
		}
		$this->_output['filename'] =$filenm;
		$this->_output['id_post'] =$this->_input['post'];
		$this->_output['status'] =$this->_input['status'];
		$this->_output['csvname'] =$this->_input['name'];
		$this->_output['data'] =$arr;
		$this->_output['code'] =$this->_input['user']['code'];
		$this->_output['tblid'] =$this->_input['tblid'];
		$this->_output['coulmndata'] = $columnarr;
		$this->_output['tpl'] = 'admin/csvuser/managecsv';
	}
        
        function _search_csv(){
            $csvnamesql = get_search_sql("csvdata","1 group by name order by name asc","name");
            $namesql = getrows($csvnamesql);
            $str="<option value=''>Choose One</option>";
            $str.="<option value='any'>Any</option>";
            foreach ($namesql as $key => $value) {
            $str.= "<option value=" ."'". $value['name'] ."'". ">" . $value['name'] . "</option>";
            } 
            echo $str;
            exit;
        }
        
        function _checkuniquename(){
            ob_clean();
                if($this->_input['id']){
                   $res=  recordExists('csvdata',"name='{$this->_input['value']}' AND id!='{$this->_input['id']}'");
                   }else{
                    $res=recordExists("csvdata","name='{$this->_input['value']}'");
                   }
                    if(empty($res)){
                            echo 1;exit;
                    }else{
                            echo 2;exit;
                    }
        }
//	function _uploadcsvfile() {//pr($this->_input);exit;
//		ob_clean();
//		$matcharr = $GLOBALS['conf']['CSV_USER_DATA'];
//		foreach ($matcharr as $mkey => $mval) {
//			$matcharr[$mkey] = explode(",", $mval);
//		}
//		$postid = $this->_input['post'] ? '_' . $this->_input['post'] : '';
//		if ($_FILES['csvfile']['type'] = "text/csv") {
//			if($this->_input['status']==1){
//			$drpsql = "DROP TABLE IF EXISTS " . TABLE_PREFIX . "csvtbl_{$_SESSION['id_election']}{$postid}";
//			execute($drpsql);
//			$drpsqlvt = "DROP TABLE IF EXISTS " . TABLE_PREFIX . "votings_{$_SESSION['id_election']}{$postid}";
//			execute($drpsqlvt);
//			}
//			$handel = fopen($_FILES['csvfile']['tmp_name'], "r");
//			$flg = 1;
//			$str='';
//			while (($data = fgetcsv($handel, 1000, ",")) !== FALSE) {
//				if ($flg == 1) {
//					if($this->_input['status']!=2){
//					$columnarr = array();
//					foreach ($data as $key => $val) {
//						$setflg = 0;
//						foreach ($matcharr as $k => $v) {
//							if($setflg == 0){
//							if (in_array(strtolower($val), $v)) {
//								$setflg = 1;
//								$clmval = $k;
//							} else {
//								$clmval = $val;
//							}
//							}
//						}
//						if ($setflg == 1) {
//							array_push($columnarr, $clmval);
//						} else {
//							$clmval = generateurlcode($clmval);
//							array_push($columnarr, $clmval);
//						}
//					}
//					$createsql = "CREATE TABLE " . TABLE_PREFIX . "csvtbl_{$_SESSION['id_election']}{$postid} (id_csvtbl INT(10) PRIMARY KEY AUTO_INCREMENT";
//					foreach ($columnarr as $ck => $cv) {
//						$createsql.=",{$cv} VARCHAR(255)";
//					}
//					$createsql.=",voted_flg INT(10));";
//					execute($createsql, $err);
//					$sqlunic="ALTER TABLE " . TABLE_PREFIX . "csvtbl_{$_SESSION['id_election']}{$postid} ADD UNIQUE (username)";
//					execute($sqlunic);
//					$create_sql_vote = "CREATE TABLE " . TABLE_PREFIX . "votings_{$_SESSION['id_election']}{$postid} LIKE " . TABLE_PREFIX . "votings";
//					execute($create_sql_vote, $err);
//					}
//					$flg = 0;
//				} else {
////					$arr1 = array_combine($columnarr, $data);
//					$str.="(''";
//					foreach($data as $k=>$v){
//						$str.=",'{$v}'";
//					}
//					$str.=",''),";
//				}
//			}
//			fclose($handel);
//		}
//		$str=  trim($str, ",");
//		$insertsql="REPLACE INTO ".TABLE_PREFIX."csvtbl_{$_SESSION['id_election']}{$postid} VALUES{$str}";
//		execute($insertsql, $err);
//		if($postid==''){
//			$upsql="UPDATE ".TABLE_PREFIX."election SET voter_flg=1,csv_name='{$this->_input['name']}' WHERE id_election={$_SESSION['id_election']}";
//		}else{
//			$upsql="UPDATE ".TABLE_PREFIX."postmapping SET voter_flg=1,csv_name='{$this->_input['name']}' WHERE id_election={$_SESSION['id_election']} AND id_post={$this->_input['post']}";
//		}
//		execute($upsql);
//		$_SESSION['raise_message']['global'] = 'CSV File Uploaded Successfully';
//		redirect(LBL_ADMIN_SITE_URL . "csvuser/userdata_list");
//	}
        
        function _downlodecsv(){
                ob_clean();
                global $link;
                $sql = "SELECT * FROM " .TABLE_PREFIX. "sample WHERE 1";//pr($sql);exit;
                $data = mysqli_query($link, $sql);//pr($data);exit;
                $f = fopen("php://memory", "w");
                $del=",";
                $filename="sample.csv";

                foreach ($data as $line) {
                fputcsv($f,$line,$del);//pr($data);exit;
                }
                fseek($f, 0);
                // tell the browser it's going to be a csv file
                header('Content-Type: application/csv');
                // tell the browser we want to save it instead of displaying it
                header('Content-Disposition: attachment; filename="'.$filename.'";');
                // make php send the generated csv lines to the browser
                fpassthru($f);
        }
        
        function _samplecsvfile(){
              ob_clean();
              $smarty = getSmarty();
              $recordlist = 'First name,Last name,Username,Password';
              $valuelist = 'Sourav,Kumar,sorab,111111';
              header("Content-Disposition: attachment; filename = sample.csv");
              header("Content-Type:application/csv");
              print $recordlist . "\n";
              print $valuelist . "\n";
          }
		  
          function _downlodefile(){//pr($this->_input);exit;
               ob_clean();
               global $link;
               $rescontent= recordExists("csvcontent", "csvid={$this->_input['id']}");
	       $rescontentdata=  json_decode($rescontent['content'],true);
               $resdt = recordExists("csvdata", "id = '{$this->_input['id']}'");
               $sql = "SELECT * FROM " .TABLE_PREFIX."csvtbl_{$this->_input['id']} WHERE 1";
//               $data = mysqli_query($link, $sql);
//               $row=mysqli_fetch_assoc($data);
               $res=getrows($sql);
               $f = fopen("php://memory", "w");
               $filename="sample_{$resdt[name]}.csv";
               $coulmnarr=array();
               foreach($rescontentdata['columnnm'] as $clk=>$clv){
		     if($rescontentdata['save'][$clk]==1){
                        array_push($coulmnarr,$clv);
                     }
                }
                $column = implode(",", $coulmnarr);
//                $line .= $column."\n";
                fputcsv($f, explode(',', $column));
               
                foreach ($res as $clk=>$clv) {
                    $data='';
                    foreach($coulmnarr as $ck=>$cv){
                        $data .= $clv[$cv].",";
                    }
                 fputcsv($f, explode(',', $data));
                }//exit;
//                fputcsv($f,$line,$del);
                 fseek($f, 0);
                    // tell the browser it's going to be a csv file
                    header('Content-Type: application/csv');
                    // tell the browser we want to save it instead of displaying it
                    header('Content-Disposition: attachment; filename="'.$filename.'";');
                    // make php send the generated csv lines to the browser
                    fpassthru($f);
                    exit;
                }
          
		  
		  function _insertcsvdata() {
			  $csvdata=$this->_input['csv'];
			  if($this->_input['tblid']){
				  $tblid=$this->_input['tblid'];
				  $this->dataObj->updateArrayFormat($csvdata, "id=" . $this->_input['id'], "");
			  }else{
				  $dt=date("Y-m-d H:i:s");
				  $tblid = $this->dataObj->insertArrayFormat($csvdata,"add_time,ip","'{$dt}','{$_SERVER['REMOTE_ADDR']}'","csvdata");
                                  $csvcontent=  json_encode($this->_input);
				  $cntsql="INSERT INTO ".TABLE_PREFIX."csvcontent VALUES('',{$tblid},'{$csvcontent}')";
				  execute($cntsql, $err);
			  }
		if ($this->_input['status'] == 1){
			$drpsql = "DROP TABLE IF EXISTS " . TABLE_PREFIX . "csvtbl_{$tblid}";
			execute($drpsql);
		}
		if ($this->_input['status'] != 2) {
			$createsql = "CREATE TABLE " . TABLE_PREFIX . "csvtbl_{$tblid} (id_csvtbl INT(10) PRIMARY KEY AUTO_INCREMENT,login_data VARCHAR(255),login_pass VARCHAR(255)";
			foreach ($this->_input['columnnm'] as $ck => $cv) {
				$size = $this->_input['datatyp'][$ck] == 'VARCHAR' ? '(255)' : '';
				$uniq='';
				if($this->_input['loginval'] == $this->_input['oldclmnm'][$ck]){
					$uniq='UNIQUE';
					$unickyval=$ck;
				}
				if($this->_input['passval'] == $this->_input['oldclmnm'][$ck]){
					$passval=$ck;
				}
				if ($this->_input['save'][$ck] == 1)
					$createsql.=",`{$cv}` {$this->_input['datatyp'][$ck]}{$size} {$uniq}";
			}
			$createsql.=",csv_flg INT(10));";
			execute($createsql);
		}
		$arr = $this->_input['save'];
		foreach ($arr as $sk => $sv) {
			if ($sv == 1)
				unset($arr[$sk]);
		}
		$handel = fopen(APP_ROOT . 'image/csv/' . $this->_input['filename'], "r");
		$flg = 1;
		$str = '';
		while (($data = fgetcsv($handel, 1000, ",")) !== FALSE) {
			if ($flg == 1) {
				$flg = 0;
			} else {
				foreach ($arr as $sk => $sv) {
					unset($data[$sk]);
				}
				$str.="('','{$data[$unickyval]}','{$data[$passval]}'";
				foreach ($data as $k => $v) {
					$str.=",'{$v}'";
				}
				$str.=",''),";
			}
		}
		fclose($handel);
		$str = trim($str, ",");
		$insertsql = "REPLACE INTO " . TABLE_PREFIX . "csvtbl_{$tblid} VALUES{$str}";
		execute($insertsql);
                if($this->_input['code']){
//                $this->_output['code'] = $this->_input['code']; 
//		$_SESSION['raise_message']['global'] = 'CSV File Uploaded Successfully';
//		redirect(LBL_ADMIN_SITE_URL . "election/uploadvoter/code/{$this->_input['code']}");
                }else{
		$_SESSION['raise_message']['global'] = 'CSV File Uploaded Successfully';
		redirect(LBL_ADMIN_SITE_URL . "csvuser/listing");
                }
	}

}


<?php
class csvuser_manager extends mod_manager {

    /**
     * This is a constructor to initialized category module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'csvuser', $modTable = '') {
        $this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
        //check_session();
    }

    function get_module_name() {
        return 'csvuser';
    }

    function get_manager_name() {
        return 'csvuser';
    }
};
?>

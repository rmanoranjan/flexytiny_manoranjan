<?php

class admin_module extends module_manager {

  /**
   *
   * This is a constructor to initialized module admin module.This overrides user side constructor.
   * @param object $smarty Reference of smarty object
   * @param Array $_output Output query string
   * @param Array $_input Input query string
   */
  public function __construct(& $smarty, & $_output, & $_input) {
	if ($_SESSION['id_admin']) {
	  parent::__construct($smarty, $_output, $_input);
	} else {
	  redirect(LBL_ADMIN_SITE_URL);
	}
  }

  ################################ module management ###############################

  function _formModule() {
	if ($handle = opendir(APP_ROOT . "flexymvc/modules")) {
	  while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != ".." && $file != "user" && $file != ".svn" && $file != "module") {
		  $arr[$file] = $file;
		}
	  }
	  closedir($handle);
	}
	if ($handle = opendir(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "common")) {
	  while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != ".." && $file != "user") {
		  $arr_tpl[$file] = $file;
		}
	  }
	  closedir($handle);
	}
	$this->_output['file'] = $arr;
	$this->_output['user_templates'] = $this->_traverseHierarchy(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "common");
	$admin_templates = glob(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/*.*");
	foreach ($admin_templates as $key => $value) {
	  $admin_templates[$key] = str_replace(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/", "", $value);
	}
	$this->_output['admin_templates'] = $admin_templates;
	$this->_output['tpl'] = 'admin/module/form_module';
  }

  function _chkModule() {
	if (file_exists(APP_ROOT . "flexymvc/modules/" . $this->_input['module_name'])) {
	  print 1;
	} else {
	  print 0;
	}
  }

  function _createModule() {
	if (file_exists(APP_ROOT . "flexymvc/modules/" . $this->_input['module_name'])) {
	  $_SESSION['raise_message']['global'] = getmessage('MODULE_EXIST'); //"This module already exists";
	  redirect(LBL_ADMIN_SITE_URL . 'module/formModule');
	} else {
	  $mod_name = $this->_input['module_name'];
	}

	############ creating module_manager ##########################
	$dir = APP_ROOT . "flexymvc/modules/" . $mod_name;
	$file = APP_ROOT . "flexymvc/modules/" . $mod_name . "/" . $mod_name . "_manager.php";
	$txt = "manager.txt";
	$this->create($dir, $file, $txt);
	chmod($file, 0777);

	############ creating admin manager ##########################
	$dir = APP_ROOT . "flexymvc/modules/" . $this->_input['module_name'] . '/admin';
	$file = APP_ROOT . "flexymvc/modules/" . $this->_input['module_name'] . "/admin/" . 'admin_' . $this->_input['module_name'] . ".php";
	$txt = "admin_manager.txt";
	$this->create($dir, $file, $txt);
	chmod($file, 0777);

	################## creating templates###################

	$dir = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "/" . $this->_input['module_name'];
	$file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "/" . $this->_input['module_name'] . "/home.tpl.html";
	$txt = "home.txt";
	$this->create($dir, $file, $txt, 'tpl');
	chmod($file, 0777);

	$dir = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "/admin/" . $this->_input['module_name'];
	$file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "/admin/" . $this->_input['module_name'] . "/home.tpl.html";
	$txt = "home.txt";
	$this->create($dir, $file, $txt, 'tpl');
	chmod($file, 0777);

	$_SESSION['raise_message']['global'] = $this->_input['module_name'] . " " . getmessage('MODULE_CREATE');

	redirect(LBL_ADMIN_SITE_URL . 'module/formModule');
  }

  function create($dirpath, $filepath, $txt, $type = '') {
	if ($dirpath) {
	  mkdir("$dirpath", 0777) or die("Permission denied");
	}
	$fp = @fopen($filepath, 'w');
	$data = file_get_contents(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/module/module_txt/" . $txt);
	$fdata = str_replace('[module]', $this->_input['module_name'], $data);
	if (!$type) {
	  $fdata = "<?php\n" . $fdata;
	}
	fwrite($fp, $fdata);
	fclose($fp);
  }

  function _details() {
	$data = $this->_input;
	if ($data['type'] == "admin") {
	  $file = APP_ROOT . "flexymvc/modules/" . $data['mod_name'] . "/admin/" . $data['fname'];
	} elseif ($data['type'] == "mgr") {
	  $file = APP_ROOT . "flexymvc/modules/" . $data['mod_name'] . "/" . $data['fname'];
	} elseif ($data['type'] == "tpl") {
	  if ($data['temp_type'] == "admin") {
		$file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/" . $data['mod_name'] . "/" . $data['fname'];
	  } else {
		$file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . $data['mod_name'] . "/" . $data['fname'];
	  }
	}
	if (@fopen($file, 'r') && !is_dir($file)) {
	  $this->_output['file_content'] = htmlspecialchars(file_get_contents($file));
	} else {
	  $this->_output['fmsg'] = "File does not exist";
	}
	$this->_output['file'] = $file;
	$this->_output['mod_name'] = $data['mod_name'];
	$this->_output['tpl'] = 'admin/module/edit_detail';
  }

  function _updateFiles() {
	$arr = explode("\\r\\n", $this->_input['efile']);
	$cntNum = count($arr);
	$i = 0;
	$fp = fopen($this->_input['fpath'], 'w');
	foreach ($arr as $k => $v) {
	  $i++;
	  if ($i == $cntNum)
		fwrite($fp, stripslashes($v));
	  else
		fwrite($fp, stripslashes($v) . "\n");
	}
	fclose($fp);
	redirect(LBL_ADMIN_SITE_URL . 'module/formModule');
  }

  function _delete() {
	$mod_name = $this->_input['mname'];
	$this->_deleteDir(APP_ROOT . "flexymvc/modules/" . $mod_name);
	unlink(APP_ROOT . "flexymvc/classes/data/" . $mod_name . ".php");
	$this->_deleteDir(APP_ROOT . "templates/" . $_SESSION['multi_language'] . $mod_name);
	$this->_deleteDir(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/" . $mod_name);
	$_SESSION['raise_message']['global'] = "$mod_name " . getmessage('MODULE_DELETE'); //module is deleted";
	redirect(LBL_ADMIN_SITE_URL . 'module/formModule');
  }

  function _deleteDir($path) {
	$files = glob("$path/*");
	foreach ($files as $file) {
	  if (is_dir($file) && !is_link($file)) {
		$this->_deleteDir($file);
	  } else {
		unlink($file) or die("llll");
	  }
	}
	rmdir($path);
  }

  function _traverseHierarchy($path) {
	$return_array = array();
	$dir = @opendir($path);
	if ($dir) {
	  while (($file = @readdir($dir)) !== false) {
		if ($file == '.' || $file == '..' || $file == '.svn')
		  continue;
		$fullpath = $path . '/' . $file;
		if (is_dir($fullpath))
		  $return_array = array_merge($return_array, $this->_traverseHierarchy($fullpath));
		else {
		  $ext_file = explode('.', $file);
		  if ($ext_file[count($ext_file) - 1] != 'LCK') {
			$art = explode('templates/' . $_SESSION['multi_language'], $fullpath);
			$ar = explode('/', $art[1]);
			$return_array[] = ($ar[0] == 'admin') ? $ar[2] : $ar[1];
		  }
		}
	  }
	}
	return $return_array;
  }

  function _getFiles() {
	$mod_name = $this->_input['mod_name'];
	$files['admin'] = "admin_" . $mod_name . ".php";
	$files['mgr'] = $mod_name . "_manager.php";
	$templates['admin'] = $this->_traverseHierarchy(APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/" . $mod_name);
	$templates['genral'] = $this->_traverseHierarchy(APP_ROOT . "templates/" . $_SESSION['multi_language'] . $mod_name);
	$this->_output['files'] = $files;
	$this->_output['templates'] = $templates;
	$this->_output['mod_name'] = $mod_name;
	$this->_output['tpl'] = 'admin/module/files';
  }

//Added By Parwesh And Tanmaya For Import Module Functionality(Not Ready)
  #################################################
  ################ IMPORTING MODULES ##############
  #################################################
  function _importmodule() {
	$folder = $_FILES;
	$foldername = $folder['imp_file']['name'] ? $folder['imp_file']['name'] : '';
	$path = $this->_input['imp_path'] ? $this->_input['imp_path'] : '';

	//Check For Import Module Exist Or Not
	if ($this->check_module_exist($foldername, $path)) {
	  $_SESSION['raise_message']['global'] = getmessage('MODULE_EXISTS'); //"This module is already exist in this project folder.";
	  redirect(LBL_ADMIN_SITE_URL . 'module/form_module/status-imp');
	}

	if ($foldername != '' || $path != '') {
	  $dir = $foldername ? str_replace('.zip', '', $foldername, $c) : str_replace('.zip', '', substr(strrchr($path, '/'), 1), $c);
	  $dest = APP_ROOT;
	  $out = $dest . 'abcd' . $dir . '/';
	  if ($c > 0) {
		$pathtosave = $dest . $dir . '.zip';
		if ($foldername) {
		  @copy($folder['imp_file']['tmp_name'], $pathtosave);
		} else {
		  $fp = fopen($pathtosave, 'w+');
		  $ch = curl_init($path);
		  curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		  curl_setopt($ch, CURLOPT_FILE, $fp);
		  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		  curl_exec($ch);
		  curl_close($ch);
		  fclose($fp);
		}
		passthru("unzip -ou $pathtosave -d $dest");

		@rename($dest . $dir, $dest . 'abcd' . $dir);
		$con = @rename($dest . 'abcd' . $dir . '/' . $dir, $dest . 'abcd' . $dir . '/modulecontent');
		unlink($pathtosave);
	  } else {
		$_SESSION['raise_message']['global'] = getmessage('MODULE_REQZP'); //"Please provide only zip file";
		redirect(LBL_ADMIN_SITE_URL . 'module/form_module/status-imp');
	  }
	  if (is_dir($out) && scandir($out)) {
		$this->copy_directory($out, $dir);
		$this->Removedirectory($out);
		$_SESSION['raise_message']['global'] = "$dir " . getmessage('MODULE_SUCC'); //module integrated successfully.";
		redirect(LBL_ADMIN_SITE_URL . 'module/form_module');
	  } else {
		$_SESSION['raise_message']['global'] = "System does not find module,upload with absolute path.";
		redirect(LBL_ADMIN_SITE_URL . 'module/form_module/status-imp');
	  }
	} else {
	  $_SESSION['raise_message']['global'] = getmessage('MODULE_REQ_FIELD'); //"Please give value to one of these fields";
	  redirect(LBL_ADMIN_SITE_URL . 'module/form_module/status-imp');
	}
  }

  #################################################
  ########## Check For Module Already Exist #######
  #################################################

  function check_module_exist($fname = '', $path = '') {
	$zfile = $fname ? substr($fname, 0, -4) : substr(substr(strrchr($path, '/'), 1), 0, -4);
	if (file_exists(APP_ROOT . "flexymvc/modules/" . $zfile)) {
	  return 1;
	} else {
	  return 0;
	}
  }

  #################################################
  ########## Creating dir,subdir,files ############
  #################################################

  function copy_directory($abspath, $root, $filters = array()) {
	$dir = array_diff(scandir($abspath), array_merge(array(".", ".."), $filters));
	foreach ($dir as $d) {
	  //If block only creates folders and subfolders of module whereas else block copy files to respective folders and subfolders.
	  if (is_dir($abspath . $d)) {
		//Create Folder For Manager Class
		$module_fol = strstr($abspath . $d, '/modulecontent');
		if ($module_fol != '') {
		  $module_dir_path = APP_ROOT . 'flexymvc/modules' . str_replace('modulecontent', $root, $module_fol, $c) . '/';
		  @mkdir($module_dir_path, 0755);
		}

		//Create Folder For Templates
		$tmp_fol = strstr($abspath . $d, "/templates");
		if ($tmp_fol != '') {
		  $tmp_dir_path = APP_ROOT . 'templates/default' . str_replace('/templates', '', $tmp_fol) . '/';
		  @mkdir($tmp_dir_path, 0755);
		}
		$this->copy_directory($abspath . $d . '/', $root, $filters);
	  } else {
		//Copy File Of Data Class
		$data_class = strstr($abspath . $d, "data");
		if ($data_class != '') {
		  $data_dir = APP_ROOT . 'flexymvc/classes/' . $data_class;
		  @copy($abspath . $d, $data_dir);
		}

		//Copy File Of Module Class
		$module_class_file = strstr($abspath . $d, 'modulecontent');
		if ($module_class_file != '') {
		  $module_class_dir = APP_ROOT . 'flexymvc/modules/' . str_replace('modulecontent', $root, $module_class_file, $c);
		  @copy($abspath . $d, $module_class_dir);
		}

		//Copy Files Of Templates Class
		$tmp_class = strstr($abspath . $d, "/templates/");
		if ($tmp_class != '') {
		  $tmp_dir_path = APP_ROOT . 'templates/default' . str_replace('/templates', '', $tmp_class);
		  @copy($abspath . $d, $tmp_dir_path);
		}

		//Copy File Of Common Class If Exist Like paypal module paypal_class_ipn.php class
		$common_file = strstr($abspath . $d, "/common");
		if ($common_file != '') {
		  $common_dir_path = APP_ROOT . 'flexymvc/classes' . $common_file;
		  @copy($abspath . $d, $common_dir_path);
		}

		//Run .sql file
		if (strstr($abspath . $d, ".sql") != '') {
		  $this->sql_dump($abspath . $d);
		}
	  }
	}
  }

  #################################################
  ################ DUMPING SQL FILES ##############
  #################################################

  function sql_dump($fname) {
	$handle = @fopen($fname, "r");
	if ($handle) {
	  $buffer = '';
	  while (!feof($handle)) {
		$str = fgets($handle, 4096);
		if (!(substr($str, 0, 2) == '--'))
		  $buffer.= $str;
	  }
	  $buffer = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $buffer);
	  $buffer = str_replace("flexy__", TABLE_PREFIX, $buffer);
	}
	fclose($handle);
	$sql = explode(";", $buffer);
	foreach ($sql as $k => $v)
	  mysql_query($v);
  }

  #################################################
  ########## REMOVING DIRECTORY AND FILES #########
  #################################################

  function Removedirectory($dir) {
	if (is_dir($dir)) {
	  $objects = scandir($dir);
	  foreach ($objects as $object) {
		if ($object != "." && $object != "..") {
		  if (filetype($dir . "/" . $object) == "dir")
			$this->Removedirectory($dir . "/" . $object);
		  else
			unlink($dir . "/" . $object);
		}
	  }
	  reset($objects);
	  rmdir($dir);
	}
  }

//End Of Import Module Functionality
  //start of add Template
  function _addTemplate() {
	$this->_output['mod_name'] = $this->_input['mod_name'];
	$this->_output['type'] = $this->_input['type'];
	$this->_output['tpl'] = "admin/module/addTemplate";
  }

  function _createTemp() {
	$type = ($this->_input['type'] == "gen") ? "default/" : "default/admin/";
	$file_path = APP_ROOT . "templates/" . $type . $this->_input['mod_name'];
	//if module folder does not exist create folder then home.tpl
	if (!is_dir($file_path)) {
	  mkdir($file_path);
	  $home_fileopen = @fopen($file_path . "/home.tpl.html", 'w');
	  fwrite($home_fileopen, str_replace('[module]', $this->_input['mod_name'],file_get_contents(APP_ROOT . "templates/" .$type ."module/module_txt/home.txt")));
	  fclose($home_fileopen);
	  chmod($home_fileopen, 0777);
	}
	$file = $file_path . "/{$this->_input['temp_name']}.tpl.html";
	$fp = @fopen($file, 'w');
	fwrite($fp, $this->_input['content']);
	fclose($fp);
	chmod($file, 0777);
	$_SESSION['raise_message']['global'] = "Template Added Successfully";
	redirect(LBL_ADMIN_SITE_URL . 'module/formModule');
  }

  function _deleteTemplate() {
	$data = $this->_input;
	if ($data['temp_type'] == "admin") {
	  $file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . "admin/" . $data['mod_name'] . "/" . $data['fname'];
	} else {
	  $file = APP_ROOT . "templates/" . $_SESSION['multi_language'] . $data['mod_name'] . "/" . $data['fname'];
	}
	unlink($file);
  }

}

;
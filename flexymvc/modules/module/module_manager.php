<?php

class module_manager extends mod_manager {

    /**
     * This is a constructor to initialized module module
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'module', $modTable = '') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }
    function get_module_name() {
	return 'module';
    }

    function get_manager_name() {
	return 'module';
    }

    function _default() {
	
    }

};
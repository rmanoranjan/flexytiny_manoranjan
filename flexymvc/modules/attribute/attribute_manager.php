<?php

class attribute_manager extends mod_manager {

    /**
     * This is a constructor to initialized product module
     * 
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'attribute', $modTable = 'attribute') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
	//check_session();
    }

    function get_module_name() {
	return 'attribute';
    }

    function get_manager_name() {
	return 'attribute';
    }

    function _default() {
	
    }

    function _listing() {
	print("hello user");
    }

   function courseArrToBeModified($id_attr='', $is_delete=0) {
	global $link;
	//$coltype = getsingleindexrow(get_search_sql("attribute", "id_attribute=$id_attr", "id_attribute,datatype"));
	//$coltyp = (!empty($coltype) && $coltype['datatype']=='2') ? "VARCHAR(255)" : "INT" ;
	$coltype = getsingleindexrow(get_search_sql("attribute", "id_attribute=$id_attr", "id_attribute,datatype,entry_type,search_type"));
	if(!empty($coltype)){
	    if($coltype['datatype']=='2'){
		$coltyp="VARCHAR(255)";
	    }elseif($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget'] || $coltype['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget']){ // for calender widget,date case
		$coltyp="DATE";
	    }elseif(($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['dropdown'] || $coltype['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['dropdown']) && ($coltype['dropdown_values'] == $GLOBALS['conf']['BOARD_TYPE']['board'] || $coltype['dropdown_values'] == $GLOBALS['conf']['BOARD_TYPE']['university'])){
			$coltyp="VARCHAR(255)"; // for board or university : varchar for multiauto : varchar
	    }else{
		$coltyp="INT";
	    }    
	    
	   /* if($coltype['datatype']=='2'){
		$coltyp="VARCHAR(255)";
	    }elseif($coltype['entry_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget'] || $rec['search_type']==$GLOBALS['conf']['ATTRIBUTE_TYPE']['calender_widget']){ // for calender widget,date case
		$coltyp="DATE";
	    }
	    else{
		$coltyp="INT";
	    }*/
	}
	$tbl = "" . TABLE_PREFIX . "attributedata";
//		$tblpricolnm = $value;
		$col = "c$id_attr";
//		$sql = ($is_delete) ? " CALL dropcolfromtable('$tbl','$col')" : " CALL createaltertable('$tbl','$col','$coltyp') ";
		$sql = ($is_delete) ? " ALTER TABLE {$tbl} DROP {$col}" : "ALTER TABLE {$tbl} ADD {$col} {$coltyp}";
		execute($sql,$err);
//		mysqli_query($link,$sql);
//	if (!empty($courses)) {
//	    foreach ($courses as $key => $value) {
//		$tbl = "" . TABLE_PREFIX . "inst__$value";
//		$tblpricolnm = $value;
//		$col = "c$id_attr";
//		$sql = ($is_delete) ? " CALL dropcolfromtable('$tbl','$col')" : " CALL createaltertable('$tbl','$col','$coltyp','$tblpricolnm') "; 
//		mysqli_query($link,$sql);
//	    }
//	}
    }
    
    function _addAttrToTables($id_attr='', $is_delete=0) {
//	$cond = ($idcourses) ? " id_course IN ($idcourses) " : " 1 ";
//	$courses = getindexrows(get_search_sql("course", $cond, "id_course,code"), "id_course", "code");
	if ($id_attr)
	    $this->courseArrToBeModified($id_attr, $is_delete);
//	    $this->courseArrToBeModified($courses, $id_attr, $is_delete);
	$this->dataObj->updateStringFormat("flag", "1", "id_attr=$id_attr", "track");
    }

    function _deleteAttrFromTable($id_attr='') {
	$id_attr = ($id_attr) ? $id_attr : $this->_input['id_attr'];
	if ($id_attr) {
	    $this->dataObj->updateStringFormat("flag", "0", "id_attr=$id_attr", "track");
	    $courses = getsingleindexrow(get_search_sql("attribute", "id_attribute=$id_attr", "id_attribute,id_course"));
//	    $this->_addAttrToTables($id_attr, $courses['id_course'], 1);
	    $this->_addAttrToTables($id_attr, 1);
	}
	$this->dataObj->delete("id_attribute=$id_attr", "attribute");
    }
    function cronToMangeTransaction(){
	global $link;
	$cond = get_search_sql("track", "flag=0", "id_attr");
	$sql = get_search_sql("attribute", " id_attribute IN ($cond) ", "id_attribute,id_course");
	$res = mysqli_query($link,$sql);
	while($rec=mysqli_fetch_assoc($res)){
	    $this->_addAttrToTables($rec['id_attribute'], $rec['id_course'], 1);
	}
    }
    function _reorder() {
		ob_clean();
		$res = explode(",", $this->_input['tbl']);
		$sql = "UPDATE " . TABLE_PREFIX . "product_images SET id_seq = CASE id_image";
		$i = 1;
		foreach ($res as $key => $value) {
			$sql .= " WHEN " . $value . " THEN " . $i++ . " ";
		}
		$sql .= "END WHERE id_image IN (" . $this->_input['tbl'] . ") AND id_product=" . $this->_input['id_product'];
		execute($sql, $err);
		exit;
	}
}

;


<?php

class admin_ipblocking extends ipblocking_manager {

     /**
     * This is a constructor to initialized ipblocking admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
	if ($_SESSION['id_admin']) {
	    parent::__construct($smarty, $_output, $_input);
	}else{
	    redirect(LBL_ADMIN_SITE_URL);
	}
    }
    
    /**
     * @method _ipblocking() take the user to the ipblock template.
     * 
     * @return void
     */
    function _ipblocking() {
	$this->_input['tpl'] = "admin/ipblocking/ipblock";
    }

    /**
     * @method _blockedIpList() Function lists the blocked users.
     * 
     * @return template blockediplist template with prefilled values.
     */
    function _blockedIpList() {
	//$cond = "ipstatus=1";
	$sql = get_search_sql("blockedip");
	$res = getrows($sql, $err);
	$this->_output['blockedip'] = $res;
	$this->_output['tpl'] = 'admin/ipblocking/blockediplist';
    }

    /**
     * @method _deleteblockip() Delete a record from the blockedip table according to the id provided. 
     * 
     * @return string Url to blockedIpList method.
     */
    function _deleteBlockIp() {
	$this->_input['ipid'] = stripslashes(trim($this->_input['ipid'], ','));
	if ($this->_input['ipid']) {
	    $this->dataObj->delete("ip =".$this->_input['ipid'],"blockedip");
	} else {
	    //$this->_output['failmsg'] = getmessage('IPBLOCKING_FAIL');
        $_SESSION['raise_message']['global'] = getmessage('IPBLOCKING_FAIL');
	}
	$_SESSION['raise_message']['global'] = getmessage('IPBLOCKING_DELETE');
	ob_clean();
	print LBL_ADMIN_SITE_URL . "ipblocking/blockedIpList";
	exit;
    }

    /**
     * @method _blockip() Take the user to the blockip template.
     * 
     * @return template blockip template. 
     */
    function _blockip() {
	$this->_output['tpl'] = "admin/ipblocking/blockip";
    }

    /**
     * @method _block_ip_by_admin() Block a user by admin.
     * 
     * @return void Redirect to the blockedIpList.
     */
    function _block_ip_by_admin() {
	$query = get_search_sql('blockedip', "ip='" . $this->_input['ip_addr']."'");
	$data = getrows($query, $err);
	if ($data) {
	    $_SESSION['raise_message']['global'] = getmessage('IPBLOCKING_CHECHK_IP'); //"This ip is already blocked";
	} else {
	    if ($this->_input['ch'] == 'bl_ul') {
		$tim = '+ 9000 days ' . $this->_input['Time_Hour'] . ' hours ' . $this->_input['Time_Minute'] . ' minutes';
	    } else {
		$tim = '+' . $this->_input['days'] . ' days ' . $this->_input['Time_Hour'] . ' hours ' . $this->_input['Time_Minute'] . ' minutes';
	    }
	    if ($this->_input['bl_day']) {
		$date = $this->mysqldateformat($this->_input['bl_day'], $format = 'Y-m-d');
	    } else {
		$date = date('Y-m-d H:i:s', strtotime($tim));
	    }
	    //$sql = "INSERT INTO " . TABLE_PREFIX . "blockedip(id_block,ip,username,reason,time_fail,time_upto,added_by) VALUES('','Block By Admin','" . $this->_input['ip_addr'] . "','" . $this->_input['reason'] . "',0,1,now(),'" . $date . "')";
	    $ex=$this->dataObj->insertStringFormat("username,ip,reason,time_fail,time_upto,added_by","'Admin','" . $this->_input['ip_addr'] . "','" . $this->_input['reason'] . "',now(),'" . $date . "',1","","blockedip");
        //print $sql;exit;
	    $_SESSION['raise_message']['global'] = getmessage('IPBLOCKING_INSERT'); //"IP has blocked successfully";
	}
	redirect(LBL_ADMIN_SITE_URL . "ipblocking/blockedIpList");
    }

    /**
     * @method mysqldateformat() function is used to convert the input date to the format requested.
     * 
     * @param string $dt take the $dt(date) as string to which we intend to convert.
     * 
     * @param string $format It is the format required.
     * 
     * @return date 
     */
    function mysqldateformat($dt, $format='Y-m-d') {
	$dt_arr = split('[-/: ]', $dt);
	$dd = $dt_arr['1'];
	$mm = $dt_arr['0'];
	$yy = $dt_arr['2'];
	$hr = isset($dt_arr['3']) ? $dt_arr['3'] : 0;
	$min = isset($dt_arr['4']) ? $dt_arr['4'] : 0;
	$s = isset($dt_arr['5']) ? $dt_arr['5'] : 0;
	return date($format, mktime($hr, $min, $s, $mm, $dd, $yy));
    }

}

?>

<?php

class admin_socialNetwork extends socialNetwork_manager {

    /**
     * This is a constructor to initialized product admin module.This overrides user side constructor.
     * 
     * @param object $smarty Reference of smarty object
     * 
     * @param Array $_output Output query string 
     * 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input) {
	if ($_SESSION['id_admin']) {
	    parent::__construct($smarty, $_output, $_input);
	} else {
	    redirect(LBL_ADMIN_SITE_URL);
	}
    }

};
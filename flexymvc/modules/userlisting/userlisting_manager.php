<?php

class userlisting_manager extends mod_manager {

    /**
     * This is a constructor to initialized product module
     * 
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'userlisting', $modTable = 'practice') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
	//check_session();
    }

    function get_module_name() {
	return 'userlisting';
    }

    function get_manager_name() {
	return 'userlisting';
    }

    function _default() {
	
    }
    
}
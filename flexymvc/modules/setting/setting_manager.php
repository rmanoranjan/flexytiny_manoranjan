<?php

class setting_manager extends mod_manager {

    /**
     * 
     * This is a constructor to initialized setting module
     * @param object $smarty Reference of smarty object
     * @param Array $_output Output query string 
     * @param Array $_input Input query string
     */
    public function __construct(& $smarty, & $_output, & $_input, $modName = 'setting', $modTable = 'config') {
	$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
    }
    function get_module_name() {
	return 'setting';
    }

    function get_manager_name() {
	return 'setting';
    }
};
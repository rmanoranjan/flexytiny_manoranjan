<?php

class product_manager extends mod_manager {

	/**
	 * This is a constructor to initialized product module
	 *
	 * @param object $smarty Reference of smarty object
	 * @param Array $_output Output query string
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input, $modName = 'product', $modTable = 'product') {
		$this->mod_manager($smarty, $_output, $_input, $modName, $modTable);
		check_session();
	}

	function get_module_name() {
		return 'product';
	}

	function get_manager_name() {
		return 'product';
	}

	function _default() {

	}

	function _rem() {
		$val = $this->_input['d'];
		switch ($val) {
			case 1:
				$dir = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_orig'];
				break;
			case 2:
				$dir = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_thumb'];
				break;
			case 3:
				$dir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_orig'] . 'product/';
				break;
			case 4:
				$dir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_thumb'] . 'product/';
				break;
		}
		//print $dir;exit;
		$dh = opendir($dir);
		while ($file = readdir($dh)) {
			if ($file != "." && $file != "..") {
				@unlink($dir . $file);
			}
		}
	}

########################################## Common Functions For Single And Multiple Images ##########################################
	/**
	 * @method _listing() Listing of single or multiple images
	 *
	 * @return go to admin side or listed user side products
	 */

	function _listing() {//echo 11111;exit;
		$this->_output['field'] = array("name" => array("Name", 1), "code_product" => array("Code", 1));
		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_USER']['rec_per_page'];
		$this->_output['show'] = $GLOBALS['conf']['PAGINATE_USER']['show_page'];
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box
		$this->_output['uri'] = 'product/listing';
		$this->_output['ajax'] = '1';
		$this->_output['img_path'] = APP_ROOT_URL . $GLOBALS['conf']['IMAGE']['image_thumb'] . $this->_input['page'] . "/";
		$this->_output['prod_type'] = $GLOBALS['conf']['PRODUCT']['image_type'];
		if ($GLOBALS['conf']['PRODUCT']['image_type'] == 1) {
			$this->_output['sql'] = get_search_sql("product", "image_type !=2");
			$this->blObj->page_listing($this, 'product/singlelist');
		} else {
			$sql = "SELECT p.*,pi.id_image,pi.image_name,pi.code_product,count(pi.image_name) as total FROM " . TABLE_PREFIX . "product p LEFT JOIN " . TABLE_PREFIX . "product_images pi ON p.id_product=pi.id_product WHERE p.image_type !=1 GROUP BY code_product desc ORDER BY id_product DESC";
			$this->_output['sql'] = $sql;
			//$this->_output['http_referer'] = $_SERVER['HTTP_REFERRER'];
			$this->blObj->page_listing($this, 'product/list');
		}
	}

	/**
	 * @method _addProduct() Add single or multiple image
	 *
	 * @return add product template with form elements
	 */
	function _addProduct() {
		ob_clean();
		$catStatus = $GLOBALS['conf']['CATEGORY']['category_status'];
		$imgType = $GLOBALS['conf']['PRODUCT']['image_type'];

		if ($this->_input['id_product']) {
			$res = recordExists("product", "id_product='" . $this->_input['id_product'] . "'");
			$res['name'] = addslashes($res['name']);
			$res['code'] = addslashes($res['code']);
			$this->_output['res'] = $res;
			if ($imgType == 2) {
				$sql = get_search_sql('product_images', "id_product='" . $this->_input['id_product'] . "'");
				$this->_output['imgs'] = getrows($sql, $err);
			}
			$this->_output['referer'] = $this->_input['uri'];
		}
		$this->_output['qstart'] = $this->_input['qstart'];

		if ($catStatus == 1) {
			$sqlCat = get_search_sql('category', "1 ORDER BY name ASC");
			$resCat = getindexrows($sqlCat, 'id_category', 'name');
			if (count($resCat) == 0) {
				print "no category";
				exit;
			} else {
				$this->_output['category'] = $resCat;
			}

			$this->_output['id_category'] = "";
			if ($this->_input['id_category']) {
				$this->_output['id_category'] = $this->_input['id_category'];
				$this->_output['caterory_reflex'] = 1;
			} else if ($this->_output['res']['id_category'])
				$this->_output['id_category'] = $this->_output['res']['id_category'];
		}
		if ($this->_input['pname'])
			$this->_output['pname'] = $this->_input['pname'];
		if ($this->_input['product_category'])
			$this->_output['product_category'] = $this->_input['product_category'];
		if ($this->_input['cat_flag']) {
			$this->_output['cat_flag'] = $this->_input['cat_flag'];
		}
		$this->_output['active_cat'] = $catStatus;

		if ($_SESSION['id_admin']) {
			$this->_output['tpl'] = ($imgType == 1) ? 'admin/product/addsingle' : 'admin/product/add';
		} else {
			//Create a new template(s) in in product and copy total form elements and javascript from admin side
			$this->_output['tpl'] = ($imgType == 1) ? '' : '';
		}
	}

	/**
	 * @method _previewImage() Preview image when image browse
	 *
	 * @return image
	 */
	function _previewImage() {
		ob_clean();
		//$imgId is array element number to delete session preview image
		$imgId = $this->_input['imgId'] ? $this->_input['imgId'] : 0;
		previewImage($_FILES['image'], $imgId, $GLOBALS['conf']['PRODUCT']['image_type']);
	}

	function _previewImageNew() {
		/* print "<pre>";
		  print_r($this->_input);
		  print_r($_FILES);
		  print "</pre>";exit; */
		$ar['name'][0] = $_FILES['img_fl']['name'];
		$ar['tmp_name'][0] = $_FILES['img_fl']['tmp_name'];
		ob_clean();
		$imgId = $this->_input['imgId'] ? $this->_input['imgId'] : 0;
		previewImage($ar, $imgId, $GLOBALS['conf']['PRODUCT']['image_type']);
	}

	/**
	 * @method _insertProduct() Insert single or multiple product
	 *
	 * @return listing or javascript function
	 */
	function _insertProduct() {            
            echo "<pre>";
            print_r($this->_input);
            print_r($_FILES);exit;
		ob_clean();
		$product = $this->_input['product'];

		$chkCond = "code = '" . $product['code'] . "'";
		if (!empty($product['id_category'])) {
			$chkCond .= " AND id_category = " . $product['id_category'];
		}
		recordExists("product", $chkCond, "code", 0, "no");
		$id_product = $this->dataObj->insertArrayFormat($product, "cr_date,ip", "NOW(),'" . IP . "'");
		$imgType = $GLOBALS['conf']['PRODUCT']['image_type'];

		if ($id_product && $this->_input['prev_img']) {
			if ($imgType == 1) {
				$prevImage = $this->_input['prev_img'];
				uploadImageFromPreview($id_product, $prevImage, "product", "", $new_arr);
				$newFileName = substr($prevImage, (strpos($prevImage, "_") + 1));
				$this->dataObj->updateStringFormat("image", $newFileName, "id_product = $id_product");
			} else {
				$new_arr[0]['width'] = $GLOBALS['conf']['IMAGE']['carousel_width'];
				$new_arr[0]['height'] = $GLOBALS['conf']['IMAGE']['carousel_height'];
				$new_arr[0]['newPath'] = $GLOBALS['conf']['IMAGE']['image_carousel_path'];
				$new_arr[1]['width'] = $GLOBALS['conf']['IMAGE']['thumb_width'];
				$new_arr[1]['height'] = $GLOBALS['conf']['IMAGE']['thumb_height'];
				$new_arr[1]['newPath'] = '';

				$cntImages = count($this->_input['prev_img']);
				for ($i = 0; $i < $cntImages; $i++) {
					if (!empty($this->_input['prev_img'][$i])) {
						$prevImage = $this->_input['prev_img'][$i];
						$imgFileName = substr($prevImage, (strpos($prevImage, "_") + 1));
						$imgArr = array("id_product" => $id_product, "code_product" => $product['code'], "image_name" => $imgFileName);
						$id_image = $this->dataObj->insertArrayFormat($imgArr, "", "", "product_images");
						uploadImageFromPreview($id_image, $prevImage, "product", "", $new_arr);

						@unlink(APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_orig'] . $prevImage);
						@unlink(APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_thumb'] . $prevImage);
					}
				}
			}
		}

		$_SESSION['raise_message']['global'] = $id_product ? getmessage('PRODUCT_INSERT') : getmessage('PRODUCT_INS_FAIL');

		if ($this->_input['caterory_reflex']) {
			print "1";
			exit;
		} else {
			$this->_input['insEditDel'] = 1;
			$this->_input['choice'] = 'listing';
			$this->_listing();
		}
	}

	/**
	 * @method _updateProduct() Update single or multiple product
	 *
	 * @return listing or javascript function
	 */
	function _updateProduct() {
		ob_clean();
		$product = $this->_input['product'];
		$id_product = $this->_input['id_product'];
		$cond = "name = '" . $product['name'] . "' AND id_product != '" . $id_product . "'";
		if (isset($this->_input['product']['id_category']))
			$cond .= " AND id_category =" . $this->_input['product']['id_category'];
		recordExists("product", $cond, "name", 0, "noname");
		$this->dataObj->updateArrayFormat($this->_input['product'], "id_product = '" . $id_product . "'");
		$imgType = $GLOBALS['conf']['PRODUCT']['image_type'];
		if ($imgType == 1) {
			if ($this->_input['prev_img'] != $this->_input['previmage']) {
				$prevImage = $this->_input['prev_img'];
				uploadImageFromPreview($id_product, $prevImage, "product", $this->_input['previmage'], $new_arr);
				$newFileName = substr($prevImage, (strpos($prevImage, "_") + 1));
				$this->dataObj->updateStringFormat("image", $newFileName, "id_product = $id_product");
			}
		} else {
			$new_arr[0]['width'] = $GLOBALS['conf']['IMAGE']['carousel_width'];
			$new_arr[0]['height'] = $GLOBALS['conf']['IMAGE']['carousel_height'];
			$new_arr[0]['newPath'] = $GLOBALS['conf']['IMAGE']['image_carousel_path'];
			$new_arr[1]['width'] = $GLOBALS['conf']['IMAGE']['thumb_width'];
			$new_arr[1]['height'] = $GLOBALS['conf']['IMAGE']['thumb_height'];
			$new_arr[1]['newPath'] = '';
			if (!empty($this->_input['prev_img'][0])) {
				$cntImg = count($this->_input['prev_img']);
				for ($i = 0; $i < $cntImg; $i++) {
					if (!empty($this->_input['prev_img'][$i])) {
						$prevImage = $this->_input['prev_img'][$i];
						$imgFileName = substr($prevImage, (strpos($prevImage, "_") + 1));
						$imgArr = array("id_product" => $id_product, "code_product" => $this->_input['pro_cod'], "image_name" => $imgFileName);
						$id_image = $this->dataObj->insertArrayFormat($imgArr, "", "", "product_images");
						uploadImageFromPreview($id_image, $prevImage, "product", "", $new_arr);
					}
				}
			}
		}

		$_SESSION['raise_message']['global'] = getmessage('PRODUCT_UPDATE');
		$this->_input['insEditDel'] = 1;
		$this->_input['choice'] = 'listing';
		$this->_input['psearch'] = 1;
		$this->_listing();
	}

	/**
	 * @method _deleteProduct() Delete single or multiple product
	 *
	 * @param int $id_category category id specifies in category module
	 *
	 * @return listing function or category module
	 */
	function _deleteProduct($id_category = "") {
		$cond = $id_category != "" ? "id_category = " . $id_category : "id_product = " . $this->_input['id_product'];
		$res = getrows(get_search_sql("product", $cond), $err);
		for ($i = 0; $i < count($res); $i++) {
			if ($res[$i]['image_type'] == 1) {
				$delVal = $this->dataObj->delete("id_product = " . $res[$i]['id_product']);
				if ($res[$i]['image'] && $delVal) {
					$uploadDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_orig'] . "product/";
					$thumbnailDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_thumb'] . "product/";
					@unlink($uploadDir . $res[$i]['id_product'] . "_" . $res[$i]['image']);
					@unlink($thumbnailDir . $res[$i]['id_product'] . "_" . $res[$i]['image']);
					@unlink($thumbnailDir . $GLOBALS['conf']['IMAGE']['image_carousel_path'] . $res[$i]['id_product'] . "_" . $res[$i]['image']);
				}
			} else if ($res[$i]['image_type'] == 2) {
				$imgCond = $id_category != "" ? "id_product = " . $res[$i]['id_product'] : $cond;
				$img = getrows(get_search_sql("product_images", $imgCond), $err);
				$delVal = $this->dataObj->delete($cond);
				if ($delVal && $img) {
					for ($i = 0; $i < count($img); $i++) {
						$uploadDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_orig'] . "product" . "/";
						$thumbnailDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_thumb'] . "product" . "/";
						@unlink($uploadDir . $img[$i]['id_image'] . "_" . $img[$i]['image_name']);
						@unlink($thumbnailDir . $img[$i]['id_image'] . "_" . $img[$i]['image_name']);
						@unlink($thumbnailDir . $GLOBALS['conf']['IMAGE']['image_carousel_path'] . $img[$i]['id_image'] . "_" . $img[$i]['image_name']);
					}
				}
			}
		}
		if ($id_category) {
			return true;
		} else {
			$this->_input['insEditDel'] = 1;
			$this->_input['choice'] = 'listing';
			$this->_listing();
		}
	}

	/**
	 * @method _productDetail() Show details of selected product
	 *
	 * @return Template with particular product data
	 */
	function _productDetail() {
		$this->_output['imgType'] = $GLOBALS['conf']['PRODUCT']['image_type'];
		if ($this->_output['imgType'] == 2)
			$this->_output['imgs'] = getrows(get_search_sql('product_images', "id_product='" . $this->_input['id_product'] . "'") . " ORDER BY id_seq ASC", $err);
		$this->_output['res'] = recordExists("product", "id_product='" . $this->_input['id_product'] . "'");
		$this->_output['tpl'] = $_SESSION['id_admin'] ? 'admin/product/product_detail' : 'product/product_detail';
	}

	/**
	 * @method _checkValue() Check for name or code of product exists or not
	 *
	 * @return message as set to recordExists() function if data found or 0 if not found
	 */
	function _checkValue() {
		ob_clean();
		$cond = $this->_input['keyName'] . " = '" . $this->_input['keyValue'] . "'";
		if ($this->_input['keyName'] == 'name') {
			if ($this->_input['id_category']) {
				$cond .= " AND id_category = " . $this->_input['id_category'];
			}
		}
		if ($this->_input['id_product']) {
			$cond .= " AND id_product != " . $this->_input['id_product'];
		}
		recordExists("product", $cond, $this->_input['keyName'], 0, "exist");
	}

########################################### End Common Functions #######################################
########################################## Only for Multiple Image #####################################
	/**
	 * @method _unlinkPreviewImage() Unlink preview images during insert or update of product
	 *
	 * @return remove preview image
	 */

	function _unlinkPreviewImage() {
		$uploadDir_prev = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_orig'];
		$thumbnailDir_prev = APP_ROOT . $GLOBALS['conf']['IMAGE']['preview_thumb'];
		@unlink($uploadDir_prev . $this->_input['img_name']);
		@unlink($thumbnailDir_prev . $this->_input['img_name']);
		$_SESSION['pv_img'] = '';
	}

	/**
	 * @method _showGallery() Show gallery for multiple images
	 *
	 * @return template with gallary images
	 */
	function _showGallery() {
		$sql = get_search_sql("product_images", "id_product = '" . $this->_input['id_product'] . "'", "id_image,image_name");
		$this->_output['data'] = getindexrows($sql, 'id_image', 'image_name');
		$this->_output['tpl'] = $_SESSION['id_admin'] ? 'admin/product/gallery' : 'product/gallery';
	}

	/**
	 * @method _deleteProductImage() Delete product image during update of product
	 *
	 * @return 1 or 0 as conditio specified
	 */
	function _deleteProductImage() {
		ob_clean();
		$id = $this->dataObj->delete("id_image = '" . $this->_input['id_image'] . "'", "product_images");
		if ($id) {
			$uploadDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_orig'] . $this->_input['page'] . "/";
			$thumbnailDir = APP_ROOT . $GLOBALS['conf']['IMAGE']['image_thumb'] . $this->_input['page'] . "/";

			@unlink($uploadDir . $this->_input['id_image'] . "_" . $this->_input['image_name']);
			@unlink($thumbnailDir . $this->_input['id_image'] . "_" . $this->_input['image_name']);
			@unlink($thumbnailDir . $GLOBALS['conf']['IMAGE']['image_carousel_path'] . $this->_input['id_image'] . "_" . $this->_input['image_name']);
			print 1;
		} else {
			print 0;
		}
		exit;
	}

	function _newTplToCrop() {
		foreach (glob(APP_ROOT . $GLOBALS['conf']['IMAGE']['new_crop_thumb_path'] . "*.*") as $filename) {
			//echo "$filename size " . filesize($filename) . "\n<br>";
			$arr[] = "http://" . substr($filename, strlen("/var/www/vhosts/"));
		}
		$this->_output['res_im'] = $arr;
		$this->_output['tpl'] = 'product/NewImageCrop';
	}

	function _newImageCropTech() {
		//print "<pre>";
		//print_r($_POST);exit;
		$fname = convert_me($_POST['image_in_prev']);
		$targ_w = $targ_h = 150;
		$jpeg_quality = 90;
		$src = APP_ROOT . "image/preview/orig/" . $fname; //'demo_files/glitter_background_b4.gif';
		$ext = pathinfo($src);
		if ($ext['extension'] == 'jpeg' || $ext['extension'] == "jpg")
			$ext['extension'] = 'jpeg';
		//$fun=imagecreatefrom.$ext['extension'];
		//$img_r =$fun($src);
		$img_r = imagecreatefromjpeg($src);
		$dst_r = ImageCreateTrueColor($targ_w, $targ_h);
		imagecopyresampled($dst_r, $img_r, 0, 0, $_POST['x'], $_POST['y'], $targ_w, $targ_h, $_POST['w'], $_POST['h']);
		//header('Content-type: image/jpeg');
		imagejpeg($dst_r, APP_ROOT . $GLOBALS['conf']['IMAGE']['new_crop_thumb_path'] . $fname, $jpeg_quality);
		$this->_newTplToCrop();
		//exit;
	}

########################################## End Of Multiple Image ########################################

	function _multiautocomplete() {
		$items = array(
		    array("name" => "Sahasranshu", "value" => "11"),
		    array("name" => "Sanjit", "value" => "13"),
		    array("name" => "Priya", "value" => "21"),
		    array("name" => "Pratap", "value" => "4"),
		    array("name" => "Priyadarsini", "value" => "24"),
		    array("name" => "Saswati", "value" => "22"),
		    array("name" => "Sarita", "value" => "26"),
		    array("name" => "Satya", "value" => "27"),
		    array("name" => "Amit", "value" => "32"),
		    array("name" => "Ajit", "value" => "33"),
		    array("name" => "Arun", "value" => "34"),
		    array("name" => "Abhishek", "value" => "38"),
		    array("name" => "Siba", "value" => "50"),
		    array("name" => "Sugyana", "value" => "111"),
		    array("name" => "Subha", "value" => "62")
		);

		print json_encode($items); // will return data in json format
		exit;
	}

}

;
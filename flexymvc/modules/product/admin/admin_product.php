<?php

class admin_product extends product_manager {

	/**
	 * This is a constructor to initialized product admin module.This overrides user side constructor.
	 *
	 * @param object $smarty Reference of smarty object
	 *
	 * @param Array $_output Output query string
	 *
	 * @param Array $_input Input query string
	 */
	public function __construct(& $smarty, & $_output, & $_input) {
		if ($_SESSION['id_admin']) {
			parent::__construct($smarty, $_output, $_input);
		} else {
			redirect(LBL_ADMIN_SITE_URL);
		}
	}

	function _listing() {//echo 22;exit;
		if ($GLOBALS['conf']['PRODUCT']['image_type'] == 1) {
			$this->_singleProductListing();
		} else {
			$this->_productListing();
		}
	}

	/**
	 * @method _singleProductListing Listed single image products for admin side
	 *
	 * @return template with single image products data
	 */
	function _singleProductListing() {
		$cat_status = $GLOBALS['conf']['CATEGORY']['category_status'];
		$uri = PAGINATE_ADMIN_PATH . "product/listing";
		$cond = 1;
		if ($this->_input['psearch']) {
			$uri = $uri . "/psearch/1";
			if ($this->_input['pname'] != '') {
				$cond .= " AND p.name LIKE '" . $this->_input['pname'] . "%'";
				$uri = $uri . "/pname/" . urlencode($this->_input['pname']);
			}
			if ($this->_input['code'] != '') {
				$cond .= " AND p.code LIKE '" . $this->_input['code'] . "%'";
				$uri = $uri . "/code/" . $this->_input['code'];
			}
			if ($this->_input['desc'] != '') {
				$cond .= " AND p.description LIKE '" . $this->_input['desc'] . "%'";
				$uri = $uri . "/desc/" . $this->_input['desc'];
			}
			if ($cat_status == 1) {
				if ($this->_input['product_category'] != '') {
					$cond .= " AND p.id_category ='" . $this->_input['product_category'] . "'";
					$uri = $uri . "/product_category/" . $this->_input['product_category'];
				}
			}
			if ($this->_input['cat_flag'] != '') {
				$uri = $uri . "/cat_flag/" . $this->_input['cat_flag'];
			}
		}

		if ($cat_status == 1) {
			if ($this->_input['id_category'])
				$cond .= " AND p.id_category = " . $this->_input['id_category'];
			$sql = "SELECT p.*, c.name AS category_name
		    FROM " . TABLE_PREFIX . "product AS p LEFT JOIN " . TABLE_PREFIX . "category c
		    ON p.id_category = c.id_category WHERE " . $cond . " AND p.image_type !=2 ORDER BY p.id_category DESC ";
		}else {
			$sql = get_search_sql("product AS p", $cond . " AND p.image_type !=2 ORDER BY p.id_product DESC", "p.*");
		}
		// AND p.image_type !=2  this part is added by prabhu
		$this->_output['field'] = array("name" => array("Name", 1), "code_product" => array("Code", 1));
		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] = $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['sql'] = $sql;
		$this->_output['ajax'] = 1;
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box

		$this->_output['uri'] = urlencode($uri);
		$this->_output['prod_type'] = $GLOBALS['conf']['PRODUCT']['image_type'];
		$this->_output['active_cat'] = $cat_status;

		if (!(($this->_input['insEditDel'] == 1) || ($this->_input['pg'] == 1) || ($this->_input['psearch'] == 1) || $this->_input['id_category'])) {
			if ($cat_status == 1) {
				$res = getindexrows(get_search_sql('category', "1 ORDER BY id_category ASC"), 'id_category', 'name', $err);
				$this->_output['category'] = $res;
			}
			$this->blObj->page_listing($this, 'admin/product/search');
		} else {
//	    $this->_output['id_cat'] = $this->_input['id_category'];
			$this->_output['id_cat'] = ($this->_input['cat_flag']) ? $this->_input['product_category'] : $this->_input['id_category'];
			$this->blObj->page_listing($this, 'admin/product/singlelisting');
		}
	}

	/**
	 * @method _productListing Listed multiple image products for admin side
	 *
	 * @return template with multiple image products data
	 */
	function _productListing($is_excel = 0) {
            $cat_status = $GLOBALS['conf']['CATEGORY']['category_status'];
		$uri = PAGINATE_ADMIN_PATH . "product/listing";
		$cond = 1;
		if ($this->_input['psearch']) {
			$uri = $uri . "/psearch/1";
			if ($this->_input['name'] != '') {
				$cond .= " AND pr.name LIKE '" . $this->_input['name'] . "%'";
				$uri = $uri . "/name/" . $this->_input['name'];pr($uri);exit;
			}
			if ($this->_input['code'] != '') {
				$cond .= " AND pr.code LIKE '" . $this->_input['code'] . "%'";
				$uri = $uri . "/code/" . $this->_input['code'];
			}
			if ($this->_input['desc'] != '') {
				$cond .= " AND pr.description LIKE '" . $this->_input['desc'] . "%'";
				$uri = $uri . "/desc/" . $this->_input['desc'];
			}
			if ($cat_status == 1) {
				if ($this->_input['product_category'] != '') {
					$cond .= " AND pr.id_category ='" . $this->_input['product_category'] . "'";
					$uri = $uri . "/product_category/" . $this->_input['product_category'];
				}
			}
			if ($this->_input['cat_flag'] != '') {
				$uri = $uri . "/cat_flag/" . $this->_input['cat_flag'];
			}
		}

		if ($cat_status == 1) {
			if ($this->_input['id_category'])
				$cond .= " AND pr.id_category = " . $this->_input['id_category'];
			$sql = "SELECT pr.*, c.name AS category_name
		    FROM (SELECT p.*, pi.id_image, pi.image_name, count( pi.image_name ) AS total
			  FROM " . TABLE_PREFIX . "product p LEFT JOIN " . TABLE_PREFIX . "product_images pi
			      ON p.id_product = pi.id_product GROUP BY p.id_product DESC ) AS pr LEFT JOIN " . TABLE_PREFIX . "category c
				  ON pr.id_category = c.id_category WHERE " . $cond . " AND pr.image_type !=1 ORDER BY pr.id_product DESC ";
		}else {
			$sql = "SELECT pr.*, pi.id_image, pi.image_name, count(pi.image_name) AS total
		    FROM " . TABLE_PREFIX . "product pr LEFT JOIN " . TABLE_PREFIX . "product_images pi ON pr.id_product = pi.id_product
		    WHERE $cond  AND pr.image_type !=1 GROUP BY pr.id_product DESC";
		}
		if ($is_excel) {
			return $sql;
		}
		$this->_output['field'] = array("name" => array("Name", 1), "code_product" => array("Code", 1));
		$this->_output['limit'] = $GLOBALS['conf']['PAGINATE_ADMIN']['rec_per_page'];
		$this->_output['show'] = $GLOBALS['conf']['PAGINATE_ADMIN']['show_page'];
		$this->_output['sql'] = $sql;
		$this->_output['ajax'] = 1;
		$this->_output['type'] = "box"; // extra, nextprev, advance, normal, box

		$this->_output['uri'] = $uri;
		$this->_output['prod_type'] = $GLOBALS['conf']['PRODUCT']['image_type'];
		$this->_output['active_cat'] = $cat_status;
		$this->_output['prod_type'] = $GLOBALS['conf']['PRODUCT']['image_type'];

		if (!(($this->_input['insEditDel'] == 1) || ($this->_input['pg'] == 1) || ($this->_input['psearch'] == 1) || $this->_input['id_category'])) {
			if ($cat_status == 1) {
				$res = getindexrows(get_search_sql('category', "1 ORDER BY id_category ASC"), 'id_category', 'name', $err);
				//$res['xxx'] = "* No Parent_category";
				$this->_output['category'] = $res;
			}
			$this->blObj->page_listing($this, 'admin/product/search');
		} else {
			$this->_output['id_cat'] = ($this->_input['cat_flag']) ? $this->_input['product_category'] : $this->_input['id_category'];
			$this->blObj->page_listing($this, 'admin/product/listing');
		}
	}

	/**
	 * @method _autoSearchField() Autocomplete function for search filled data
	 *
	 * @return template with auto pre filled data
	 */
	function _autoSearchField() {
		$img_type = $GLOBALS['conf']['PRODUCT']['image_type'];
		$cond = $this->_input['fieldName'] . " LIKE '%" . $this->_input['q'] . "%'  AND image_type={$img_type} ORDER BY " . $this->_input['fieldName'] . " ASC";
		$this->_output['data'] = getautorows(get_search_sql("product", $cond, " DISTINCT " . $this->_input['fieldName']), $this->_input['fieldName'], "");
		$this->_output['tpl'] = 'product/autoSearch';
	}

	//used in next phase
	function _delete_product_video() {
		$uploadDir = APP_ROOT . $GLOBALS['conf']['VIDEO']['video_path'];
		//print $uploadDir.$this->_input['video_name'];
		@unlink($uploadDir . $this->_input['video_name']);
		$vname = "";
		$this->dataObj->update_video($vname, $this->_input['code'], $this->_input['id_product']);
		if ($this->_input['jv'] == 1) {
			$this->_edit();
		} else {
			redirect(LBL_ADMIN_SITE_URL . "product/product_listing");
		}
	}

	function _show_video() {
		$sql = get_search_sql('product', "id_product='" . $this->_input['id'] . "' LIMIT 1");
		$this->_output['res'] = getsingleindexrow($sql, $err);
		$this->_output['tpl'] = 'admin/product/show_video';
	}

	function _reorder() {
		ob_clean();
		$res = explode(",", $this->_input['tbl']);
		$sql = "UPDATE " . TABLE_PREFIX . "product_images SET id_seq = CASE id_image";
		$i = 1;
		foreach ($res as $key => $value) {
			$sql .= " WHEN " . $value . " THEN " . $i++ . " ";
		}
		$sql .= "END WHERE id_image IN (" . $this->_input['tbl'] . ") AND id_product=" . $this->_input['id_product'];
		execute($sql, $err);
		exit;
	}

	function _showImageList() {
		$sql = "SELECT pim.*,p.name FROM " . TABLE_PREFIX . "product_images pim," . TABLE_PREFIX . "product p WHERE pim.id_product=p.id_product AND pim.id_product=" . $this->_input['id_product'] . " ORDER BY pim.id_seq ASC";
		//$sql = get_search_sql("product_images","id_product=".$this->_input['id_product']."  ORDER BY id_seq ASC","id_image,image_name");
		$this->_output['list'] = getrows($sql, $err);
		$this->_output['tpl'] = 'admin/product/product_imagelist';
	}

	function _downloadExcel() {
		ob_clean();
		global $link;
		$sql = $this->_productListing(1);
		$res = mysqli_query($link, $sql);
		$remove = array("\n", "\t", "\r");
		header("Content-type: application/msexcel");
		header("Content-disposition: filename=" . "exportproduct_" . date("Y-m-d_H-i", time()) . ".xls");
		echo "NAME" . "\t" . "DESCRIPTION" . "\t" . "CODE" . "\n";
		while ($row = mysqli_fetch_assoc($res)) {
			$description = str_replace($remove, "", str_replace('"', "'", $row['description']));
			echo '"' . $row['name'] . '"' . "\t" . '"' . $description . '"' . "\t" . '"' . $row['code'] . '"' . "\n";
			ob_flush();
		}
		exit;
	}

}

;

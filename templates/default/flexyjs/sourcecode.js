$(function() {
    $('.txtSource').click(function() {
        $(this).select();
    });
    $('.viewSource').click(function() {
        $('.codeContainer').fadeIn(800);
    });
    $('.closeModalBox').click(function() {
        $('.codeContainer').fadeOut(800);
    });
    $(window).on('keyup', function(e) {
        if (e.which == 27) {
            $('.codeContainer').fadeOut(800);
        }
    });
});

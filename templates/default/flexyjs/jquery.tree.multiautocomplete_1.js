jQuery(function($) {
    $.fn.autocompleteTree = function(opt) {
        return this.each(function() {
            function init() {
                createList();
                preSet();
                addInput(0);
            }

            function createList() {
                element.hide();
                element.attr("multiple", "multiple");
                if (element.attr("name").indexOf("[]") == -1) {
                    element.attr("name", element.attr("name") + "[]");
                }

                holder = $(document.createElement("ul"));
                holder.attr("class", "holder");
                holder.attr("id", elemid + "_holder");
                holder.width(options.width);
                element.after(holder);

                complete = $(document.createElement("div"));
                complete.addClass("facebook-auto");
                complete.append('<div class="default">' + options.complete_text + "</div>");

                if (browser_msie) {
                    complete.append('<iframe class="ie6fix" scrolling="no" frameborder="0"></iframe>');
                    browser_msie_frame = complete.children('.ie6fix');
                }

                feed = $(document.createElement("ul"));
                feed.attr("id", elemid + "_feed");

                complete.prepend(feed);
                holder.after(complete);
                feed.css("width", complete.width());
            }

            function preSet() {
                element.children("option").each(function(i, option) {
                    option = $(option);
                    if (option.hasClass("selected") || option.is(':selected')) {
                        addItem(option.text(), option.val(), true, option.hasClass("locked"));
                        option.attr("selected", "selected");
                    } else {
                        option.removeAttr("selected");
                    }

                    cache.push({
                        name: option.text(),
                        value: option.val()
                    });
                    search_string += "" + (cache.length - 1) + ":" + option.text() + ";";
                });
            }

            //public method to add new item
            $(this).bind("addItem", function(event, data) {
                addItem(data.title, data.value, 0, 0, 0);
            });

            function addItem(title, value, preadded, locked, focusme) {
                if (!maxItems()) {
                    return false;
                }
                var li = document.createElement("li");
                var txt = document.createTextNode(title);
                var liclass = "bit-box" + (locked ? " locked" : "");
                $(li).attr({
                    "class": liclass,
                    "rel": value
                });
                $(li).prepend(txt);
                var aclose = document.createElement("a");
                $(aclose).attr({
                    "class": "closebutton",
		    "href": "javascript:void(0)",
			"style":"top: 0em; font-size: 80%", // added by amar
			"title":"Remove"// added by amar
                });
                $(aclose).bind('click', function(e) {
                    removeNextall(this);
                });
		$(aclose).prepend("X");// added by amar
                li.appendChild(aclose);
                if (options.newline)
                    holder.append(li).append("<div class='clear'></div>");
                else
                    holder.append(li);
                if (!preadded) {
                    $("#" + elemid + "_annoninput").remove();
                    var _item;
                    addInput(focusme);
                    if (element.children("option[value=" + value + "]").length) {
                        _item = element.children("option[value=" + value + "]");
                        _item.get(0).setAttribute("selected", "selected");
                        if (!_item.hasClass("selected")) {
                            _item.addClass("selected");
                        }
                    } else {
                        var _item = $(document.createElement("option"));
                        _item.attr("value", value).get(0).setAttribute("selected", "selected");
                        _item.attr("value", value).addClass("selected");
                        _item.text(title);
                        element.append(_item);
                    }
                    if (options.onselect.length) {
                        funCall(options.onselect, _item)
                    }
                    element.change();
                }
                holder.children("li.bit-box.deleted").removeClass("deleted");
                feed.hide();
                browser_msie ? browser_msie_frame.hide() : '';
            }
	    function removeNextall(e) {
//                var pid = $(e).closest().find('select').attr("id");
               var pid = $(e).closest('div').find('select').attr("id");
                var myli = $(e).closest("li");
                if (options.tree) {
		    (myli.nextAll("li")).each(function(i) {
                        if ($(this).attr("rel")) {
                            removeItem($(this), $("#" + pid));
                        }
                    });
                }
                    if (myli.attr("rel")) {
                        removeItem(myli, $("#" + pid));
                    }
                }

            function removeItem(item, myele) {
                if (myele)
                    element = myele;
                if (!item.hasClass('locked')) {
                    //alert(item.attr("rel") + "----------" + item.html() + "-----" + element.html());
                    item.fadeOut("fast");
                    if (options.onremove.length) {
                        var _item = element.children("option[value=" + item.attr("rel") + "]");
                        funCall(options.onremove, _item)
                    }
                    element.children('option[value="' + item.attr("rel") + '"]').removeAttr("selected").removeClass("selected").remove();
                    //holder.click();
		    if (!options.checkbox)
                        feed.html('');
                    item.remove();
                    element.change();
                }
            }

            function addInput(focusme) {
                var li = $(document.createElement("li"));
                var input = $(document.createElement("input"));
                var getBoxTimeout = 0;

		li.attr({"class": "bit-input", "id": elemid + "_annoninput"});
		input.attr({"type": "text", "class": "maininput", "size": "1"});
                holder.append(li.append(input));

                input.focus(function(e) {
                    _me = $("#" + elemid).val();
                    if (_me) {
                        if (_me.indexOf(",") == -1)
                            nos = 1;
                        else
                            nos = _me.split(",");
                    } else {
                        nos = 0;
                    }
                    if (nos < options.maxitems) {
                        complete.fadeIn("fast");
                    }
                });
                input.change(function(event) {
                    complete.hide();
                });
                input.blur(function(event) {
                    if (!options.checkbox) {
                        complete.fadeOut("fast");
                        if (options.noselection) {
                            var etext = xssPrevent(input.val());
                            if (etext != "")
                                addItem(etext, "%" + etext);
                        } else {
                            if (feed.children("li:visible:first").length > 0) {
                                var etext = xssPrevent(input.val());
                                if (etext != "")
                                    addItem(feed.children("li:visible:first").text(), feed.children("li:visible:first").attr("rel"));
                            } else {
                                input.val("");
                            }
                        }
                        complete.hide();
                    }
                });

                holder.click(function() {
                    input.focus();
                    if ((feed.length && input.val().length) || options.showonfocus) {
                        feed.show();
                    } else {
                        feed.hide();
                        browser_msie ? browser_msie_frame.hide() : '';
                        complete.children(".default").show();
                    }
                });
                input.keypress(function(event) {
                    if (event.keyCode == 13 || event.keyCode == 9) {
                        var fields = $(this).parents('form:eq(0),body').find('button:enabled,input:enabled,textarea:enabled,select:visible');
                        var index = fields.index(this);
                        if (index > -1 && (index + 1) < fields.length) {
                            fields.eq(index + 1).focus();
                        }
                        return false;
                    }
                    //auto expand input
                    input.attr("size", input.val().length + 1);
                });

                input.keydown(function(event) {
                    //prevent to enter some bad chars when input is empty
                    if (event.keyCode == 191) {
                        event.preventDefault();
                        return false;
                    }
                });

                input.keyup(function(event) {
                    var etext = xssPrevent(input.val());
                    if (event.keyCode == 8 && etext.length == 0) {
                        feed.hide();
                        browser_msie ? browser_msie_frame.hide() : '';
                        if (!holder.children("li.bit-box:last").hasClass('locked')) {
                            if (holder.children("li.bit-box.deleted").length == 0) {
                                holder.children("li.bit-box:last").addClass("deleted");
                                return false;
                            } else {
                                holder.children("li.bit-box.deleted").fadeOut("fast", function() {
                                    removeItem($(this));
                                    input.focus();
                                    return false;
                                });
                            }
                        }
                    }
                    if (event.keyCode != 40 && event.keyCode != 38) {
                        counter = 0;
                        if (options.json_url) {
                            if (options.cache && json_cache) {
                                addMembers(etext);
                                bindEvents();
                            } else {
                                getBoxTimeout++;
                                var getBoxTimeoutValue = getBoxTimeout;
                                setTimeout(function() {
                                    feed.html('');
                                    if (getBoxTimeoutValue != getBoxTimeout)
                                        return;
                                    getdatatoshow(etext);
                                }, options.delay);
                            }
                        } else {
                            addMembers(etext);
                            bindEvents();
                        }

                        complete.children(".default").hide();
                        feed.show();
                    }
                });
                if ($('ul.holder').find('li.bit-box').length > 0)
                    focusme = 1;
                if (focusme) {
                    setTimeout(function() {
                        complete.children(".default").show();
                    }, 1);
                }
            }
            function getdatatoshow(etext) {
                _me = $("#" + elemid).val();
                if (options.maxitems == 1 || !options.tree)
                    lastele = 0;
                else
                    lastele = _me ? _me[_me.length - 1] : 0;
                //debug_text(_me+"------"+lastele);
		$.getJSON(options.json_url + (options.json_url.indexOf("?") > -1 ? "&" : "?") + "tag=" + etext + "&id=" + lastele, null, function(data) {
                    addMembers(etext, data);
                    json_cache = true;
                    bindEvents();
                });
            }
            function addMembers(etext, data) {
                feed.html('');
                if (!options.cache && data != null) {
                    cache = new Array();
                    search_string = "";
                }
                addTextItem(etext);
                if (data != null && data.length) {
                    $.each(data, function(i, val) {
                        cache.push({
                            name: val.name,
                            value: val.value
                        });
                        search_string += "" + (cache.length - 1) + ":" + val.name + ";";
                    });
                }
                var maximum = options.maxshownitems < cache.length ? options.maxshownitems : cache.length;
                var filter = "i";
                if (options.filter_case) {
                    filter = "";
                }
                var myregexp, match;
                try {
                    myregexp = eval('/(?:^|;)\\s*(\\d+)\\s*:[^;]*?' + etext + '[^;]*/g' + filter);
                    match = myregexp.exec(search_string);
                } catch (ex) {
                }
                var content = '';
                while (match != null && maximum > 0) {
                    var id = match[1];
                    var object = cache[id];
                    if (options.checkbox) {
                        if (options.filter_selected && element.children("option[value=" + object.value + "]").hasClass("selected")) {
                            cbox = options.checkbox ? "<input type='checkbox' checked='checked' class='noaction' >" : "";
                        } else {
                            cbox = options.checkbox ? "<input type='checkbox' class='noaction' >" : "";
                        }
                        content += '<li rel="' + object.value + '">' + cbox + itemIllumination(object.name, etext) + '</li>';
                        counter++;
                        maximum--;
                    } else {
                        if (options.filter_selected && element.children("option[value=" + object.value + "]").hasClass("selected")) {
                            //nothing here...
                        } else {
                            content += '<li rel="' + object.value + '">' + itemIllumination(object.name, etext) + '</li>';
                            counter++;
                            maximum--;
                        }
                    }
                    match = myregexp.exec(search_string);
                }
                feed.append(content);
                if (options.firstselected) {
                    focuson = feed.children("li:visible:first");
                    focuson.addClass("auto-focus");
                }
                if (counter > options.height) {
                    feed.css({
                        "height": (options.height * 24) + "px",
                        "overflow": "auto"
                    });
                    if (browser_msie) {
                        browser_msie_frame.css({
                            "height": (options.height * 24) + "px",
                            "width": feed.width() + "px"
                        }).show();
                    }
                } else {
                    feed.css("height", "auto");
                    if (browser_msie) {
                        browser_msie_frame.css({
                            "height": feed.height() + "px",
                            "width": feed.width() + "px"
                        }).show();
                    }
                }
            }

            function itemIllumination(text, etext) {
                if (options.filter_case) {
                    try {
                        eval("var text = text.replace(/(.*)(" + etext + ")(.*)/gi,'$1<em>$2</em>$3');");
                    }
                    catch (ex) {
                    }
                    ;
                } else {
                    try {
                        eval("var text = text.replace(/(.*)(" + etext.toLowerCase() + ")(.*)/gi,'$1<em>$2</em>$3');");
                    }
                    catch (ex) {
                    }
                    ;
                }
                return text;
            }

            function bindFeedEvent() {
                feed.children("li").mouseover(function() {
                    feed.children("li").removeClass("auto-focus");
                    $(this).addClass("auto-focus");
                    focuson = $(this);
                });

                feed.children("li").mouseout(function() {
                    $(this).removeClass("auto-focus");
                    focuson = null;
                });
            }

            function removeFeedEvent() {
                feed.children("li").unbind("mouseover");
                feed.children("li").unbind("mouseout");
                feed.mousemove(function() {
                    bindFeedEvent();
                    feed.unbind("mousemove");
                });
            }

            function bindEvents() {
                var maininput = $("#" + elemid + "_annoninput").children(".maininput");
                bindFeedEvent();
                feed.children("li").unbind("mousedown");
                feed.children("li").mousedown(function(e) {
                    var option = $(this);
                    if (options.checkbox) {
                        if (option.find("input:checked").length == 0) {
                            option.find("input:checkbox").attr("checked", "checked");
                            addItem(option.text(), option.attr("rel"));
                        } else {
                            option.find("input:checkbox").removeAttr("checked");
                            var pid = option.parents().find('select').attr("id") + "_holder";
                            var obj = $("#" + pid).find("li[rel=" + option.attr("rel") + "]");
                            removeItem(obj);
                        }
                        feed.show();
                    } else {
                        addItem(option.text(), option.attr("rel"));
                        feed.hide();
                        browser_msie ? browser_msie_frame.hide() : '';
                        complete.hide();
                    }
		    setTimeout(function(){ $("#" + elemid).parent('div').find('.maininput').focus();}, 0);
                    //		    $("#" + elemid + '_ddown').trigger('click');
                });
                maininput.unbind("keydown");
                maininput.keydown(function(event) {
                    if (event.keyCode == 191) {
                        event.preventDefault();
                        return false;
                    }
                    if (event.keyCode != 8) {
                        holder.children("li.bit-box.deleted").removeClass("deleted");
                    }
                    if ((event.keyCode == 13 || event.keyCode == 9) && checkFocusOn()) {
                        var option = focuson;
                        addItem(option.text(), option.attr("rel"));
                        complete.hide();
                        event.preventDefault();
                        focuson = null;
			setTimeout(function(){ $("#" + elemid).parent('div').find('.maininput').focus();}, 0);
                        return false;
                    }
                    if ((event.keyCode == 13 || event.keyCode == 9) && !checkFocusOn()) {
                        if (options.newel) {
                            var value = xssPrevent($(this).val());
                            addItem(value, value);
                            complete.hide();
                            event.preventDefault();
                            focuson = null;
                        }
                        return false;
                    }
                    if (event.keyCode == 40) {
                        removeFeedEvent();
                        if (focuson == null || focuson.length == 0) {
                            focuson = feed.children("li:visible:first");
                            feed.get(0).scrollTop = 0;
                        } else {
                            focuson.removeClass("auto-focus");
                            focuson = focuson.nextAll("li:visible:first");
                            var prev = parseInt(focuson.prevAll("li:visible").length, 10);
                            var next = parseInt(focuson.nextAll("li:visible").length, 10);
			    if ((prev > Math.round(options.height / 2) || next <= Math.round(options.height / 2)) && typeof(focuson.get(0)) != "undefined") {
                                feed.get(0).scrollTop = parseInt(focuson.get(0).scrollHeight, 10) * (prev - Math.round(options.height / 2));
                            }
                        }
                        feed.children("li").removeClass("auto-focus");
                        focuson.addClass("auto-focus");
                    }
                    if (event.keyCode == 38) {
                        removeFeedEvent();
                        if (focuson == null || focuson.length == 0) {
                            focuson = feed.children("li:visible:last");
                            feed.get(0).scrollTop = parseInt(focuson.get(0).scrollHeight, 10) * (parseInt(feed.children("li:visible").length, 10) - Math.round(options.height / 2));
                        } else {
                            focuson.removeClass("auto-focus");
                            focuson = focuson.prevAll("li:visible:first");
                            var prev = parseInt(focuson.prevAll("li:visible").length, 10);
                            var next = parseInt(focuson.nextAll("li:visible").length, 10);
			    if ((next > Math.round(options.height / 2) || prev <= Math.round(options.height / 2)) && typeof(focuson.get(0)) != "undefined") {
                                feed.get(0).scrollTop = parseInt(focuson.get(0).scrollHeight, 10) * (prev - Math.round(options.height / 2));
                            }
                        }
                        feed.children("li").removeClass("auto-focus");
                        focuson.addClass("auto-focus");
                    }
                });
            }

            function maxItems() {
                if (options.maxitems != 0) {
                    if (holder.children("li.bit-box").length < options.maxitems) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            function addTextItem(value) {
                if (options.newel && maxItems()) {
                    feed.children("li[fckb=1]").remove();
                    if (value.length == 0) {
                        return;
                    }
                    var li = $(document.createElement("li"));
                    li.attr({
                        "rel": value,
                        "fckb": "1"
                    }).html(value);
                    feed.prepend(li);
                    counter++;
                } else {
                    return;
                }
            }

            function funCall(func, item) {
                var _object = "";
                for (i = 0; i < item.get(0).attributes.length; i++) {
                    if (item.get(0).attributes[i].nodeValue != null) {
                        _object += "\"_" + item.get(0).attributes[i].nodeName + "\": \"" + item.get(0).attributes[i].nodeValue + "\",";
                    }
                }
                _object = "{" + _object + " notinuse: 0}";
                func.call(func, _object);
            }

            function checkFocusOn() {
                if (focuson == null) {
                    return false;
                }
                if (focuson.length == 0) {
                    return false;
                }
                return true;
            }

            function xssPrevent(string) {
                string = string.replace(/[\"\'][\s]*javascript:(.*)[\"\']/g, "\"\"");
                string = string.replace(/script(.*)/g, "");
                string = string.replace(/eval\((.*)\)/g, "");
                string = string.replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '');
                return string;
            }
            function clearAutoComplete() {
                holder.children("li").each(function(i) {
                    if (!$(this).is(':last-child')) {
                        $(this).remove();
                    }
                });
                $("#" + elemid).find('option').remove().end();
                feed.html('');
            }
            var options = $.extend({
                json_url: null,
                prefetch: false,
                cache: true,
                height: "10",
                newel: false,
                firstselected: false,
                filter_case: false,
                filter_selected: true,
                filter_hide: true,
                complete_text: "Start to type...",
		maxshownitems: 1000,
		maxitems: 500,
                onselect: "",
                newline: 0,
                onremove: "",
                delay: 350,
                width: "209px",
                clearField: '',
                showonfocus: false,
                tree: true,
                checkbox: false,
		noselection: false
            }, opt);

            //system variables
            var holder = null;
            var feed = null;
            var complete = null;
            var counter = 0;
            var cache = new Array();
            var json_cache = false;
            var search_string = "";
            var focuson = null;
            var browser_msie = "\v" == "v";
            var browser_msie_frame;
            var toggleme = 1;

            var element = $(this);
            var elemid = element.attr("id");
                $("#" + elemid).wrap('<div class="wrap_select"></div>');
	    $("#" + elemid).parent('.wrap_select').append('<img id="' + elemid + '_ddown' + '" src="http://demos4clients.com/banjaara/templates/images/dropdown.png" >');
            if (options.checkbox) {
                $("#" + elemid + '_ddown').click(function() {
		    if (toggleme == 1) {
                        toggleme = 2;
                        $("#" + elemid).parent('div').find('input:text').keyup().focus();
		    } else {
                        toggleme = 1;
                        $("#" + elemid).parent('div').find('input:text').change();
                    }
                });
	    } else {
                $("#" + elemid + '_ddown').click(function() {
                    $("#" + elemid).parent('div').find('input:text').keyup().focus();
                });
            }
            if (options.checkbox) {
                $('.noaction').live('click', function(e) {
                    e.preventDefault();
                });
            }
            if (options.clearField) {
                $("#" + options.clearField).click(function() {
                    clearAutoComplete();
                });
            }
            if (options.prefetch) {
                getdatatoshow('');
            }
            init();
            return this;
        });
    };
});
$(document).ready(function(){
    $('.tabhd a').click(function(){
        $('.tabhd a.selected').removeClass('selected');
        $(this).addClass('selected');
        $('.tabsector .selected').removeClass('selected');
        $('.tabsector .tabsectorcontent').eq($('.tabhd a').index(this)).addClass('selected');
    });
    $('.formtbl').each(function(){
        $(this).find('tr:even').addClass('even');
    });
    var hiteset;
    $('.seemore').each(function(){
	hiteset = $(this).attr('sethite') ? $(this).attr('sethite') : 105 ;
	if($(this).attr('sethite') > $(this).height())
	    hiteset = $(this).height();
	$(this).data('test',{tothite : $(this).height(), smlhite : hiteset});
    });
    $('.seemoreswitch').click(function(){
	if($(this).html() == 'Hide Content'){
	    $(this).siblings('.seemorecontent').animate({
		height : $(this).parent('.seemore').data('test').smlhite+'px'
	    },'slow');
	    $(this).html('See more');
	}
	else{
	    $(this).siblings('.seemorecontent').animate({
		height : $(this).parent('.seemore').data('test').tothite+'px'
	    },'slow');
	    $(this).html('Hide Content');
	}	    
    });
    $('.seemoreswitch').each(function(){
	$(this).trigger('click');
    });
});

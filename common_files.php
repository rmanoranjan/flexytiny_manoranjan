<?php

$_SESSION['debug'] = isset($_REQUEST['debug']) ? $_REQUEST['debug'] : (isset($_SESSION['debug']) ? $_SESSION['debug'] : 0);

/**
 *
 * @author Afixi
 * Used to check session for the user to access the site
 * @param string $url url to be specified for redirection.[Default is LBL_SITE_URL]
 * @return message value
 */
function check_session($url = LBL_SITE_URL) {
    if (!$_SESSION['id_user']) {
	if (isset($_SERVER[PARSE_FOR_COLLECT_INPUT])) {
	    $nextRed = preg_replace("/\/([a-zA-Z0-9_]*)\/index.php([\/]*)/", "", $_SERVER[PARSE_FOR_COLLECT_INPUT]);
	    if ($nextRed)
		$_SESSION['nextredirect'] = $nextRed;
	}
	$_SESSION['raise_message']['global'] = getmessage('USER_CHK_SESSION');
	header("Location: $url");
	exit;
    }else{
	return true;
    }
}
function pre($arr,$exit=0){
    print "<pre>";
    print_r($arr);
    print "</pre>";
	if($exit)
		exit();
}
/**
 *
 * @author Parwesh
 * Used to get your message from database as specified in coding
 * @param string $type message key
 * @return message value
 */
function getmessage($type) {
    $lang_code = 'en'; //Put your language code as required
    $msg = $type . "_" . strtoupper($lang_code);
    return constant($msg);
}

/**
 *
 * @method get_search_sql Return sql query
 * @param String $tbname Table name where you want to fetch data
 * @param String $search_condition Condition for seach. Default is 1.
 * @param String $field_name Field name(s) to be search. Deafault "*" means all fields.
 * @return string $sql Sql statement
 * @example get_search_sql("user")
 * @example get_search_sql("product","id_product = 1");
 * @example get_search_sql("category","","name,code");
 */
function get_search_sql($tbname, $search_condition="1", $field_name='*') {
    $sql = "SELECT $field_name FROM " . TABLE_PREFIX . "$tbname WHERE $search_condition";
    return $sql;
}

/**
 * This function takes a integer value and returns the years in an associative array having the keys and values same i.e the year itself.
 * @example example if $range is 3 then the output is 
 Array
  (
  [2011] => 2011
  [2010] => 2010
  [2009] => 2009
  [2008] => 2008
  )

 * @param int $range
 * @return array 
 */
function get_year($range='') {
    $dt = date("Y");
    $range = $dt - $range;
    for ($i = $dt; $i >= $range; $i--) {
	$date[$i] = $dt;
	$dt--;
    }
    return $date;
}

/**
 * Function to compare the entered date with the today date and returns 1 if the conditions are satisfied.
 * The conditios are if the year is greater  than the year today or if the year is equal and the month is greater than the todays month or
 * if the month is equal and the date is greater than the todays date or if the date is greater than the todays date then return 1 else 0.
 * @param string $str
 * @return int 1 or 0
 */
function date_check1($str) {
    $todaydate = date('Y-m-d');
    $tddate = explode('-', $todaydate);
    $dtchk = explode('-', $str);
    if ($dtchk[0] > $tddate[0])
	return 1;
    else if ($dtchk[0] == $tddate[0] && $dtchk[1] > $tddate[1])
	return 1;
    else if ($dtchk[1] == $tddate[1] && $dtchk[2] > $tddate[2])
	return 1;
    else if ($dtchk[2] == $tddate[2])
	return 1;
    else
	return 0;
}

/**
 * Function to convert celcious to farenhiet.
 * The return type may be float or int.
 * @param number $cel
 * @return number 
 */
function CelToFar($cel) {
    $Far = $cel * 9 / 5 + 32;
    return $Far;
}

/**
 * Function to convert kilometers to meter per hours
 * @param string $km
 * @return string 
 */
function kmToMPH($km) {
    //global $MPH;
    $MPH = $km / 1.6;
    return $MPH;
}

/**
 * Function to convert hexa decimal number into its respective binary.
 * @param string $str
 * @return string 
 */
function hex2binary($str) {
    $str = str_replace(" ", "", $str);
    $text_array = explode("\r\n", chunk_split($str, 2));

    for ($n = 0; $n < count($text_array) - 1; $n++) {
	$binary .= substr("0000" . base_convert($text_array[$n], 16, 2), -8);
    }//end for loop
    $newstring = chunk_split($binary, 8, " ");
    return $newstring;
}

/**
 * Function to check each bit to see if it is set or not i.e 1 
 * @param int $val
 * @return int 
 */
function EachBit($val) {

    $String = hex2binary($val);
    $length = strlen($String);
    $start = 0;
    for ($i = 0; $i < $length; $i++) {
	$output = substr($String, $start, 1);
	if ($output == 1) {
	    $bitplace = $i;
	}//end if
	$start++;
    }
    return $bitplace;
}

/**
 * This function will checks whether the session is expired or not.
 * User's session has expired.  Go back to the login screen.
 * Other than if the session var timeout is still alive, reset the timer
 */
function PageSession() {
    if (time() > $_SESSION['timeout']) {
	session_unset();
	session_destroy();
	session_start();
	$_SESSION['raise_message']['global'] = "Your session has expired.  Please log back in.";
	redirect(LBL_SITE_URL . "loginForm");
	exit;
    }
    if (isset($_SESSION['timeout'])) {
	$_SESSION['timeout'] = (time() + (SESSION_TIMEOUT * 60)); //session_timout is set on login2
    }
}
/**
 *
 * @param type $place
 * @return type 
 */
function PID03switch($place) {
    switch ($place) {
	case 0:
	    return "Open loop - has not yet satisfied conditions to go closed loop";
	    break;
	case 1:
	    return "Closed loop - using oxygen sensor(s) as feedback for fuel control";
	    break;
	case 2:
	    return "Open loop due to driving conditions (e.g., power enrichment, deceleration enleanment)";
	    break;
	case 3:
	    return "Open loop - due to detected system fault";
	    break;
	case 4:
	    return "Closed loop, but fault with at least one oxygen sensor - may be using single oxygen snsor for fuel control";
	    break;
	default:
	    return "Error in PID03switch";
    }
}

/**
 * This function is used to reorder the unorder array.
 * The only parameter is the unorder array and return type is an order array.
 * The out put is an indexed array.
 * @param array $a
 * @return array 
 */
function reorder_key($a) {
    if (is_array($a)) {
	$out = array();
	foreach ($a as $v)
	    $out[] = $v;
    } else
	$out = $a;
    return $out;
}

/**
 * Function to fetch required result from another site according to the url specified.
 * @param string $url
 * @return string 
 */
function get_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $feed = curl_exec($ch);
    curl_close($ch);
    return $feed;
}

/**
 * This function shows the errors in the form of a table if the $_SESSION['debug'] is set.
 * $_input is the array containing all the request parameters like your page,choice,form field values etc..
 * $report is an array containing all the debug informations.
 * To set the debug information we have to pass debug=1 in the url.
 * To unset the debug information pass debug =0. 
 * @global array $_input
 * @global array $report
 */
function showdebug() {
    global $_input, $report;
    if (!isset($_input['ce'])) {
	if ($_SESSION['debug'] != 0) {
	    $p = count($report['stats']) - 1;
	    if (isset($report['stats'][$p]['func'])) {
		$r = $report['stats'][$p]['func'];
		if (is_array($r)) {
		    foreach ($r as $k => $v) {
			print_r("<font color='red'><b>" . $k . "</font></b><br>");
			print "<table style='border:1px solid red;'><tr><th>Sql</th><th>Time</th><th>Record</th></tr>";
			foreach ($v['sql'] as $k1 => $v1) {
			    print "<tr><td>";
			    print($v1['sql']['sql']);
			    print "</td><td>";
			    print($v1['sql']['time']);
			    print "</td><td>";
			    print(isset($v1['sql']['records']) ? $v1['sql']['records'] : 0);
			    print "</td></tr>";
			}
			print "</table>";
		    }
		}
	    }
	}
    }
}

/**
 *
 * @author Parwesh
 * @param String $datestr Date string
 * @param String $format Format of date, default is '%Y-%m-%d'
 * @param Boolean $today Either to check for today date or not(e,g if TRUE then it return 1 for current date)
 * @return int Either 1 or 0
 * @example date_check_new("2011-12-26")
 * @example date_check_new("12-2011-26", "%m-%Y-%d")
 * @example date_check_new("12-2011-26", "%m-%Y-%d", FALSE)
 */
function date_check_new($datestr, $format = '%Y-%m-%d',$today = TRUE) {
    $tddate = explode('-', date('Y-m-d'));

    if($format == '%Y-%m-%d')
	$dtchk  = explode('-', date('Y-m-d', strtotime($datestr)));
    else {
	$a	= strptime($datestr, $format);
	$dtchk  = explode('-', date('Y-m-d', mktime(0, 0, 0, $a['tm_mon'] + 1, $a['tm_mday'], $a['tm_year'] + 1900)));
    }

    if ($dtchk[0] > $tddate[0])
	return 1;
    else if ($dtchk[0] >= $tddate[0] && $dtchk[1] > $tddate[1])
	return 1;
    else if ($dtchk[0] >= $tddate[0] && $dtchk[1] >= $tddate[1] && ($today) ? $dtchk[2] >= $tddate[2] : $dtchk[2] > $tddate[2])
	return 1;
    else
	return 0;
}

function insrtBinaryInBtwn($tbl = "", $fieldnm = "", $max = "") {
	global $link;
	$max = $max - 1;
	$sql = "SELECT " . $fieldnm . " FROM " . TABLE_PREFIX . $tbl . " WHERE 1 ORDER BY " . $fieldnm;
	$res = mysqli_query($link, $sql);
	if ($res->num_rows) {
		$i = 0;
		while ($rec = mysqli_fetch_assoc($res)) {
			if (!$i) {
				$bin = 1;
				$tmp = $rec[$fieldnm];
			} else {
				$bin = $tmp * 2;
				$tmp = $rec[$fieldnm];
			}
			if ($bin != $rec[$fieldnm]) {
				return $bin;
			}
			$i++;
		}
		if ($i - 1 == $max)
			return "-1";
		else {
			return (2 * $tmp);
		}
	} else {
		return 1;
	}
}
function pr($res){
	print"<pre>";
	print_r($res);
	print"</pre>";
}
function url_encode_arr(&$arr) {
    $value = is_array($arr) ? array_map('url_encode_arr', $arr) : urlencode($arr);
    return $value;
}

//Function to send mail through PHPmailer(In local it works)
function phpMailerlocal($to, $cc = '', $bcc = '', $from, $subject, $body,$attachment) {//pr("here");exit;
// pr($attachment);exit;
    $reply = '';
    require_once APP_ROOT . '/flexymvc/classes/common/PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->isSMTP(); // Set mailer to use SMTP
//    $mail->isMail(); // Set mailer to use SMTP
//      $mail->isSendMail();
////    $mail->Host = 'smtp.gmail.com'; // Specify main and backup server
    $mail->Host = 'mail.smtp2go.com'; // Specify main and backup server
    $mail->SMTPAuth = true; // Enable SMTP authentication
////    $mail->Username = "afixi.swagatika90@gmail.com"; //"afixi.swagatika90@gmail.com"; //$GLOBALS['conf']['PHP_MAILER']['username']; // SMTP username
    $mail->Username = "manoranjan.rout@afixi.com"; //"afixi.swagatika90@gmail.com"; //$GLOBALS['conf']['PHP_MAILER']['username']; // SMTP username
////    $mail->Password = "swagatika90"; //swagatika90"; //$GLOBALS['conf']['PHP_MAILER']['password'];
    $mail->Password = "LeC0siSjGlF1"; //swagatika90"; //$GLOBALS['conf']['PHP_MAILER']['password'];
    $mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    if (strpos($from, ">")) {
        $fromNxt = explode("<", $from);
        $fromName = trim($fromNxt[0], " >");
    }
    
    
    
    $mail->From = 'manoranjan.rout@afixi.com';
    
    
    $mail->FromName = $fromName ? $fromName : $from;
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AltBody = strip_tags($body);
//   $mail->Debugoutput='html';
    if (!is_array($to)) {
        $to = trim($to, ",");
        $to = extract_email_addresses($to);
    }

    if (count($to)) {
        foreach ($to as $key => $val) {
            $mail->addAddress($key, $val); // Add a recipient
        }
    }

    if ($reply == '') {
        $reply = $from;
    }
    if (!is_array($reply)) {
        $reply = trim($reply, ",");
        $reply = extract_email_addresses($reply);
    }
    if (count($reply)) {
        foreach ($reply as $key => $val) {
            $mail->addReplyTo($key, $val);
        }
    }

    if (!is_array($cc)) {
        $cc = trim($cc, ",");
        $cc = extract_email_addresses($cc);
    }
    if (count($cc)) {
        foreach ($cc as $key => $val) {
            $mail->addCC($key, $val);
        }
    }

    if (!is_array($bcc)) {
        $bcc = trim($bcc, ",");
        $bcc = extract_email_addresses($bcc);
    }
    if (count($bcc)) {
        foreach ($bcc as $key => $val) {
            $mail->addBCC($key, $val);
        }
    }

    $mail->WordWrap = 50; // Set word wrap to 50 characters
//$mail->addAttachment($attachment['name'],$attachment['tmp_name']);
    foreach ($attachment as $key => $val) {
        $namearr = $attachment[$key];
        $temparr = $attachment[$key];
        foreach ($namearr as $ak => $av) {
            $mail->addAttachment($temparr[$ak], $av);
        }
    }
    $mail->isHTML(true);
//      pr($mail);exit;
    if ($mail->send()) {
        return true;
        exit;
    } else {
        echo "mailer error".$mail->ErrorInfo;
        return false;
        exit;
    }
}

function extract_email_addresses($emailstring) {
    $commaseparated = explode(',', $emailstring);
    foreach ($commaseparated as $username_email) {
        $username_email = trim($username_email, " <>");
        $username_email = explode('<', $username_email);
        $username = $username_email[0] ? $username_email[0] : $username_email[1];
        $email = $username_email[1] ? $username_email[1] : $username_email[0];
        $emailarray[$email] = $username;
    }
    return $emailarray;
}

function phpMailerlive($to, $from, $subject, $body,$cc) {
    require_once APP_ROOT . '/flexymvc/classes/common/PHPMailer/PHPMailerAutoload.php';
    //Create a new PHPMailer instance
    $mail = new PHPMailer();
    $mail->Debugoutput = 'html';
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->setFrom($from, 'MVISTOR');
    $mail->addReplyTo($from, 'MVISTOR');
    $mail->addAddress($to, 'MVISTOR');
    $mail->Subject = $subject;
    $mail->msgHTML($body);
    $mail->AltBody = strip_tags($body);
    $mail->AddCC($cc);

//send the message, check for errors
    if ($mail->send()) {
        return true;
        exit;
    } else {
        return false;
        exit;
//    }
//        return false;
//        exit;
    }
}

function phpmailer($to, $cc = '', $bcc = '', $from, $subject, $body, $attachment = array(), $reply = '', $msg_id, $username = '', $password = '') {
    require_once APP_ROOT . 'flexymvc/classes/common/PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->isSMTP();  // Set mailer to use SMTP
    $mail->Host = 'mail.demos4clients.com';  // Specify main and backup server
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $username = "trackme@demos4clients.com"; //trim($username);
    $password = "p455w0rd"; //trim($password);
//    $mail->Username = $username ? $username : $GLOBALS['conf']['PHP_MAILER']['username'];  // SMTP username, required field
//    $mail->Password = $password ? $password : $GLOBALS['conf']['PHP_MAILER']['password'];   // SMTP password, required field
    $mail->Username = $username;  // SMTP username, required field
    $mail->Password = $password;   // SMTP password, required field
    $mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted
//    $mail->From = ''; // optional, and if not same as username in gmail automatically it will be same as username
    $mail->FromName = "MVISTOR"; // optional
    $mail->Subject =  $subject;
    $mail->Body = $body;
    if (!is_array($to)) {
        $to = trim($to, ",");
        $to = extract_email_addresses($to);
    }
    if (count($to)) {
        foreach ($to as $key => $val) {
            $mail->addAddress($key, $val);  // Add a recipient
        }
    }

    if (!is_array($reply)) {
        $reply = trim($reply, ",");
        $reply = extract_email_addresses($reply);
    }
    if (count($reply)) {
        foreach ($reply as $key => $val) {
            $mail->addReplyTo($key, $val);
        }
    }

    if (!is_array($cc)) {
        $cc = trim($cc, ",");
        $cc = extract_email_addresses($cc);
    }
    if (count($cc)) {
        foreach ($cc as $key => $val) {
            $mail->addCC($key, $val);
        }
    }

    if (!is_array($bcc)) {
        $bcc = trim($bcc, ",");
        $bcc = extract_email_addresses($bcc);
    }
    if (count($bcc)) {
        foreach ($bcc as $key => $val) {
            $mail->addBCC($key, $val);
        }
    }
    if ($attachment) {
        $mail->addAttachment($attachment['path'],$attachment['name']);
    }
    $mail->isHTML(true);
    if (!$mail->send()) {
        /* echo 'Message could not be sent.<br/>';
          echo 'Mailer Error: ' . $mail->ErrorInfo; */
        return false;
        exit;
    }
//echo 'Message has been sent';
    return true;
    exit;
}

<?php /* Smarty version 2.6.7, created on 2017-08-12 10:03:14
         compiled from admin/cms/attribute.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'admin/cms/attribute.tpl.html', 22, false),array('modifier', 'cat', 'admin/cms/attribute.tpl.html', 26, false),array('modifier', 'date_format', 'admin/cms/attribute.tpl.html', 30, false),array('modifier', 'explode', 'admin/cms/attribute.tpl.html', 43, false),array('function', 'html_options', 'admin/cms/attribute.tpl.html', 50, false),array('function', 'bitwise', 'admin/cms/attribute.tpl.html', 73, false),)), $this); ?>

<!-- Template: admin/cms/attribute.tpl.html Start 12/08/2017 10:03:13 --> 
 <?php $this->assign('attribute_type', $this->_tpl_vars['util']->get_values_from_config('ATTRIBUTE_TYPE'));  $this->assign('board_type', $this->_tpl_vars['util']->get_values_from_config('BOARD_TYPE'));  echo '
<script>
     $(document).ready(function(){
	$(\'.p_dateFormat\').datepicker({
	    dateFormat: "dd-mm-yy",
	    changeMonth:true,
	    changeYear:true,
	    yearRange: "-10:+5"
	 });
      });
</script>
'; ?>

<table>
    <input type="hidden" value="<?php echo $this->_tpl_vars['sm']['selectedAttributeRes']['id']; ?>
" name="idattr">
<?php if (count($_from = (array)$this->_tpl_vars['sm']['attributeList'])):
    foreach ($_from as $this->_tpl_vars['keyAttr'] => $this->_tpl_vars['itemAttr']):
?>
	<tr>
	    <td style="vertical-align: top;padding-top:4px;">
		<?php echo ((is_array($_tmp=$this->_tpl_vars['itemAttr']['attribute_name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
 :
	    </td>
	    <td align="left" class="tbl_chk" style="min-width:150px;">
		<?php if (count($_from = (array)$this->_tpl_vars['attribute_type'])):
    foreach ($_from as $this->_tpl_vars['keyAttrType'] => $this->_tpl_vars['itemAttrType']):
?>
		<?php $this->assign('ida', ((is_array($_tmp='c')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['itemAttr']['id_attribute']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['itemAttr']['id_attribute']))); ?>
		    <?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['itemAttrType']): ?>
			<?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['text'] || $this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['calender_widget']): ?><!--Checking field for text and date range -->
			    <?php $this->assign('attrType', 'text'); ?>
			    <input type="<?php echo $this->_tpl_vars['attrType']; ?>
" name="attrdata[c<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
]" <?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['calender_widget']): ?>class="p_dateFormat" readonly="readonly" id="dt_<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
"<?php else: ?>id="text_<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
" <?php if ($this->_tpl_vars['itemAttr']['datatype'] == 1): ?> onclick="$('#digerr<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
').html('');" onkeypress="return intonly(event,'digerr<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
');"<?php endif; ?> <?php endif; ?> value="<?php if ($this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['ida']]):  if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['calender_widget']):  echo ((is_array($_tmp=$this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['ida']])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y"));  else:  echo $this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['ida']];  endif;  endif; ?>" />
				    <div class="mrgn_btm5"><span id="digerr<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
" style="color:red;"></span></div>
			    <?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['calender_widget']): ?><!-- If a field is a datetype -->
				<input type="hidden" name="dt[c<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
]" value="<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
"/>
			    <?php endif; ?>
			<?php elseif ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['dropdown']): ?><!--Checking field for drop down -->
			    <?php $this->assign('clmnName', ((is_array($_tmp='c')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['itemAttr']['id_attribute']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['itemAttr']['id_attribute']))); ?>
			    
			    <?php if ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['university']): ?>
				<?php $this->assign('board_university', $this->_tpl_vars['sm']['university']); ?>
			    <?php elseif ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['board']): ?>
				<?php $this->assign('board_university', $this->_tpl_vars['sm']['board']); ?>
			    <?php else: ?>
				<?php $this->assign('attrLabel', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['itemAttr']['attribute_label']) : explode($_tmp, $this->_tpl_vars['itemAttr']['attribute_label']))); ?>
				<?php $this->assign('attrValues', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['itemAttr']['attribute_values']) : explode($_tmp, $this->_tpl_vars['itemAttr']['attribute_values']))); ?>
			    <?php endif; ?>
			    <select name="attrdata[c<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
]" id="dropdown_<?php echo $this->_tpl_vars['item']; ?>
" <?php if ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['university'] || $this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['board']): ?> multiple="multiple" class="p_multiauto" <?php endif; ?>>
				<?php if ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['other']): ?>
				<!-- getting other type values -->
				    <option value="">--Select--</option>
				    <?php echo smarty_function_html_options(array('values' => ((is_array($_tmp=$this->_tpl_vars['attrValues'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)),'output' => $this->_tpl_vars['attrLabel'],'selected' => $this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['clmnName']]), $this);?>

				<?php elseif ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['university'] || $this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['board']): ?>
				    				    <!-- getting university or board type values -->
				    <?php $this->assign('selBordUnivArr', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['clmnName']]) : explode($_tmp, $this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['clmnName']]))); ?>
				    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['selBordUnivArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
				    <?php $this->assign('d', $this->_tpl_vars['selBordUnivArr'][$this->_sections['i']['index']]); ?>	    
					<?php if (count($_from = (array)$this->_tpl_vars['board_university'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
					<?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['d']): ?>
					<option  selected="selected" value="<?php echo $this->_tpl_vars['d']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
				    <?php endfor; endif; ?>
				<?php endif; ?>
			    </select>
			    <input type="hidden" id="boardType" value="<?php if ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['board']): ?>1<?php elseif ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['university']): ?>2<?php endif; ?>" /><!--For board  1 ,university 2--><br />
			    <input type="hidden" name="boardOrUnivFlg_c<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
" value="<?php if ($this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['university'] || $this->_tpl_vars['itemAttr']['dropdown_values'] == $this->_tpl_vars['board_type']['board']): ?>1<?php else: ?>0<?php endif; ?>" /><!--For board or University is 1 else 0--><br />
			<?php else: ?><!--Checking field for checkbox and radios -->
			    <?php $this->assign('attrLabel', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['itemAttr']['attribute_label']) : explode($_tmp, $this->_tpl_vars['itemAttr']['attribute_label']))); ?>
			    <?php $this->assign('attrValues', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['itemAttr']['attribute_values']) : explode($_tmp, $this->_tpl_vars['itemAttr']['attribute_values']))); ?>
			    <?php if (count($_from = (array)$this->_tpl_vars['attrLabel'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
				<?php $this->assign('clmnName', ((is_array($_tmp='c')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['itemAttr']['id_attribute']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['itemAttr']['id_attribute']))); ?>
				<?php $this->assign('attrType', $this->_tpl_vars['keyAttrType']); ?>
				<?php echo smarty_function_bitwise(array('var1' => $this->_tpl_vars['attrValues'][$this->_tpl_vars['key']],'var2' => $this->_tpl_vars['sm']['selectedAttributeRes'][$this->_tpl_vars['clmnName']],'opt' => '&','assign' => 'bit'), $this);?>

				<input type="<?php echo $this->_tpl_vars['attrType']; ?>
" name="attrdata[c<?php echo $this->_tpl_vars['itemAttr']['id_attribute']; ?>
]<?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['checkbox']): ?>[]<?php endif; ?>" id="<?php if ($this->_tpl_vars['itemAttr']['entry_type'] == $this->_tpl_vars['attribute_type']['radio']): ?>radio_<?php echo $this->_tpl_vars['item'];  else: ?>checkbox_<?php echo $this->_tpl_vars['item'];  endif; ?>" value="<?php echo $this->_tpl_vars['attrValues'][$this->_tpl_vars['key']]; ?>
" <?php if ($this->_tpl_vars['bit'] != 0): ?>checked=checked<?php endif; ?> /><?php echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
<!--For checkbox and radio format--><br />
			    <?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
		    <?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
	    </td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>
<!-- Template: admin/cms/attribute.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-08-19 10:59:00
         compiled from admin/student/studentviewdata.tpl.html */ ?>

<!-- Template: admin/student/studentviewdata.tpl.html Start 19/08/2017 10:59:00 --> 
 <html>
    <head>
    </head>
    <body>
    <div>
        <table cellspacing="0" cellpadding="5"  border="0" align="center" style="border:solid navy thick; background-color:#FFF8DC; ">
            <tr>
                <td colspan="2"><h3 style="color: darkblue " align="center"><u>STUDENT DETAILS</u></h3></td>
            </tr>
            <?php unset($this->_sections['lst']);
$this->_sections['lst']['name'] = 'lst';
$this->_sections['lst']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lst']['show'] = true;
$this->_sections['lst']['max'] = $this->_sections['lst']['loop'];
$this->_sections['lst']['step'] = 1;
$this->_sections['lst']['start'] = $this->_sections['lst']['step'] > 0 ? 0 : $this->_sections['lst']['loop']-1;
if ($this->_sections['lst']['show']) {
    $this->_sections['lst']['total'] = $this->_sections['lst']['loop'];
    if ($this->_sections['lst']['total'] == 0)
        $this->_sections['lst']['show'] = false;
} else
    $this->_sections['lst']['total'] = 0;
if ($this->_sections['lst']['show']):

            for ($this->_sections['lst']['index'] = $this->_sections['lst']['start'], $this->_sections['lst']['iteration'] = 1;
                 $this->_sections['lst']['iteration'] <= $this->_sections['lst']['total'];
                 $this->_sections['lst']['index'] += $this->_sections['lst']['step'], $this->_sections['lst']['iteration']++):
$this->_sections['lst']['rownum'] = $this->_sections['lst']['iteration'];
$this->_sections['lst']['index_prev'] = $this->_sections['lst']['index'] - $this->_sections['lst']['step'];
$this->_sections['lst']['index_next'] = $this->_sections['lst']['index'] + $this->_sections['lst']['step'];
$this->_sections['lst']['first']      = ($this->_sections['lst']['iteration'] == 1);
$this->_sections['lst']['last']       = ($this->_sections['lst']['iteration'] == $this->_sections['lst']['total']);
?>
            <?php $this->assign('x', $this->_tpl_vars['sm']['data'][$this->_sections['lst']['index']]); ?>
            <tr>
                <td align="center" width="40%"><b>FIRSTNAME:</b></td>
                <td><?php echo $this->_tpl_vars['x']['firstname']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>LASTNAME:</b></td>
                <td><?php echo $this->_tpl_vars['x']['lastname']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>MOBILE:</b></td>
                <td><?php echo $this->_tpl_vars['x']['mobile']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>EMAIL:</b></td>
                <td><?php echo $this->_tpl_vars['x']['email']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>GENDER:</b></td>
                <td><?php echo $this->_tpl_vars['x']['gender']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>ADDRESS:</b></td>
                <td><?php echo $this->_tpl_vars['x']['address']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>HOBBY:</b></td>
                <td><?php echo $this->_tpl_vars['x']['hobbies']; ?>
</td>
            </tr>
            <tr>
                <td align="center" width="40%"><b>IMAGE:</b></td>
                <td><img id='prfimg' src='http://manoranjan.afixiindia.com/flexytiny_new/image/practice/thumb/<?php echo $this->_tpl_vars['x']['id']; ?>
_<?php echo $this->_tpl_vars['x']['profile_img']; ?>
'></td>
           
            <?php endfor; endif; ?>
        </table>
    </div>
    </body>
</html>

<!-- Template: admin/student/studentviewdata.tpl.html End --> 
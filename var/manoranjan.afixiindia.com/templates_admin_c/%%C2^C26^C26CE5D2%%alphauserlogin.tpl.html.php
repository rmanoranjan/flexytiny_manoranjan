<?php /* Smarty version 2.6.7, created on 2017-08-28 09:52:59
         compiled from admin/user/alphauserlogin.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'admin/user/alphauserlogin.tpl.html', 63, false),array('modifier', 'date_format', 'admin/user/alphauserlogin.tpl.html', 87, false),)), $this); ?>

<!-- Template: admin/user/alphauserlogin.tpl.html Start 28/08/2017 09:52:59 --> 
 <!--done by gayatree starts-->
<?php echo '
<style type="text/css">
  .alfauser_admin{display:none; }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    jQuery(\'.widget .tools .icon-chevron-down\').click(function () {
        var el = jQuery(this).parents(".widget").children(".widget-body");
        if (jQuery(this).hasClass("icon-chevron-down")) {
            jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
            el.slideUp(200);
         } else {
            jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
            el.slideDown(200);
         }
     });

    jQuery(\'.widget .tools .icon-remove\').click(function () {
        jQuery(this).parents(".widget").parent().remove();
     });
   });
</script>
'; ?>


<!-- BEGIN ADVANCED TABLE widget-->

<div class="row-fluid">
  <div class="span12">
    <div id="user_getUserLogin">
      <div id="sherr"></div>
      <?php $this->assign('x', $this->_tpl_vars['sm']['c']); ?>
      <!-- BEGIN EXAMPLE TABLE widget-->
      <div class="widget red">
        <div class="widget-title">
          
          <span class="tools">
            <a class="icon-chevron-down" href="javascript:;"></a>
            <a class="icon-remove" href="javascript:;"></a>
          </span>
          <h4><i class="icon-reorder"></i><?php if ($this->_tpl_vars['sm']['search']): ?>Search Result-<?php endif; ?> Login User Detail</h4>
          <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
          <div  class="fltrht pdngin">
            <?php if ($this->_tpl_vars['sm']['list']): ?>
            <input type="button" class="btn btn-success" name="search" value="Delete" onclick="loginDelete('<?php echo $this->_tpl_vars['x']; ?>
');" />
            <?php endif; ?>
            <input type="button" class="btn btn-success" name="search" value="Search" onclick="searchLogin();" />
            <?php if ($this->_tpl_vars['sm']['sfld']): ?>
            <a class="btn" href="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/user/loginUsers">Main List</a>
            <?php endif; ?>
          </div>
          <?php endif; ?>
          <div class="clear"></div>
          

        </div>
        <div class="widget-body">
          <input type='hidden' value="<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
" id="total_record_m" />
          <input type="hidden" id="q_start_multi" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
" />
          <input type="hidden" id="limit_multi" value="<?php echo $this->_tpl_vars['sm']['limit']; ?>
" />
          <input type="hidden" id="rec_page_multi" value="<?php echo count($this->_tpl_vars['sm']['list']); ?>
" />
          <table class="table table-striped table-bordered" id="sample_1">
            <?php if ($this->_tpl_vars['sm']['list']): ?>
            <thead>
              <tr>

                <th><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"  id="chkall" name="chkall" /><label style="margin-left:20px; margin-top: -16px;">Select All</label></th>
                <th>Username</th>
                <th class="hidden-phone">IP Address</th>
                <th class="hidden-phone">Date</th>
                <th class="hidden-phone">Email Id</th>

              </tr>
            </thead>
            <?php endif; ?>


            <tbody>
              <?php $this->_foreach['user_detail_table'] = array('total' => count($_from = (array)$this->_tpl_vars['sm']['list']), 'iteration' => 0);
if ($this->_foreach['user_detail_table']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['user_detail_table']['iteration']++;
?>
              <tr class="odd gradeX" <?php if (($this->_foreach['user_detail_table']['iteration']-1)%2 == 0): ?>even<?php endif; ?>>
                  <td width="8%"><input type="checkbox" class="checkboxes" id="id_chk<?php echo $this->_tpl_vars['item']['id_login']; ?>
" value="<?php echo $this->_tpl_vars['item']['id_login']; ?>
" name="chk[]" onclick="unSelect()" /></td>
                <td><?php echo $this->_tpl_vars['item']['username']; ?>
</td>
                <!--<td class="hidden-phone"><a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>-->
                <td class="hidden-phone"><?php echo $this->_tpl_vars['item']['ip']; ?>
</td>
                <td class="center hidden-phone"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['date_login'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")); ?>
</td>
                <td class="hidden-phone"><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
              </tr>
              <?php endforeach; else: ?>
              <tr>
                <td colspan="2">No record found.</td>
              </tr>
              <?php endif; unset($_from); ?>
            </tbody>
            <!--                            <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>soa bal</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@yahoo.com">soa bal@yahoo.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">1.12.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>ram sag</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">soa bal@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">7.2.2013</td>
                                            <td class="hidden-phone"><span class="label label-inverse">Blocked</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>durlab</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">test@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">03.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>durlab</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>sumon</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>bombi</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>ABC ho</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>test</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>soa bal</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">soa bal@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">03.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-inverse">Blocked</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>test</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">test@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">03.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>goop</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>sumon</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">01.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-inverse">Blocked</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>woeri</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">09.10.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>soa bal</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">soa bal@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">9.12.2013</td>
                                            <td class="hidden-phone"><span class="label label-inverse">Blocked</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>woeri</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">test@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">14.12.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>uirer</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">13.11.2013</td>
                                            <td class="hidden-phone"><span class="label label-warning">Suspended</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>samsu</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">17.11.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>dipsdf</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">05.04.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>soa bal</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">soa bal@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">03.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-inverse">Blocked</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>hilor</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">test@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">03.07.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>test</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">19.12.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>botu</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">17.12.2013</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                            <td>sumon</td>
                                            <td class="hidden-phone"><a href="mailto:soa bal@gmail.com">lorem-ip@gmail.com</a></td>
                                            <td class="hidden-phone">33</td>
                                            <td class="center hidden-phone">15.11.2011</td>
                                            <td class="hidden-phone"><span class="label label-success">Approved</span></td>
                                        </tr>
                                        </tbody>-->
          </table>
        </div>
        <div align="center">
          <?php if ($this->_tpl_vars['sm']['type'] == 'advance'): ?>
          <div class="pagination_adv">
            <?php echo $this->_tpl_vars['sm']['next_prev']->generateadv(); ?>

          </div>
          <?php elseif ($this->_tpl_vars['sm']['type'] == 'box'): ?>
          <div class="pagination_box">
            <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
          </div>
          <?php elseif ($this->_tpl_vars['sm']['type'] == 'normal'): ?>
          <div class="pagination_box">
            <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
          </div>
          <?php elseif ($this->_tpl_vars['sm']['type'] == 'nextprev'): ?>
          <div class="pagination_box">
            <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->onlynextprev(); ?>
</div>
          </div>
          <?php elseif ($this->_tpl_vars['sm']['type'] == 'extra'): ?>
          <div class="pagination_box">
            <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generateextra(); ?>
</div>
          </div>
          <?php else: ?>
          <?php if ($this->_tpl_vars['sm']['type'] != 'no'): ?>
          <div>
            <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
          </div>
          <?php endif; ?>
          <?php endif; ?>
        </div>




      </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
  </div>
</div>



<!--done by gayatree ends-->
<div id="user_getUserLogin" class="alfauser_admin">
  <div id="sherr"></div>
  <?php $this->assign('x', $this->_tpl_vars['sm']['c']); ?>
  <div class="makebox center" id="dv1" style="width:100%">
    <div class="headprt settheme">
      <div class="mdl">
        <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
        <div  class="fltrht">
          <?php if ($this->_tpl_vars['sm']['list']): ?>
          <input type="button" class="buton" name="search" value="Delete" onclick="loginDelete('<?php echo $this->_tpl_vars['x']; ?>
');" />
          <?php endif; ?>
          <input type="button" class="buton" name="search" value="Search" onclick="searchLogin();" />
          <?php if ($this->_tpl_vars['sm']['sfld']): ?>
          <a class="buton" href="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/user/loginUsers">Main List</a>
          <?php endif; ?>
        </div>
        <?php endif; ?>
        <span> <?php if ($this->_tpl_vars['sm']['search']): ?>Search Result-<?php endif; ?> Login User Detail</span>
        <div class="clear"></div>
      </div>
    </div>
    <div class="bodyprt">
      <input type='hidden' value="<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
" id="total_record_m" />
      <input type="hidden" id="q_start_multi" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
" />
      <input type="hidden" id="limit_multi" value="<?php echo $this->_tpl_vars['sm']['limit']; ?>
" />
      <input type="hidden" id="rec_page_multi" value="<?php echo count($this->_tpl_vars['sm']['list']); ?>
" />
      <table width="97%" border="0" align="center" class="tbl_listing">
        <?php if ($this->_tpl_vars['sm']['list']): ?>
        <thead>
          <tr>
            <th><input type="checkbox" id="chkall" name="chkall"/>Select All</th>
            <th>Username</th>
            <th>IP Address</th>
            <th>Date</th>
            <th>Email Id</th>
          </tr>
        </thead>
        <?php endif; ?>
        <tbody>
          <?php $this->_foreach['user_detail_table'] = array('total' => count($_from = (array)$this->_tpl_vars['sm']['list']), 'iteration' => 0);
if ($this->_foreach['user_detail_table']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['user_detail_table']['iteration']++;
?>
          <tr class="<?php if (($this->_foreach['user_detail_table']['iteration']-1)%2 == 0): ?>even<?php endif; ?>">
            <td style="vertical-align:middle;"><input type="checkbox" id="id_chk<?php echo $this->_tpl_vars['item']['id_login']; ?>
" value="<?php echo $this->_tpl_vars['item']['id_login']; ?>
" name="chk[]" onclick="unSelect()" /></td>
            <td style="vertical-align:middle;"><?php echo $this->_tpl_vars['item']['username']; ?>
</td>
            <td style="vertical-align:middle;"><?php echo $this->_tpl_vars['item']['ip']; ?>
</td>
            <td style="vertical-align:middle;"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['date_login'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")); ?>
</td>
            <td style="vertical-align:middle;"><?php echo $this->_tpl_vars['item']['email']; ?>
</td>
          </tr>
          <?php endforeach; else: ?>
          <tr>
            <td colspan="2">No record found.</td>
          </tr>
          <?php endif; unset($_from); ?>
        </tbody>
      </table>
    </div>

    <br />
    <div align="center">
      <?php if ($this->_tpl_vars['sm']['type'] == 'advance'): ?>
      <div class="pagination_adv">
        <?php echo $this->_tpl_vars['sm']['next_prev']->generateadv(); ?>

      </div>
      <?php elseif ($this->_tpl_vars['sm']['type'] == 'box'): ?>
      <div class="pagination_box">
        <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
      </div>
      <?php elseif ($this->_tpl_vars['sm']['type'] == 'normal'): ?>
      <div class="pagination_box">
        <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
      </div>
      <?php elseif ($this->_tpl_vars['sm']['type'] == 'nextprev'): ?>
      <div class="pagination_box">
        <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->onlynextprev(); ?>
</div>
      </div>
      <?php elseif ($this->_tpl_vars['sm']['type'] == 'extra'): ?>
      <div class="pagination_box">
        <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generateextra(); ?>
</div>
      </div>
      <?php else: ?>
      <?php if ($this->_tpl_vars['sm']['type'] != 'no'): ?>
      <div>
        <div align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>
</div>
      </div>
      <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php echo '
<script type="text/javascript">
  '; ?>

    var from_search = "<?php echo $this->_tpl_vars['sm']['sfld']; ?>
";
    var uri = "<?php echo $this->_tpl_vars['sm']['url']; ?>
";
    var qstart = parseInt("<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
");
    var total_rec = parseInt("<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
");
    var limit = parseInt("<?php echo $this->_tpl_vars['sm']['limit']; ?>
");
    var rec_page = parseInt("<?php echo count($this->_tpl_vars['sm']['list']); ?>
");
    <?php echo '
    function unSelect() {
      if ($(\'[id^="id_chk"]\').length == $(\'[id^="id_chk"]:checked\').length)
        $(\'#chkall\').attr(\'checked\', \'checked\');
      else
        $(\'#chkall\').removeAttr(\'checked\');
     }
    function loginDelete(character) {
      var c;
      var choice = confirm(\'Do you want to delete?\');
      if (choice) {
        if ($(\'#chkall\').attr(\'checked\')) {
          if (eval(qstart + limit) >= total_rec) {
            qstart = eval(qstart - limit);
           }
          else {
            qstart = qstart;
           }
         } else {
          if (qstart) {
            qstart = qstart;
           } else {
            qstart = 0;
           }
          if (rec_page == 1 && qstart) {
            qstart = eval(qstart - limit);
           }
         }
        if (qstart < 0)
          qstart = 0;
        var x = \'\';
        $("input[id^=id_chk]:checked").each(function () {
          x = x + \',\' + $(this).val();
         });
        if (x != "") {
          $(\'#showerr\').html(\'\');
          var url = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
          $.post(url, {"page": "user", "choice": "deleteUserLogin", ce: 0, loginid: x, c: character, "qstart": qstart, \'sfld\': from_search, \'uri\': uri }, function (res) {
            $(\'#user_getUserLogin\').html(res);
            css_even_odd();
            messageShow("Deleted successfully.");
           });
         } else {
          $(\'#sherr\').html(\'<center><font color="red">Check one checkbox.</font></center>\');
         }
       }
     }
    $("#chkall").click(function ()
    {
      if ($("#chkall").attr(\'checked\'))
        $(\'[id^=id_chk]\').attr(\'checked\', \'checked\');
      else
        $(\'[id^=id_chk]\').removeAttr(\'checked\');
     });
    function searchLogin() {
      var url = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
      $.fancybox.showActivity();
      $.post(url, {"page": "user", "choice": "searchTpl", "ce": 0 }, function (res) {
        show_fancybox(res);
        css_even_odd();
       });
     }
</script>
'; ?>

<!-- Template: admin/user/alphauserlogin.tpl.html End --> 
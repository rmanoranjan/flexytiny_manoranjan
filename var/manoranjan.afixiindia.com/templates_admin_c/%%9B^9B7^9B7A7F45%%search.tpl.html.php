<?php /* Smarty version 2.6.7, created on 2017-09-15 13:14:58
         compiled from admin/cms/search.tpl.html */ ?>

<!-- Template: admin/cms/search.tpl.html Start 15/09/2017 13:14:58 --> 
 <?php echo '
<script type="text/javascript">
	function searchList(i){
	    if(i){
            var searchval = $("#search").serialize();
		$.post(siteurl,{"page" : "cms", "choice" : "listing", \'psearch\' : 1,"searchval":searchval, \'ce\' : 0 },function(res){
		    $("#cms_listing").html(res);
		    css_even_odd();
		 });
		
	     }else{
         $.post(siteurl,{"page" : "cms", "choice" : "listing", \'psearch\' : 1,\'ce\' : 0,"reset":1 },function(res){
		    $("#cms_listing").html(res);
		    css_even_odd();
		 });
		$(\'#search\')[0].reset();
	     }
	 }
 </script>
'; ?>

<div id="page-wraper">
     <div class="center">
           <div class="row-fluid">
                    <div class="span12">
                         <div class="" id="search_box">
                        <div class="widget red">
                         
                            <div class="widget-title">
                               <span class="tools">
             <a href="javascript:;" class="icon-chevron-down"></a>
             <a href="javascript:;" class="icon-remove"></a>
         </span>
                               <h4><i class="icon-reorder"></i>Search Content</h4>
                            </div>
                            <div class="widget-body">
                              <form name="search" id="search" action="" method="post" onsubmit="return validateFormElement();" class="form-horizontal">
                                 
                            <div class="control-group">
                                <label class="control-label">Name   </label>
                                <div class="controls">
                                  <input class="span6 " id="searchq" type="text" name="searchq"   />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Code </label>
                                <div class="controls">
                                     <input class="span6 " id="code" type="text" name="code"   />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Category </label>
                                <div class="controls">
                                     <input class="span6 " id="cat" type="text" name="category"   />
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <input class="btn btn-success" type="button" value="Search" onclick="searchList(1);" />
				<input class="btn btn-success" type="reset" value="Reset" onclick="searchList(0);"/>
                            </div>
                                   
                            </form>
                              
                            </div>
                        </div>
                          </div>
                    </div>
             </div>
       </div>
    <div id="cms_listing"> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/cms/listing.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>

<!-- Template: admin/cms/search.tpl.html End --> 
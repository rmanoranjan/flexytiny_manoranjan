<?php /* Smarty version 2.6.7, created on 2017-07-03 15:23:48
         compiled from admin/product/add.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/product/add.tpl.html', 408, false),array('modifier', 'count', 'admin/product/add.tpl.html', 467, false),)), $this); ?>

<!-- Template: admin/product/add.tpl.html Start 03/07/2017 15:23:48 --> 
 <?php $this->assign('image_limit', $this->_tpl_vars['util']->get_values_from_config('IMAGE')); ?>
<?php echo '
 <script>
$.validator.prototype.checkForm = function() {
 //overriden in a specific page
this.prepareForm();
for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
 for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
 this.check(this.findByName(elements[i].name)[cnt]);
 }
 } else {
this.check(elements[i]);
 }
 }
return this.valid();
 };

$.validator.prototype.showErrors=function(errors) {
if(errors) {
// add items to error list and map
$.extend( this.errorMap, errors );
this.errorList = [];
for ( var name in errors ) {
this.errorList.push({
message: errors[name],
/* NOTE THAT IM COMMENTING THIS OUT
element: this.findByName(name)[0]
*/
element: this.findById(name)[0]
 });
 }
// remove items from success list
this.successList = $.grep( this.successList, function(element) {
return !(element.name in errors);
 });
 }
this.settings.showErrors
? this.settings.showErrors.call( this, this.errorMap, this.errorList )
: this.defaultShowErrors();
 };

$.validator.prototype.findById=function( id ) {
// select by name and filter by form for performance over form.find(“[id=…]”)
var form = this.currentForm;
return $(document.getElementById(id)).map(function(index, element) {
return element.form == form && element.id == id && element || null;
 });
 };
</script>
<style type="text/css">
label.error{
font-size:12px;
display:block;
 }
.addproductfancy_admin{display:none; }
</style>
<script type="text/javascript">
$(document).ready(function(){ 
var prod_name = "';  echo $this->_tpl_vars['sm']['res']['name'];  echo '";
var code = "';  echo $this->_tpl_vars['sm']['res']['code'];  echo '";
var pname = "';  echo $this->_tpl_vars['sm']['pname'];  echo '";
$(\'#name_product\').val(prod_name);
$(\'#code_product\').val(code);
$("#pname1").val(pname);
 });
 var extArray = new Array("jpg","gif", "png","jpeg","bmp");

function validateProduct() {
if($(\'#product_error_name\').html() || $(\'#product_error_code\').html()){
$(\'#product_error_name\').html(\'\');
$(\'#product_error_code\').html(\'\');
 }
var validator=$("#adminaddproduct").validate({
rules: {
"product[name]": {
required:true
 },
"product[description]":{
required: true
 },
"product[code]": {
required:true
 },
"image[]": {
 required:true
 }
 },
messages: {
"product[name]":{
required:flexymsg.required
 },
"product[description]":{
required:flexymsg.required
 },
"product[code]": {
required:flexymsg.required
 },
"image[]": {
required:flexymsg.required
 }
 }
 });
return validator.form();
 }

    function callbackFun(response) {
	if(response==\'no\'){
	    $(\'#product_error_code\').html(\'<font color="red">This code already exist.Choose another one</font>\');
	    $(\'#code_product\').val(\'\').focus();
	 }else if(response==\'noname\'){
	    $(\'#product_error_name\').html(\'<font color="red">This name already exist.Choose another one</font>\');
	    $(\'#name_product\').val(\'\').focus();
	 }else{
	    var id_product = $(\'#id_product\').val();
	    if(id_product){
		if($(\'#caterory_reflex\').val()){
		    window.location.href = siteurl + "/category/listing/msg/2";
		 }else{
		    messageShow("Product updated successfully.");//$(\'#succ_msg\').html(\'<font color="red">\'+"Product updated successfully"+\'</font>\');
		 }
	     }else{
		if($(\'#caterory_reflex\').val()){
		    var newUrl = siteurl + "category/listing/";
		    window.location.href = newUrl;
		 }else{
		    messageShow("Product added successfully.");//$(\'#succ_msg\').html(\'<font color="red">\'+"Product added successfully"+\'</font>\');
		 }
	     }
	    $.fancybox.close();
	    $(\'#product_listing\').html(response);

	 }
     }
    function convertMe(obj){
	var cval=obj.value;
	var pattern=/[-\\s]/g;
	var nval=cval.replace(pattern,"");
	nval = nval.toLowerCase();
	if($(\'#product_error_name\').html() || $(\'#product_error_code\').html()){
	    $(\'#product_error_name\').html(\'\');
	    $(\'#product_error_code\').html(\'\');
	 }
	'; ?>

	var pname ="<?php echo $this->_tpl_vars['sm']['res']['name']; ?>
";
	<?php echo ';
	var chkV= $(\'#name_product\').val(); 
	if(pname != chkV){
	    chkNameCodeExit(\'name\',nval,"code_product");
	 }
     }

    var code1,kc,cc;
    function chkSpecialChars(e) {
	code1 = e.which;
	if(!code1){
	    code1=e.keyCode;
	 }
	kc=e.keyCode;
	cc=e.charCode;
	if(kc==8 && cc==0){//For Backspace
	    return true;
	 }else if(kc==46 && cc==0){//For Delete
	    return true;
	 }else if(kc==36 && cc==0){//For Home
	    return true;
	 }else if(kc==35 && cc==0){//For End
	    return true;
	 }else if(kc==39 && cc==0){//For Forward Key
	    return true;
	 }else if(kc==37 && cc==0){//For Backward Key
	    return true;
	 }else if(kc==9 && cc==0){//For Tab
	    return true;
	 }else if(kc==46 || cc==46){
	    return true;
	 }else if(kc==95 || cc==95){
	    return true;
	 }else if(kc==32 || cc==32){
	    return false;
	 }else if(97<=cc && cc<=122){
	    return true;
	 }else if(97<=kc && kc<=122){
	    return true;
	 }else if(65<=cc && cc<=90){
	    return true;
	 }else if(65<=kc && kc<=90){
	    return true;
	 }else if(48<=cc && cc<=57){
	    return true;
	 }else if(48<=kc && kc<=57){
	    return true;
	 }else{
	    return false;
	 }	
     }

    function chkNameCodeExit(chkName, fval, target){
	if($(\'#product_error_code\').html()){
	    $(\'#product_error_code\').html(\'\');
	 }
	if(chkName == \'name\'){
	    var chkVal	= $(\'#name_product\').val();
	 }else{
	    var chkVal	= $(\'#code_product\').val();
	 }
	var id_category	= ($(\'#id_category\').val() == undefined) ? "" : $(\'#id_category\').val();
	var id_product	= ($(\'#id_product\').val() == undefined) ? "" : $(\'#id_product\').val() ;
	if(chkVal){
	    $.post(siteurl,{"page":"product","choice":"checkValue",ce:0,\'keyName\':chkName, \'keyValue\':chkVal,\'id_category\':id_category,\'id_product\' : id_product },function(res){
		if(res == \'exist\'){
		    if(chkName == \'name\'){
			$("#code_product").val(\'\');
		     }
		    $(\'#product_error_\'+chkName).html("<font color=\'red\'>This "+chkName+" already exist.Choose another one</font>");
		    $("#" + chkName + "_product").val(\'\').focus();
		 }else{
		    if(fval != \'\' && chkName == \'name\'){
			$("#"+target).val(fval);
		     }
		    $(\'#product_error_\' + chkName).html(\'\');		    
		 }
	     });
	 }else{
	    return false;
	 }
     }

    var imgId = 1;
    function showPreviewImage(imgId){
	$(".login_btn").attr(\'disabled\', \'disabled\');//code to avoid submit before the image is loaded completely.
	var imgName=$(\'#image\'+imgId).val();
	if(imgName){
	    if(checkExt(imgId)){
		var prevUrl = siteurl + "product/previewImage/imgId/"+imgId+"/ce/0/";
		$.ajaxFileUpload({
		    url : prevUrl,
		    secureuri:false,
		    fileElementId:\'image\'+imgId,
		    dataType: \'json\',		    
		    complete: function (data, status){	
			var z = data.responseText;
			var img="<img src=\'http://manoranjan.afixiindia.com/flexytiny_new/image/preview/thumb/"+z+"\' width=\'100\' height=\'100\' />";
			$(\'#prev_img\'+imgId).val(z);
			$(\'#hid_image\').val(z);
			$(\'#r\'+imgId).html(img+"<span id=\'remove_link\'"+imgId+"\'><a href=\'javascript:void(0);\' onClick=\'removeFormField( "+imgId+"); return false;\'>Remove</a></span>");

			var count_img = parseInt($(\'#imagecnt\').val());
			if(count_img){
			    var count_img = ($(\'.imageclass1\').length + count_img);
			 }else{
			    var count_img=$(\'.imageclass1\').length;	
			 }
			var imgLimit = parseInt($("#imgLimit").html());
			if(count_img == imgLimit){
			    $(\'#count_img_num\').hide();
			 }
			$(".login_btn").removeAttr("disabled");
		     }
		 });
	     }
	 }else{
	    return false;
	 }
     }
    function checkExt(imgId) {	    
	var tot_file = $(\'#adminaddproduct\').find(\'input[class="imageclass"]\');
	if (tot_file.length < 1){
	    return false;
	 }else{
	    var form_submit = false, j = 0, i = 0, file_val = new Array();
	    for(i=0; i < tot_file.length; i++){
		file_val[i] = $(\'#adminaddproduct\').find(\'input[class="imageclass"]\').eq(i).val();
		if(file_val[i] == ""){
		    continue;
		 }
		form_submit = false;
		file_val[i] = file_val[i].toLowerCase();
		var chk = (/[.]/.exec(file_val[i])) ? /[^.]+$/.exec(file_val[i]) : undefined;
		for (var k = 0; k < extArray.length; k++) {
		    if (extArray[k] == chk) {
			form_submit = true;
			if($(\'.invalid_ext\').html()!=null){
			    $(\'.invalid_ext\').remove();
			 }
		     }
		 }
		if (form_submit == false) {
		    j=i;
		 }
	     }
	 }
	if(form_submit){
	    return true;
	 }else{	    
	    var sp = \'<div class="invalid_ext">This is not a valid file extension.</div>\';
	    alert("Please only upload ."+ (extArray.join("/")) + " files\\nPlease select a new file to upload and submit again.");
	    if($(\'.invalid_ext\').html()==null){
		$(\'#adminaddproduct\').find(\'input[class="imageclass"]\').eq(j).after(sp);
	     }
	    $(\'#image\'+imgId).val("");
	    return false;
	 }
     }

    
    function addFormField() {
	var id = $("#id").val();
//	var tot_file = $(\'#adminaddproduct\').find(\'input[class="imageclass1"]\');
//	var file_val = new Array();alert(file_val);
//	for(i=0; i<tot_file.length; i++){
//	    file_val[i] = $(\'#adminaddproduct\').find(\'input[class="imageclass1"]\').eq(i).val();
//	    if(file_val[i] == ""){
//		$(\'#r\'+parseInt(id-1)).html(\'<font color="red"><Br>This field is required</span>\');
//		return false;
//	     }
//	 }
	$("#divTxt").append("<p id=\'row" + id + "\'><input type=\'file\' size=\'20\' name=\'image[]\' class=\'imageclass\' id=\'image" + id + "\'  onchange=\'showPreviewImage("+id+");\'><input type=\'hidden\' name=\'prev_img[]\' id=\'prev_img" + id + "\' class=\'imageclass1\' ><span id=\'r" + id + "\'></span><p>");
	id = (id - 1) + 2;
	$("#id").val(id);
     }

    function removeFormField(id) {
	if($(\'#prev_img\'+id).val()){
	    $.post(siteurl,{"page":"product","choice":"unlinkPreviewImage",\'img_name\':$(\'#prev_img\'+id).val(),ce:0 },function(res){ });
	 }
	if($(\'#image\'+id).val()){
	    $(\'#image\'+id).val(\'\');
	 }
	var file_val=new Array();
	var tot_file = $(\'#adminaddproduct\').find(\'input[class="imageclass1"]\');
	for(i=0;i<tot_file.length;i++){
	    file_val[i] = $(\'#adminaddproduct\').find(\'input[class="imageclass1"]\').eq(i).val();
	 }
	$(\'#prev_img\'+id).val(\'\');
	if(file_val.length==1){
	    $(\'#r\'+id).html(\'\');
	 }else{
	    $(\'#row\'+id).remove();
	 }
	var imgLimit = parseInt($("#imgLimit").html());
	if($(\'.imageclass1\').length <= imgLimit){
	    $(\'#count_img_num\').show();
	 }
     }

    function deleteProductImage(id_image,image_name){
	if(confirm("Are you sure to delete this image ?")){
	    $.post(siteurl,{"page":"product", "choice":"deleteProductImage", "ce":0, "id_image":id_image,"image_name":image_name },function(res){
		if(res == 1){
		    $(\'#imagecnt\').val($(\'#imagecnt\').val() - 1);
		    $("#imgEdit_"+id_image).remove();
		    $("#count_img_num").show();
		 }else{
		    return false;
		 }
	     });
	 }else{
	    return false;
	 }
     }		
</script>

'; ?>

<!--done by gayatree starts-->
<div id="dv2">
    <div style="width:600px;">
      <div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i><?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?> Product</h4>
<!--                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>-->
                            </div>
                            <div class="widget-body">
                              <form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/product/<?php if ($this->_tpl_vars['sm']['res']): ?>updateProduct<?php else: ?>insertProduct<?php endif; ?>/ce/0/" name="adminaddproduct" id="adminaddproduct" enctype="multipart/form-data" method="post" onsubmit="return AsyncUpload.submitForm(this, validateProduct, callbackFun);">
                                <input type="hidden" name="product[image_type]" value="2" />
                <input type="hidden" name="product[id_user]" value="<?php echo $_SESSION['id_user']; ?>
" /> 
                <input type="hidden" name="qstart" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
" />   
                <input type="hidden" id="referer" name='referer' value="<?php echo $this->_tpl_vars['sm']['referer']; ?>
" />
                <input type="hidden" name="id_product" id="id_product" value="<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
" />
                <input type="hidden" name="pro_cod" value="<?php echo $this->_tpl_vars['sm']['res']['code']; ?>
" />
								<input type="hidden" name="pname" id="pname1" value="" />
								<input type="hidden" name="product_category" id="product_cat" value="<?php echo $this->_tpl_vars['sm']['product_category']; ?>
" />
                                <table class="table table-striped">
                                  <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
		    <input type='hidden' value='<?php echo $this->_tpl_vars['sm']['caterory_reflex']; ?>
' name="caterory_reflex" id="caterory_reflex"/>
<!--                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>-->
                                    <tbody>
                                    <tr>
                                        <td>Category :</td>
                                        <td>
					<?php if (! $this->_tpl_vars['sm']['cat_flag']): ?>
			    <select name="product[id_category]" id="id_category">
				<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['category'],'selected' => $this->_tpl_vars['sm']['id_category']), $this);?>

			    </select> 
					<?php else: ?>
					<?php $this->assign('indx', $this->_tpl_vars['sm']['id_category']); ?>
					<?php echo $this->_tpl_vars['sm']['category'][$this->_tpl_vars['indx']]; ?>

					<input type="hidden" name="product[id_category]" value="<?php echo $this->_tpl_vars['indx']; ?>
" id="id_category"/>
					<?php endif; ?> 
					
<!--					<select name="product[id_category]" id="id_category" multiple="multiple" style="display: none;" value="name[]"></select>-->
			</td>
<!--                                        <td>Otto</td>
                                        <td>@mdo</td>-->
                                    </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td>Name :</td>
                                        <td>
			    <input type="text" class="txt" name="product[name]" id="name_product" value="" <?php if (! $this->_tpl_vars['sm']['res']['code']): ?>onblur="convertMe(this);"<?php else: ?>onblur="chkNameCodeExit('name','','');"<?php endif; ?> />
				
			    <span id='product_error_name'></span>
			</td>
<!--                                        <td>Thornton</td>
                                        <td>@fat</td>-->
                                    </tr>
                                    <?php if (! $this->_tpl_vars['sm']['res']['code']): ?>
                                    <tr>
                                        <td>Code :</td>
                                        <td>
			    <input type="text" name="product[code]" id="code_product" value="" onblur="chkNameCodeExit('code', '', '');" onkeypress="return chkSpecialChars(event);"/>
			   
			    <span id='product_error_code'></span>
			</td>
<!--                                        <td>the Bird</td>
                                        <td>@twitter</td>-->
                                    </tr>
                                    <?php endif; ?>
                                    <tr>
			<td>Description :</td>
			<td>
			    <textarea name="product[description]" id="description" ><?php echo $this->_tpl_vars['sm']['res']['description']; ?>
</textarea>
			</td>
		    </tr>
            <?php if ($this->_tpl_vars['sm']['res']['video']): ?>
		    <tr>
			<td>Current Video:</td>
			<td>
			    <?php echo $this->_tpl_vars['sm']['res']['video']; ?>

			    <a href="javascript:void(0);" onclick="delete_product_video('<?php echo $this->_tpl_vars['sm']['res']['code_product']; ?>
','<?php echo $this->_tpl_vars['sm']['res']['video']; ?>
','<?php echo $this->_tpl_vars['sm']['res']['id_product']; ?>
');">Delete</a>
			</td>
		    </tr>
		    <tr>
			<td>Update Video:</td>
			<td><input type="file" name="video1" id="video1" /></td>
		    </tr>
		    <?php endif; ?>
            <tr>
			<td colspan="2" align="center">Maximum <span id="imgLimit"><?php echo $this->_tpl_vars['image_limit']['number']; ?>
</span> number of image can be uploaded.</td>
		    </tr>
            <tr>
<!--		    <input type="hidden" name="imagecnt" id="imagecnt" value="<?php echo count($this->_tpl_vars['sm']['imgs']); ?>
"/>-->
		    		    <?php $this->assign('mulImgCnt', 0); ?>
				<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['imgs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
			    <?php $this->assign('im', $this->_tpl_vars['sm']['imgs'][$this->_sections['cur']['index']]); ?>
					<?php if ($this->_tpl_vars['im']['image_name']): ?>
					<?php $this->assign('mulImgCnt', $this->_tpl_vars['mulImgCnt']+1); ?>
					<?php endif; ?>
			    <?php endfor; endif; ?>
				<input type="hidden" name="imagecnt" id="imagecnt" value="<?php echo $this->_tpl_vars['mulImgCnt']; ?>
"/>
		    <td>Image:</td>
		    <td>
			<div id="divTxt">
			    <?php if ($this->_tpl_vars['mulImgCnt'] < $this->_tpl_vars['image_limit']['number']): ?>
			    <p id='row1'>
				<input type="file" name="image[]" id="image1" class="imageclass" onchange="showPreviewImage(1);" />
				<input type="hidden" name="prev_img[]" id="prev_img1" class="imageclass1"/>
				<span id="r1"></span>
			    </p>
			    <?php endif; ?>
			</div>
			<?php if ($this->_tpl_vars['sm']['imgs']): ?>
			<div id=''><!--edit case for design-->
			    <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['imgs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
			    <?php $this->assign('im', $this->_tpl_vars['sm']['imgs'][$this->_sections['cur']['index']]); ?>
					<?php if ($this->_tpl_vars['im']['image_name']): ?>
			    <p id="imgEdit_<?php echo $this->_tpl_vars['im']['id_image']; ?>
">
				<img src="http://manoranjan.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['im']['id_image']; ?>
_<?php echo $this->_tpl_vars['im']['image_name']; ?>
"  alt="no image" />
				<a href='javascript:void(0);' onClick="deleteProductImage('<?php echo $this->_tpl_vars['im']['id_image']; ?>
','<?php echo $this->_tpl_vars['im']['image_name']; ?>
'); return false;">Remove</a>
			    </p>		    
					<?php endif; ?>
			    <?php endfor; endif; ?>
			</div>
			<?php endif; ?>
		    </td>
		    </tr>
            <tr>
			<td></td>
			<td style="padding-left:5px;">
			    			    <span id="count_img_num" <?php if ($this->_tpl_vars['mulImgCnt'] >= $this->_tpl_vars['image_limit']['number']): ?>style="display:none"<?php endif; ?> >
				  <a href="javascript:void(0);" onClick="addFormField(); return false;" >Add More Image(s)</a>
			    </span>
			    			    <input type="hidden" id="id" value="2" />
			</td>
		    </tr>
            
            
            
            
                                    
                                    </tbody>
                                </table>
                                <div class="form-actions" align="center">
<!--<button class="btn btn-success" type="submit" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Insert<?php endif; ?>"></button>-->
<input type="submit" class="btn btn-success" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Insert<?php endif; ?>" />
<!--<button class="btn" type="button" value="Cancel" onclick="$.fancybox.close();">Cancel</button>-->
<input type="button" class="btn btn-success" value="Cancel" onclick="$.fancybox.close();" />

</div>
                                
                                
                                
                                </form>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
        </div>
      </div>
  </div>
<!--done by gayatree ends-->









<!-- Template: admin/product/add.tpl.html End --> 
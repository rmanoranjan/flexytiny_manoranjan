<?php /* Smarty version 2.6.7, created on 2017-05-01 13:14:25
         compiled from admin/user/details.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/user/details.tpl.html', 44, false),array('modifier', 'default', 'admin/user/details.tpl.html', 44, false),)), $this); ?>

<!-- Template: admin/user/details.tpl.html Start 01/05/2017 13:14:25 --> 
 <?php echo '
<style type="text/css">
  .profiledetailsadmin_fancy{display:none; }
</style>
'; ?>

<!--started by gayatree-->
<div style="width:600px;" id="dv1">
  <div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget green">
                          <div class="widget-title">
                                <h4><i class="icon-reorder"></i>  Profile Details</h4>
<!--                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>-->
                            </div>
<!--	<div class="headprt settheme">
		<div class="mdl">
			<div class="fltlft">
		     Profile Details
			</div>
			<div class="clear"></div>
		</div>
	</div>-->
	<div class="widget-body">
		<table class="table table-striped">
		    <tr>
			<th>Name:</th><td><?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
 <?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
</td>
		    </tr>
		    <tr>
			<th>Email:</th><td><a title="Email <?php echo $this->_tpl_vars['sm']['res']['email']; ?>
 " href="mailto:<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
"><?php echo $this->_tpl_vars['sm']['res']['email']; ?>
</a></td>
		    </tr>
		    <tr>
			<th>Password:</th><td><?php echo $this->_tpl_vars['sm']['res']['password']; ?>
</td>
		    </tr>
		     <tr>
			<th>Status:</th><td><?php if ($this->_tpl_vars['sm']['res']['user_status'] == 1): ?>Active <?php else: ?>Inactive<?php endif; ?></td>
		    </tr>
		    <tr>
			<th>Dob:</th><td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sm']['res']['dob'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")))) ? $this->_run_mod_handler('default', true, $_tmp, 'NA') : smarty_modifier_default($_tmp, 'NA')); ?>
</td>
		    </tr>
		    <tr>
			<th>Gender:</th><td><?php if ($this->_tpl_vars['sm']['res']['gender'] == M): ?>Male<?php elseif ($this->_tpl_vars['sm']['res']['gender'] == F): ?>Female<?php else: ?>N/A<?php endif; ?></td>
		    </tr>
		   
		    <tr>
			<th>Signup Date:</th><td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sm']['res']['add_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")))) ? $this->_run_mod_handler('default', true, $_tmp, 'NA') : smarty_modifier_default($_tmp, 'NA')); ?>
</td>
		    </tr>
		    <tr>
			<th>Login Ip:</th><td><?php if ($this->_tpl_vars['sm']['res']['ip'] == ""): ?>N/A<?php else:  echo $this->_tpl_vars['sm']['res']['ip'];  endif; ?></td>
		    </tr>
		</table>
		</div>
                          </div>
                        </div>
    </div>
</div>
                        

<!--ended by gayatree-->




<div class="makebox center profiledetailsadmin_fancy" id="dv1">
	<div class="headprt settheme">
		<div class="mdl">
			<div class="fltlft">
		     Profile Details
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="bodyprt">
		<table cellspacing="0" width="400">
		    <tr>
			<th>Name:</th><td><?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
 <?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
</td>
		    </tr>
		    <tr>
			<th>Email:</th><td><a title="Email <?php echo $this->_tpl_vars['sm']['res']['email']; ?>
 " href="mailto:<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
"><?php echo $this->_tpl_vars['sm']['res']['email']; ?>
</a></td>
		    </tr>
		    <tr>
			<th>Password:</th><td><?php echo $this->_tpl_vars['sm']['res']['password']; ?>
</td>
		    </tr>
		     <tr>
			<th>Status:</th><td><?php if ($this->_tpl_vars['sm']['res']['user_status'] == 1): ?>Active <?php else: ?>Inactive<?php endif; ?></td>
		    </tr>
		    <tr>
			<th>Dob:</th><td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sm']['res']['dob'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")))) ? $this->_run_mod_handler('default', true, $_tmp, 'NA') : smarty_modifier_default($_tmp, 'NA')); ?>
</td>
		    </tr>
		    <tr>
			<th>Gender:</th><td><?php if ($this->_tpl_vars['sm']['res']['gender'] == M): ?>Male<?php elseif ($this->_tpl_vars['sm']['res']['gender'] == F): ?>Female<?php else: ?>N/A<?php endif; ?></td>
		    </tr>
		   
		    <tr>
			<th>Signup Date:</th><td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sm']['res']['add_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m-%d-%Y") : smarty_modifier_date_format($_tmp, "%m-%d-%Y")))) ? $this->_run_mod_handler('default', true, $_tmp, 'NA') : smarty_modifier_default($_tmp, 'NA')); ?>
</td>
		    </tr>
		    <tr>
			<th>Login Ip:</th><td><?php if ($this->_tpl_vars['sm']['res']['ip'] == ""): ?>N/A<?php else:  echo $this->_tpl_vars['sm']['res']['ip'];  endif; ?></td>
		    </tr>
		</table>
		</div>
</div>
</br>
<!-- Template: admin/user/details.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-08-12 10:03:13
         compiled from admin/cms/add.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/cms/add.tpl.html', 238, false),array('function', 'html_options', 'admin/cms/add.tpl.html', 278, false),)), $this); ?>

<!-- Template: admin/cms/add.tpl.html Start 12/08/2017 10:03:13 --> 
 <?php $this->assign('ltype', $this->_tpl_vars['util']->get_values_from_config('LANGUAGE')); ?>
<?php $this->assign('mul_lang', $this->_tpl_vars['util']->get_values_from_config('MULTI_LANG')); ?>
<?php $this->assign('mul_lang_rev', $this->_tpl_vars['util']->get_values_from_config_reverse('LANGUAGE')); ?>
<?php echo '
<style type="text/css">
	span#name_msg{
		font-weight:normal;
	 }
	.formtbl td input[type="text"]{
		width:90%;
	 }
</style>
<script type="text/javascript" language="javascript">
	var uri;
	function getPermalink(){
		var tname	= $(\'#tname\').val();
		var permcode	= $(\'#permcode\').val();
		var id_content	= $(\'#id_content\').val() ? $(\'#id_content\').val() : 0;
		var ln		= ($(\'#multi_lan\').val() != 0) ? $(\'#language\').val()+"/" : "";

		if(tname){
			if(permcode){
				$.post(siteurl,{"page":"cms","choice":"getPermalink","permcode":permcode,"id_content":id_content,ce:0 },function(res){
					if(res == 1){
						$(\'#name_msg\').html(\'<font color="red">Duplicate Permcode.Please enter another one</font>\');
						$(\'#permcode\').val(\'\').focus();
						return false;
					 }else{
						$(\'#permalink\').html(\'http://manoranjan.afixiindia.com/flexytiny_new/\'+ln+$.trim(res)+\'.html\');
						$(\'#con_name\').val(tname);
						$(\'#con_cmscode\').val(res);
						$(\'#name_msg\').html(\'\');
					 }
				 });
			 }else{
				//messageShow("Please enter Permcode");
				$(\'#name_msg\').html(\'<font color="red">Please enter Permcode</font>\');
				$(\'#permcode\').focus();
				return false;
			 }
		 }else{
			//messageShow("Please enter Name");
			$(\'#name_msg\').html(\'<font color="red">Please enter Name</font>\');
			$(\'#tname\').focus();
			return false;
		 }
	 }

	function getCode(){
		var tname	= $(\'#tname\').val();
		var permcode	= $(\'#permcode\').val();
		var cmscat	= $(\'#cmscat\').val();
                var tlink       = $(\'#targetlink\').val();
                var code        =$(\'#opcode\').val();
		permcode=permcode?permcode:tname;
		//  var ln		= ($(\'#multi_lan\').val() != 0) ? $(\'#language\').val()+"/" : "";
		var ln  =  $(\'#language\').val()+"/" ;
		if(permcode){
		    var fnllink=convertToCodeArticle(permcode);
				$(\'#permalink\').html(\'http://manoranjan.afixiindia.com/flexytiny_new/\'+cmscat+"/"+fnllink+\'.php\');
				$(\'#con_name\').val(tname);
				$(\'#permcode\').val(fnllink);
				$(\'#cmscategory\').val(cmscat);
				$(\'#con_cmscode\').val(fnllink);
				$(\'#con_targetlink\').val(tlink);
				$(\'#con_code\').val(code);
				$(\'#name_msg\').html(\'\');
				if(tname && permcode && cmscat && tlink && code){
				    var id_cnt=$("#id_content").val();
		    $.post(siteurl,{"page":"cms","choice":"getCodeexist","tname":tname,"permcode":fnllink,"cmscat":cmscat,"tlink":tlink,"code":code,ce:0,"id_cnt":id_cnt },function(res){
			if(res==1){
			    $("#checklinkunique").html(\'\');
			 }else{
			$("#checklinkunique").html(\'<font color="red">This link is already exist</font>\');
		     }
		     });
		 }
		 }
	 }
        
        function convertToCodeArticle(strval){
//	var strval=$("#v_title").val();
	var p1 = /[\'’.]+/g;
	var nval1=strval.replace(p1,"");
	var str = \'\';
	for(var i=0;i<nval1.length;i++)
	    str += convertExtendedAsciiCharArticle(nval1[i]);
	var pattern=/[^\\&\\w\\d]+|[\\s]+/g;
	var nval=str.replace(pattern,"-").toLowerCase();
	pattern = /-$|^-/g;
	var finl = nval.replace(pattern,"");
	return finl;
//	$(\'#article_code\').val(finl);
     }
    /*
     * convert all spanish letter to us US keyboard letter.
     */
    function convertExtendedAsciiCharArticle(str){
	str = str.replace(/[ÀÁÂÃÄÅÆàáâãäåæ]+/g, \'a\');
	str = str.replace(/[öÒÓÔÕÖðòóôõö]+/g, \'o\');
        str = str.replace(/[ç]+/g, \'c\');
        str = str.replace(/[şŠš]+/g, \'s\');
        str = str.replace(/[ı¡]+/g, \'i\');
        str = str.replace(/[ğ]+/g, \'g\');
        str = str.replace(/[üÙÚÛÜùúûü]+/g, \'u\'); 
	str = str.replace(/[ýÿŸ¥]+/g, \'y\');
	str = str.replace(/[ž]+/g, \'z\');
	str = str.replace(/[€Œ]+/g, \'e\');
	str = str.replace(/[µ]+/g, \'e\');
	return str;
     }

	function close_win(){
		var qstr    = String(window.location);
		var myqstr  = qstr.substr(qstr.indexOf("?") + 1); // This part is for showing listing based on searching.
		var url	    = siteurl + "cms/listing/";
		if(qstr.indexOf("?") != -1){
			url += "?" + myqstr;
		 }
		window.location.href = url;
	 }

	function getcontent(){
		var langVal	= $("#langVal").val();
		var id= $("#id_cms").val();
		var cms_code	= $(\'#con_cmscode\').val();
		var tname	= $(\'#tname\').val();
		if(tname && langVal){
			$.post(siteurl,{"page":"cms","choice":"add",ce:0,"chk":1,"language": langVal,"code": cms_code,"tname":tname },function(res){
				$("#multilang").html(res);
				if($(\'#con_cmscode\').val()){
					var cms_code =  (\'#con_cmscode\').val();
				 }
				$(\'#permalink\').html(\'http://manoranjan.afixiindia.com/flexytiny_new/\'+langVal+"/"+$.trim(cms_code)+\'.html\');
			 });
		 }else{
			$("#langVal").val($("#language").val());
			$(\'#name_msg\').html(\'<font color="red">Please mentioned name and permacode</font>\');
			return false;
		 }
	 }
        
        
//	function validate_cms(){
//		var conname = $(\'#con_name\').val();
//		var concode = $(\'#con_cmscode\').val();
//		var cmscat = $(\'#cmscategory\').val();
//                    var ln	    = $(\'#language\').val();
//		if(conname && concode && cmscat){
//			var validator=$("#cms_en").validate({
//				rules: {
//					"cms[title]": {
//						required:true
//					 }
//				 },
//				messages: {
//					"cms[title]":{
//						required:"<br/>"+flexymsg.required
//					 }
//				 }
//			 });
//			var x =validator.form();
//			var y=$("#checklinkunique").html().length;
//			if(x && !y){
//				var langVal	= $("#langVal").val();
//				var name= $(\'#tname\').val();
//				var id_content	= ($(\'#id_cms\').length) ?$(\'#id_cms\').val(): "";
//				if(name){
////					$.post(siteurl,{"page":"cms","choice":"checkName","ce":0,"name":name,"language": langVal,"id_content":id_content },function(res){
////						if(res == \'1\'){
////							$(\'#multilanguagecms_error_name\').html("<font color=\'red\'>Duplicate Name.Please enter another one</font>");
////							return false;
////						 }else{
//							$(\'#multilanguagecms_error_name\').html(\'\');
//							$(\'#tname\').val();
//							if($(\'#id_cms\').val()){
//								var uri = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/cms/update";
//							 }else{
//								var uri = "http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/cms/insert";
//							 }
//							document.cms_en.action = uri;
//							document.cms_en.submit();
//							return true;
////						 }
////					 });
//				 }
//
//			 }else{
//				return false;
//			 }
//		 }else{
//			//messageShow("Please enter Name and Permacode");
//			$(\'#name_msg\').html(\'<font color="red">Please specify name,category and permacode</font>\');
//			$(\'#tname\').focus();
//			return false;
//		 }
//	 }

	function checkExists(){
		var name= $(\'#tname\').val();
		var id_content	= ($(\'#id_cms\').val() == undefined) ? "" : $(\'#id_cms\').val() ;
		if(name){
			$.post(siteurl,{"page":"cms","choice":"checkName","ce":0,"name":name,"id_content":id_content },function(res){
				if(res == \'1\'){
					$(\'#multilanguagecms_error_name\').html("<font color=\'red\'>Duplicate Name.Please enter another one</font>");
					return false;
				 }
				else{
					$(\'#multilanguagecms_error_name\').html(\'\');
					$(\'#tname\').val();
					return true;
				 }
			 });
		 }
	 }
</script>
'; ?>

<div class="center">
  <div class="row-fluid">
                    <div class="span12">
                      <div class="widget red">
                        <div class="widget-title">
                               <span class="tools">
             <a href="javascript:;" class="icon-chevron-down"></a>
             <a href="javascript:;" class="icon-remove"></a>
         </span>
                               <h4><i class="icon-reorder"></i><?php if ($this->_tpl_vars['sm']['cms']['id_content']): ?>Edit<?php else: ?>Add<?php endif; ?> Contents</h4>
                            </div>
                        
	<div class="widget-body">

            <table width="100%" border="0" align="center" class="formtbl">
			<tr>
				<td width="15%">Name:</td>
				<td>
					<!--<input  type="text" id="tname" name="cms[tname]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onblur ="$('#con_name').val($('#tname').val());" />-->
					<input  type="text" id="tname" name="cms[tname]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" onblur="getCode();" />
					<br/>
					<span id='multilanguagecms_error_name'></span>
				</td>
			</tr>
			<tr>
		    <td width="15%">Category:</td>
		    <td>
			<select id="cmscat" name="cmscategory" onchange="getCode();">
			     <option value="">--Select--</option>
			     <?php if (count($_from = (array)$this->_tpl_vars['sm']['cmscategory'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			     <option value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['sm']['cms']['cmscategory']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['item']; ?>
</option>
			     <?php endforeach; endif; unset($_from); ?>
			</select>
		    </td>
		</tr>
		<tr>
			<td>Permacode:</td>
			<td>
				<input type="text" id="permcode" name="permcode" size="90" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
" onblur="getCode();" />
				<br /><span id="name_msg"></span>
			</td>
		</tr>
		<tr>
			<td>Permalink:</td>
			<td>
				<span id="permalink"><?php if ($this->_tpl_vars['sm']['cms']['cmscode']): ?>http://manoranjan.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['cms']['cmscategory']; ?>
/<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
.php<?php else: ?>http://manoranjan.afixiindia.com/flexytiny_new/<?php endif; ?></span>
				<br><span id="checklinkunique"></span>
			</td>
		</tr>
                <tr>
			<td>Targetlink:</td>
			<td>
                            <input type="text" id="targetlink" name="targetlink" onblur="getCode();" value="<?php echo $this->_tpl_vars['sm']['cms']['targetlink']; ?>
"/>
			</td>
		</tr>
                <tr>
			<td>code:</td>
			<td>
                            <!--<select name="code" id="code" onchange="getCode();"><?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['code'],'values' => $this->_tpl_vars['sm']['code']), $this);?>
 </select>-->
                            <select name="opcode" id="opcode" onchange="getCode();"> <option value="">--Select--</option>
			     <?php if (count($_from = (array)$this->_tpl_vars['sm']['code'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			     <option value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['sm']['cms']['code']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['item']; ?>
</option>
			     <?php endforeach; endif; unset($_from); ?>
                            </select>
			</td>
		</tr>

		</table>
	</div>
                        </div>
                      </div>
    </div>
</div>
<br />
<div class="center">
	
			<div class="cont_txt" id="language_type">
				<div class="txtbg_mdl" id="multilang">
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/cms/add_form.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				</div>
				<div class="txtbg_btm fltlft"></div>
				<div class="clrbth"></div>
			</div>
		</div>
	
<br /><br />

<!-- Template: admin/cms/add.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-08-28 09:39:16
         compiled from admin/ipblocking/blockediplist.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'admin/ipblocking/blockediplist.tpl.html', 49, false),array('modifier', 'date_format', 'admin/ipblocking/blockediplist.tpl.html', 52, false),)), $this); ?>

<!-- Template: admin/ipblocking/blockediplist.tpl.html Start 28/08/2017 09:39:16 --> 
 <!--done by gayatree starts-->
<?php echo '
<style type="text/css">
  .blocklist_admin{display:none; }
  .block_ip a{color:#fff !important; }
</style>
'; ?>

 <div class="row-fluid">
                <div class="span12">
                  
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget red">
                    <div class="widget-title">
                       
                        <h4><i class="icon-reorder"></i>Blocked IP List</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        <div  class="fltrht pdngin block_ip">
            <?php if ($this->_tpl_vars['sm']['blockedip']): ?>
                <a href="javascript:void(0);" onclick="unBlockIp();" class="btn btn-success">Unblock IP</a>
            <?php endif; ?>
                <a href="javascript:void(0);" onclick="blockIp();" class="btn btn-success">Add</a>
            </div>
                    </div>
                    <div class="widget-body">
                     <form>
                        <table class="table table-striped table-bordered" id="sample_1">
                          <?php if ($this->_tpl_vars['sm']['blockedip']): ?>
                            <thead>
                         <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"  iid="chkall" name="chkall" />Select All</th>
                                <th>Username</th>
						<th class="hidden-phone">IP Address</th>
						<th class="hidden-phone">Blocking Time</th>
						<th class="hidden-phone">Blocking Upto</th>
                        <th class="hidden-phone">Reason</th>
                                
                            </tr>
                            </thead>
                            <?php endif; ?>
                                
                             <tbody>
                              <?php if (count($_from = (array)$this->_tpl_vars['sm']['blockedip'])):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" class="checkboxes" id="id_chk<?php echo $this->_tpl_vars['item']['id_block']; ?>
"  value="'<?php echo $this->_tpl_vars['item']['ip']; ?>
'" name="chk[]" onclick="unSelect();" /></td>
                                <td><?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['username'])) ? $this->_run_mod_handler('default', true, $_tmp, 'Not Entered') : smarty_modifier_default($_tmp, 'Not Entered')); ?>
</td>
                                <!--<td class="hidden-phone"><a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>-->
                                <td class="hidden-phone"><?php echo $this->_tpl_vars['item']['ip']; ?>
</td>
                                <td class="center hidden-phone"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['time_fail'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")); ?>
</td>
                                <td class="hidden-phone"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']['time_upto'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")))) ? $this->_run_mod_handler('default', true, $_tmp, 'Login Failure') : smarty_modifier_default($_tmp, 'Login Failure')); ?>
 }</td>
                                <td class="hidden-phone"><?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['reason'])) ? $this->_run_mod_handler('default', true, $_tmp, '&nbsp;') : smarty_modifier_default($_tmp, '&nbsp;')); ?>
</td>
                            </tr>
                            <?php endforeach; else: ?>
                            <tr>
						<td colspan="6" style="border:none;text-align:center;">No record found.</td>
					</tr>
                    <?php endif; unset($_from); ?>
                    </tbody>
                         
                        </table>
                       </form>
                    </div>
                  
            
                  
                  
                  
                </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
          



<!--done by gayatree ends-->






<div class="makebox center wid60 blocklist_admin">
    <div class="headprt settheme">
        <div class="mdl">
            <div  class="fltrht">
            <?php if ($this->_tpl_vars['sm']['blockedip']): ?>
                <a href="javascript:void(0);" onclick="unBlockIp();" class="buton">Unblock IP</a>
            <?php endif; ?>
                <a href="javascript:void(0);" onclick="blockIp();" class="buton">Add</a>
            </div>
            <span>Blocked IP List</span>
            <div class="clear"></div>
        </div>
    </div>
    <div class="bodyprt">
		<form>
            <table  class="tbl_listing">
            <?php if ($this->_tpl_vars['sm']['blockedip']): ?>
                <thead>
                    <tr>
                        <th><input type="checkbox" id="chkall" name="chkall" /><span style="vertical-align:top;">Select All</span></th>
                        <th>Username</th>
                        <th>Ip Address</th>
                        <th>Blocking Time</th>
                        <th>Blocking Upto</th>
                        <th>Reason</th>
                    </tr>
                </thead>
            <?php endif; ?>
                <tbody>
                    <?php if (count($_from = (array)$this->_tpl_vars['sm']['blockedip'])):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                    <tr>
                        <td><input type="checkbox" id="id_chk<?php echo $this->_tpl_vars['item']['id_block']; ?>
"  value="'<?php echo $this->_tpl_vars['item']['ip']; ?>
'" name="chk[]" onclick="unSelect();"/></td>
                        <td><?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['username'])) ? $this->_run_mod_handler('default', true, $_tmp, 'Not Entered') : smarty_modifier_default($_tmp, 'Not Entered')); ?>
</td>
                        <td><?php echo $this->_tpl_vars['item']['ip']; ?>
</td>
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['time_fail'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")); ?>
</td>
                        <td><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']['time_upto'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%Y %r") : smarty_modifier_date_format($_tmp, "%m/%d/%Y %r")))) ? $this->_run_mod_handler('default', true, $_tmp, 'Login Failure') : smarty_modifier_default($_tmp, 'Login Failure')); ?>
</td>
                        <td><?php echo ((is_array($_tmp=@$this->_tpl_vars['item']['reason'])) ? $this->_run_mod_handler('default', true, $_tmp, '&nbsp;') : smarty_modifier_default($_tmp, '&nbsp;')); ?>
</td>
                    </tr>
                    <?php endforeach; else: ?>
                    <tr>
                        <td colspan="6" style="border:none;text-align:center;">No record found.</td>
                    </tr>
                    <?php endif; unset($_from); ?>
                </tbody>
            </table>
        </form>
    </div>        
</div>
<?php echo '
<script type="text/javascript">
	function unSelect() {
		$("input[id^=id_chk]:unchecked").each(function(){
				$(\'#chkall\').removeAttr(\'checked\');
		 });
	 }
	function unBlockIp() {
		var choice=confirm(\'Do you want to unblock this?\');
		if(choice){
			var x=\'\';
			$("input[id^=id_chk]:checked").each(function(){
				x=x+\',\'+$(this).val();
			 });
			if(x==\'\'){
messageShow("Check a checkbox please.");
				//$(\'#showerr\').show();
                
			 }else{
				//$(\'#showerr\').hide();
				var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/ipblocking/deleteBlockIp";
				$.post(url,{ce:0,ipid:x },function(res){
					if(res) {
						window.location.href = res;
					 }
				 });
			 }
		 }
	 }
	$("#chkall").click(function(){
		var checked_status = this.checked;
		$("input[id^=id_chk]").each(function()
			{
				this.checked = checked_status;
			 });
	 });
		
	function blockIp() {
		var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/ipblocking/blockIp";
	 	$.fancybox.showActivity();
		$.post(url,{ce:0 },function(res){
			$.fancybox(res);
			css_even_odd();
		 });

	 }
</script>
'; ?>

<!-- Template: admin/ipblocking/blockediplist.tpl.html End --> 
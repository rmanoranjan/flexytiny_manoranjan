<?php /* Smarty version 2.6.7, created on 2017-08-19 10:30:55
         compiled from admin/dutil/checkList.tpl.html */ ?>

<!-- Template: admin/dutil/checkList.tpl.html Start 19/08/2017 10:30:55 --> 
 <?php $this->assign('checklist', $this->_tpl_vars['util']->get_values_from_config('CHECK_LIST')); ?>
<div id="chklist">
    <div id="dv1">
	<div class="makebox wid60 center">
	    <div class="headprt settheme">
		<div class="mdl">
		    <div  class="fltrht"><input type="button" class="buton" name="search" value="Add" onclick="addCheckList();" /></div>
		    <span>Check List</span>
		    <div class="clear"></div>
		</div>
	    </div>
	    <div class="bodyprt">
	    <?php if ($this->_tpl_vars['sm']['res']): ?>
		<table width="97%" border="0" align="center" class="tbl_listing">
		    <thead>
			<tr>
			    <th>Category</th>
			    <th>Check Data</th>
			    <th>Action</th>
			</tr>
		    </thead>
		    <tbody>
			<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['res']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
			<?php $this->assign('x', $this->_tpl_vars['sm']['res'][$this->_sections['cur']['index']]); ?>
			<tr>
			    <td><?php echo $this->_tpl_vars['checklist'][$this->_tpl_vars['x']['category']]; ?>
</td>
			    <td><?php echo $this->_tpl_vars['x']['checkvalue']; ?>
</td>
			    <td>
				<a href="javascript:void(0);" onclick="editCheckList('<?php echo $this->_tpl_vars['x']['id_checklist']; ?>
');">
				    <img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/pencil.png" alt="Edit CheckList" title="Edit CheckList"/>
				</a>&nbsp;&nbsp;
				<a href="javascript:void(0);" onclick="return deleteCheckList('<?php echo $this->_tpl_vars['x']['id_checklist']; ?>
');">
				    <img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" alt="Delete CheckList" title="Delete CheckList"/>
				</a>&nbsp;&nbsp;
			    </td>
			</tr>
			<?php endfor; endif; ?>
		    </tbody>
		</table>
	    </div>
	    <?php else: ?>
	    <div>No check list found</div>
	    <?php endif; ?>
	    <br />
        </div>
    </div>
</div>
<?php echo '
<script type="text/javascript">
    function addCheckList(){
	$(\'#succ_msg,.alert\').html(\'\');
	$.fancybox.showActivity();
	$.post(siteurl,{"page" : "dutil", "choice" : "addCheckList", "ce" : "0" },function(res){
	    show_fancybox(res);
		css_even_odd();
	 });
		
     }
    function editCheckList(id){
	$(\'#succ_msg,.alert\').html(\'\');
	$.fancybox.showActivity();
	$.post(siteurl,{"page" : "dutil", "choice" : "editCheckList", "ce" : "0", "id" : id },function(res){
	    show_fancybox(res);
		css_even_odd();
	 });
     }
    function deleteCheckList(id){
	$(\'#succ_msg,.alert\').html(\'\');
	var conf=confirm(\'Are you sure to delete this record ?\');
	if(conf){
	    $.post(siteurl,{"page" : "dutil", "choice" : "deleteCheckList", "ce" : "0", "id" : id },function(res){
		//$(\'.alert\').html(\'Deleted successfully\');
		messageShow("Deleted successfully.");
		$("#chklist").html(res);
		css_even_odd();
	     });
	 }else
	    return false;
     }
</script>

'; ?>

<!-- Template: admin/dutil/checkList.tpl.html End --> 
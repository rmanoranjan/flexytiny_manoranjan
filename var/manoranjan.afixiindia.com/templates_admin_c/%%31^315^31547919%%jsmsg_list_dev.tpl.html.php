<?php /* Smarty version 2.6.7, created on 2017-08-19 10:33:42
         compiled from admin/setting/jsmsg_list_dev.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/setting/jsmsg_list_dev.tpl.html', 35, false),)), $this); ?>
<?php echo '
<style type="text/css">
	 .formtbl tr td{
		  padding:0px 5px;
	  }
	 .formtbl tr td img:hover{
		  cursor:pointer;
	  }
</style>
<script type="text/javascript">
    $(document).ready(function() {
	$(\'#shr\').focus(function(){	    
	    var e=$(\'#shr\').val();
	    if(e==\'Search by level or message\'){
		$(this).val(\'\');
	     }
	 }).blur(function(){
	    var e=$(\'#shr\').val();
	    if(e==\'\') {
		$(this).val(\'Search by level or message\');
	     }
	 });

     });
    var i=0;
    var level = new Array();
    var scriptmsg = new Array();
    var rec_id = new Array();
    var desc = new Array();
    '; ?>

    <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
    <?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
    level[i]="<?php echo $this->_tpl_vars['x']['level']; ?>
";
    scriptmsg[i]="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['x']['message'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'quotes') : smarty_modifier_escape($_tmp, 'quotes')); ?>
";
    rec_id[i]="<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
";
    desc[i]="<?php echo $this->_tpl_vars['x']['description']; ?>
";
    i++;
    <?php endfor; endif; ?>
    <?php echo '
</script>
'; ?>

<div class="makebox center wid60" id="dv1">
    <div class="headprt settheme">
	<div class="mdl">
	    <div  class="fltrht">
		<input type="button" class="buton" onclick="addMsg();" value="Addmore" />
                <?php if ($this->_tpl_vars['sm']['list']): ?><input class="buton" type="submit" value="Update" /><?php endif; ?>
	    </div>
	    <span>JAVASCRIPT MESSAGE LIST</span>
	    <div class="clear"></div>
	</div>
    </div>
    <div class="bodyprt">
	    <div align="center">
		<input type="text" id="shr" name="search_item" style="width:250px;color:gray" value='<?php if ($_REQUEST['search_item']):  echo $_REQUEST['search_item'];  else: ?>Search by level or message<?php endif; ?>' onKeyup="showRecords();" />&nbsp;
		<span class="settheme" style="padding:3px 0px 4px;"><input class="buton" type="button" value="Search" onClick="search1();" /></span>
		<div class="clear"></div>
	    </div>
	    <div class="clear"></div>
	<table class="formtbl">
	    <thead>
		<tr>
		    <th>Level</th>
		    <th>Message</th>
		    <th>Description</th>
		    <th>Delete</th>
		</tr>
	    </thead>
	    <tbody>

			<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
			    <?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
		<tr id="tr<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
">
		    <td><input type="text" value="<?php echo $this->_tpl_vars['x']['level']; ?>
" id="lev<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
" name="res[<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
][level]" onKeyUp="checkLvl('<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
');" /><br><span id="lvlerr<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
"></span></td>
		    <td><input type="text" value="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['x']['message'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'quotes') : smarty_modifier_escape($_tmp, 'quotes')); ?>
" id="msg<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
" name="res[<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
][message]" onKeyUp="checkMsg('<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
');" /><br><span id="mesgerr<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
"></span></td>
		    <td><textarea id="txt<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
" name="res[<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
][description]" cols="25" rows="2"><?php echo $this->_tpl_vars['x']['description']; ?>
</textarea></td>
		    <td style="padding-left: 12px;">
			<img src="http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" onclick="del('<?php echo $this->_tpl_vars['x']['id_jsmsg']; ?>
');" />
		    </td>
		</tr>
			    <?php endfor; else: ?>
		<tr>
		    <td colspan="3">
				    No message found .
		    </td>
		</tr>
			    <?php endif; ?>
			    <tr>
		    <td align="right" colspan="4">
					<?php if ($this->_tpl_vars['sm']['list']): ?>
			<div align="right" class="updt pdng_rite_15">
			    <div class="fltrht settheme">
				<input class="buton" type="submit" value="Update" />
			    </div>
			    <div class="clear"></div>
			</div>
					<?php endif; ?>
		    </td>
		</tr>
	    </tbody>
	</table>
    </div>
</div>
<br><br>
<?php echo '
<script>
    function del(id){
	var item_pos = rec_id.indexOf(""+id+"");
	var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
	if(confirm(\'Are you sure to delete this record?\')){
	    $.post(url,{"page":"setting","choice":"deleteJsMsg","ce":0,"id":id },function(res){
		$(\'#tr\'+id).remove();
		level.splice(item_pos,1);
		scriptmsg.splice(item_pos,1);
		rec_id.splice(item_pos,1);
		desc.splice(item_pos,1);
		alert("Javascript message deleted successfully.");
	     });
	 }else{
	    return false;
	 }
     }
    function addJsMsg(){
        if($("#spn").html() != \'\'){
	    $(\'#levl\').focus();
	    return;
	 }else{
	    var lev=document.getElementById(\'levl\').value.replace(/\\s/g,"");
	    var msg=document.getElementById(\'msgl\').value;
	    var des=document.getElementById(\'txtdev\').value;
	    if(lev==\'\' || msg==\'\'){
		if(lev==\'\'){
		    $(\'#spn\').html("<font color=\'red\'>This field is required.</font>");
		    document.getElementById(\'levl\').focus();

		 }else{
		    document.getElementById(\'msgl\').focus();
		    $(\'#spng\').html("<font color=\'red\'>This field is required.</font>");

		 }
		return false;
	     }else{
		var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
		$.post(url,{"page":"setting","choice":"insertJsMsg","ce":0,"lev":lev,"msg":msg,"des":des },function(response){
		    var arr=response.split(\'=\');
		    alert(arr[1]);
		    window.location.href = arr[0];
		 });
	     }
	 }	
     }
    function search1(){
	var val=document.getElementById(\'shr\').value;
	if(val == \'Search by level or message\'){
	    val=\'\';
	 }
	var dev="dev";
	var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
	$.post(url,{"page":"setting","choice":"searchJsMsg","ce":0,"val":val,"dev":dev },function(response){
	    $(\'#content\').html(response);
	    css_even_odd();

	 });
     }
    function addMsg(){
	$.fancybox.showActivity();
	$.fancybox(\'<div style="width: 450px;"><div class="headprt settheme"><div class="mdl"><span>Add Message</span></div></div><div class="bodyprt"><table border="0" class="formtbl"><tr><td class="t1"><label for="some1">Level:</label></td><td><input type="text" id="levl" onblur="return cheeck();"><br><span id="spn"><div style="color:red; font-weight:normal;">*Please donot use any special character for level name.</font></span></td></tr><tr><td class="t1"><label for="some1">Message:</label></td><td><input type="text" id="msgl" onKeyUp="animateError();"><span id="spng"></span></td></tr><tr><td class="t1"><label for="some1">Description:</label></td><td><textarea id="txtdev"></textarea></td></tr><tr><td></td><td><div class="settheme fltlft"><input type="button" class="buton" value="Add" onclick="addJsMsg()" /></div></td></tr></table></div></div>\');
     }
    function cheeck(){
	var newval=document.getElementById(\'levl\').value;
	var ns = "^[A-Za-z]{1 }[A-Za-z0-9]*$";
	ns = ns.replace(" ","");
	var regstr = new RegExp(ns, "i");
	var ct = regstr.test(newval);
	if(ct){
	    if(newval){
		var url="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/";
		$.post(url,{"page":"setting","choice":"checkLevel","ce":0,"val":newval },function(response){
		    $(\'#spn\').html(response);
		    if(response){
			$(\'#levl\').val(\'\');
			$(\'#levl\').focus();
		     }else{
			$(\'#spn\').html(\'\');
		     }

		 });
	     }
	 }else{
	    $(\'#spn\').html(\'<font color="red">*Please donot use any special character for level name.</font>\');
	    return false;
	 }
     }
    function animateError(){
	if($("#msgl").val()){
	    var x = /["]/.test($("#msgl").val());
	    if(x)
		$(\'#spng\').html("<font color=\'red\'>*Please donot use any special character for message.</font>");
	    else
		$(\'#spng\').html(\'\');
	 }else{
	    //$(\'#spng\').focus();
	    $(\'#spng\').html("<font color=\'red\'>This field is required.</font>");
	 }
     }
    function showRecords(){
	var searchitem = $(\'#shr\').val();
	var regex = new RegExp("^"+searchitem,"ig");
	var htmlobj="";
	for(var j=0;j<level.length;j++){
	    if(level[j].match(regex) || scriptmsg[j].match(regex)){
		htmlobj += "<tr id=\'tr"+rec_id[j]+"\'><td>"+level[j]+"</td><td><input type=\'text\' value=\'"+scriptmsg[j]+"\' id=\'msg"+rec_id[j]+"\' name=\'res["+rec_id[j]+"][message]\' onKeyUp=\'checkMsg("+rec_id[j]+");\'><br><span id=\'mesgerr"+rec_id[j]+"\'></span></td><td><textarea id=\'txt\'"+rec_id[j]+"\' name=\'res["+rec_id[j]+"][description]\' cols=\'25\' rows=\'2\'>"+desc[j]+"</textarea></td><td><img src=\'http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png\' onclick=\'del("+rec_id[j]+");\'></td></tr>";
	     }
	 }
	if(!(htmlobj.length)){
	    htmlobj = "No message found.";
	    $(\'.updt\').hide();
	 }else{
	    $(\'.updt\').show();
	 }
	$(\'tbody\').html(htmlobj);
	css_even_odd();
     }
    function checkLvl(id){
	var newval=$(\'#lev\'+id).val();
	var ns = "^[A-Za-z]{1 }[A-Za-z0-9]*$";
	ns = ns.replace(" ","");
	var regstr = new RegExp(ns, "i");
	var ct = regstr.test(newval);
	if(ct)
	    $("#lvlerr"+id).html(\'\');
	else
	    $("#lvlerr"+id).html("<font color=\'red\'>Please donot give special characters for level name.</font>");
     }
    function checkMsg(id){
	var value=$(\'#msg\'+id).val();
	var ns = \'["]\';
	ns = ns.replace(" ","");
	var regstr = new RegExp(ns);
	var ct = regstr.test(value);
	if(ct)
	    $("#mesgerr"+id).html("<font color=\'red\'>Please donot give double quote for message.</font>");
	else
	    $("#mesgerr"+id).html(\'\');
     }
    function vaalidate(){
	var chk=0;
	$("span[id^=\'mesgerr\']").each(function(i,v){
	    if($(this).html()){
		chk=1;
		return false;
	     }
	 });
	if(chk){
	    alert("Plese donot provide any special character for the level name or message.");
	    return false;
	 }
     }
</script>
'; ?>
	
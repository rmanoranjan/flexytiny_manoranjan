<?php /* Smarty version 2.6.7, created on 2017-07-18 16:57:09
         compiled from admin/csvuser/managecsv.tpl.html */ ?>

<!-- Template: admin/csvuser/managecsv.tpl.html Start 18/07/2017 16:57:09 --> 
 <?php echo '
<script>
    $(function(){
	chngeval=\'\';
	option_disable();
	$(".hiddendata").attr("style","display:none;");
     });
    
    function option_disable(){
	$(".clmnselect").find(\'option\').attr("disabled", false);
	$(".clmnselect").each(function(){
	    var sval=$(this).val();
	    $(\'.clmnselect option[value="\'+sval+\'"]\').attr("disabled", true);
	    $(this).find(\'option[value="\'+sval+\'"]\').attr("disabled", false);
	 });
     }
    
    function managedata(flg,id){
	if(flg==1 || flg==2){
	    $(".editdata"+id).attr("style","display:none;");
	    $(".editbtn"+id).removeAttr("style");
	    $(".skipbtn"+id).removeAttr("style");
	    var saveval=(flg==2)?1:\'\';
		$("#save"+id).val(saveval);
		option_disable();
	 }else if(flg==3){
	$(".hiddendata").attr("style","display:none;");
	$(".editdata"+id).removeAttr("style");
	$(".alleditbtn").removeAttr("style");
	$(".editbtn"+id).attr("style","display:none;");
	$(".skipbtn"+id).removeAttr("style");
	$(".uniqredsp"+id).removeAttr("style");
	$("#skipspan"+id).attr("style","display:none;");
	$("#skipdiv"+id).removeAttr("style");
     }else if(flg==4){
	$(".skipbtn"+id).attr("style","display:none;");
	$(".editdata"+id).attr("style","display:none;");
	$(".editbtn"+id).removeAttr("style");
	$(".editdatanew"+id).attr("style","display:none");
	$("#clselect"+id).removeAttr("style");
	$("#save"+id).val(2);
	var chknw=$("#newclmnchk"+id).val();
	if(chknw==1){
	$("#clmnnm"+id).val(chngeval);
	$("#clmnnm"+id).attr("style","display:none");
	$("#clselect"+id).find(\'option[value="\'+chngeval+\'"]\').prop("selected", true);
	 }
	$(".uniqred"+id).prop("checked",false);
	$(".uniqredsp"+id).attr("style","display:none;");
	$("#skipdiv"+id).attr("style","display:none;");
	$("#skipspan"+id).removeAttr("style");
     }else{
	$("#clmnnm"+id).val(chngeval);
	$("#clmnnm"+id).attr("style","display:none");
	$(".editdatanew"+id).attr("style","display:none");
	$(".editdatanm"+id).removeAttr("style");
	$("#clselect"+id).removeAttr("style");
	$("#clselect"+id).find(\'option[value="\'+chngeval+\'"]\').prop("selected", true);
     }
     }
    
//    function setuniqueclmn(id){
//	$("#uniqvla"+id).val(1);
//     }
    function addclmnname(id){
	var clnm=$("#clmnnm"+id).val();
	$(".newclmnid").before("<option value=\'"+clnm+"\'>"+clnm+"</option>");
	$("#clselect"+id).removeAttr("style");
	$("#clselect"+id).find(\'option[value="\'+clnm+\'"]\').prop("selected", true);
	$("#clmnnm"+id).attr("style","display:none");
	$(".editdata"+id).attr("style","display:none;");
	$(".editbtn"+id).removeAttr("style");
	$(".skipbtn"+id).removeAttr("style");
	$("#save"+id).val(1);
	$(".editdatanew"+id).attr("style","display:none");
	option_disable();
     }
    function showNewFieldForm(id){
	chngeval=$("#clmnnm"+id).val();
	var slval=$("#clselect"+id).val();
	if(slval==\'new\'){
	$("#clmnnm"+id).removeAttr("style");
	$("#newclmnchk"+id).val(1);
	$(".editdatanew"+id).removeAttr("style");
	$(".editdatanm"+id).attr("style","display:none");
	$("#clselect"+id).attr("style","display:none");
     }else{
	$("#newclmnchk"+id).val(\'\');
     }
     }
    
    function typeclmn(id){
	var typ=$("#typeclmn"+id).val();
	var typval=\'\';
	if(typ==\'VARCHAR\' || typ==\'TEXT\'){
	    typval=\'Text Field\';
	 }else if(typ==\'DATE\'){
	    typval=\'Date Field\';
	 }else{
	   typval=\'Number Field\'; 
	 }
	$("#typflg"+id).html(typval);
     }
    
    function validatefrm(){
	var chkflg=1;
	if ($("input[name=loginval]:checked").length == 0) {
	    chkflg=0;
	    alert(\'Please select one column as unique\');
	    return false;
	 }
	if ($("input[name=passval]:checked").length == 0) {
	    chkflg=0;
	    alert(\'Please select one column as password\');
	 }
	if(chkflg){
	   return true; 
	 }else{
	return false;
     }
     }
</script>
'; ?>


        <div class="headprt settheme">
             <div class="mdl">
                 <span>Manage CSV Of <?php echo $this->_tpl_vars['sm']['csvname']; ?>
</span>
             </div>
        </div>	<form name="csvdata" id="csvdata" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/csvuser/insertcsvdata" method="post" enctype="multipart/form-data" onsubmit="return validatefrm();">
	    <input type="hidden" name="post" value='<?php echo $this->_tpl_vars['sm']['post']; ?>
' ><br>
	    <input type="hidden" name="status" value='<?php echo $this->_tpl_vars['sm']['status']; ?>
'><br>
	    <input type="hidden" name="csv[name]" value='<?php echo $this->_tpl_vars['sm']['csvname']; ?>
'><br>
	    <input type="hidden" name="filename" value='<?php echo $this->_tpl_vars['sm']['filename']; ?>
'><br>
	    <input type="hidden" name="code" value='<?php echo $this->_tpl_vars['sm']['code']; ?>
'><br>
	    <input type="hidden" name="tblid" value='<?php echo $this->_tpl_vars['sm']['tblid']; ?>
'><br>
<table border="1">
    <tr>
	<?php if (count($_from = (array)$this->_tpl_vars['sm']['coulmndata'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
	<td>
	    <div>
		<!--<span><?php echo $this->_tpl_vars['item']['0']; ?>
</span><span><input type='hidden' value='' name='uniqueval[]' id='uniqvla<?php echo $this->_tpl_vars['key']; ?>
'><input type='checkbox' onclick='setuniqueclmn(<?php echo $this->_tpl_vars['key']; ?>
);'>Unique</span><br>-->
		<div id="skipdiv<?php echo $this->_tpl_vars['key']; ?>
"><span><?php echo $this->_tpl_vars['item']['0']; ?>
</span>
		<span class="uniqredsp<?php echo $this->_tpl_vars['key']; ?>
"><input type='radio' name="loginval" value="<?php echo $this->_tpl_vars['item']['0']; ?>
" class="uniqred<?php echo $this->_tpl_vars['key']; ?>
">Unique</span><br>
		<span class="uniqredsp<?php echo $this->_tpl_vars['key']; ?>
"><input type='radio' name="passval" value="<?php echo $this->_tpl_vars['item']['0']; ?>
" class="uniqred<?php echo $this->_tpl_vars['key']; ?>
">Password</span><br>
		<span id="typflg<?php echo $this->_tpl_vars['key']; ?>
" >Text Field</span><br>
		</div>
		<span id="skipspan<?php echo $this->_tpl_vars['key']; ?>
" style="display:none;">Will not be imported</span>
		<div class="hiddendata editdata<?php echo $this->_tpl_vars['key']; ?>
">
		<span><b>Column Name</b></span><br>
		<span>
		    <input type='text' value='<?php echo $this->_tpl_vars['item']['0']; ?>
' id='clmnnm<?php echo $this->_tpl_vars['key']; ?>
' style='display:none;'>
		    <input type='hidden' value='' id='newclmnchk<?php echo $this->_tpl_vars['key']; ?>
'>
		    <select name='columnnm[]' id='clselect<?php echo $this->_tpl_vars['key']; ?>
' class='clmnselect' onchange="showNewFieldForm(<?php echo $this->_tpl_vars['key']; ?>
);">
		    <option value="">Make a Selection</option>
		    <optgroup label="Create a New Column">
			<?php if (count($_from = (array)$this->_tpl_vars['sm']['coulmndata'])):
    foreach ($_from as $this->_tpl_vars['kyc'] => $this->_tpl_vars['itc']):
?>
			<?php if ($this->_tpl_vars['itc']['1'] == 0): ?>
			<option value="<?php echo $this->_tpl_vars['itc']['0']; ?>
" <?php if ($this->_tpl_vars['item']['0'] == $this->_tpl_vars['itc']['0']): ?>selected=selected<?php endif; ?>><?php echo $this->_tpl_vars['itc']['0']; ?>
</option> 
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
			<option value="new" class="newclmnid">New column name</option> 
		    </optgroup>
		    <optgroup label="Available Column Name">
			<?php if (count($_from = (array)$this->_tpl_vars['sm']['coulmndata'])):
    foreach ($_from as $this->_tpl_vars['kyc'] => $this->_tpl_vars['itc']):
?>
			<?php if ($this->_tpl_vars['itc']['1'] == 1): ?>
			<option value="<?php echo $this->_tpl_vars['itc']['0']; ?>
" <?php if ($this->_tpl_vars['item']['0'] == $this->_tpl_vars['itc']['0']): ?>selected=selected<?php endif; ?>><?php echo $this->_tpl_vars['itc']['0']; ?>
</option> 
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		    </optgroup>
		    </select></span><br>
		    <span><b>Field Type</b></span><br>
		    <span>
			<select name='datatyp[]'  id="typeclmn<?php echo $this->_tpl_vars['key']; ?>
" onchange="typeclmn(<?php echo $this->_tpl_vars['key']; ?>
);">
			    <option value="VARCHAR">VARCHAR</option>
			    <option value="TEXT">TEXT</option>
			    <option value="DATE">DATE</option>
			    <option value="INT">INT</option>
			    <option value="BIGINT">BIGINT</option>
			</select>
			</span>
		    </div><br>
		<span>
		    <input type="button" class="hiddendata editdata<?php echo $this->_tpl_vars['key']; ?>
 editdatanm<?php echo $this->_tpl_vars['key']; ?>
" value="Back" onclick="managedata(1,<?php echo $this->_tpl_vars['key']; ?>
);">
		    <input type="button" class="hiddendata editdata<?php echo $this->_tpl_vars['key']; ?>
 editdatanm<?php echo $this->_tpl_vars['key']; ?>
" value="Save" onclick="managedata(2,<?php echo $this->_tpl_vars['key']; ?>
);">
		    <input type="button" class="hiddendata editdatanew<?php echo $this->_tpl_vars['key']; ?>
" value="Cancel" onclick="managedata(5,<?php echo $this->_tpl_vars['key']; ?>
);">
		    <input type="button" class="hiddendata editdatanew<?php echo $this->_tpl_vars['key']; ?>
" value="Save" onclick="addclmnname(<?php echo $this->_tpl_vars['key']; ?>
);">
		    <input type="button"  class="editbtn<?php echo $this->_tpl_vars['key']; ?>
 alleditbtn" value="Edit" onclick="managedata(3,<?php echo $this->_tpl_vars['key']; ?>
);">
		    <input type="button"  value="Skip" class="skipbtn<?php echo $this->_tpl_vars['key']; ?>
" onclick="managedata(4,<?php echo $this->_tpl_vars['key']; ?>
);">
		    </span>
		   <input type="hidden" name="oldclmnm[]" value='<?php echo $this->_tpl_vars['item']['0']; ?>
' id='oldclmnm<?php echo $this->_tpl_vars['key']; ?>
'><br>
		    <input type="hidden" name="save[]" value='' id='save<?php echo $this->_tpl_vars['key']; ?>
'><br>
	    </div><br><br>
	    <div>
		<span><?php echo $this->_tpl_vars['item']['0']; ?>
</span><br>
		<?php if (count($_from = (array)$this->_tpl_vars['sm']['data'][$this->_tpl_vars['item']['0']])):
    foreach ($_from as $this->_tpl_vars['ky'] => $this->_tpl_vars['it']):
?>
		<!--<input type='hidden1' value='<?php echo $this->_tpl_vars['it']; ?>
' name='csvval[<?php echo $this->_tpl_vars['key']; ?>
][]'>-->
		<span><?php echo $this->_tpl_vars['it']; ?>
</span><br>
		<?php endforeach; endif; unset($_from); ?>
	    </div>
	</td>
	<?php endforeach; endif; unset($_from); ?>
    </tr>
    <tr><td><input class="myButton userbuton" type="submit" value="SUBMIT" id="csvsubmit"></td></tr>
</table>
	</form>
    

<!-- Template: admin/csvuser/managecsv.tpl.html End --> 
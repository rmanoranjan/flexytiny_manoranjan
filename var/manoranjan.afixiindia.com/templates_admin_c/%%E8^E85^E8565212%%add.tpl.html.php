<?php /* Smarty version 2.6.7, created on 2017-08-12 15:27:52
         compiled from admin/attribute/add.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'admin/attribute/add.tpl.html', 398, false),array('modifier', 'explode', 'admin/attribute/add.tpl.html', 463, false),array('modifier', 'count', 'admin/attribute/add.tpl.html', 464, false),array('function', 'html_options', 'admin/attribute/add.tpl.html', 408, false),)), $this); ?>

<!-- Template: admin/attribute/add.tpl.html Start 12/08/2017 15:27:52 --> 
 <?php $this->assign('board_type', $this->_tpl_vars['util']->get_values_from_config('BOARD_TYPE')); ?>
<?php $this->assign('match_type', $this->_tpl_vars['util']->get_values_from_config('MATCH_TYPE')); ?>
<?php $this->assign('txt_srch', $this->_tpl_vars['util']->get_values_from_config('TEXT_SEARCH_TYPE')); ?>
<?php echo '
<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/flexyjs/jquery.multiautocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/css_theme/multiautocomplete.css" />
<script type="text/javascript">
	
    function validateAttribute() {
	var validator=$("#adminaddattribute").validate({
	    rules: {
		"attribute[attribute_name]": {
		    required:true
		 },
		"attribute[entry_type]":{
		    required: true
		 },
		"attribute[search_type]": {
		    required:true
		 },
		"attribute[attribute_values]": {
		    required:true
		 },
//		"attribute[dropdown_values]": {
//		    required:true
//		 },
		"attribute[datatype]": {
		    required:true
		 },
		"attribute[text_values]": {
		    required:true
		 },
		"attribute[attribute_label]": {
		    required:true
		 },
		"attribute[optype]": {
		    required:true
		 },
//		"attribute[id_course]":{
//		    required:true
//		 }
	     },
	    messages: {
		"attribute[attribute_name]":{
		    required:flexymsg.required
		 },
		"attribute[entry_type]":{
		    required:flexymsg.required
		 },
		"attribute[search_type]": {
		    required:flexymsg.required
		 },
		"attribute[attribute_values]": {
		    required:flexymsg.required
		 },
//		"attribute[dropdown_values]": {
//		    required:flexymsg.required
//		 },
		"attribute[datatype]": {
		    required:flexymsg.required
		 },
		"attribute[text_values]": {
		    required:flexymsg.required
		 },
		"attribute[attribute_label]": {
		    required:flexymsg.required
		 },
		"attribute[optype]": {
		    required:flexymsg.required
		 },
//		"attribute[id_course]": {
//		    required:flexymsg.required
//		 }
	     }
	 });
//	var courseSelected = true;
//	if($(\'#boardRadio\').attr("checked") || $(\'#univRadio\').attr("checked") || $(\'#otherRadio\').attr("checked")){
//	   if($(\'#course_name\').val()){
//	       courseSelected = true;
//	    }else{
//	       $(\'#errCourse\').html("<font color=\'red\'>This field is required.</font>");
//	       courseSelected = false;
//	    }
//	 }
//	if(courseSelected){
//	    return validator.form();
//	 }else{
//	    return false;
//	 }
var x = validator.form();
var y = $("#attribute_error_name").text().length;
if (x && !y) {
$(\'#sbmt\').attr("disabled",true);
return x;
 } else {
$(\'#sbmt\').removeAttr("disabled");
return false;
 }
     }
    
    function hideCourseErrMsg(){
	$(\'#errCourse\').html("");
     }
    function hideValueErrMsg(){
	$(\'#errValue\').html("");
     }
	
    function callbackFun(response) {
	if(response==\'no\'){
	    $(\'#attribute_error_code\').html(\'This code already exist.Choose another one\');
	    $(\'#code_attribute\').val(\'\').focus();
	 }else if(response==\'noname\'){
	    $(\'#attribute_error_name\').html(\'This code already exist.Choose another one\');
	    $(\'#name_attribute\').val(\'\').focus();
	 }else{
	    var id_attribute = $(\'#id_attribute\').val();
	    var mess = "";
	    if(id_attribute){
		 mess = "Attribute updated successfully";
	     }else{
		 mess = "Attribute added successfully";	
	     }
	    $.fancybox.close();
	    messageShow(mess);
	    $(\'#attribute_listing\').html(response);
	    css_even_odd();
	 }
     }
	
    function convertMe(obj){
	var cval=obj.value;
	var pattern=/[-\\s]/g;
	var nval=cval.replace(pattern,"");
	nval = nval.toLowerCase();
	chkNameCodeExit(\'name\', nval, \'code_attribute\');
     }

    var code1,kc,cc;
    function chkSpecialChars(e) {
	code1 = e.which;
	if(!code1){
	    code1=e.keyCode;
	 }
	kc=e.keyCode;
	cc=e.charCode;
	if(kc==8 && cc==0){//For Backspace
	    return true;
	 }else if(kc==46 && cc==0){//For Delete
	    return true;
	 }else if(kc==36 && cc==0){//For Home
	    return true;
	 }else if(kc==35 && cc==0){//For End
	    return true;
	 }else if(kc==39 && cc==0){//For Forward Key
	    return true;
	 }else if(kc==37 && cc==0){//For Backward Key
	    return true;
	 }else if(kc==9 && cc==0){//For Tab
	    return true;
	 }else if(kc==46 || cc==46){
	    return true;
	 }else if(kc==95 || cc==95){
	    return true;
	 }else if(kc==32 || cc==32){
	    return false;
	 }else if(97<=cc && cc<=122){
	    return true;
	 }else if(97<=kc && kc<=122){
	    return true;
	 }else if(65<=cc && cc<=90){
	    return true;
	 }else if(65<=kc && kc<=90){
	    return true;
	 }else if(48<=cc && cc<=57){
	    return true;
	 }else if(48<=kc && kc<=57){
	    return true;
	 }else{
	    return false;
	 }	
     }
	
    function attributeExist(id_attribute){
        var attributename=$("#name_attribute").val();
        $.post(siteurl,{"page":"attribute","choice":"checkAttribute","attributename":attributename,\'id_attribute\':id_attribute,"ce":0 },function(res){
	   if(res){
		$(\'#attribute_error_name\').html("<font color=\'red\'>This attribute is already exist.</font>");
		$("#name_attribute").val(\'\');
		$("#name_attribute").focus();
	     }else{
		$(\'#attribute_error_name\').html(\'\');
	     }
         });
     }

      function showEntryType(obj){
	var entryType = obj.options[obj.selectedIndex].text;
	if(entryType == "Radio" || entryType == "Checkbox"){
	   $(\'#txtfield\').hide();
	   $(\'#boardfield\').hide();
	   $(\'#boardRadio\').removeAttr("checked");
	   $(\'#univRadio\').removeAttr("checked");
	   $(\'#otherRadio\').removeAttr("checked");
	   $(\'#valuefield\').show();
	   $(\'#valuetext\').attr("value","");
	   $(\'#optType\').show();
	   $(\'#strictType\').removeAttr("checked");
	   $(\'#allType\').removeAttr("checked");
	   $.post(siteurl,{"page" : "attribute", "choice" : "showType","ce" : 0 },function(res){
		//alert(res);return;
		$(\'#searchtypetd\').html(res);
	     });
	 }else if(entryType == "Dropdown"){
	    $(\'#txtfield\').hide();
//	    $(\'#valuefield\').hide();
	    $(\'#valuefield\').show();
	    $(\'#optType\').hide();
	    $(\'#strictType\').removeAttr("checked");
	    $(\'#allType\').removeAttr("checked");
	    $(\'#valuetext\').attr("value","");
	    $(\'#boardfield\').show();
	    $(\'#searchtypetd\').html(\'<select name="attribute[search_type]" id="searchtype"><option value="\'+$(obj).val()+\'">\'+entryType+\'</option></select>\');
	    
	 }else if(entryType == "Text"){
	    $(\'#valuefield\').hide();
	    $(\'#boardfield\').hide();
	    $(\'#boardRadio\').removeAttr("checked");
	    $(\'#univRadio\').removeAttr("checked");
	    $(\'#otherRadio\').removeAttr("checked");
	    $(\'#optType\').hide();
	    $(\'#strictType\').removeAttr("checked");
	    $(\'#allType\').removeAttr("checked");
	    $(\'#valuetext\').attr("value","");
	    $(\'#txtfield\').show();
	    var res = "';  echo $this->_tpl_vars['sm']['res']['text_values'];  echo '";
	    if(!res){  
		$(\'#txt_dt_int\').attr("checked","checked");
		$(\'#txt_dt_int_div\').show();
		$(\'#txt_1\').attr("checked","checked");
	     }
	    $(\'#searchtypetd\').html(\'<select name="attribute[search_type]" id="searchtype"><option value="\'+$(obj).val()+\'">\'+entryType+\'</option></select>\');
	    
	 }else{
	    $(\'#txtfield\').hide();
	    $(\'#boardfield\').hide();
	    $(\'#boardRadio\').removeAttr("checked");
	    $(\'#univRadio\').removeAttr("checked");
	    $(\'#otherRadio\').removeAttr("checked");
	    $(\'#valuefield\').hide();
	    $(\'#optType\').hide();
	    $(\'#strictType\').removeAttr("checked");
	    $(\'#allType\').removeAttr("checked");
	    $(\'#valuetext\').attr("value","");
	    $(\'#searchtypetd\').html(\'<select name="attribute[search_type]" id="searchtype"><option value="\'+$(obj).val()+\'">\'+entryType+\'</option></select>\');
	 }
     }   
     function showSearchType(obj){
	var searchType = obj.options[obj.selectedIndex].text;
	$(\'#entrytype\').each(function(){
		$(\'option\').each(function(){
		   if($(obj).val() == $(this).val()){
		       $(this).attr("selected", "selected");
		    } 
		 });
	     });
	if(searchType == "Radio" || searchType == "Checkbox"){
	   $(\'#txtfield\').hide();
	   $(\'#valuefield\').show();
	   $(\'#valuetext\').attr("value","");
	   $(\'#optType\').show();
	   $(\'#strictType\').removeAttr("checked");
	   $(\'#allType\').removeAttr("checked");
	   $(\'#boardfield\').hide();
	   $(\'#boardRadio\').removeAttr("checked");
	   $(\'#univRadio\').removeAttr("checked");
	   $(\'#otherRadio\').removeAttr("checked");
	 }else if(searchType == "Dropdown"){
	    $(\'#txtfield\').hide();
//	    $(\'#valuefield\').hide();
	    $(\'#valuefield\').show();
	    $(\'#optType\').hide();
	    $(\'#strictType\').removeAttr("checked");
	    $(\'#allType\').removeAttr("checked");
	    $(\'#valuetext\').attr("value","");
	    $(\'#boardfield\').show();	    
	 }else if(searchType == "Text"){
	    $(\'#boardfield\').hide();
	    $(\'#boardRadio\').removeAttr("checked");
	    $(\'#univRadio\').removeAttr("checked");
	    $(\'#otherRadio\').removeAttr("checked");
	    $(\'#valuefield\').hide();
	    $(\'#optType\').hide();
	    $(\'#strictType\').removeAttr("checked");
	    $(\'#allType\').removeAttr("checked");
	    $(\'#valuetext\').attr("value","");
	    $(\'#txtfield\').show();
	    var res = "';  echo $this->_tpl_vars['sm']['res']['text_values'];  echo '";
	    if(!res){
		$(\'#txt_dt_int\').attr("checked","checked");
		$(\'#txt_dt_int_div\').show();
		$(\'#txt_1\').attr("checked","checked");
	     }
	 }else{
	   $(\'#txtfield\').hide();
	   $(\'#valuefield\').hide();
	   $(\'#optType\').hide();
	   $(\'#strictType\').removeAttr("checked");
	   $(\'#allType\').removeAttr("checked");
	   $(\'#valuetext\').attr("value","");	   
	   $(\'#boardfield\').hide();
	   $(\'#boardRadio\').removeAttr("checked");
	   $(\'#univRadio\').removeAttr("checked");
	   $(\'#otherRadio\').removeAttr("checked");
	 }
     }
    
    function ckeckname(id){
        var name=$("#name_attribute").val();
        $.post(siteurl,{\'page\':\'attribute\',\'choice\':\'unique_name\',\'id\':id,\'name\':name,\'ce\':0 },function(res){//alert(res);//return false;
        if(res==1){
           $("#attribute_error_name").html("");
         }else{
           $("#attribute_error_name").html("This name is already exists.");
           }
         });  
     }

    function checkPattern(){
	var pattern=/(\\s+)?\\,+(\\s+)?/g;
	var string = $(\'#valuetext\').val();
	var nval=string.replace(pattern,",");
	$(\'#valuetext\').attr("value",nval);
     }
    $(document).ready(function() {
	    var courses = $(\'#course_name\').val();
	    var url = siteurl + \'attribute/autoSearchkey/course/\'+encodeURIComponent(courses)+\'/ce/0/\';
	   // var url = siteurl + \'attribute/autoSearchkey/ce/0/\';
	    $("#course_name").autocomplete({json_url:url,width:"200px",cache:false,json_cache:false,delay:0.2,firstselected:true });
	
	
	    var pname   = $(\'#attribute_name\').val();
	    var etype = $(\'#entrytype\').val();
	    var stype = $(\'#searchtype\').val();
	    $(\'#pname\').attr("value", pname);
	    $(\'#etype\').attr("value",etype);
	    $(\'#stype\').attr("value",stype);
//	    $(\'#otherRadio\').click(function(){
//		$(\'#valuefield\').show();
//	     });
//	    $(\'#boardRadio\').click(function(){
//		$(\'#valuefield\').hide();
//		$(\'#valuetext\').attr("value","");
//	     });
//	    $(\'#univRadio\').click(function(){
//		$(\'#valuefield\').hide();
//		$(\'#valuetext\').attr("value","");
//	     });
     });
</script> 
<style>
    label.error	{
	color:#FF0000;
	font-size:15px;
     }
	.listing_tbl tr td input[type="text"]{width:96%; }
	.listing_tbl tr td select{width:100%; }
	.holder{width:96% !important; }
	.facebook-auto{width:65.5% !important;overflow: none !important; }
	.facebook-auto ul{width:100% !important; }
	#searchtypetd table{margin:0px !important; }
	#searchtypetd table tr td{padding:0px !important; }
</style>
'; ?>

<!--by gayatree-->
<div id="dv2">
  <div style="width:500px;">
     <div class="headprt settheme">
            <div class="mdl">
                <span><?php if ($this->_tpl_vars['sm']['res']): ?>Edit<?php else: ?>Add<?php endif; ?> Attribute</span>
            </div>
        </div>
    <div class="bodyprt">
      <form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/attribute/<?php if ($this->_tpl_vars['sm']['res']): ?>updateAttribute<?php else: ?>insertAttribute<?php endif; ?>/ce/0" name="adminaddattribute" id="adminaddattribute" method="post" onsubmit="return AsyncUpload.submitForm(this, validateAttribute, callbackFun);">
                <!--<form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/attribute/<?php if ($this->_tpl_vars['sm']['res']): ?>updateAttribute<?php else: ?>insertAttribute<?php endif; ?>/ce/0" name="adminaddattribute" id="adminaddattribute" method="post" onsubmit="return validateAttribute();">-->
                <input type="hidden" name="id_user" value="<?php echo $_SESSION['id_user']; ?>
" /> 
                <input type="hidden" name="qstart" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
" />   
                <input type="hidden" name="pname" id="pname" value="<?php echo $this->_tpl_vars['sm']['pname']; ?>
" />
                <input type="hidden" name="ctype" id="ctype" value="<?php echo $this->_tpl_vars['sm']['ctype']; ?>
" />
                <input type="hidden" name="stype" id="stype" value="<?php echo $this->_tpl_vars['sm']['stype']; ?>
" />
                <input type="hidden" id="referer" name='referer' value="<?php echo $this->_tpl_vars['sm']['referer']; ?>
" />
                <input type="hidden" name="id_attribute" id="id_attribute" value="<?php echo $this->_tpl_vars['sm']['res']['id_attribute']; ?>
" />
                <table border="0" class="formtbl">
                    <tr>
                        <td>Name :</td>
                        <td align="left">
                            <input type="text" class="txt" name="attribute[attribute_name]" id="name_attribute" onblur="ckeckname('<?php echo $this->_tpl_vars['sm']['res']['id_attribute']; ?>
');" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['attribute_name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
" /><!-- onblur="attributeExist('<?php echo $this->_tpl_vars['sm']['res']['id_attribute']; ?>
');" -->
                               <br/>
                            <span id='attribute_error_name' style=" color: red"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Entry Type :</td>
                        <td>
                            <select name="attribute[entry_type]" id="entrytype" onchange="showEntryType(this);" >
                            <option value="">--Select--</option>
                            <?php echo smarty_function_html_options(array('options' => ((is_array($_tmp=$this->_tpl_vars['sm']['attr'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)),'selected' => $this->_tpl_vars['sm']['res']['entry_type']), $this);?>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Search Type :</td>
                        <td id="searchtypetd" style="vertical-align:top;">
                            <select name="attribute[search_type]" id="searchtype" onchange="showSearchType(this);">
                            <option value="">--Select--</option>
                            <?php echo smarty_function_html_options(array('options' => ((is_array($_tmp=$this->_tpl_vars['sm']['attr'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)),'selected' => $this->_tpl_vars['sm']['res']['search_type']), $this);?>

                            </select>
                        </td>
                    </tr>
		     <tr id="txtfield" <?php if ($this->_tpl_vars['sm']['res']['datatype']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Type :</td>
			 <td>
			     <div>
				<input type="radio" name="attribute[datatype]" id="txt_dt_int" value="1" onclick="$('#txt_dt_int_div').show();" <?php if ($this->_tpl_vars['sm']['res']['datatype'] == 1): ?>checked="checked"<?php endif; ?>/>Integer
				<input type="radio" name="attribute[datatype]" id="txt_dt_str" value="2" onclick="$('#txt_dt_int_div').hide();" <?php if ($this->_tpl_vars['sm']['res']['datatype'] == 2): ?>checked="checked"<?php endif; ?>/>String
				<br/>
				<label style="display:none;" class="error" generated="true" for="attribute[datatype]">This field  required</label>
				<br/>
			    </div>
			     <div id="txt_dt_int_div"<?php if ($this->_tpl_vars['sm']['res']['text_values']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
				<?php if (count($_from = (array)$this->_tpl_vars['txt_srch'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
				    <input type="radio" id="txt_<?php echo $this->_tpl_vars['key']; ?>
" name="attribute[text_values]" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['text_values'] == $this->_tpl_vars['key']): ?>checked="checked"<?php endif; ?>/><?php echo $this->_tpl_vars['item']; ?>

				<?php endforeach; endif; unset($_from); ?>
				<br>
				<label style="display: none;" class="error" generated="true" for="attribute[text_values]">This field is required</label>
			    </div>
                        </td>
                    </tr>
                    <tr id="boardfield" <?php if ($this->_tpl_vars['sm']['res']['dropdown_values']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td colspan="2"><input type="hidden" id="otherRadio" name="attribute[dropdown_values]" value="<?php echo $this->_tpl_vars['board_type']['other']; ?>
" /></td>
                    </tr>
                    <tr id="valuefield" <?php if ($this->_tpl_vars['sm']['res']['attribute_label']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Value :</td>
                        <td align="left"><input type="text" id="valuetext" name="attribute[attribute_label]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['attribute_label'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
" onblur="checkPattern();" onkeypress="hideValueErrMsg()"/>
                        <br><i>Note : Multiple values can be entered in comma(,) separated only.</i>
                        <br /><span id="errValue" class="error"></span>
                        </td>
                    </tr>
		    <tr id="optType" <?php if ($this->_tpl_vars['sm']['res']['attribute_label']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Match Type :</td>
                        <td>
			    <input type="radio" id="strictType" name="attribute[optype]" value="<?php echo $this->_tpl_vars['match_type']['strict']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['optype'] == $this->_tpl_vars['match_type']['strict']): ?>checked="checked"<?php endif; ?>/>Strict  <!--<?php echo $this->_tpl_vars['optype']['strict']; ?>
-->
                            <input type="radio" id="allType" name="attribute[optype]" value="<?php echo $this->_tpl_vars['match_type']['all']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['optype'] == $this->_tpl_vars['match_type']['all']): ?>checked="checked"<?php endif; ?>/>All	    <!--<?php echo $this->_tpl_vars['optype']['all']; ?>
-->
			    <br><label style="display: inline;" class="error" generated="true" for="attribute[optype]"></label>
			</td>
                    </tr>
<!--                    <tr>
                        <td>Courses :</td>
                        <td align="left">
                            <select name="attribute[id_course]" multiple="multiple" id="course_name" onchange="hideCourseErrMsg()">
                            <?php if ($this->_tpl_vars['sm']['res']['id_course']): ?>
                            <?php $this->assign('courseArr', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['sm']['res']['id_course']) : explode($_tmp, $this->_tpl_vars['sm']['res']['id_course']))); ?>
                            <?php $this->assign('countArr', count($this->_tpl_vars['courseArr'])); ?>
                            <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['courseArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                            <?php $this->assign('d', $this->_tpl_vars['courseArr'][$this->_sections['i']['index']]); ?>	    
                                <?php if (count($_from = (array)$this->_tpl_vars['sm']['course'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['d']): ?>
                                 <option  selected="selected" value="<?php echo $this->_tpl_vars['d']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
                                <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endfor; endif; ?>
                            <?php endif; ?>
                            </select>
                            <br /><span id="errCourse" class="error"></span>
                        </td>
                    </tr>-->
 <tr>
                      <td></td>    
                           <td>
                           	<table>
                            	<tr>
                                	<td><div class="settheme fltlft"><input type="submit" class="login_btn buton" id="sbmt" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?>"/></div></td>
                                    <td><div class="settheme fltlft"><input type="button" class="login_btn buton" name="cancel" value="Cancel" onclick="$.fancybox.close();" /></div></td>
                                </tr>
                            </table>
                           
                                
                           </td>
                      </tr>



<!--                    <tr>
                        <td>&nbsp;</td>
                        <td class="padd_tp">
                            <input type="submit" class="login_btn" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?>"/>
                            <input type="button" class="login_btn" name="cancel" value="Cancel" onclick="$.fancybox.close();" />
                        </td>
                    </tr>-->
                </table>
                </form>
    </div>
  </div>
</div>
<!--by gayatree-->






<!--<div id="dv2">    
    <div class="smler_box">
	<div class="top"></div>
	<div class="mdl">
	    <div class="cont_hdr1 fltlft">
            <div class="cont_hdr_lft fltlft"></div>
            <div class="cont_hdr_mdl1 fltlft" align="left"><?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?> Attribute</div>
            <div class="cont_hdr_rht fltlft"></div>
            <div class="clear"></div>
	    </div>
	    <div class="clear"></div>
	    <center>
            <div class="txtbg_top_smler"></div>
            <div class="txtbg_mdl_smler">
                <form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/attribute/<?php if ($this->_tpl_vars['sm']['res']): ?>updateAttribute<?php else: ?>insertAttribute<?php endif; ?>/ce/0" name="adminaddattribute" id="adminaddattribute" method="post" onsubmit="return AsyncUpload.submitForm(this, validateAttribute, callbackFun);">
                <form action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/attribute/<?php if ($this->_tpl_vars['sm']['res']): ?>updateAttribute<?php else: ?>insertAttribute<?php endif; ?>/ce/0" name="adminaddattribute" id="adminaddattribute" method="post" onsubmit="return validateAttribute();">
                <input type="hidden" name="id_user" value="<?php echo $_SESSION['id_user']; ?>
" /> 
                <input type="hidden" name="qstart" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
" />   
                <input type="hidden" name="pname" id="pname" value="<?php echo $this->_tpl_vars['sm']['pname']; ?>
" />
                <input type="hidden" name="ctype" id="ctype" value="<?php echo $this->_tpl_vars['sm']['ctype']; ?>
" />
                <input type="hidden" name="stype" id="stype" value="<?php echo $this->_tpl_vars['sm']['stype']; ?>
" />
                <input type="hidden" id="referer" name='referer' value="<?php echo $this->_tpl_vars['sm']['referer']; ?>
" />
                <input type="hidden" name="id_attribute" id="id_attribute" value="<?php echo $this->_tpl_vars['sm']['res']['id_attribute']; ?>
" />
                <table border="0" width="92%" align="center" class="listing_tbl">
                    <tr>
                        <td width="23%">Name :</td>
                        <td align="left">
                            <input type="text" class="txt" name="attribute[attribute_name]" id="name_attribute" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['attribute_name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
" /> onblur="attributeExist('<?php echo $this->_tpl_vars['sm']['res']['id_attribute']; ?>
');" 
                               <br/>
                            <span id='attribute_error_name'></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Entry Type :</td>
                        <td>
                            <select name="attribute[entry_type]" id="entrytype" onchange="showEntryType(this);" >
                            <option value="">--Select--</option>
                            <?php echo smarty_function_html_options(array('options' => ((is_array($_tmp=$this->_tpl_vars['sm']['attr'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)),'selected' => $this->_tpl_vars['sm']['res']['entry_type']), $this);?>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Search Type :</td>
                        <td id="searchtypetd" style="vertical-align:top;">
                            <select name="attribute[search_type]" id="searchtype" onchange="showSearchType(this);">
                            <option value="">--Select--</option>
                            <?php echo smarty_function_html_options(array('options' => ((is_array($_tmp=$this->_tpl_vars['sm']['attr'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)),'selected' => $this->_tpl_vars['sm']['res']['search_type']), $this);?>

                            </select>
                        </td>
                    </tr>
		     <tr id="txtfield" <?php if ($this->_tpl_vars['sm']['res']['datatype']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Type :</td>
			 <td>
			     <div>
				<input type="radio" name="attribute[datatype]" id="txt_dt_int" value="1" onclick="$('#txt_dt_int_div').show();" <?php if ($this->_tpl_vars['sm']['res']['datatype'] == 1): ?>checked="checked"<?php endif; ?>/>Integer
				<input type="radio" name="attribute[datatype]" id="txt_dt_str" value="2" onclick="$('#txt_dt_int_div').hide();" <?php if ($this->_tpl_vars['sm']['res']['datatype'] == 2): ?>checked="checked"<?php endif; ?>/>String
				<br/>
				<label style="display:none;" class="error" generated="true" for="attribute[datatype]">This field  required</label>
				<br/>
			    </div>
			     <div id="txt_dt_int_div"<?php if ($this->_tpl_vars['sm']['res']['text_values']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
				<?php if (count($_from = (array)$this->_tpl_vars['txt_srch'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
				    <input type="radio" id="txt_<?php echo $this->_tpl_vars['key']; ?>
" name="attribute[text_values]" value="<?php echo $this->_tpl_vars['key']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['text_values'] == $this->_tpl_vars['key']): ?>checked="checked"<?php endif; ?>/><?php echo $this->_tpl_vars['item']; ?>

				<?php endforeach; endif; unset($_from); ?>
				<br>
				<label style="display: none;" class="error" generated="true" for="attribute[text_values]">This field is required</label>
			    </div>
                        </td>
                    </tr>
                    <tr id="boardfield" <?php if ($this->_tpl_vars['sm']['res']['dropdown_values']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td colspan="2"><input type="hidden" id="otherRadio" name="attribute[dropdown_values]" value="<?php echo $this->_tpl_vars['board_type']['other']; ?>
" /></td>
                    </tr>
                    <tr id="valuefield" <?php if ($this->_tpl_vars['sm']['res']['attribute_label']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Value :</td>
                        <td align="left"><input type="text" id="valuetext" name="attribute[attribute_label]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['attribute_label'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
" onblur="checkPattern();" onkeypress="hideValueErrMsg()"/>
                        <br><i>Note : Multiple values can be entered in comma(,) separated only.</i>
                        <br /><span id="errValue" class="error"></span>
                        </td>
                    </tr>
		    <tr id="optType" <?php if ($this->_tpl_vars['sm']['res']['attribute_label']): ?>style="display:true;"<?php else: ?>style="display:none;"<?php endif; ?>>
                        <td>Match Type :</td>
                        <td>
			    <input type="radio" id="strictType" name="attribute[optype]" value="<?php echo $this->_tpl_vars['match_type']['strict']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['optype'] == $this->_tpl_vars['match_type']['strict']): ?>checked="checked"<?php endif; ?>/>Strict  <?php echo $this->_tpl_vars['optype']['strict']; ?>

                            <input type="radio" id="allType" name="attribute[optype]" value="<?php echo $this->_tpl_vars['match_type']['all']; ?>
" <?php if ($this->_tpl_vars['sm']['res']['optype'] == $this->_tpl_vars['match_type']['all']): ?>checked="checked"<?php endif; ?>/>All	    <?php echo $this->_tpl_vars['optype']['all']; ?>

			    <br><label style="display: inline;" class="error" generated="true" for="attribute[optype]"></label>
			</td>
                    </tr>
                    <tr>
                        <td>Courses :</td>
                        <td align="left">
                            <select name="attribute[id_course]" multiple="multiple" id="course_name" onchange="hideCourseErrMsg()">
                            <?php if ($this->_tpl_vars['sm']['res']['id_course']): ?>
                            <?php $this->assign('courseArr', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['sm']['res']['id_course']) : explode($_tmp, $this->_tpl_vars['sm']['res']['id_course']))); ?>
                            <?php $this->assign('countArr', count($this->_tpl_vars['courseArr'])); ?>
                            <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['courseArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                            <?php $this->assign('d', $this->_tpl_vars['courseArr'][$this->_sections['i']['index']]); ?>	    
                                <?php if (count($_from = (array)$this->_tpl_vars['sm']['course'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['d']): ?>
                                 <option  selected="selected" value="<?php echo $this->_tpl_vars['d']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
                                <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endfor; endif; ?>
                            <?php endif; ?>
                            </select>
                            <br /><span id="errCourse" class="error"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="padd_tp">
                            <input type="submit" class="login_btn" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?>"/>
                            <input type="button" class="login_btn" name="cancel" value="Cancel" onclick="$.fancybox.close();" />
                        </td>
                    </tr>
                </table>
                </form>	 
            </div>
            <div class="txtbg_btm_smler"></div>
	    </center>
	</div>
    <div class="btm"></div>
    </div>
</div>-->
<!-- Template: admin/attribute/add.tpl.html End --> 
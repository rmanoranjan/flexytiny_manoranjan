<?php /* Smarty version 2.6.7, created on 2017-08-25 17:04:07
         compiled from admin/practice/xmllisting.tpl.html */ ?>

<!-- Template: admin/practice/xmllisting.tpl.html Start 25/08/2017 17:04:07 --> 
 <!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo '
        <script>
            function editxml(id,title,des){
                $("#xmlid").val(id);
                $("#title").val(title);
                $("#description").val(des);
             }
            function deletexml(id){
                $.post(siteurl,{"page":"practice","choice":"deletexml","ce":0,"sno":id },function(res){
                window.location.href=siteurl+"practice/xmldemo";
                 });
             }
        </script>
        '; ?>

    </head>
    <body>
        <div>
            <form id="xmlform" method="post" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/practice/insertxml">
                <input type="hidden" id="xmlid" value="" name="id">
                <table ALIGN="center">
                    <tr><td>TITLE:</td><td><input type="text" id="title" name="title"></td></tr>
                    <tr><td>DESCRIPTION:</td><td><textarea id="description" name="description"></textarea></td></tr>
                    <tr><td colspan="2" align="center"><input type="submit" value="submit" class="btn-warning"></td></tr>
                </table>
            </form>
        </div>
        <div>
            <table align="center" width="50%" border="1">
                <thead>
                    <tr>
                        <th colspan="3">
                            XML DATA
                        </th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th>ID</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>ACTION</th>
                </tr>
                <?php if (count($_from = (array)$this->_tpl_vars['sm']['list'])):
    foreach ($_from as $this->_tpl_vars['data']):
?>
                <?php $this->assign('x', $this->_tpl_vars['data']); ?>
                <tr> 
                    <td align="center"><?php echo $this->_tpl_vars['data']['id']; ?>
</td>
                    <td align="center"><?php echo $this->_tpl_vars['data']['title']; ?>
</td>
                    <td align="center"><?php echo $this->_tpl_vars['data']['description']; ?>
</td>
                    <td align="center"><a onclick="editxml('<?php echo $this->_tpl_vars['data']['id']; ?>
','<?php echo $this->_tpl_vars['data']['title']; ?>
','<?php echo $this->_tpl_vars['data']['description']; ?>
')">edit</a>&nbsp;<a onclick="deletexml('<?php echo $this->_tpl_vars['data']['id']; ?>
')">delete</a></td>
                </tr>
                <?php endforeach; endif; unset($_from); ?>
                </tbody>
            </table>
        </div>
    </body>
</html>

<!-- Template: admin/practice/xmllisting.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-06-06 12:47:44
         compiled from admin/userlisting/form_user.tpl.html */ ?>

<!-- Template: admin/userlisting/form_user.tpl.html Start 06/06/2017 12:47:44 --> 
 
<html>
    <head>
        <?php echo '
        <script>
  function checkValidate(){
var validator=$("#regform").validate({
    rules:{
        \'user[name]\':
            {
        required:true,
         },
        \'user[phoneno]\':{
        required:true,
        number:true,
        maxlength:10,
        minlength:10,
         },
        \'user[gender]\':{
        required:true,
         },
        \'user[address]\':{
        required:true
         },
        \'user[pin]\':{
        required:true,
        number:true,
        maxlength:6,
        minlength:6,
         }
         },
        messages:{
        \'user[name]\':{
        required:flexymsg.required,
         },
        \'user[phoneno]\':{
        required:flexymsg.required,
         },
        \'user[gender]\':{
        required:flexymsg.required,
         },
        \'user[address]\':{
        required:flexymsg.required,
         },
        \'user[pin]\':{
        required:flexymsg.required,
         }
         }
     });
    var x=validator.form();
    if(x)
    {
     return x;
     }
    else{
        return false;
     }
        }; 
//function callbackFun(res){
//var msg='; ?>
 <?php echo $this->_tpl_vars['sm']['res'];  echo ' ?\'updated successfully\':\'inserted successfully\';
//$.fancybox.close();
//messageShow(msg);
//$("#f1").html(res);
// }

         </script>
        '; ?>

        </head>
    <body>
        <form  id="regform" method="post" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin//userlisting/insert_user/ce/0"  align="center" onsubmit="return AsyncUpload.submitForm(this,checkValidate,callbackFun);" enctype="multipart/form-data" >
    <h1 align="center" style="color: darkblue "><?php if ($this->_tpl_vars['sm']['res']): ?>Update User Data<?php else: ?>Add user Data<?php endif; ?></h1>
    <table  cellspacing="0" cellpadding="5"  border="0" align="center" style="border:solid blue thick; background-color:#FFF8DC; width:50%" >
        <tr>
            <td>NAME<font class='errcls' color='red'><sup>*</sup></font>:</td>
            <td ><input type="text" name="user[name]" id="name" value=<?php echo $this->_tpl_vars['sm']['res']['name']; ?>
></td>
            <td></td>
        </tr>
        <tr>
            <td>PHONE NO<font class='errcls' color='red'><sup>*</sup></font>:</td>
            <td ><input type="text" name="user[phoneno]" id="phno" value="<?php echo $this->_tpl_vars['sm']['res']['phoneno']; ?>
"></td>
            <td><span id='sp1'></span></td>
        </tr>
        <tr>
            <td>GENDER<font class='errcls' color='red'><sup>*</sup></font>:</td>
            <td ><input name="user[gender]" id="male" value="male" type="radio"   <?php if ($this->_tpl_vars['sm']['res']['gender'] == male): ?>checked<?php endif; ?> ><lable>Male</lable>
                <input name="user[gender]" id="female" value="female"  type="radio" <?php if ($this->_tpl_vars['sm']['res']['gender'] == female): ?>checked<?php endif; ?> ><lable>Female</lable>
                 <label for="user[gender]" generated="true" class="error"></label>        
    </td>
         </tr>          
        <tr>
            <td>ADDRESS<font class='errcls' color='red'><sup>*</sup></font>:</td>
            <td><textarea name="user[address]" id="adrs" cols="20" rows="5"><?php echo $this->_tpl_vars['sm']['res']['address']; ?>
</textarea></td>
           
        </tr>
        <tr>
            <td>PIN<font class='errcls' color='red'><sup>*</sup></font>:</td>
            <td><input type='text' id='pin' name='user[pin]' VALUE='<?php echo $this->_tpl_vars['sm']['res']['pin']; ?>
'></td>
        </tr>
        <tr>
            <td><input type="hidden" name='user[id]' id='hd1' value='<?php echo $this->_tpl_vars['sm']['res']['id']; ?>
'></td>
            <td ><input type="submit" id="submit"<?php if ($this->_tpl_vars['sm']['res']): ?> value="update"<?php else: ?> value="submit"<?php endif; ?>>
               
            </td> 
        </tr>  
    </table> 
    </form>
    </body>
</html>

<!-- Template: admin/userlisting/form_user.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-08-24 13:19:57
         compiled from admin/user/register.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_radios', 'admin/user/register.tpl.html', 289, false),array('function', 'html_checkboxes', 'admin/user/register.tpl.html', 297, false),array('function', 'html_select_date', 'admin/user/register.tpl.html', 316, false),)), $this); ?>

<!-- Template: admin/user/register.tpl.html Start 24/08/2017 13:19:57 --> 
 <?php $this->assign('check_field', $this->_tpl_vars['util']->get_values_from_config('LOGIN')); ?>
<?php echo '
<style type="text/css">
	 .formtbl tr td select{
		  width:120px;
		  margin-right:5px;
	  }
     .updateprofileadmin_fancy{display:none; }
</style>
<script type="text/javascript">
    var emailcheck;
    var usernamecheck;
    $(function(){
	var c="';  echo $this->_tpl_vars['check_field']['name'];  echo '";
	if(c==1){
	    usernamecheck=true;
	    emailcheck=false;
	 }else{
	    usernamecheck=false;
	    emailcheck=true;
	 }
     });
    $.validator.addMethod("username", function(value) {
	return /^[a-z0-9]([a-z0-9_.]+)?$/i.test(value);
     },\'<br>Please enter a valid username\'
);
    function checkValidate() {
	var validator=$("#signupform").validate({
	    rules: {
		"user[first_name]": {
		    required:true,
		    maxlength: 50
		 },
		"user[last_name]":{
		    required: true,
		    maxlength: 50						
		 },
		"user[username]":{
		    required:true,
		    maxlength: 20,
		    username:usernamecheck,
		    email:emailcheck
		 },
		"user[email]":{
		    required: true,
		    email: true
		 },
		"user[password]":{
		    required: true,
		    minlength: 6,
		    maxlength: 20
		 },
		"cpwd":{
		    required: true,
		    equalTo:\'#pwd\'
		 },
		"gender":{
		    required: true
		 },
		"user[address]":{
		    required: true
		 },
		"dob_Month":{
		    required: true
		 },
		"dob_Year":{
		    required: true
		 },
		"dob_Day":{
		    required: true
		 }
	     },
	    messages: {
		"user[first_name]": {
		    required:flexymsg.required,
		    maxlength:flexymsg.mixlength
		 },
		"user[last_name]":{
		    required: flexymsg.required,
		    maxlength:flexymsg.mixlength
		 },
		"user[username]" :{
		    required: flexymsg.required,
		    maxlength:flexymsg.mixlength
		 },
		"user[email]" :{
		    required: flexymsg.required,
		    email: flexymsg.email
		 },
		"user[password]":{
		    required:flexymsg.required,
		    minlength:flexymsg.minlength,
		    maxlength:flexymsg.mixlength
		 },
		"cpwd":{
		    required: flexymsg.required,
		    equalTo: flexymsg.equalTo
		 },
		"gender" :{
		    required: flexymsg.required
		 },
		"user[address]" :{
		    required: flexymsg.required
		 },
		"dob_Month" :{
		    required: flexymsg.required_dob
		 },
		"dob_Year" :{
		    required: flexymsg.required_dob
		 },
		"dob_Day" :{
		    required: flexymsg.required_dob
		 }					
	     }
	 });
	var x=validator.form();
	return x;
     }
    function checkDate(e) {
			
     }
    function showUsernameExist() {
	var username=document.getElementById(\'username\').value;
	var url="http://manoranjan.afixiindia.com/flexytiny_new/";
	$.post(url,{"page":"user","choice":"checkUsername",ce:0,"username":username },function(res){
			
	    if(res!=0 && username!=""){
		$(\'#add_user_msg\').html("<div class=\'message\'>This Username Already Exist.</div>");
		$(\'#username\').val("");
		return false;
	     }else{
		$(\'#add_user_msg\').html(\'\');
		return true;
	     }
	 });
     }
    function checkPassword() {
	var text=document.getElementById(\'pwd\').value;
	var i,s,color,width;
	var n_o_small_char=0;
	var n_o_cap_char=0;
	var n_o_spe_char=0;
	var n_o_dig=0;
	var point=0;
	for(i=0;i<text.length;i++){
	    if(97<=text.charCodeAt(i) && text.charCodeAt(i)<=122) {
		point++;
		n_o_small_char=n_o_small_char+1;
	     }else if(65<=text.charCodeAt(i) && text.charCodeAt(i)<=90){
		point=point+2;
		n_o_cap_char++;
	     }else if(48<=text.charCodeAt(i) && text.charCodeAt(i)<=57){
		point=point+2;
		n_o_dig++;
	     }else if(text.charCodeAt(i)!=32){
		n_o_spe_char++;
		point=point+3;
	     }
	 }	
			
	if(n_o_small_char>0 && n_o_cap_char>0 && n_o_spe_char>0 && n_o_dig>0){
	    point=point+4;
	 }
	if(n_o_spe_char>2){
	    point=point+3;
	 }
			
	if(point<10){
	    if(!point){
		s="";
		width=0;
	     }else{
		s="poor";
		color="#FFFFCC";
		width=30;
	     }
	 }else if(10<=point && point<=15){
	    s="good";
	    color="#CCFFFF";
	    width=60;
	 }else if(point>=15){
	    s="best";
	    width=100;
	    color="#00FF00";
	 }
	document.getElementById(\'status\').innerHTML=s;
	//document.getElementById(\'pnts\').innerHTML=point;
	document.getElementById(\'subpassprogressbar\').style.backgroundColor=color;
	document.getElementById(\'subpassprogressbar\').style.width=width+\'%\';
     }
	function updateUser(){
		var qstart = "';  echo $this->_tpl_vars['sm']['qstart'];  echo '"; 
		var url = siteurl+"?"+$("#signupform").serialize();
		$.post(url,{\'page\':\'user\',\'choice\':\'updateUserProfile\',\'ce\':\'0\',\'qstart\':qstart },function(res){
			//show_fancybox(res);
			$("#user_listing").html(res);
			$.fancybox.close();
		 });
	 }
</script> 
'; ?>

<!--started by gayatree-->
<div style="width: 500px;">
  <div class="row-fluid">
<div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget green">
                           <div class="widget-title">
                                <h4><i class="icon-reorder"></i><?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?> User Profile</h4>
<!--                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>-->
                            </div>
<!--    <div class="headprt settheme">
	<div class="mdl">
	    <span><?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?> User Profile</span>
	</div>
    </div>-->
    <div id="error" align="center" style="display:none"></div>
    <div class="widget-body">
	<form id="signupform" name="signupform" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/user/<?php if ($this->_tpl_vars['sm']['flag']): ?>updateUserProfile<?php else: ?>insert<?php endif; ?>" method="post" onSubmit="return checkValidate();">
	    <input type="hidden" name="id_user" value="<?php echo $this->_tpl_vars['sm']['res']['id_user']; ?>
" />
	    <input type="hidden" name="qstart" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
">
	    <input type="hidden" name="search[fname]" value="<?php echo $this->_tpl_vars['sm']['search_val']['0']; ?>
"/>
	    <input type="hidden" name="search[email]" value="<?php echo $this->_tpl_vars['sm']['search_val']['1']; ?>
"/>
	    <input type="hidden" name="search[addr]" value="<?php echo $this->_tpl_vars['sm']['search_val']['2']; ?>
"/>
	    <input type="hidden" name="search[cond]" value="0"/>
	    <table class="table table-striped">
		<tr>
		    <td width="20%">First Name:</td>
		    <td>
			<input type="text" name="user[first_name]" id="fname" value="<?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
" />
			<span id="err"><?php echo $this->_tpl_vars['sm']['err']['first_name']; ?>
</span> 
		    </td>
		</tr>
		<tr>
		    <td>Last Name :</td>
		    <td>
			<input type="text" name="user[last_name]" id="lname" value="<?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
" />
			<span id="err1"><?php echo $this->_tpl_vars['sm']['err']['last_name']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Username :</td>
		    <td>
			<input type="text" name="user[username]" id="username" value="<?php echo $this->_tpl_vars['sm']['res']['username']; ?>
" <?php if ($this->_tpl_vars['sm']['flag']): ?>readonly="readonly"<?php endif; ?> <?php if (! $this->_tpl_vars['sm']['flag']): ?>onblur="return showUsernameExist();"<?php endif; ?> />
			       <span id="add_user_msg"><b></b></span>
			<span id="err2"><?php if ($this->_tpl_vars['sm']['d_name']):  echo $this->_tpl_vars['sm']['d_name'];  else:  echo $this->_tpl_vars['sm']['err']['username'];  endif; ?></span>
		    </td>
		</tr>
		<tr>
		    <td>Email :</td>
		    <td>
			<input type="text" name="user[email]" id="email" value="<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
" />
			<span id="err3"><?php echo $this->_tpl_vars['sm']['err']['email']; ?>
</span>
		    </td>
		</tr>
		<?php if (! $this->_tpl_vars['sm']['flag']): ?>
		<tr>
		    <td>Password :</td>
		    <td>
			<input type="password" name="user[password]" id="pwd" value="<?php echo $this->_tpl_vars['sm']['res']['password']; ?>
" onkeyup="checkPassword();"/>
			<span id="err4"><?php echo $this->_tpl_vars['sm']['err']['password']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td colspan="2">
			<div id="passprogressbar">
			    <div id="subpassprogressbar">
				<span id="status">&nbsp;</span>
			    </div>
			</div>
		    </td>
		</tr>
		<tr>
		    <td>Confirm Password :</td>
		    <td>
			<input type="password" name="cpwd" id="cpwd" value="<?php echo $this->_tpl_vars['sm']['conf_pwd']; ?>
" />
			<span id="err9"><?php echo $this->_tpl_vars['sm']['err']['conf_pwd']; ?>
</span>
		    </td>
		</tr>
		<?php endif; ?>
		<tr>
                    <td>Gender </td>
		    <td>
		    <?php echo smarty_function_html_radios(array('name' => 'gender','options' => $this->_tpl_vars['sm']['gender'],'selected' => $this->_tpl_vars['sm']['res']['gender']), $this);?>
<br />
			<label for="gender" class="error" style="display:none">Please choose one option.</label>
			<span id="err5"><?php echo $this->_tpl_vars['sm']['err']['gender']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Hobbies :</td>
		    <td>
		<?php echo smarty_function_html_checkboxes(array('name' => 'hobbies','options' => $this->_tpl_vars['sm']['hobbies'],'selected' => $this->_tpl_vars['sm']['res']['hobbies']), $this);?>

			<span id="err6"><?php echo $this->_tpl_vars['sm']['err']['hobbies']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Address :</td>
		    <td>
			<textarea name="user[address]" id="address" cols="16" rows="3" ><?php echo $this->_tpl_vars['sm']['res']['address']; ?>
</textarea>
			<span id="err7"><?php echo $this->_tpl_vars['sm']['err']['address']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Date of Birth :</td>
		    <td>
		    <?php if ($this->_tpl_vars['sm']['res']['dob']): ?>
		    <?php $this->assign('date', $this->_tpl_vars['sm']['res']['dob']); ?>
		    <?php else: ?>
		    <?php $this->assign('date', "--"); ?>
		    <?php endif; ?>
		    <?php echo smarty_function_html_select_date(array('prefix' => 'dob_','start_year' => "-110",'end_year' => "+1",'year_empty' => 'Year','month_empty' => 'Month','day_empty' => 'Date','reverse_years' => true,'time' => $this->_tpl_vars['date'],'month_extra' => 'onchange="check_date();"','day_extra' => 'id="dt"'), $this);?>

			<span id="err8"><?php echo $this->_tpl_vars['sm']['err']['dob']; ?>
</span>
		    </td>
		</tr>
<!--		<tr>
		    <td>&nbsp;</td>
		    <td>
			<div class="settheme fltlft">
			<input type="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>submit<?php endif; ?>" name="sub" value="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>Register<?php endif; ?>" class="buton" <?php if ($this->_tpl_vars['sm']['flag']): ?>onclick="updateUser();"<?php endif; ?>/>
			</div>
			<div class="clear"></div>
		    </td>
		</tr>-->
	    </table>
        <div class="form-actions" align="center">

<input type="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>submit<?php endif; ?>" name="sub" value="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>Register<?php endif; ?>" class="btn btn-success" <?php if ($this->_tpl_vars['sm']['flag']): ?>onclick="updateUser();"<?php endif; ?>//>

<!--<input type="button" class="btn" value="Cancel" onclick="$.fancybox.close();" />-->

</div>
	</form>
      </div>
    </div>
                        </div>
    </div>
</div>

<!--ended by gayatree-->




<div style="width: 500px;" class="updateprofileadmin_fancy">
    <div class="headprt settheme">
	<div class="mdl">
	    <span><?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Add<?php endif; ?> User Profile</span>
	</div>
    </div>
    <div id="error" align="center" style="display:none"></div>
    <div class="bodyprt">
	<form id="signupform" name="signupform" action="http://manoranjan.afixiindia.com/flexytiny_new/flexyadmin/user/<?php if ($this->_tpl_vars['sm']['flag']): ?>updateUserProfile<?php else: ?>insert<?php endif; ?>" method="post" onSubmit="return checkValidate();">
	    <input type="hidden" name="id_user" value="<?php echo $this->_tpl_vars['sm']['res']['id_user']; ?>
" />
	    <input type="hidden" name="qstart" value="<?php echo $this->_tpl_vars['sm']['qstart']; ?>
">
	    <input type="hidden" name="search[fname]" value="<?php echo $this->_tpl_vars['sm']['search_val']['0']; ?>
"/>
	    <input type="hidden" name="search[email]" value="<?php echo $this->_tpl_vars['sm']['search_val']['1']; ?>
"/>
	    <input type="hidden" name="search[addr]" value="<?php echo $this->_tpl_vars['sm']['search_val']['2']; ?>
"/>
	    <input type="hidden" name="search[cond]" value="0"/>
	    <table border="0" class="formtbl">
		<tr>
		    <td width="20%">First Name:</td>
		    <td>
			<input type="text" name="user[first_name]" id="fname" value="<?php echo $this->_tpl_vars['sm']['res']['first_name']; ?>
" />
			<span id="err"><?php echo $this->_tpl_vars['sm']['err']['first_name']; ?>
</span> 
		    </td>
		</tr>
		<tr>
		    <td>Last Name :</td>
		    <td>
			<input type="text" name="user[last_name]" id="lname" value="<?php echo $this->_tpl_vars['sm']['res']['last_name']; ?>
" />
			<span id="err1"><?php echo $this->_tpl_vars['sm']['err']['last_name']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Username :</td>
		    <td>
			<input type="text" name="user[username]" id="username" value="<?php echo $this->_tpl_vars['sm']['res']['username']; ?>
" <?php if ($this->_tpl_vars['sm']['flag']): ?>readonly="readonly"<?php endif; ?> <?php if (! $this->_tpl_vars['sm']['flag']): ?>onblur="return showUsernameExist();"<?php endif; ?> />
			       <span id="add_user_msg"><b></b></span>
			<span id="err2"><?php if ($this->_tpl_vars['sm']['d_name']):  echo $this->_tpl_vars['sm']['d_name'];  else:  echo $this->_tpl_vars['sm']['err']['username'];  endif; ?></span>
		    </td>
		</tr>
		<tr>
		    <td>Email :</td>
		    <td>
			<input type="text" name="user[email]" id="email" value="<?php echo $this->_tpl_vars['sm']['res']['email']; ?>
" />
			<span id="err3"><?php echo $this->_tpl_vars['sm']['err']['email']; ?>
</span>
		    </td>
		</tr>
		<?php if (! $this->_tpl_vars['sm']['flag']): ?>
		<tr>
		    <td>Password :</td>
		    <td>
			<input type="password" name="user[password]" id="pwd" value="<?php echo $this->_tpl_vars['sm']['res']['password']; ?>
" onkeyup="checkPassword();"/>
			<span id="err4"><?php echo $this->_tpl_vars['sm']['err']['password']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td colspan="2">
			<div id="passprogressbar">
			    <div id="subpassprogressbar">
				<span id="status">&nbsp;</span>
			    </div>
			</div>
		    </td>
		</tr>
		<tr>
		    <td>Confirm Password :</td>
		    <td>
			<input type="password" name="cpwd" id="cpwd" value="<?php echo $this->_tpl_vars['sm']['conf_pwd']; ?>
" />
			<span id="err9"><?php echo $this->_tpl_vars['sm']['err']['conf_pwd']; ?>
</span>
		    </td>
		</tr>
		<?php endif; ?>
		<tr>
		    <td><Gender :/td>
		    <td>
		    <?php echo smarty_function_html_radios(array('name' => 'gender','options' => $this->_tpl_vars['sm']['gender'],'selected' => $this->_tpl_vars['sm']['res']['gender']), $this);?>
<br />
			<label for="gender" class="error" style="display:none">Please choose one option.</label>
			<span id="err5"><?php echo $this->_tpl_vars['sm']['err']['gender']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Hobbies :</td>
		    <td>
		<?php echo smarty_function_html_checkboxes(array('name' => 'hobbies','options' => $this->_tpl_vars['sm']['hobbies'],'selected' => $this->_tpl_vars['sm']['res']['hobbies']), $this);?>

			<span id="err6"><?php echo $this->_tpl_vars['sm']['err']['hobbies']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Address :</td>
		    <td>
			<textarea name="user[address]" id="address" cols="16" rows="3" ><?php echo $this->_tpl_vars['sm']['res']['address']; ?>
</textarea>
			<span id="err7"><?php echo $this->_tpl_vars['sm']['err']['address']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>Date of Birth :</td>
		    <td>
		    <?php if ($this->_tpl_vars['sm']['res']['dob']): ?>
		    <?php $this->assign('date', $this->_tpl_vars['sm']['res']['dob']); ?>
		    <?php else: ?>
		    <?php $this->assign('date', "--"); ?>
		    <?php endif; ?>
		    <?php echo smarty_function_html_select_date(array('prefix' => 'dob_','start_year' => "-110",'end_year' => "+1",'year_empty' => 'Year','month_empty' => 'Month','day_empty' => 'Date','reverse_years' => true,'time' => $this->_tpl_vars['date'],'month_extra' => 'onchange="check_date();"','day_extra' => 'id="dt"'), $this);?>

			<span id="err8"><?php echo $this->_tpl_vars['sm']['err']['dob']; ?>
</span>
		    </td>
		</tr>
		<tr>
		    <td>&nbsp;</td>
		    <td>
			<div class="settheme fltlft">
			<input type="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>submit<?php endif; ?>" name="sub" value="<?php if ($this->_tpl_vars['sm']['flag']): ?>Update<?php else: ?>Register<?php endif; ?>" class="buton" <?php if ($this->_tpl_vars['sm']['flag']): ?>onclick="updateUser();"<?php endif; ?>/>
			</div>
			<div class="clear"></div>
		    </td>
		</tr>
	    </table>
	</form>
    </div>
</div>
<!-- Template: admin/user/register.tpl.html End --> 
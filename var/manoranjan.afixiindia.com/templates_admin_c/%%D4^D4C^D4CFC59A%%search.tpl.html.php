<?php /* Smarty version 2.6.7, created on 2017-09-15 13:15:01
         compiled from admin/attribute/search.tpl.html */ ?>
<?php echo '
<style>
 .custom-combobox {
    position: relative;
    display: inline-block;
   }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    border:1px solid #cccccc;
   }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
   }
  ul.ui-autocomplete{max-height: 300px; overflow: hidden; overflow-y: auto; }
  /*.ui-icon{display:none !important; }*/
  .even td{background: none !important; }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active { border: 1px solid #ddd !important; background: #ffffff url(images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x; font-weight: bold; color: black !important; }
</style>
<script type="text/javascript">
//    $(document).ready(function(){
//	$("#combobox").autocomplete(siteurl + \'attribute/autoSearchField/fieldName/attribute_name/tblName/attribute/ce/0/\',{
//	    delay: 500
//	 });
		$(\'#search\').bind(\'keypress\', function(e) {//"search" is the id of from field and pass the parameter to searchByEnter function like wise  
		    searchByEnter(e,"searchList","1");// searchList(1) is ypur function name;
		 });
//     });
    function searchList(i){
	$(\'#succ_msg\').html(\'\');	
	if(i){
	    var pname   = $(\'#combobox\').val();
	    var etype = $(\'#combobox1\').val();
	    var stype = $(\'#combobox2\').val();
	    $.post(siteurl,{"page" : "attribute", "choice" : "listing", \'psearch\' : 1, \'pname\' : pname, \'etype\' : etype, \'stype\' : stype, \'ce\' : 0 },function(res){
		$("#attribute_listing").html(res);
		css_even_odd();
	     });
		
	 }else{
	    $.post(siteurl,{"page" : "attribute", "choice" : "listing", \'psearch\' : 1,\'ce\' : 0 },function(res){
		$("#attribute_listing").html(res);
		css_even_odd();
	     });
	    $(\'#search\')[0].reset();
	 }
     }
    
$(function(){
$("#combobox,#combobox1,#combobox2").combobox();
$("#toggle" ).on( "click", function() {
$("#combobox,#combobox1,#combobox2").toggle();
 });
$.post(siteurl, {"page": "attribute", "choice": "search_attribute", "psearch": 1,"ce": 0 }, function(res) {//alert(res);//return false;
var arr = res.split(\'@@\');
$(\'#combobox\').html(arr[0]);
$(\'#combobox1\').html(arr[1]);
$(\'#combobox2\').html(arr[2]);
 });
 });
</script>
'; ?>


<div class="center">
     <div id="succ_msg" align="center"></div>
     <div id="dv1">
         <div id="page-wraper">
           <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Search Attribute</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                            </div>
                            <div class="widget-body">
                        <form name="search" id="search" method="post" action="javascript:void(0);" class="form-horizontal">
                            <div class="control-group">
                               <table border="0" align="center" class="table table-striped table-bordered">
                    <tr>
			    <td width="16%" align="right">Attribute Name :</td>
			    <td>
                                <div class="ui-widget">
                                    <select name="attribute_name" id="combobox"></select>
                                </div>
			    </td>
			    <td width="16%" align="right">Entry Type :</td>
			    <td>
                                <div class="ui-widget">
				<select name="enttype" id="combobox1"></select>
                                </div>
			    </td>
                            <td width="16%" align="right">Search Type :</td>
			    <td> 
                                <div class="ui-widget">
                                    <select name="sertype" id="combobox2"></select>
                                </div>
			    </td>
			</tr>
                    <tr>
                        <td colspan="3">
                           <button class="btn btn-success fltrht" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
                        </td>
                        <td colspan="3">
                             <button class="btn btn-success" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>
                        </td>
                         </tr>
		         </table> 
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
             
                </div>
             </div>
       </div>
     </div>
 <div id="attribute_listing"> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/attribute/listing.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
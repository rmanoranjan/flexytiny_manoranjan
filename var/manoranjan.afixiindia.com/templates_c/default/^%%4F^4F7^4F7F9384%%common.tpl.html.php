<?php /* Smarty version 2.6.7, created on 2017-09-25 11:46:57
         compiled from common/common.tpl.html */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/metatag.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php $this->assign('footer', $this->_tpl_vars['util']->get_values_from_config('FOOTER')); ?>
		<?php $this->assign('facebook', $this->_tpl_vars['util']->get_values_from_config('FACEBOOK')); ?>
		<?php $this->assign('linkedin', $this->_tpl_vars['util']->get_values_from_config('LINKEDIN')); ?>
		<?php echo '
		<script>
			var lightboxImageUrl = "http://manoranjan.afixiindia.com/flexytiny_new/";//Here tp write this variable because in jq ver.1.7 it giving error lightboxImageUrl not defined.
		</script>
		'; ?>

		<?php if ($this->_tpl_vars['facebook']['fconnect'] == 1): ?>
		<script src="http://connect.facebook.net/en_US/all.js"></script>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['linkedin']['is_linkedin'] == 1): ?>
		<script type="text/javascript" src="http://platform.linkedin.com/in.js">
			api_key: <?php echo $this->_tpl_vars['linkedin']['api_key']; ?>

			scope: r_emailaddress
			authorize: true
		</script>
		<?php endif; ?>
		<?php echo '
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/1.7/jquery-1.7.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/js/jquery.lightbox.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/fancybox/jquery.fancybox-1.3.2.pack.js"></script>
		<!--<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/js/jquery.validate.js"></script>-->
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/pluginfor1.7/jquery.validate.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/flexyjs/flexymessage.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/js/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jqueryui/1.8.16/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/js/ajaxfileupload.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/jquery/js/ajaxupload.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexymvc_core/libsext/fckeditor/fckeditor.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/flexyjs/designjs.js"></script>
		<script type="text/javascript" src="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/flexyjs/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript">
			$(document).ajaxStart(function() {
				$.fancybox.showActivity();
			 });
			$(document).ajaxComplete(function() {
				$.fancybox.hideActivity();
			 });
			$(document).ajaxStop(function() {
				$.fancybox.hideActivity();
			 });
			var siteurl = "http://manoranjan.afixiindia.com/flexytiny_new/";
			//Show fancybox modal with content
			function show_fancybox(res) {
				$.fancybox.showActivity();
				$.fancybox(res, {centerOnScroll: true, hideOnOverlayClick: false });
			 }
			$(document).ajaxError(function(event, jqxhr, settings, exception) {
				if (jqxhr.status == 401) {
					alert("Session is expired. Please relogin to work on this page.");
					window.location.href = "http://manoranjan.afixiindia.com/flexytiny_new/";
				 }
				if (jqxhr.status == 404) {
					alert("Page not found.");
				 }
			 });
			//Pagination
			function get_next_page(url, start, limit, div_id) {
				if (!document.getElementById(div_id)) {
					div_id = "content";
				 }
				$("#" + div_id).load(url, {\'qstart\': start, \'limit\': limit, \'ce\': 0, \'pg\': 1 }, function(res) {
					css_even_odd();
				 });
			 }

			function ajaxLogin() {
				$.post(siteurl, {"page": "user", "choice": "loginForm", "ce": 0, "flg": 1 }, function(res) {
					show_fancybox(res);
				 });

				$.post(siteurl, {"page": "user", "choice": "loginForm", "ce": 0, "flg": 1 }, function(res) {
					$.fancybox.showActivity();
					$.fancybox(res, {
						hideOnOverlayClick: false,
						centerOnScroll: true,
						onComplete: function() {
							window.IN.init();
						 }
					 });
				 });
			 }

			function ajaxRegister() {
				$.post(siteurl, {"page": "user", "choice": "register", "ce": 0, "flg": 1 }, function(res) {
					show_fancybox(res);
				 });
			 }
			var fb_connect = "';  echo $this->_tpl_vars['facebook']['fconnect'];  echo '";
			if (parseInt(fb_connect)) {
				FB.init({
					appId: "';  echo $this->_tpl_vars['facebook']['app_id'];  echo '", cookie: true,
					xfbml: true, oauth: true, status: true
				 });
			 }

			function logout_fb() {
				var flag = "';  if ($_SESSION['fconnect_flag']):  echo $_SESSION['fconnect_flag'];  else: ?>0<?php endif;  echo '";
				FB.getLoginStatus(function(response) {
					$.post(siteurl, {\'page\': \'socialNetwork\', \'choice\': \'getUserId\', \'ce\': \'0\' }, function(res) {
						if (response.status == "connected" && (response.authResponse.userID == res) && flag) {
							//										alert("https://www.facebook.com/logout.php?next="+siteurl+"user/logout"+"&access_token=" + response.authResponse.accessToken);//return false;
							window.location.href = "https://www.facebook.com/logout.php?next=" + siteurl + "user/logout" + "&access_token=" + response.authResponse.accessToken;
						 } else {
							window.location.href = siteurl + "user/logout";
						 }
					 });
				 });
			 }

			function logout_linkedin() {
//				IN.User.logout(function() {
					window.location.href = siteurl + "user/logout";
//				 });
			 }



		</script>
		<link href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/lightbox.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/jquery-ui-1.8.16.custom.css"/>
		<link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/fancybox/jquery.fancybox-1.3.2.css"/>
		<link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/transform.css" />
		<link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/transformuser.css" />
        <link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/styleadmin.css" /><!--by gayatree-->
         <link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/bootstrap.css" /><!--by gayatree-->
        <link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/style-responsive.css" /><!--by gayatree-->
        <link rel="stylesheet" type="text/css" id="style_color" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/style-default.css"/><!--by gayatree-->
        <link rel="stylesheet" type="text/css" href="http://manoranjan.afixiindia.com/flexytiny_new/templates/default/css_theme/font-awesome.css" /><!--by gayatree-->
		'; ?>

	</head>
	<body>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        

		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/menu.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><br />
		<div class="center">
			<div id="mymodal"></div>
			<div style="color:#FF0000"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/messages.tpl.html", 'smarty_include_vars' => array('module' => 'global')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
			<div id="container"><?php echo $this->_tpl_vars['content_fetch']; ?>
</div>
			<div class="clear"></div>
			<div class="push"></div>
		</div>

		<div class="ftr_txt" align="center"><?php echo $this->_tpl_vars['footer']['copyright']; ?>
</div>
	</body>
</html>
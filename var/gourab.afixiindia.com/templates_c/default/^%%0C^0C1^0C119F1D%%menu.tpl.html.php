<?php /* Smarty version 2.6.7, created on 2017-04-04 12:48:32
         compiled from common/menu.tpl.html */ ?>

<!-- Template: common/menu.tpl.html Start 04/04/2017 12:48:32 --> 
 <?php $this->assign('rating', $this->_tpl_vars['util']->get_values_from_config('RATE')); ?>
<?php $this->assign('facebook', $this->_tpl_vars['util']->get_values_from_config('FACEBOOK')); ?>
<?php $this->assign('linkedin', $this->_tpl_vars['util']->get_values_from_config('LINKEDIN')); ?>
<?php $this->assign('twitter', $this->_tpl_vars['util']->get_values_from_config('TWITTER')); ?>
<div class="navigation" style="margin-top: 62px;">
    <div class="center" style="margin-left: 22px;">
        <ul class="user_menu">
            <?php if ($_SESSION['id_admin'] || $_SESSION['id_user']): ?>
                <li><a href="http://gourab.afixiindia.com/flexytiny_new/user/userHome" <?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'userHome'): ?>class="current"<?php endif; ?>>Home</a></li>
                <li><a href="http://gourab.afixiindia.com/flexytiny_new/product/listing" <?php if ($_REQUEST['page'] == 'product' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Product List</a></li>
                <li><a href="http://gourab.afixiindia.com/flexytiny_new/user/edit" <?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'edit'): ?>class="current"<?php endif; ?>>Edit Profile</a></li>
            <?php else: ?>
            <!-- Add your menu for visitor -->
            <?php endif; ?>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<?php echo '
<script type="text/javascript">
    $(document).ready(function () {
        $(\'ul#menu li.openul\').hover(function () {
            jQuery(this).find(\'> ul\').stop(true, true).slideDown(300).css(\'width\', \'150px\');
         }, function () {
            $(this).find(\'ul\').slideUp(300);
         });
     });
</script>
'; ?>


<!-- Template: common/menu.tpl.html End --> 
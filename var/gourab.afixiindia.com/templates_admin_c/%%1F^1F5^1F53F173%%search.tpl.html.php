<?php /* Smarty version 2.6.7, created on 2017-04-04 13:01:59
         compiled from admin/product/search.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/product/search.tpl.html', 123, false),array('modifier', 'escape', 'admin/product/search.tpl.html', 132, false),)), $this); ?>
<?php echo '
<style type="text/css">
.serch_admin{display:none; }
.tblbdr tr td{border-top:none !important; }
</style>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#product_name").autocomplete(siteurl + \'product/autoSearchField/fieldName/name/ce/0/\',{
		delay: 500
	     });
	    $("#product_code").autocomplete(siteurl + \'product/autoSearchField/fieldName/code/ce/0/\',{
		delay: 500
	     });
	    $("#product_desc").autocomplete(siteurl + \'product/autoSearchField/fieldName/description/ce/0/\',{
		delay: 500
	     });
        $(\'#search\').bind(\'keypress\', function(e) {//"search" is the id of from field and pass the parameter to searchByEnter function like wise  
            searchByEnter(e,"searchList","1");// searchList(1) is ypur function name;
         });
        searchList();
//        var sort_order = "';  echo $_SESSION['productlisting']['sort_order'];  echo '";
//        var sort_by = "';  echo $_SESSION['productlisting']['sort_by'];  echo '";
//        //alert(sort_by+"-----"+sort_order);
//        //    alert(\'<img src="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/\'+sort_order+\'.gif" />\');
//        $("#sort_by"+sort_by).html(\'<img src="http://gourab.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/\'+sort_order+\'.gif" />\');
//        //      alert($("#sort_by"+sort_by).html());
	 });
	function searchList(i){//alert(123)
	    $(\'#succ_msg\').html(\'\');	
	    if(i){
        var prod_srch = $("#search").serialize();
		$.post(siteurl,{"page" : "product", "choice" : "productListing", \'psearch\' : 1,"searchval":prod_srch, \'ce\' : 0 },function(res){//alert(res);
		    $("#product_listing").html(res);
		    css_even_odd();
		 });
		
	     }else{
         $.post(siteurl,{"page" : "product", "choice" : "productListing", \'psearch\' : 1,\'ce\' : 0,"reset":1 },function(res){
		    $("#product_listing").html(res);
		    css_even_odd();
		 });
		$(\'#search\')[0].reset();
	     }
	 }

    </script>
'; ?>


<!--done by gayatree starts-->

         <!-- BEGIN PAGE CONTAINER-->
         <!--<div class="container-fluid">-->
           <!--<div class="row-fluid">-->
             <!--<div class="span12">-->
           <!-- BEGIN THEME CUSTOMIZER-->
<!--                   <div id="theme-change" class="hidden-phone">
                       <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                   </div>-->
                   <!-- END THEME CUSTOMIZER-->
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
<!--                   <h3 class="page-title">
                     Product
                   </h3>-->
<!--                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Home</a>
                           <span class="divider">/</span>
                       </li>
                       <li>
                           <a href="#">Management</a>
                           <span class="divider">/</span>
                       </li>
                       <li class="active">
                           Product
                       </li>
                       <li class="pull-right search-wrap">
                           <form action="search_result.html" class="hidden-phone">
                               <div class="input-append search-input-area">
                                   <input class="" id="appendedInputButton" type="text">
                                   <button class="btn" type="button"><i class="icon-search"></i> </button>
                               </div>
                           </form>
                       </li>
                   </ul>-->
                   <!-- END PAGE TITLE & BREADCRUMB-->
           
           
           
           <!--</div>-->
           <!--</div>-->
           <div class="center">
           <div id="page-wraper">
           <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i> Search Product</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                            </div>
                            <div class="widget-body">
                              <form name="search" id="search" method="post" action="javascript:void(0);" class="form-horizontal">
                                  <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
                            <div class="control-group">
                                <label class="control-label">Category  </label>
                                <div class="controls">
                                  <select name="product_category" id="product_category" class="span6 " >
                                     <option value=''> -Select-</option>
                                     <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['category'],'selected' => $this->_tpl_vars['sm']['cat_select']), $this);?>

                                </select>
                                    <!--<input type="text" class="span6 " />-->
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Product Name  </label>
                                <div class="controls">
                                    <input class="span6 " type="text" name="product_name" id="product_name" value="<?php if ($this->_tpl_vars['sm']['res']['name']):  echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  else:  echo ((is_array($_tmp=$_REQUEST['pname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?>" />
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <button class="btn btn-success" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
<button class="btn btn-success" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>
                            </div>
                                   <?php endif; ?>
                            </form>
                              
<!--                              <form name="search" id="search" class="basic search_frm" method="post" action="javascript:void(0);">
                                <table class="table tblbdr">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
                                    <tr>
                                        <td class="algnrht pdtop">Category :</td>
                                        <td class="algnlft pdtop"><select name="product_category" id="product_category" >
                                     <option value=''> -Select-</option>
                                     <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['category'],'selected' => $this->_tpl_vars['sm']['cat_select']), $this);?>

                                </select></td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td class="algnrht pdtop">Product Name :</td>
                                        <td  class="algnlft pdtop">
                                <input type="text" name="name" id="product_name" value="<?php if ($this->_tpl_vars['sm']['res']['name']):  echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  else:  echo ((is_array($_tmp=$_REQUEST['pname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?>" />
                           </td>
                                        <td><div class="form-actions" align="center">
<button class="btn btn-success" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
<button class="btn btn-success" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>

</div></td>
                                        
                                    </tr>
                                    <?php endif; ?>-->
                                    
<!--                                            <td><div><input type="button" name="search" value="Search" onclick="searchList(1);" class="buton" /></div></td>
                                            <td><div><input type="reset" name="search" value="Reset" onclick="searchList(0);" class="buton" /></div></td>-->
                                            
                                            
                                             
                                        
<!--                                        <td>the Bird</td>
                                        <td>@twitter</td>-->
                                    
                                    <!--</tbody>-->
                                <!--</table>-->
<!--                                <div class="form-actions" align="center">
<button class="btn btn-success" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
<button class="btn" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>

</div>-->
                                <!--</form>-->
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
             
                </div>
             </div>
             </div>
           <div id="product_listing" class="">
    <?php if ($this->_tpl_vars['sm']['prod_type'] == 1): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/product/singlelisting.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/product/listing.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>
</div>
         <!--</div>-->
         
<!--done by gayatree end-->









<!--





<div id="succ_msg" align="center"></div>
<div id="dv1" class="fltlft mrglft5 serch_admin">
    <div class="makebox center">
    	<div class="headprt settheme">
            <div class="mdl">
            	<span>Search Product</span>
            </div>
        </div>
        <div class="bodyprt">
            <form name="search" id="search" class="basic" method="post" action="javascript:void(0);">
                 <table border="0" align="center" class="formtbl">
                      <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
                      <tr>
                           <td>Category :</td>
                           <td>
                                <select name="product_category" id="product_category" >
                                     <option value=''> -Select-</option>
                                     <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['category'],'selected' => $this->_tpl_vars['sm']['cat_select']), $this);?>

                                </select>
                           </td>
                           </tr>
                           <tr>
                           <td>Product Name :</td>
                           <td>
                                <input type="text" name="product_name" id="product_name" value="<?php if ($this->_tpl_vars['sm']['res']['name']):  echo ((is_array($_tmp=$this->_tpl_vars['sm']['res']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  else:  echo ((is_array($_tmp=$_REQUEST['pname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?>" />
                           </td>
                      </tr>
                      <?php endif; ?>
                      <tr>
                           <td>Product Code :</td>
                           <td>
                                <input type="text" name="product_code" id="product_code" value="<?php if ($this->_tpl_vars['sm']['res']['code']):  echo $this->_tpl_vars['sm']['res']['code'];  else:  echo $_REQUEST['code'];  endif; ?>" />
                           </td>
                           </tr>
                           <tr>
                           <td>Product Description :</td>
                           <td>
                                <input type="text" name="product_desc" id="product_desc" value="<?php if ($this->_tpl_vars['sm']['res']['desc']):  echo $this->_tpl_vars['sm']['res']['desc'];  else:  echo $_REQUEST['desc'];  endif; ?>" />
                           </td>
                      </tr>
                      <tr>
                      <td></td>    
                           <td>
                           	<table>
                            	<tr>
                                	<td><div class="settheme fltlft"><input type="button" name="search" value="Search" onclick="searchList(1);" class="buton" /></div></td>
                                    <td><div class="settheme fltlft"><input type="reset" name="search" value="Reset" onclick="searchList(0);" class="buton" /></div></td>
                                </tr>
                            </table>
                           
                                
                           </td>
                      </tr>
                 </table>
                </form>
        </div>
    </div>
</div>
<div id="product_listing" class="">
    <?php if ($this->_tpl_vars['sm']['prod_type'] == 1): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/product/singlelisting.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/product/listing.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>
</div>-->
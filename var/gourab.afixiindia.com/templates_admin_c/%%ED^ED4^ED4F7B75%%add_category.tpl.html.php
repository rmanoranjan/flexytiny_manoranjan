<?php /* Smarty version 2.6.7, created on 2017-04-04 13:00:37
         compiled from admin/cms/add_category.tpl.html */ ?>

<!-- Template: admin/cms/add_category.tpl.html Start 04/04/2017 13:00:37 --> 
 <?php $this->assign('image_limit', $this->_tpl_vars['util']->get_values_from_config('IMAGE'));  echo '
<style type="text/css">
	 label.error{
		  font-size:12px;
		  display:block;
	  }
</style>
<script type="text/javascript">

    function validateProduct() {
	var validator=$("#adminaddproduct").validate({
	    rules: {
		"cat[name]": {
		    required:true
		 },
		"cat[description]":{
		    required: true
		 },
		"cat[code]": {
		    required:true
		 }
	     },
	    messages: {
		"cat[name]":{
		    required:"<br>"+flexymsg.required
		 },
		"cat[description]":{
		    required:"<br>"+flexymsg.required
		 },
		"cat[code]": {
		    required:"<br>"+flexymsg.required
		 }
	     }
	 });
	x=validator.form();
        var y = $(\'#code_err\').text().length;
        var z = $(\'#name_err\').text().length;
        if(x && !y && !z){
            $(\'#btn_sbmt\').attr("disabled",true);
            return x;
         }else{
            $(\'#btn_sbmt\').removeAttr("disabled");
            return false;
 
         }
     }

    function callbackFun(response) {
        var msg = "';  echo $this->_tpl_vars['sm']['res'];  echo '"?"Category updated Successfully":"Category added successfully";
	 $.fancybox.close();
         messageShow(msg);
	    $(\'#cms_category_listing\').html(response);
	 }
	function convertToCodeArticle(){
        var strval=$("#name_cat").val();
	var p1 = /[\'’.]+/g;
	var nval1=strval.replace(p1,"");
	var str = \'\';
	for(var i=0;i<nval1.length;i++)
	str += convertExtendedAsciiCharArticle(nval1[i]);
	var pattern=/[^\\w\\d]+|[\\s]+/g;
	var nval=str.replace(pattern,"-").toLowerCase();
	pattern = /-$|^-/g;
	var finl = nval.replace(pattern,"");
	$(\'#code_code\').val(finl);
         }
    /*
     * convert all spanish letter to us US keyboard letter.
     */
    function convertExtendedAsciiCharArticle(str){
	str = str.replace(/[ÀÁÂÃÄÅÆàáâãäåæ]+/g, \'a\');
	str = str.replace(/[öÒÓÔÕÖðòóôõö]+/g, \'o\');
        str = str.replace(/[ç]+/g, \'c\');
        str = str.replace(/[şŠš]+/g, \'s\');
        str = str.replace(/[ı¡]+/g, \'i\');
        str = str.replace(/[ğ]+/g, \'g\');
        str = str.replace(/[üÙÚÛÜùúûü]+/g, \'u\'); 
	str = str.replace(/[ýÿŸ¥]+/g, \'y\');
	str = str.replace(/[ž]+/g, \'z\');
	str = str.replace(/[€Œ]+/g, \'e\');
	str = str.replace(/[µ]+/g, \'e\');
	return str;
     }
    
    function uniquename(id){
        var nameval = $(\'#name_cat\').val();
        $.post(siteurl,{"page" : "cms", "choice" : "uniquename", "ce" : "0", "nameval" : nameval,"id":id },function(res){//alert(res);return false;
        if(res == 1){
                $("#name_err").html(\'<font color="red">Name already Exist.</font>\');
             }else{
               $("#name_err").html(\'\'); 
               convertToCodeArticle();
             }
         });
         }
    function uniquecode(id){
        var codeval = $(\'#code_code\').val();
        $.post(siteurl,{"page" : "cms", "choice" : "uniquecode", "ce" : "0", "codeval" : codeval,"id":id },function(res){//alert(res);return false;
        if(res == 1){
                $("#code_err").html(\'<font color="red">Code already Exist.</font>\');
             }else{
               $("#code_err").html(\'\'); 
             }
         });
         }
</script>
'; ?>

<div id="dv2">
    <div style="width:500px;">
        <div class="headprt settheme">
            <div class="mdl">
                <span><?php if ($this->_tpl_vars['sm']['res']): ?>Edit<?php else: ?>Add<?php endif; ?> Category</span>
            </div>
        </div>
        <div class="bodyprt">
            <form action="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/cms/insertCategory/ce/0/" name="adminaddproduct" id="adminaddproduct" enctype="multipart/form-data" method="post" onsubmit="return AsyncUpload.submitForm(this, validateProduct, callbackFun);">
            <!--<form action="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/cms/insertCategory/ce/0/" name="adminaddproduct" id="adminaddproduct" enctype="multipart/form-data" method="post">-->
                <input type="hidden" name="qstart" value="<?php if ($this->_tpl_vars['sm']['qstart']):  echo $this->_tpl_vars['sm']['qstart'];  else: ?>0<?php endif; ?>" />   
                <input type="hidden" name="id_cat" value="<?php echo $this->_tpl_vars['sm']['res']['id_cmscategory']; ?>
" />   
                <table border="0" class="formtbl">
		    <tr>
			<td>Name :</td>
			<td>
			    <input type="text" class="txt" name="cat[name]" id="name_cat" value="<?php echo $this->_tpl_vars['sm']['res']['name']; ?>
" onchange="uniquename('<?php echo $this->_tpl_vars['sm']['res']['id_cmscategory']; ?>
');"/>
                            <div id="name_err"></div>
			</td>
		    </tr>
		    <tr>
			<td>Code :</td>
			<td>
			    <input type="text" name="cat[code]" id="code_code" value="<?php echo $this->_tpl_vars['sm']['res']['code']; ?>
" onblur="uniquecode('<?php echo $this->_tpl_vars['sm']['res']['id_cmscategory']; ?>
');"/>
			    <div id="code_err"></div>
                        </td>
		    </tr>
		    <tr>
			<td>Description :</td>
			<td>
			    <textarea name="cat[description]" id="description" ><?php echo $this->_tpl_vars['sm']['res']['description']; ?>
</textarea>
			</td>
		    </tr>
            <tr>
                      <td></td>    
                           <td>
                           	<table>
                            	<tr>
                                    <td><div class="settheme fltlft"><input type="submit" class="buton" id="btn_sbmt" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Insert<?php endif; ?>" /></div></td>
                                    <td><div class="settheme fltlft"><input type="button" class="buton" value="Cancel" onclick="$.fancybox.close();" /></div></td>
                                </tr>
                            </table>
                           
                               </td>
                      </tr>
            
<!--		   <tr>
		       <td>
            	<table>
                	<tr>
                      <td></td>
                    	<td>
                            <div class="settheme fltlft">
                            <input type="submit" class="buton" name="submit" value="<?php if ($this->_tpl_vars['sm']['res']): ?>Update<?php else: ?>Insert<?php endif; ?>" />
                            </div>
                          <div class="settheme fltlft">
                            <input type="button" class="buton" value="Cancel" onclick="$.fancybox.close();" />
                            </div>
                        </td>
                        <td>
                            <div class="settheme fltlft">
                            <input type="button" class="buton" value="Cancel" onclick="$.fancybox.close();" />
                            </div>
                        </td>
                    </tr>
                </table>
			</td>
		    </tr>-->
            </table>
            </form>
        </div>
    </div>
</div>
<!-- Template: admin/cms/add_category.tpl.html End --> 
<?php /* Smarty version 2.6.7, created on 2017-04-04 13:01:59
         compiled from admin/menu.tpl.html */ ?>

<!-- Template: admin/menu.tpl.html Start 04/04/2017 13:01:59 --> 
 <?php $this->assign('cat_type', $this->_tpl_vars['util']->get_values_from_config('CATEGORY')); ?>
<!--<ul id="menu" class="menu" style="margin-top:10px;">
	<li class="openul">
		<div class="fltlft lft_prt"></div>
		<div class="fltlft mdl_prt"><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/" title="Go to Home" class="menu_head">Home</a></div>
		<div class="fltlft rht_prt"></div>
	</li>
	<li class="openul">
		<div class="fltlft lft_prt"></div>
		<div class="fltlft mdl_prt"><a href="javascript:void(0);" class="menu_head">Management</a></div>
		<div class="fltlft rht_prt"></div>
		<ul>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/product/listing"<?php if ($_REQUEST['page'] == 'product' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Product</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/cms/listing"<?php if ($_REQUEST['page'] == 'cms' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>CMS</a></li>

		</ul>
	</li>
	<li class="openul">
		<div class="fltlft lft_prt"></div>
		<div class="fltlft mdl_prt"><a href="javascript:void(0);" class="menu_head">User Logs</a></div>
		<div class="fltlft rht_prt"></div>
		<ul>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/user/listing"<?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Listing</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/user/loginUsers"<?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'loginUsers'): ?>class="current"<?php endif; ?>>Login Details</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/ipblocking/blockedIpList"<?php if ($_REQUEST['page'] == 'ipblocking' && $_REQUEST['choice'] == 'blockedIpList'): ?>class="current"<?php endif; ?>>Blocked IP List</a></li>
		</ul>
	</li>
	<li class="openul">
		<div class="fltlft lft_prt"></div>
		<div class="fltlft mdl_prt"><a href="javascript:void(0);" class="menu_head">Settings</a></div>
		<div class="fltlft rht_prt"></div>
		<ul>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/listing"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Config</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/msgList"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'msgList'): ?>class="current"<?php endif; ?>>Message</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/jsMsgList"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'jsMsgList'): ?>class="current"<?php endif; ?>>JS Message</a></li>
			<?php if ($_SESSION['id_developer']): ?>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/module/formModule"<?php if ($_REQUEST['page'] == 'module' && $_REQUEST['choice'] == 'formModule'): ?>class="current"<?php endif; ?>>Module</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/dutil/show_db"<?php if ($_REQUEST['page'] == 'dutil' && $_REQUEST['choice'] == 'show_db'): ?>class="current"<?php endif; ?>>Database Dump</a></li>
			<li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/dutil/checkList"<?php if ($_REQUEST['page'] == 'dutil' && $_REQUEST['choice'] == 'checkList'): ?>class="current"<?php endif; ?>>Checklist</a></li>
			<?php endif; ?>
		</ul>
	</li>
	<li class="openul">
		<div class="fltlft lft_prt"></div>
		<div class="fltlft mdl_prt"><a class="menu_head" href="http://gourab.afixiindia.com/flexytiny_new/" target="_blank">Go to User Site &gt;&gt;</a></div>
		<div class="fltlft rht_prt"></div>
	</li>
</ul>-->
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
           <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <div class="navbar-inverse">
            <form class="navbar-search visible-phone">
               <input type="text" class="search-query" placeholder="Search" />
            </form>
         </div>
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
 <ul class="sidebar-menu">
              <li class="sub-menu active">
                  <a class="" href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/">
                      <i class="icon-dashboard"></i>
                      <span>Dashboard</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-book"></i>
                      <span>Management</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/product/listing"<?php if ($_REQUEST['page'] == 'product' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Product</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/cms/listing"<?php if ($_REQUEST['page'] == 'cms' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>CMS</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/attribute/listing"<?php if ($_REQUEST['page'] == 'attribute' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Attribute</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/redirect/listing"<?php if ($_REQUEST['page'] == 'redirect' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>301 Redirection</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/csvuser/listing"<?php if ($_REQUEST['page'] == 'csvuser' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>CSV</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/cms/category_listing"<?php if ($_REQUEST['page'] == 'cms' && $_REQUEST['choice'] == 'category_listing'): ?>class="current"<?php endif; ?>>Catagory</a></li>
<!--                      <li><a class="" href="slider.html">Sliders</a></li>
                      <li><a class="" href="metro_view.html">Metro View</a></li>
                      <li><a class="" href="tabs_accordion.html">Tabs & Accordions</a></li>
                      <li><a class="" href="typography.html">Typography</a></li>
                      <li><a class="" href="tree_view.html">Tree View</a></li>
                      <li><a class="" href="nestable.html">Nestable List</a></li>-->
                  </ul>
              </li>
<!--<li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-book"></i>
                      <span>Management</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a class="" href="">Product</a></li>
                      <li><a class="" href="button.html">CMS</a></li>
                      <li><a class="" href="slider.html">Sliders</a></li>
                      <li><a class="" href="metro_view.html">Metro View</a></li>
                      <li><a class="" href="tabs_accordion.html">Tabs & Accordions</a></li>
                      <li><a class="" href="typography.html">Typography</a></li>
                      <li><a class="" href="tree_view.html">Tree View</a></li>
                      <li><a class="" href="nestable.html">Nestable List</a></li>
                  </ul>
              </li>-->
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-cogs"></i>
                      <span>User Logs</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/user/listing"<?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Listing</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/user/loginUsers"<?php if ($_REQUEST['page'] == 'user' && $_REQUEST['choice'] == 'loginUsers'): ?>class="current"<?php endif; ?>>Login Details</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/ipblocking/blockedIpList"<?php if ($_REQUEST['page'] == 'ipblocking' && $_REQUEST['choice'] == 'blockedIpList'): ?>class="current"<?php endif; ?>>Blocked IP List</a></li>
<!--                      <li><a class="" href="flot_chart.html">Flot Charts</a></li>
                      <li><a class="" href="gallery.html"> Gallery</a></li>-->
                  </ul>
              </li>
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-tasks"></i>
                      <span>Settings</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/listing"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'listing'): ?>class="current"<?php endif; ?>>Config</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/msgList"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'msgList'): ?>class="current"<?php endif; ?>>Message</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/setting/jsMsgList"<?php if ($_REQUEST['page'] == 'setting' && $_REQUEST['choice'] == 'jsMsgList'): ?>class="current"<?php endif; ?>>JS Message</a></li>
                      <?php if ($_SESSION['id_developer']): ?>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/module/formModule"<?php if ($_REQUEST['page'] == 'module' && $_REQUEST['choice'] == 'formModule'): ?>class="current"<?php endif; ?>>Module</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/dutil/show_db"<?php if ($_REQUEST['page'] == 'dutil' && $_REQUEST['choice'] == 'show_db'): ?>class="current"<?php endif; ?>>Database Dump</a></li>
                      <li><a href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/dutil/checkList"<?php if ($_REQUEST['page'] == 'dutil' && $_REQUEST['choice'] == 'checkList'): ?>class="current"<?php endif; ?>>Checklist</a></li>
                      <?php endif; ?>
                  </ul>
              </li>
              <li class="sub-menu">
                  <a href="http://gourab.afixiindia.com/flexytiny_new/" target="_blank" class="">
                      <i class="icon-th"></i>
                      <span>Go to User Site &gt;&gt;</span>
                      <span class="arrow"></span>
                  </a>

              </li>
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-fire"></i>
                      <span>Icons</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a class="" href="font_awesome.html">Font Awesome</a></li>
                      <li><a class="" href="glyphicons.html">Glyphicons</a></li>
                  </ul>
              </li>


              <li>
                  <a class="" href="http://gourab.afixiindia.com/flexytiny_new/flexyadmin/">
                    <i class="icon-user"></i>
                    <span>Login Page</span>
                  </a>
              </li>
          </ul>
</div>
      </div>
</div>

<?php echo '
<script type="text/javascript">
	$(document).ready(function(){
//		$(\'ul#menu li.openul\').hover(function(){
//			jQuery(this).find(\'> ul\').stop(true, true).slideDown(300).css(\'width\',\'150px\');
//		 },function(){
//			$(this).find(\'ul\').slideUp(300);
//		 });
        //    sidebar dropdown menu

    jQuery(\'#sidebar .sub-menu > a\').click(function () {
        var last = jQuery(\'.sub-menu.open\', $(\'#sidebar\'));
        last.removeClass("open");
        jQuery(\'.arrow\', last).removeClass("open");
        jQuery(\'.sub\', last).slideUp(200);
        var sub = jQuery(this).next();
        if (sub.is(":visible")) {
            jQuery(\'.arrow\', jQuery(this)).removeClass("open");
            jQuery(this).parent().removeClass("open");
            sub.slideUp(200);
         } else {
            jQuery(\'.arrow\', jQuery(this)).addClass("open");
            jQuery(this).parent().addClass("open");
            sub.slideDown(200);
         }
        var o = ($(this).offset());
        diff = 200 - o.top;
        if(diff>0)
            $(".sidebar-scroll").scrollTo("-="+Math.abs(diff),500);
        else
            $(".sidebar-scroll").scrollTo("+="+Math.abs(diff),500);
     });

//    sidebar toggle
        
       $(\'.icon-reorder\').click(function () {
if ($(\'#sidebar > ul\').is(":visible") === true) {
$(\'#main-content\').css({
    \'margin-left\': \'0px\'
 });
$(\'#sidebar\').css({
    \'margin-left\': \'-180px\'
 });
$(\'#sidebar > ul\').hide();
$("#container").addClass("sidebar-closed");
 } else 
{
$(\'#main-content\').css({
    \'margin-left\': \'180px\'
 });
$(\'#sidebar > ul\').show();
$(\'#sidebar\').css({
    \'margin-left\': \'0\'
 });
    $("#container").removeClass("sidebar-closed");
 }
 }); 
        
        
	 });
</script>
'; ?>

<!-- Template: admin/menu.tpl.html End --> 
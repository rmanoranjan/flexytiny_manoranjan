<?php /* Smarty version 2.6.7, created on 2017-04-01 20:03:49
         compiled from admin/csvuser/csvdata_search.tpl.html */ ?>

<!-- Template: admin/csvuser/csvdata_search.tpl.html Start 01/04/2017 20:03:49 --> 
 <?php echo '
<style>
.custom-combobox {
position: relative;
display: inline-block;
width:100%;
 }
.custom-combobox-toggle {
position: absolute;
top: 0;
bottom: 0;
margin-left: -1px;
padding: 0;
 }
.custom-combobox-input {
margin: 0;
padding: 5px 10px;
width:43%;
 }
ul.ui-autocomplete{max-height: 300px; overflow: hidden; overflow-y: auto; }
</style>
<script>
$(function($){
$( "#namecombo").combobox();
$( "#toggle" ).on( "click", function() {
$( "#namecombo").toggle();
 });
combo_fill()
 });
function combo_fill(){
$.post(siteurl, {"page": "csvuser", "choice": "search_csv", "psearch": 1,"ce": 0 }, function(res) {//alert(res);//return false;
 $(\'#namecombo\').html(res);
  });
 }
    function searchList(i){
	    $(\'#succ_msg\').html(\'\');	
	    if(i){
        var prod_srch = $("#search").serialize();
		$.post(siteurl,{"page" : "csvuser", "choice" : "listing", \'psearch\' : 1,"searchval":prod_srch, \'ce\' : 0 },function(res){//alert(res);//return false;
		    $("#csvuser_listing").html(res);
		 });
		
	     }else{
         $.post(siteurl,{"page" : "csvuser", "choice" : "listing", \'psearch\' : 1,\'ce\' : 0, },function(res){
		    $("#csvuser_listing").html(res);
		 });
	     }
           }
          

</script>
'; ?>


 <div class="center">
     <div id="succ_msg" align="center"></div>
     <div id="dv1">
         <div id="page-wraper">
           <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Search</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                            </div>
                            <div class="widget-body">
                        <form name="search" id="search" method="post" action="javascript:void(0);" class="form-horizontal">
                            <div class="control-group">
                               <table border="0" align="center" class="table table-striped table-bordered">
                    <tr>
                        <td class="hid" align="right">Name :</td>
                        <td>
                            <div class="ui-widget">
			    <select  name="name" id="namecombo">
                            </select>
                        </td>
		    </tr>
                    <tr>
                        <td>
                           <button class="btn btn-success" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
                        <td >
                             <button class="btn btn-success" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>
                        </td>
                         </tr>
		         </table> 
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
             
                </div>
             </div>
       </div>
     </div>
 <div id="csvuser_listing"> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/csvuser/csvdata_list.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
       

<!-- Template: admin/csvuser/csvdata_search.tpl.html End --> 
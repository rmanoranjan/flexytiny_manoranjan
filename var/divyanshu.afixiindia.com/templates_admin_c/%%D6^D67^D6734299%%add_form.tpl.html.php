<?php /* Smarty version 2.6.7, created on 2017-04-06 09:40:14
         compiled from admin/cms/add_form.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/cms/add_form.tpl.html', 108, false),array('modifier', 'escape', 'admin/cms/add_form.tpl.html', 136, false),array('modifier', 'stripslashes', 'admin/cms/add_form.tpl.html', 189, false),array('modifier', 'default', 'admin/cms/add_form.tpl.html', 217, false),)), $this); ?>

<!-- Template: admin/cms/add_form.tpl.html Start 06/04/2017 09:40:14 --> 
 <?php echo '
<script type="text/javascript" language="javascript">
	$(function(){
		var ln = "';  echo $this->_tpl_vars['sm']['sel_lang'];  echo '";
		var oFCKeditor = new FCKeditor(\'description\'+ln)
		oFCKeditor.BasePath ="http://divyanshu.afixiindia.com/flexymvc_core/libsext/fckeditor/";
		oFCKeditor.Width = \'100%\';
		oFCKeditor.Height = \'350px\';
		oFCKeditor.Config["SkinPath"]=  oFCKeditor.BasePath + \'editor/skins/office2003/\';
		oFCKeditor.ReplaceTextarea();
	 });
	css_even_odd();
        
function callbackFun(response) {
var msg = "';  echo $this->_tpl_vars['sm']['cms']['id_content'];  echo '"?"Content updated Successfully":"Content added successfully";
$.fancybox.close();
messageShow(msg);
$(\'#cms_listing\').html(response);
 }

function validate_cms(){
		var conname = $(\'#con_name\').val();
		var concode = $(\'#con_cmscode\').val();
		var cmscat = $(\'#cmscategory\').val();
                    var ln	    = $(\'#language\').val();
		if(conname && concode && cmscat){
			var validator=$("#cms_en").validate({
				rules: {
					"cms[title]": {
						required:true
					 }
				 },
				messages: {
					"cms[title]":{
						required:"<br/>"+flexymsg.required
					 }
				 }
			 });
			var x =validator.form();
			var y=$("#checklinkunique").html().length;
			if(x && !y){
				var langVal	= $("#langVal").val();
				var name= $(\'#tname\').val();
				var id_content	= ($(\'#id_cms\').length) ?$(\'#id_cms\').val(): "";
				if(name){
//					$.post(siteurl,{"page":"cms","choice":"checkName","ce":0,"name":name,"language": langVal,"id_content":id_content },function(res){
//						if(res == \'1\'){
//							$(\'#multilanguagecms_error_name\').html("<font color=\'red\'>Duplicate Name.Please enter another one</font>");
//							return false;
//						 }else{
							$(\'#multilanguagecms_error_name\').html(\'\');
							$(\'#tname\').val();
//							if($(\'#id_cms\').val()){
//								var uri = "http://divyanshu.afixiindia.com/flexytiny_new/flexyadmin/cms/insert";
//							 }else{
//								var uri = "http://divyanshu.afixiindia.com/flexytiny_new/flexyadmin/cms/insert";
//							 }
//							document.cms_en.action = uri;
//							document.cms_en.submit();
							return true;
//						 }
//					 });
				 }

			 }else{
				return false;
			 }
		 }else{
			//messageShow("Please enter Name and Permacode");
			$(\'#name_msg\').html(\'<font color="red">Please specify name,category and permacode</font>\');
			$(\'#tname\').focus();
			return false;
		 }
	 }
</script>
<style>
  .form-horizontal .control-group{margin-top:20px; }
</style>
'; ?>


	<div class="center">
           <div class="row-fluid">
                    <div class="span12">
        <div class="widget red">
<div class="widget-title">
<span class="tools">
<a class="icon-chevron-down" href="javascript:;"></a>
<a class="icon-remove" href="javascript:;"></a>
</span>
</div>
         
          <form id="cms_en" name="cms_en" action="http://divyanshu.afixiindia.com/flexytiny_new/flexyadmin/cms/insert/ce/0" method="post" enctype="multipart/form-data" onsubmit="return AsyncUpload.submitForm(this, validate_cms,callbackFun);" class="form-horizontal">
             <input type="hidden" name="cms[name]" id="con_name" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['name']; ?>
" />
              <input type="hidden" name="qstart" value="<?php if ($this->_tpl_vars['sm']['qstart']):  echo $this->_tpl_vars['sm']['qstart'];  else: ?>0<?php endif; ?>" />   
	<input type="hidden" name="prev_name" id="title_name" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['name']; ?>
" />
	<input type="hidden" name="cms[cmscode]" id="con_cmscode" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
" />
	<input type="hidden" name="cms[language]" id="language" class="req_val" value="<?php echo $this->_tpl_vars['sm']['sel_lang']; ?>
" />
        <input type="hidden" name="cms[cmscategory]" id="cmscategory" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscategory']; ?>
" />
	<input type="hidden" name="id_content" id="id_content" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['id_content']; ?>
" /> 
    
<!--                            <div class="control-group">
                              <?php if ($this->_tpl_vars['sm']['mul_lang'] == 1): ?>
                                <label class="control-label">Choose Language  </label>
                                <div class="controls">
                                  <select class="span6" name="langVal" id="langVal" onchange="getcontent();">
					<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['language'],'selected' => $this->_tpl_vars['sm']['sel_lang']), $this);?>

				</select>
                                  
                                    <input type="text" class="span6 " />
                                    <span class="help-inline">Some hint here</span>
                                </div>
                                 <?php endif; ?>
                            </div>-->
   
<!--                            <div class="control-group">
                                <label class="control-label">Permacode  </label>
                                <div class="controls">
                                  <input class="span6" type="text" id="permcode" name="permcode" size="90" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
" onblur="getCode();" />
                                    <br /><span id="name_msg"></span>
                                    <span class="help-inline">Some hint here</span>
                                </div>
                            </div>-->
<!--    <div class="control-group">
                                <label class="control-label">Permalink  </label>
                                <div class="controls">
                                  <span class="span6" id="permalink"><?php if ($this->_tpl_vars['sm']['cms']['cmscode']): ?>http://divyanshu.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['cms']['language']; ?>
/<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
.html<?php else: ?>http://divyanshu.afixiindia.com/flexytiny_new/<?php endif; ?></span>
                                   
                                    <span class="help-inline">Some hint here</span>
                                </div>
                            </div>-->
    <div class="control-group">
                                <label class="control-label">Title  </label>
                                <div class="controls">
                                  <input class="span6" type="text" id="title" name="cms[title]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
                                   
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
    <div class="control-group">
                                <label class="control-label">Meta Description  </label>
                                <div class="controls">
                                  <textarea  class="span6" rows="3" cols="40" id="meta_description" name="cms[meta_description]"><?php echo $this->_tpl_vars['sm']['cms']['meta_description']; ?>
</textarea>
                                   
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
    <div class="control-group">
                                <label class="control-label">Meta Keywords  </label>
                                <div class="controls">
                                  <textarea  class="span6" rows="3" cols="40" id="meta_keywords" name="cms[meta_keywords]"><?php echo $this->_tpl_vars['sm']['cms']['meta_keywords']; ?>
</textarea>
                                   
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
    </div>
                            <div class="control-group">
                                <label class="control-label">Header Tag  </label>
                                <div class="controls">
                                  <input class="span6" type="text" id="h1tag" name="cms[h1tag]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['h1tag'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
                                   
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">View Description  </label>
                                <div class="controls">
                                  <textarea  class="span6"  rows="3" cols="40" name="cms[view_description]"><?php echo $this->_tpl_vars['sm']['cms']['view_description']; ?>
</textarea>
                                </div>
                            </div>
<!--                             <div class="control-group">
                                <label class="control-label">Tags </label>
                                <div class="controls">
                                  <select name="tags[]"  id="tags" class="txt" onchange="showcat_tag();">
                                    <?php if (count($_from = (array)$this->_tpl_vars['sm']['tags'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" class="selected" selected="selected"><?php echo $this->_tpl_vars['item']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                <input type="hidden" value="<?php echo $this->_tpl_vars['sm']['tagshidval']; ?>
" id="pass_on_to_tag" name="extratag">
                                </div>-->
                             <div class="control-group">
                                <div class="controls">
                                  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/cms/attribute.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                </div>
        
                              <div class="control-group">
                                <label class="control-label">Description  </label>
                                <div class="controls">
                                  <textarea class="span6" name="cms[description]" id="description<?php echo $this->_tpl_vars['sm']['sel_lang']; ?>
" cols="40" rows="5"><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['description'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
                                   
                                    <!--<span class="help-inline">Some hint here</span>-->
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-actions">
                                <input type="submit" name="submit" class="btn btn-success" value="<?php if ($this->_tpl_vars['sm']['cms']['id_content']): ?>Update<?php else: ?>Insert<?php endif; ?>" />
				<!--<input type='hidden' id ="save_exit" name="save_exit" value="">-->
                                <input type="reset" name="reset" class="btn btn-success" value="Cancel" />
                            </div>
    <input type="hidden" id="prev_val" value="" />
	<input type="hidden" id="code" value="<?php echo $this->_tpl_vars['sm']['code']; ?>
" />
	<input type="hidden" name="plang" id="plang" value="<?php echo $this->_tpl_vars['ltype']['English']; ?>
" />
	<input type="hidden" id="id_cms" value="<?php echo $this->_tpl_vars['sm']['cms']['id_content']; ?>
" />
	<input type="hidden" name="multi_lan" id="multi_lan" value="<?php echo $this->_tpl_vars['mul_lang']['islang']; ?>
" />
                                   
                            </form>
          
          
          
          
<!--          <form id="cms_en" name="cms_en" action="javascript:void(0);" class="fields"  method="post" enctype="multipart/form-data" onsubmit="return validate_cms();">
	<input type="hidden" name="cms[name]" id="con_name" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['name']; ?>
" />
	<input type="hidden" name="prev_name" id="title_name" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['name']; ?>
" />
	<input type="hidden" name="cms[cmscode]" id="con_cmscode" class="req_val" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
" />
	<input type="hidden" name="cms[language]" id="language" class="req_val" value="<?php echo $this->_tpl_vars['sm']['sel_lang']; ?>
" />
	<input type="hidden" name="id_content" id="id_content" class="req_val" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['sm']['cms']['id_content'])) ? $this->_run_mod_handler('default', true, $_tmp, 0) : smarty_modifier_default($_tmp, 0)); ?>
" />
	<table class="formtbl list_form" border="0" width="100%">
		<?php if ($this->_tpl_vars['sm']['mul_lang'] == 1): ?>
		<tr>
			<td width="15%">Choose Language:</td>
			<td>
				<select name="langVal" id="langVal" onchange="getcontent();">
					<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['sm']['language'],'selected' => $this->_tpl_vars['sm']['sel_lang']), $this);?>

				</select>
			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td>Permacode:</td>
			<td>
				<input type="text" id="permcode" name="permcode" size="90" value="<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
" onblur="getCode();" />
				<br /><span id="name_msg"></span>
			</td>
		</tr>
		<tr>
			<td>Permalink:</td>
			<td>
				<span id="permalink"><?php if ($this->_tpl_vars['sm']['cms']['cmscode']): ?>http://divyanshu.afixiindia.com/flexytiny_new/<?php echo $this->_tpl_vars['sm']['cms']['language']; ?>
/<?php echo $this->_tpl_vars['sm']['cms']['cmscode']; ?>
.html<?php else: ?>http://divyanshu.afixiindia.com/flexytiny_new/<?php endif; ?></span>
			</td>
		</tr>
		<tr>
			<td width="15%">Title:</td>
			<td><input class="txt req_val" type="text" id="title" name="cms[title]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
		</tr>
		<tr>
			<td>Meta Description:</td>
			<td><textarea  class="req_val" rows="3" cols="40" id="meta_description" name="cms[meta_description]"><?php echo $this->_tpl_vars['sm']['cms']['meta_description']; ?>
</textarea></td>
		</tr>
		<tr>
			<td>Meta Keywords:</td>
			<td><textarea  class="req_val" rows="3" cols="40" id="meta_keywords" name="cms[meta_keywords]"><?php echo $this->_tpl_vars['sm']['cms']['meta_keywords']; ?>
</textarea></td>
		</tr>
		<tr>
			<td>Header Tag:</td>
			<td><input class="txt req_val" type="text" id="h1tag" name="cms[h1tag]" size="30" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['h1tag'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
		</tr>
		<tr>
			<td colspan="2">Description:<br/>
				<textarea name="cms[description]" id="description<?php echo $this->_tpl_vars['sm']['sel_lang']; ?>
" cols="40" rows="5"><?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['cms']['description'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<div style="width: 230px; margin: 0px auto;">
					<div class="settheme fltlft" style="margin-right: 2px;">
						<input type="submit" name="search" class="buton" value="Save" onclick="$('#save_exit').val(1)"/>
					</div>
					<div class="settheme fltlft" style="margin-right: 1px;">
						<input type="submit" name="search" class="buton" value="Save & Exit" />
					</div>
					<input type='hidden' id ="save_exit" name="save_exit" value="">
					<div class="settheme fltlft">
						<input type="reset" name="search" class="buton" value="Cancel" onClick="close_win();"/>
					</div>
				</div>
			</td>
		</tr>
	</table>

	<input type="hidden" id="prev_val" value="" />
	<input type="hidden" id="code" value="<?php echo $this->_tpl_vars['sm']['code']; ?>
" />
	<input type="hidden" name="plang" id="plang" value="<?php echo $this->_tpl_vars['ltype']['English']; ?>
" />
	<input type="hidden" id="id_cms" value="<?php echo $this->_tpl_vars['sm']['cms']['id_content']; ?>
" />
	<input type="hidden" name="multi_lan" id="multi_lan" value="<?php echo $this->_tpl_vars['mul_lang']['islang']; ?>
" />
</form>-->
</div>
                      </div>
             </div>
      </div>
<!-- Template: admin/cms/add_form.tpl.html End --> 
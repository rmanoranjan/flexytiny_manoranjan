<?php /* Smarty version 2.6.7, created on 2017-04-05 19:32:29
         compiled from admin/cms/search_category.tpl.html */ ?>
<?php echo '
<style>
 .custom-combobox {
    position: relative;
    display: inline-block;
   }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    border:1px solid #cccccc;
   }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
   }
  ul.ui-autocomplete{max-height: 300px; overflow: hidden; overflow-y: auto; }
  /*.ui-icon{display:none !important; }*/
  .even td{background: none !important; }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active { border: 1px solid #ddd !important; background: #ffffff url(images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x; font-weight: bold; color: black !important; }
</style>
    <script type="text/javascript">
	function searchList(i){
	    $(\'#succ_msg\').html(\'\');	
	    if(i){
        var catsearch = $("#search").serialize();
		$.post(siteurl,{"page" : "cms", "choice" : "category_listing", \'psearch\' : 1,"searchval":catsearch, \'ce\' : 0 },function(res){
		    $("#cms_category_listing").html(res);
		    css_even_odd();
		 });
		
	     }else{
         $.post(siteurl,{"page" : "cms", "choice" : "category_listing", \'psearch\' : 1,\'ce\' : 0,"reset":1 },function(res){
		    $("#cms_category_listing").html(res);
		    css_even_odd();
		 });
		$(\'#search\')[0].reset();
	     }
	 }
        $(function(){
$("#combobox").combobox();
$("#toggle" ).on( "click", function() {
$("#combobox").toggle();
 });
$.post(siteurl, {"page": "cms", "choice": "search_catagory", "psearch": 1,"ce": 0 }, function(res) {//alert(res);//return false;
var arr = res.split(\'@@\');
$(\'#combobox\').html(arr[0]);
 });
 });
    </script>
'; ?>
    
<!--<div id="succ_msg" align="center"></div>
<div id="dv1" class="wid50 fltlft mrglft20">
    <div class="makebox center">
    	<div class="headprt settheme">
            <div class="mdl">
            	<span>Search Category</span>
            </div>
        </div>
        <div class="bodyprt">
            <form name="search" id="search" class="basic" method="post" action="javascript:void(0);">
                 <table border="0" align="center" class="formtbl">
                      <tr>
                           <td width="16%">Category Name:</td>
                           <td>
                               <div class="ui-widget">
                                    <select name="cat_name" id="combobox"></select>
                               </div>
                           </td>
                           </tr>
                      <tr>
                      <td></td>    
                           <td>
                           	<table>
                            	<tr>
                                	<td><div class="settheme fltlft"><input type="button" name="search" value="Search" onclick="searchList(1);" class="buton" /></div></td>
                                    <td><div class="settheme fltlft"><input type="reset" name="search" value="Reset" onclick="searchList(0);" class="buton" /></div></td>
                                </tr>
                            </table>
                           
                                
                           </td>
                      </tr>
                 </table>
                </form>
        </div>
    </div>
</div>
<div id="cms_category_listing" class="fltlft wid60 mrglft0">
	</div>-->

<div class="center">
     <div id="succ_msg" align="center"></div>
     <div id="dv1">
         <div id="page-wraper">
           <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN BASIC PORTLET-->
                        <div class="widget red">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Search Attribute</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                            </div>
                            <div class="widget-body">
                        <form name="search" id="search" method="post" action="javascript:void(0);" class="form-horizontal">
                            <div class="control-group">
                               <table border="0" align="center" class="table table-striped table-bordered">
                          <tr>
                           <td width="16%">Category Name:</td>
                           <td>
                               <div class="ui-widget">
                                    <select name="cat_name" id="combobox"></select>
                               </div>
                           </td>
                           </tr>
                      <tr>
                        <tr>
                        <td >
                           <button class="btn btn-success fltrht" type="button" name="search" value="Search" onclick="searchList(1);">Search</button>
                        </td>
                        <td>
                             <button class="btn btn-success" type="reset" name="search" value="Reset" onclick="searchList(0);">Reset</button>
                        </td>
                         </tr>
		         </table> 
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                    </div>
             
                </div>
             </div>
       </div>
     </div>
 <div id="cms_category_listing"> <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/cms/list_category.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
<?php /* Smarty version 2.6.7, created on 2017-04-05 19:55:56
         compiled from admin/attribute/listing.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'admin/attribute/listing.tpl.html', 36, false),array('modifier', 'capitalize', 'admin/attribute/listing.tpl.html', 36, false),array('modifier', 'count', 'admin/attribute/listing.tpl.html', 52, false),)), $this); ?>

<div class="row-fluid">
                <div class="span12">
                  
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget red">
                    <div class="widget-title">
                       
                        <h4><i class="icon-reorder"></i>Attribute List</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        <div  class="fltrht pdngin block_ip">
               <a href="javascript:void(0);" onclick="addAttribute('<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');" class="btn btn-success">Add Attribute</a>
                 </div>
                    </div>
                    <div class="widget-body">
                     <form>
                       <?php if ($this->_tpl_vars['sm']['list']): ?>
                        <table cellspacing="0" class="table table-striped table-bordered" id="attr_tbl">
      <thead>
					<tr>
						<th>Name</th>
						<th>Value</th>
						<th>Entry Type</th>
                                                 <th>Search Type</th>
						<th>Actions</th>
					</tr>
				</thead>
                <tbody>
			<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
			<?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
			<tr class="nodrag" id="<?php echo $this->_tpl_vars['x']['id_attribute']; ?>
">
			    <td class="tb dragHandle"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['x']['attribute_name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)))) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true)); ?>
</td>
			    <td><?php if ($this->_tpl_vars['x']['attribute_label']):  echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['x']['attribute_label'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)))) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true));  else: ?><i>N/A</i><?php endif; ?></td>
			    <td><?php if (count($_from = (array)$this->_tpl_vars['sm']['attribute'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
 if ($this->_tpl_vars['key'] == $this->_tpl_vars['x']['entry_type']):  echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true));  endif;  endforeach; endif; unset($_from); ?></td>
			    <td><?php if (count($_from = (array)$this->_tpl_vars['sm']['attribute'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
 if ($this->_tpl_vars['key'] == $this->_tpl_vars['x']['search_type']):  echo ((is_array($_tmp=$this->_tpl_vars['item'])) ? $this->_run_mod_handler('capitalize', true, $_tmp, true) : smarty_modifier_capitalize($_tmp, true));  endif;  endforeach; endif; unset($_from); ?></td>
			    <td>
				<a href="javascript:void(0);" onclick="searchable('<?php echo $this->_tpl_vars['x']['id_attribute']; ?>
','<?php echo $this->_tpl_vars['x']['is_searchable']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');">
				<?php if ($this->_tpl_vars['x']['is_searchable'] == 1): ?>
				    <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/searchable.png" title="Searchable" alt="Searchable" width="18" height="17">
				<?php else: ?>
				    <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/notsearchable.png" title="Not searchable" alt="Not searchable" width="18" height="17">
				<?php endif; ?>
				</a>
				&nbsp;&nbsp;
				<a href="javascript:void(0);" onclick="editAttribute('<?php echo $this->_tpl_vars['x']['id_attribute']; ?>
','<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
', '<?php echo $this->_tpl_vars['sm']['uri']; ?>
');">
				    <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/pencil.png" alt="Edit Product" title="Edit Attribute"/>
				</a>&nbsp;&nbsp;
				<a href="javascript:void(0);" onclick="return deleteAttribute('<?php echo $this->_tpl_vars['x']['id_attribute']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
', '<?php echo $this->_tpl_vars['sm']['uri']; ?>
', '<?php echo count($this->_tpl_vars['sm']['list']); ?>
','<?php echo $this->_tpl_vars['x']['attribute_name']; ?>
');">
				    <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" alt="Delete Product" title="Delete Attribute"/>
				</a>&nbsp;&nbsp;		    
				<a href="javascript:void(0);" onclick="showDetail('<?php echo $this->_tpl_vars['x']['id_attribute']; ?>
');">
				    <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/eye.png" alt="Product Detail" title="Attribute Detail"/>
				</a>&nbsp;&nbsp;
			    </td>
			</tr>			
			<?php endfor; endif; ?>
		    </tbody>
                     </table>
                       <?php else: ?>
                       <div>No records found....</div>
                       <?php endif; ?>
                       </form>
                    </div>
                 <div class="pagination_box">
		<div align="center"><?php if (! $this->_tpl_vars['sm']['id_cat']):  echo $this->_tpl_vars['sm']['next_prev']->generate();  endif; ?></div>
	        </div>
                </div>
                </div>


<?php echo '
<script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/table_dnd.js"></script>
<script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/reorder.js"></script>
<script type="text/javascript">
    var curl="http://divyanshu.afixiindia.com/flexytiny_new/education/reorder/ce/0/tbl/attribute?ids=";
    new callreorder("attr_tbl","dragHandle",curl,"showDragHandle","class1");
</script>
<style type="text/css">
.class1{
	background:#3366FF;
 }
.showDragHandle{	
	background:#FF0000;
	cursor:move;
 }
</style>
<script type="text/javascript">
    var code;
    function addAttribute(qstart){
	$(\'#succ_msg\').html(\'\');
	$(\'#target\').html(\'\');
	$.fancybox.showActivity();
/*	$.post(siteurl,{"page" : "attribute", "choice" : "addAttribute", "qstart" : qstart, "ce" : 0 },function(res){
	    show_fancybox(res);
	 });*/
	$.post(siteurl,{"page":"attribute","choice":"addAttribute","qstart" : qstart,"ce":0 },function(res){
	    $.fancybox(res,{
		hideOnOverlayClick:true,
		scrolling:"yes",
		centerOnScroll:true,
		onLoad:function(){$.fancybox.showActivity() },
		onComplete:function(){res },
		onCleanup:function(){
		    $(\'#stype\').attr("value","");
		    $(\'#etype\').attr("value","");
		 },
		onClosed:function(){
		    $.fancybox.close();
		 }
	     });
	 });
     }
    function searchable(id,status,qstart){
	status = parseInt(status)?0:1; 
	$.post(siteurl,{page:\'attribute\',choice:"searchable",ce:0,"id_attribute":id,"status":parseInt(status),"qstart":qstart }, function(res){
	    if(parseInt(res)==1){
		//messageShow("Institute reviewed sucessfully.");
	     }else{
		//messageShow("Institute can\'t be reviewed.");
	     }
	    window.location.href=siteurl+"attribute/listing/qstart/"+qstart;
	 });
     }
    
    function editAttribute(id,qstart,uri){
	$(\'#succ_msg\').html(\'\');
	$.fancybox.showActivity();
/*	$.post(siteurl,{"page" : "attribute", "choice" : "addAttribute", "ce" : "0", "id_attribute" : id, "qstart" : qstart, "uri" : uri },function(res){
	    show_fancybox(res);
	 });*/
	
	
	$.post(siteurl,{"page":"attribute","choice":"addAttribute","id_attribute" : id,"uri" : uri,"qstart" : qstart,"ce":0 },function(res){
	    $.fancybox(res,{
		hideOnOverlayClick:true,
		scrolling:"yes",
		centerOnScroll:true,
		onLoad:function(){$.fancybox.showActivity() },
		onComplete:function(){res },
		onCleanup:function(){
		    $(\'#stype\').attr("value","");
		    $(\'#etype\').attr("value","");
		 },
		onClosed:function(){
		    $.fancybox.close();
		 }
	     });
	 });
	
     }
    
    function deleteAttribute(id_attribute, qstart, uri, cnt,name){
	$(\'#succ_msg\').html(\'\');
	var conf=confirm(\'Are you sure to delete "\'+name+\'" ?\');
	var pname   = $(\'#attribute_name\').val();
	var etype = $(\'#etype\').val();
	var stype = $(\'#stype\').val();
	if(conf){
	    $.post(siteurl,{"page" : "attribute", "choice" : "deleteAttribute", "ce" : 0, "id_attribute" : id_attribute, "qstart" : qstart, "uri" : uri, "cnt" : cnt, "pname" : pname, "stype" : stype,"etype" : etype },function(res){
		//alert(res);return;
		messageShow("Attribute deleted successfully.");//$(\'#succ_msg\').html(\'<font color="red">Product deleted successfully</font>\');
		$("#attribute_listing").html(res);
		css_even_odd();
	     });
	 }else
	    return false;
     }
    function showDetail(id){
	 $.fancybox.showActivity();
	    $.post(siteurl,{"page":"attribute","choice":"attributeDetail",ce:0,"id_attribute":id },function(response){
		    //alert(response);return;
		    show_fancybox(response);
	     });
     }
    function makeSeq(pro_id){
	$.fancybox.showActivity();
	$.post(siteurl,{"page":"product","choice":"showImageList",ce:0,\'id_product\':pro_id },function(res){
	    show_fancybox(res);
	 });
     }
</script>
'; ?>

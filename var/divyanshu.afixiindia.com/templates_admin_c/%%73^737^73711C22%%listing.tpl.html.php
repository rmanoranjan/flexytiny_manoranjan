<?php /* Smarty version 2.6.7, created on 2017-04-06 09:37:34
         compiled from admin/product/listing.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin/product/listing.tpl.html', 94, false),array('modifier', 'truncate', 'admin/product/listing.tpl.html', 107, false),array('modifier', 'default', 'admin/product/listing.tpl.html', 107, false),array('modifier', 'cat', 'admin/product/listing.tpl.html', 107, false),array('modifier', 'count', 'admin/product/listing.tpl.html', 122, false),)), $this); ?>
<!--done by gayatree starts-->
<?php echo '
<script>
  // widget tools

jQuery(\'.widget .tools .icon-chevron-down\').click(function () {
var el = jQuery(this).parents(".widget").children(".widget-body");
if (jQuery(this).hasClass("icon-chevron-down")) {
jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
el.slideUp(200);
 } 
else {
jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
el.slideDown(200);
 }
 });

jQuery(\'.widget .tools .icon-remove\').click(function () {
jQuery(this).parents(".widget").parent().remove();
 });

  </script>
<style>
  .listing_admin{display:none; }
  .prdt_anchor:hover{text-decoration: none !important; }
 
</style>
'; ?>

 <div class="" id="product_listing">
<div class="row-fluid">
  <div class="span12">
    <!--BEGIN BASIC PORTLET-->
   
      <div class="widget red">
        <div class="widget-title">
          <h4><i class="icon-reorder"></i> Product Listing(<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
)</h4>
          <span class="tools">
            <a href="javascript:;" class="icon-chevron-down"></a>
            <a href="javascript:;" class="icon-remove"></a>
          </span>
           <div class="fltrht">                            
          <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
          <div  class="fltrht pdnga">
            <a class="btn btn-success" href="#" onclick="addProduct('<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');">Add Product</a>
          </div>
          <div class="fltrht pdngin">
            <input class="btn btn-success" type="button" onclick="downloadxls();" value="Download XLS"/>
          </div>
          <?php endif; ?>
          <div class="clear"></div>
          </div>
          </div>
        <div class="widget-body widget-body_scrl">
          <?php if ($this->_tpl_vars['sm']['list']): ?>
          <table class="table table-striped table-bordered table-advance table-hover" id="img_tbl">
            <thead>
              <tr>
                <th>
                  <i class="icon-picture"></i>
                  Image
                </th>
                <th>
                  <i class="icon-user"></i>
                  Name
                </th>
                <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
                <th>
                  <i class="icon-file"></i>
                  Category Name
                </th>
                <?php endif; ?>
                <th>
                  <i class="icon-question-sign"></i>
                  Description
                </th>
                <th>
                  <i class="icon-code"></i>
                  Code
                </th>
               
                <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
                <th class="actn_hd">
                   <i class="icon-suitcase"></i>
                  Action
                </th>
                <?php endif; ?>
              </tr>
            </thead>

            <tbody>
              <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
              <?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
            <input type='hidden' id='pname' value='<?php echo ((is_array($_tmp=$this->_tpl_vars['x']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
'/>
            <tr>
              <td id="tdimage<?php echo $this->_tpl_vars['x']['id_product']; ?>
">
                                <?php if ($this->_tpl_vars['x']['image_name']): ?>
                <a href="javascript:void(0);" onclick="showImages('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');">
                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
"  alt="no image" />
                </a>
                <?php else: ?>
                <!--				    No image-->
                <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
                <?php endif; ?>
              </td>
              <td><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)); ?>
(<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['x']['total'])) ? $this->_run_mod_handler('default', true, $_tmp, '0') : smarty_modifier_default($_tmp, '0')))) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;images") : smarty_modifier_cat($_tmp, "&nbsp;images")); ?>
)</td>
              <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
              <td><?php if ($this->_tpl_vars['x']['category_name']):  echo $this->_tpl_vars['x']['category_name'];  else: ?><font color='#FF0000'>*No Category</font><?php endif; ?></td>
              <?php endif; ?>
              <td title="<?php echo $this->_tpl_vars['x']['description']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 40, "....", true) : smarty_modifier_truncate($_tmp, 40, "....", true)); ?>
</td>
              <td><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['code'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)); ?>
</td>
              <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
              <td>
                <input type="hidden" value='<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['uri'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
' id="<?php echo $this->_tpl_vars['x']['id_product']; ?>
_uri" />
                <a class="prdt_anchor" href="javascript:void(0);" onclick="editProduct('<?php echo $this->_tpl_vars['x']['id_product']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');" title="Edit Product">
<!--                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/pencil.png" alt="Edit Product" title="Edit Product"/>-->
<button class="btn btn-success">
<i class="icon-edit"></i>
</button>
                </a>&nbsp;&nbsp;
                <a class="prdt_anchor" href="javascript:void(0);" onclick="return deleteProduct('<?php echo $this->_tpl_vars['x']['id_product']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
', '<?php echo count($this->_tpl_vars['sm']['list']); ?>
');" title="Delete Product">
<!--                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" alt="Delete Product" title="Delete Product"/>-->
<button class="btn btn-danger">
<i class="icon-trash"></i>
</button>
                </a>&nbsp;&nbsp;
                <a class="prdt_anchor" href="javascript:void(0);" onclick="showDetail('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');" title="Product Detail">
<!--                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/eye.png" alt="Product Detail" title="Product Detail"/>-->
<button class="btn btn-primary">
<i class="icon-eye-open"></i>
</button>
                </a>&nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="makeSeq('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');">Seq</a>
              </td>
              <?php endif; ?>
            </tr>
            <?php endfor; endif; ?>
            <!--                                    <tr>
                                                    <td>2</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>-->
            </tbody>
          </table>
          <?php else: ?>
          <div>No product found..</div>
          <?php endif; ?>
          
        </div>
        <div class="pagination_box" align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>

          </div>
        <div>
        </div>
        <!-- END BASIC PORTLET-->
      </div>
    </div>

    <!--done by gayatree ends-->

 </br>
    <?php echo '
    <script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/table_dnd.js"></script>
       <script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/reorder.js"></script>
    <script type="text/javascript">
      var code;
      function addProduct(qstart) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "addProduct", "qstart": qstart, "ce": 0 }, function (res) {
          if (res == "no category") {
            alert("please add category first");
            $.fancybox.hideActivity();
            return false;
           } else {
            show_fancybox(res);
           }
         });
       }

      function editProduct(id, qstart) {
        var uri = $("#" + id + "_uri").val();
        var pname = $(\'#product_name\').val();
        var product_category = ($(\'#product_category\').val() == undefined) ? "" : $(\'#product_category\').val();
        $(\'#succ_msg\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "addProduct", "ce": "0", "id_product": id, "qstart": qstart, "uri": uri, \'pname\': pname, \'product_category\': product_category }, function (res) {
          show_fancybox(res);
         });
       }

     function deleteProduct(id_product, qstart, cnt){
		$(\'#succ_msg\').html(\'\');
		var uri = $("#"+id_product+"_uri").val();
		var pname = $(\'#pname\').val();
		var conf=confirm(\'Are you sure to delete "\'+pname+\'" ?\');
		if(conf){
			$.post(siteurl,{"page" : "product", "choice" : "deleteProduct", "ce" : 0, "id_product" : id_product, "qstart" : qstart, "uri" : uri, "cnt" : cnt },function(res){
				messageShow("Product deleted successfully.");//$(\'#succ_msg\').html(\'<font color="red">Product deleted successfully</font>\');
				$("#product_listing").html(res);
			 });
		 }else
			return false;
	 }

      function showDetail(id_product) {
        $(\'#succ_msg\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "productDetail", "ce": 0, "id_product": id_product }, function (res) {
          show_fancybox(res);
         });
       }

      function showImages(id_product) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "showGallery", ce: 0, \'id_product\': id_product }, function (response) {
          show_fancybox(response);
         });
       }

      function show_video(id) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        var url = "http://divyanshu.afixiindia.com/flexytiny_new/flexyadmin/index.php";
        $.fancybox.showActivity();
        $.post(url, {"page": "product", "choice": "show_video", ce: 0, \'id\': id }, function (response) {
          $.fancybox(response);
         });
       }
      function makeSeq(pro_id) {
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "showImageList", ce: 0, \'id_product\': pro_id }, function (res) {
          show_fancybox(res);
         });
       }
      function downloadxls() {
        var uri = "';  echo $this->_tpl_vars['sm']['uri'];  echo '";
        window.location.href = siteurl + "product/downloadExcel/uri/" + uri + "/ce/0/";
       }
    </script>
    '; ?>


<!--    




    <div class="center listing_admin" id="product_listing">
      <div class="makebox center">
        <div class="headprt settheme">
          <div class="mdl">
            <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
            <div  class="fltrht">
              <a href="#" onclick="addProduct('<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');" class="buton">Add Product</a>
            </div>
            <div class="fltrht">
              <input class="buton" type="button" onclick="downloadxls();" value="Download XLS" >
            </div>
            <?php endif; ?>
            <span>Product Listing (<?php echo $this->_tpl_vars['sm']['next_prev']->total; ?>
)</span>
            <div class="clear"></div>
          </div>
        </div>
        <div class="bodyprt">
          <?php if ($this->_tpl_vars['sm']['list']): ?>
          <table width="100%" border="0" class="tbl_listing" id="img_tbl">
            <thead>
              <tr>
                <th width="20%">Image</th>
                <th width="20%">Name</th>
                <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
                <th>Category Name</th>
                <?php endif; ?>
                <th>Description</th>
                <th>Code</th>
                <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
                <th class="actn_hd">Action</th>
                <?php endif; ?>
              </tr>
            </thead>
            <tbody>
              <?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['sm']['list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
              <?php $this->assign('x', $this->_tpl_vars['sm']['list'][$this->_sections['cur']['index']]); ?>
            <input type='hidden' id='pname' value='<?php echo ((is_array($_tmp=$this->_tpl_vars['x']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
'/>
            <tr>
              <td id="tdimage<?php echo $this->_tpl_vars['x']['id_product']; ?>
">
                                <?php if ($this->_tpl_vars['x']['image_name']): ?>
                <a href="javascript:void(0);" onclick="showImages('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');">
                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/image/thumb/product/<?php echo $this->_tpl_vars['x']['id_image']; ?>
_<?php echo $this->_tpl_vars['x']['image_name']; ?>
"  alt="no image" />
                </a>
                <?php else: ?>
                				    No image
                <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/default/images/defaultnoImage.jpg"  alt="no image"/>
                <?php endif; ?>
              </td>
              <td><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)); ?>
(<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['x']['total'])) ? $this->_run_mod_handler('default', true, $_tmp, '0') : smarty_modifier_default($_tmp, '0')))) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;images") : smarty_modifier_cat($_tmp, "&nbsp;images")); ?>
)</td>
              <?php if ($this->_tpl_vars['sm']['active_cat'] == 1): ?>
              <td><?php if ($this->_tpl_vars['x']['category_name']):  echo $this->_tpl_vars['x']['category_name'];  else: ?><font color='#FF0000'>*No Category</font><?php endif; ?></td>
              <?php endif; ?>
              <td title="<?php echo $this->_tpl_vars['x']['description']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 40, "....", true) : smarty_modifier_truncate($_tmp, 40, "....", true)); ?>
</td>
              <td><?php echo ((is_array($_tmp=$this->_tpl_vars['x']['code'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "....", true) : smarty_modifier_truncate($_tmp, 20, "....", true)); ?>
</td>
              <?php if (! $this->_tpl_vars['sm']['id_cat']): ?>
              <td>
                <input type="hidden" value='<?php echo ((is_array($_tmp=$this->_tpl_vars['sm']['uri'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
' id="<?php echo $this->_tpl_vars['x']['id_product']; ?>
_uri" />
                <a href="javascript:void(0);" onclick="editProduct('<?php echo $this->_tpl_vars['x']['id_product']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
');">
                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/pencil.png" alt="Edit Product" title="Edit Product"/>
                </a>&nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="return deleteProduct('<?php echo $this->_tpl_vars['x']['id_product']; ?>
', '<?php echo $this->_tpl_vars['sm']['next_prev']->start; ?>
', '<?php echo count($this->_tpl_vars['sm']['list']); ?>
');">
                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/delete.png" alt="Delete Product" title="Delete Product"/>
                </a>&nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="showDetail('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');">
                  <img src="http://divyanshu.afixiindia.com/flexytiny_new/templates/css_theme/img/led-ico/eye.png" alt="Product Detail" title="Product Detail"/>
                </a>&nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="makeSeq('<?php echo $this->_tpl_vars['x']['id_product']; ?>
');">Seq</a>
              </td>
              <?php endif; ?>
            </tr>
            <?php endfor; endif; ?>
            </tbody>
          </table>
          <?php else: ?>
          <div>No product found..</div>
          <?php endif; ?>
          <div class="pagination_box" align="center"><?php echo $this->_tpl_vars['sm']['next_prev']->generate(); ?>

          </div>
        </div>
      </div>
    </div>
    </br>
    <?php echo '
    <script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/table_dnd.js"></script>
       <script type="text/javascript" src="http://divyanshu.afixiindia.com/flexymvc_core/libsext/jquery/js/reorder.js"></script>
    <script type="text/javascript">
      var code;
      function addProduct(qstart) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "addProduct", "qstart": qstart, "ce": 0 }, function (res) {
          if (res == "no category") {
            alert("please add category first");
            $.fancybox.hideActivity();
            return false;
           } else {
            show_fancybox(res);
           }
         });
       }

      function editProduct(id, qstart) {
        var uri = $("#" + id + "_uri").val();
        var pname = $(\'#product_name\').val();
        var product_category = ($(\'#product_category\').val() == undefined) ? "" : $(\'#product_category\').val();
        $(\'#succ_msg\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "addProduct", "ce": "0", "id_product": id, "qstart": qstart, "uri": uri, \'pname\': pname, \'product_category\': product_category }, function (res) {
          show_fancybox(res);
         });
       }

      function deleteProduct(id_product, qstart, cnt) {
        $(\'#succ_msg\').html(\'\');
        var uri = $("#" + id_product + "_uri").val();
        var pname = $(\'#pname\').val();
        var conf = confirm(\'Are you sure to delete "\' + pname + \'" ?\');
        if (conf) {
          $.post(siteurl, {"page": "product", "choice": "deleteProduct", "ce": 0, "id_product": id_product, "qstart": qstart, "uri": uri, "cnt": cnt }, function (res) {
            messageShow("Product deleted successfully.");//$(\'#succ_msg\').html(\'<font color="red">Product deleted successfully</font>\');
            $("#product_listing").html(res);
           });
         } else
          return false;
       }

      function showDetail(id_product) {
        $(\'#succ_msg\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "productDetail", "ce": 0, "id_product": id_product }, function (res) {
          show_fancybox(res);
         });
       }

      function showImages(id_product) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "showGallery", ce: 0, \'id_product\': id_product }, function (response) {
          show_fancybox(response);
         });
       }

      function show_video(id) {
        $(\'#succ_msg\').html(\'\');
        $(\'#target\').html(\'\');
        var url = "http://divyanshu.afixiindia.com/flexytiny_new/flexyadmin/index.php";
        $.fancybox.showActivity();
        $.post(url, {"page": "product", "choice": "show_video", ce: 0, \'id\': id }, function (response) {
          $.fancybox(response);
         });
       }
      function makeSeq(pro_id) {
        $.fancybox.showActivity();
        $.post(siteurl, {"page": "product", "choice": "showImageList", ce: 0, \'id_product\': pro_id }, function (res) {
          show_fancybox(res);
         });
       }
      function downloadxls() {
        var uri = "';  echo $this->_tpl_vars['sm']['uri'];  echo '";
        window.location.href = siteurl + "product/downloadExcel/uri/" + uri + "/ce/0/";
       }
    </script>
    '; ?>
-->